#### import the simple module from the paraview
from paraview.simple import *

source = GetActiveSource()

# create a new 'Programmable Filter'
Filter = ProgrammableFilter(Input=source)
RenameSource('Primitives', Filter)

# Properties modified on programmableFilter1
Filter.Script = \
"""from vtk.numpy_interface import algorithms as algs

CellDatas = inputs[0].CellData
FieldDatas = inputs[0].FieldData

rho = CellDatas["rho"]
energy = CellDatas["energy"]
scalar = CellDatas["scalar"]
gamma = FieldDatas["gamma"]

if "mz" in CellDatas.keys():
    mx = CellDatas["mx"]
    my = CellDatas["my"]
    mz = CellDatas["mz"]
    v  = algs.make_vector(mx, my, mz)/rho
elif "my" in CellDatas.keys():
    mx = CellDatas["mx"]
    my = CellDatas["my"]
    v  = algs.make_vector(mx, my)/rho
else:
    v  = CellDatas["mx"]/rho # dot(v, v) may not work

p = (gamma-1.0)*(energy-0.5*rho*algs.dot(v,v))

output.CellData.append(rho, "rho")
output.CellData.append(v, "v")
output.CellData.append(p, "p")
output.CellData.append(scalar/rho, "scalar")"""

Filter.RequestInformationScript = ''
Filter.RequestUpdateExtentScript = ''
Filter.CopyArrays = 0
Filter.PythonPath = ''

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# show data in view
calculator1Display = Show(Filter, renderView1)
calculator1Display.Representation = 'Outline'

# hide data in view
Hide(source, renderView1)

# update the view to ensure updated data information
renderView1.Update()

SetActiveSource(Filter)
