// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "shared/real_type.h"
#include <string>
#include "utils/config/ConfigMap.h"

struct RadiativeConvectionParams
{
public:
    real_t Trad_ymin;    /*!< internal flux at the bottom of the domain*/
    real_t Trad_ymax;    /*!< top flux at the top of the domain*/
    real_t mubar;       /*!< mean angle for 2-stream radiative transfer*/
    real_t tauchem; /*!< chemical timescale */

    bool   perturb_velocity;
    real_t amp;   /*!< relative amplitude of the perturbation */
    real_t mode;  /*!< mode of the sinus perturbation in x direction */
    real_t width; /*!< width of the exponential-decreased temp. or vel. perturbation in y direction for */

    real_t tau; /*!< time scale */

    std::string input_file; /*< input file for initial PT profile */

    bool forceTemperatureIonization; /*!< if the ionization kernel is called, the temperature can be forced to the initial temperature or to the equilibrium temperature, or it can remain the one computed */
    bool recombinationEnergy; /*!< if the ionization kernel is called, use the recombination term in the energy equation */
    bool recombinationIonizationFraction; /*!< if the ionization kernel is called, use the recombination term in the ionization fraction equation */

    RadiativeConvectionParams(ConfigMap& configMap)
    {
        input_file     = configMap.getString("radiative_convection", "input_file", ""  );
        Trad_ymin      = configMap.getFloat ("radiative_convection", "Trad_ymin" ,1400.);
        Trad_ymax      = configMap.getFloat ("radiative_convection", "Trad_ymax" ,1400.);
        mubar          = configMap.getFloat ("radiative_convection", "mubar"     ,0.5  );
        tauchem        = configMap.getFloat ("radiative_convection", "tauchem"   ,1.   );

        perturb_velocity = configMap.getBool("radiative_convection","perturb_velocity",false);
        amp              = configMap.getFloat("radiative_convection","amp",1E-2);
        mode             = configMap.getFloat("radiative_convection","mode",2.);
        width            = configMap.getFloat("radiative_convection","width",0.25);

        tau	         = configMap.getFloat("radiative_convection","tau",1.0);

        forceTemperatureIonization = configMap.getBool("radiative_convection","forceTemperatureIonization",false);
        recombinationEnergy = configMap.getBool("radiative_convection","recombinationEnergy",true);
        recombinationIonizationFraction = configMap.getBool("radiative_convection","recombinationIonizationFraction",true);
    }
};
