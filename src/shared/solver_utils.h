// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#ifndef SOLVER_UTILS_H_
#define SOLVER_UTILS_H_

#include "shared/SolverBase.h"

namespace ark_rt
{

/**
 * print monitoring information
 */
inline void print_solver_monitoring_info(SolverBase* solver)
{
    real_t t_tot   = solver->timers[TIMER_TOTAL]->elapsed();
    real_t t_comp  = solver->timers[TIMER_NUM_SCHEME]->elapsed();
    real_t t_linear_algebra  = solver->timers[TIMER_LINEAR_ALGEBRA]->elapsed();
    real_t t_dt    = solver->timers[TIMER_DT]->elapsed();
    real_t t_bound = solver->timers[TIMER_BOUNDARIES]->elapsed();
    real_t t_io    = solver->timers[TIMER_IO]->elapsed();

    int myRank = 0;
    int nProcs = 1;

#ifdef USE_MPI
    myRank = solver->params.myRank;
    nProcs = solver->params.nProcs;
#endif // USE_MPI

    int iterations = solver->m_iteration - solver->m_restart_iteration;

    // only print on master
    if (myRank == 0)
    {
        printf("total          time : %5.3f secondes\n",t_tot);
        printf("num scheme     time : %5.3f secondes %5.2f%%\n",t_comp,100*t_comp/t_tot);
        printf("linear algebra time : %5.3f secondes %5.2f%%\n",t_linear_algebra,100*t_linear_algebra/t_tot);
        printf("compute dt     time : %5.3f secondes %5.2f%%\n",t_dt,100*t_dt/t_tot);
        printf("boundaries     time : %5.3f secondes %5.2f%%\n",t_bound,100*t_bound/t_tot);
        printf("io             time : %5.3f secondes %5.2f%%\n",t_io,100*t_io/t_tot);
        printf("Perf                : %5.3f number of Mcell-updates/s\n", iterations*solver->m_nCells*nProcs/t_tot*1e-6);
    } // end myRank==0

} // print_solver_monitoring_info

} // namespace ark_rt

#endif // SOLVER_UTILS_H_
