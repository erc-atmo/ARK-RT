// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#ifndef ARK_RT_UTILS_H
#define ARK_RT_UTILS_H

#include <math.h>
#include <iosfwd>

// make the compiler ignore an unused variable
#ifndef UNUSED
#define UNUSED(x) ((void)(x))
#endif

// make the compiler ignore an unused function
#ifdef __GNUC__
#define UNUSED_FUNCTION __attribute__ ((unused))
#else
#define UNUSED_FUNCTION
#endif

#define THRESHOLD 1e-12

#define ISFUZZYNULL(a) (std::abs(a) < THRESHOLD)
#define FUZZYCOMPARE(a, b)                                              \
    ((ISFUZZYNULL(a) && ISFUZZYNULL(b)) ||                              \
     (std::abs((a) - (b)) * 1000000000000. <= std::fmin(std::abs(a), std::abs(b))))
#define FUZZYLIMITS(x, a, b)                                    \
    (((x) > ((a) - THRESHOLD)) && ((x) < ((b) + THRESHOLD)))

void        print_current_date(std::ostream& stream);
std::string get_current_date();

#endif // ARK_RT_UTILS_H
