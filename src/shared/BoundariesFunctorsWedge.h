// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#ifndef BOUNDARIES_FUNCTORS_WEDGE_H_
#define BOUNDARIES_FUNCTORS_WEDGE_H_

#include "shared/HydroParams.h"    // for HydroParams
#include "shared/kokkos_shared.h"  // for Data arrays
#include "shared/WedgeParams.h"    // for Wedge border condition

/*************************************************/
/*************************************************/
/*************************************************/
/**
 * Functors to update ghost cells (Hydro 2D) for the test case wedge,
 * also called Double Mach reflection.
 *
 * See http://amroc.sourceforge.net/examples/euler/2d/html/ramp_n.htm
 *
 * This border condition is time-dependent.
 */
template <FaceIdType faceId>
class MakeBoundariesFunctor2D_wedge
{

public:

    MakeBoundariesFunctor2D_wedge(HydroParams params,
                                  WedgeParams wparams,
                                  DataArray2d Udata) :
        params(params), wparams(wparams), Udata(Udata) {};

    static void apply(HydroParams params,
                      WedgeParams wparams,
                      DataArray2d Udata,
                      int nbCells)
    {
        MakeBoundariesFunctor2D_wedge<faceId> functor(params, wparams, Udata);
        Kokkos::parallel_for(nbCells, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int index) const
    {
        const int nx = params.nx;
        const int ny = params.ny;

        const int ghostWidth = params.ghostWidth;
        const int nbvar = params.nbvar;

        const int imin = params.imin;
        const int imax = params.imax;

        const int jmin = params.jmin;
        const int jmax = params.jmax;

        const real_t dx = params.dx;
        const real_t dy = params.dy;

        const real_t xmin = params.xmin;
        const real_t ymin = params.ymin;

        const real_t rho1   = wparams.rho1;
        const real_t rho_u1 = wparams.rho_u1;
        const real_t rho_v1 = wparams.rho_v1;
        const real_t e_tot1 = wparams.e_tot1;

        int i,j;
        int i0, j0;

        if (faceId == FACE_XMIN)
        {
            // boundary xmin (inflow)

            j = index / ghostWidth;
            i = index - j*ghostWidth;

            if(j >= jmin && j <= jmax    &&
               i >= 0    && i <ghostWidth)
            {
                Udata(i,j,ID) = rho1;
                Udata(i,j,IP) = e_tot1;
                Udata(i,j,IU) = rho_u1;
                Udata(i,j,IV) = rho_v1;
            }
        } // end FACE_XMIN

        if (faceId == FACE_XMAX)
        {
            // boundary xmax (outflow)
            j = index / ghostWidth;
            i = index - j*ghostWidth;
            i += (nx+ghostWidth);

            if(j >= jmin          && j <= jmax             &&
               i >= nx+ghostWidth && i <= nx+2*ghostWidth-1)
            {
                i0=nx+ghostWidth-1;
                for ( int iVar=0; iVar<nbvar; iVar++ )
                    Udata(i,j,iVar) = Udata(i0,j,iVar);
            }
        } // end FACE_XMAX

        if (faceId == FACE_YMIN)
        {
            // boundary ymin
            // if (x <  x_f) inflow
            // else          reflective

            i = index / ghostWidth;
            j = index - i*ghostWidth;

            real_t x = xmin + dx/2 + (i-ghostWidth)*dx;
            //real_t y = ymin + dy/2 + (j-ghostWidth)*dy;

            if(i >= imin && i <= imax    &&
               j >= 0    && j <ghostWidth)
            {
                if (x < wparams.x_f)
                {
                    // inflow
                    Udata(i,j,ID) = rho1;
                    Udata(i,j,IP) = e_tot1;
                    Udata(i,j,IU) = rho_u1;
                    Udata(i,j,IV) = rho_v1;
                }
                else
                {
                    // reflective
                    real_t sign=1.0;
                    for ( int iVar=0; iVar<nbvar; iVar++ )
                    {
                        j0=2*ghostWidth-1-j;
                        if (iVar==IV) sign=-ONE_F;
                        Udata(i,j,iVar) = Udata(i,j0,iVar)*sign;
                    }
                }
            } // end if i,j
        } // end FACE_YMIN

        if (faceId == FACE_YMAX)
        {
            // boundary ymax
            // if (x <  x_f + y/slope_f + delta_x) inflow
            // else                                outflow

            i = index / ghostWidth;
            j = index - i*ghostWidth;
            j += (ny+ghostWidth);

            real_t x = xmin + dx/2 + (i-ghostWidth)*dx;
            real_t y = ymin + dy/2 + (j-ghostWidth)*dy;

            if(i >= imin          && i <= imax              &&
               j >= ny+ghostWidth && j <= ny+2*ghostWidth-1)
            {
                if (x < wparams.x_f + y/wparams.slope_f + wparams.delta_x)
                {
                    // inflow
                    Udata(i,j,ID) = rho1;
                    Udata(i,j,IP) = e_tot1;
                    Udata(i,j,IU) = rho_u1;
                    Udata(i,j,IV) = rho_v1;
                }
                else
                {
                    // outflow
                    j0=ny+ghostWidth-1;
                    for ( int iVar=0; iVar<nbvar; iVar++ )
                    {
                        Udata(i,j,iVar) = Udata(i,j0,iVar);
                    }
                }
            } // end if i,j
        } // end FACE_YMAX
    } // end operator ()

    HydroParams params;
    WedgeParams wparams;
    DataArray2d Udata;
}; // MakeBoundariesFunctor2D_wedge

#endif // BOUNDARIES_FUNCTORS_WEDGE_H_
