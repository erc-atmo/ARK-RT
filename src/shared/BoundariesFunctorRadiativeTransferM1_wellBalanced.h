// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "shared/HydroParams.h"    // for HydroParams
#include "shared/kokkos_shared.h"  // for Data arrays
#include <iostream>

namespace ark_rt
{

  /**
   * Functors to update ghost cells (Hydro 2D) for the test case
   */
  template <FaceIdType faceId>
    class MakeBoundariesFunctor2D_RadiativeTransferWellBalanced
    {
      public:
        MakeBoundariesFunctor2D_RadiativeTransferWellBalanced(HydroParams params_, DataArray2d Udata_) :
          params(params_), Udata(Udata_) {};

        static void apply(HydroParams params, DataArray2d Udata, int nbCells)
        {
          MakeBoundariesFunctor2D_RadiativeTransferWellBalanced<faceId> functor(params, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const int ghostWidth = params.ghostWidth;

            const int imin = params.imin;
            const int imax = params.imax;

            const int jmin = params.jmin;

            const real_t dy = params.dy;
            const real_t gamma0 = params.settings.gamma0;
            const real_t T = params.settings.bc_value;

            const real_t speedOfLight = params.settings.speedOfLight;
            const real_t a = params.settings.radiativeConstant;

            if (faceId == FACE_YMIN)
            {
              const int i = index / ghostWidth;

              if(i >= imin && i <= imax)
              {
                for (int j=jmin+ghostWidth-1; j>=jmin; j--)
                {
                  const int j0 = jmin+ghostWidth; // first cell inside the domain, all boundary cells have the same value
                  const real_t sigma = Udata(i, j0, IO);
                  Udata(i, j, ID) = (TWO_F + dy) / (TWO_F - dy) * Udata(i, j+1, ID);
                  Udata(i, j, IP) = Udata(i, j, ID) / (gamma0-ONE_F);
                  Udata(i, j, IU) = ZERO_F;
                  Udata(i, j, IV) = ZERO_F;
                  Udata(i, j, ITRAD) = T;
                  Udata(i, j, IERAD) = a*T*T*T*T;
                  Udata(i, j, IO) = sigma;
                  Udata(i, j, IFX) = ZERO_F;
                  Udata(i, j, IFY) = -TWO_F*speedOfLight/(3.*sigma*dy)*(Udata(i,j0,IERAD)-Udata(i,j0-1,IERAD))-Udata(i,j0,IFY); 
                }
              }
            }
          }

        const HydroParams params;
        DataArray2d Udata;
    }; // MakeBoundariesFunctor2D_RadiativeTransferWellBalanced

} // namespace ark_rt
