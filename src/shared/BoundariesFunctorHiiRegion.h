// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "shared/HydroParams.h"    // for HydroParams
#include "shared/kokkos_shared.h"  // for Data arrays
#include <iostream>

namespace ark_rt
{

  /**
   * Functors to update ghost cells (Hydro 2D) for the test case
   */
  template <FaceIdType faceId>
    class MakeBoundariesFunctor2D_HiiRegion
    {
      public:
        MakeBoundariesFunctor2D_HiiRegion(HydroParams params_, DataArray2d Udata_) :
          params(params_), Udata(Udata_) {};

        static void apply(HydroParams params, DataArray2d Udata, int nbCells)
        {
          MakeBoundariesFunctor2D_HiiRegion<faceId> functor(params, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const int ghostWidth = params.ghostWidth;

            const int jmin = params.jmin;
            const int jmax = params.jmax;

            const real_t gamma0 = params.settings.gamma0;
            const real_t kb = code_units::constants::k_b;

            if(faceId == FACE_XMIN)
            {
              const int j = index / ghostWidth;
              const int i = index - j*ghostWidth;

              if(j >= jmin && j <= jmax &&
                  i >= 0 && 1 < ghostWidth)
              {
                const real_t Fstar = 3e13*(code_units::length_u*code_units::length_u*code_units::time_u); 
                const real_t e_gamma = 1.60218e-19/code_units::energy_u; // 1eV
                const real_t c = params.settings.speedOfLight;

                Udata(i, j, ID) = Udata(ghostWidth, j, ID);
                Udata(i, j, IP) = Udata(ghostWidth, j, IP);
                Udata(i, j, IU) = ZERO_F;
                Udata(i, j, IV) = ZERO_F;
                Udata(i, j, IS) = Udata(ghostWidth, j, IS);
                Udata(i,j,IO) = Udata(ghostWidth, j, IO);
                Udata(i, j, IERAD) = Fstar*e_gamma/c;
                Udata(i, j, ITRAD) = e_gamma*(gamma0-ONE_F)/kb;
                Udata(i, j, IFX) = params.settings.bc_value *c*Udata(i, j, IERAD);
                Udata(i, j, IFY) = ZERO_F;
              }

            }
          }

        const HydroParams params;
        DataArray2d Udata;
    }; // MakeBoundariesFunctor2D_HiiRegion

} // namespace ark_rt
