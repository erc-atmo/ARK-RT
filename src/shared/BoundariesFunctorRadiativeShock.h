// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "shared/HydroParams.h"    // for HydroParams
#include "shared/kokkos_shared.h"  // for Data arrays
#include <iostream>

namespace ark_rt
{

  /**
   * Functors to update ghost cells (Hydro 2D) for the test case
   */
  template <FaceIdType faceId>
    class MakeBoundariesFunctor2D_RadiativeShock
    {
      public:
        MakeBoundariesFunctor2D_RadiativeShock(HydroParams params_, DataArray2d Udata_) :
          params(params_), Udata(Udata_) {};

        static void apply(HydroParams params, DataArray2d Udata, int nbCells)
        {
          MakeBoundariesFunctor2D_RadiativeShock<faceId> functor(params, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const int ghostWidth = params.ghostWidth;

            const int imin = params.imin;
            const int imax = params.imax;
            const int jmin = params.jmin;
            const int jmax = params.jmax;
            const int nx = params.nx;

            const real_t cv = params.settings.cv;

            if(faceId == FACE_XMIN)
            {
              const int j = index / ghostWidth;
              const int i = index - j*ghostWidth;

              if(j >= jmin && j <= jmax &&
                  i >= 0 && i < ghostWidth)
              {
                const real_t rho = Udata(ghostWidth, j, ID);
                Udata(i, j, ID) = rho;
                Udata(i, j, IE) = Udata(ghostWidth, j, IE);
                Udata(i, j, IU) = - Udata(ghostWidth, j, IU);
                Udata(i, j, IV) = ZERO_F;
                const real_t ekin = HALF_F*(Udata(i, j, IU)*Udata(i, j, IU) + Udata(i, j, IV)*Udata(i, j, IV))/rho;
                Udata(i, j, ITRAD) = (Udata(i, j, IE)-ekin)/(cv*Udata(i, j, ID));
                Udata(i, j, IERAD) = Udata(ghostWidth,j,IERAD);
                Udata(i, j, IO) = 3.1e-10;
                Udata(i, j, IFX) = - Udata(ghostWidth, j, IFX);
                Udata(i, j, IFY) = ZERO_F;
              }

            }

            if(faceId == FACE_XMAX)
            {
              const int j = index / ghostWidth;
              const int i = index - j*ghostWidth;
              const real_t rho = Udata(nx+ghostWidth-1, j, ID);

              if(j >= jmin && j <= jmax &&
                  i >= 0 && i < ghostWidth)
              {
                Udata(i, j, ID) = rho;
                Udata(i, j, IE) = Udata(nx+ghostWidth-1, j, IE);
                Udata(i, j, IU) = -Udata(nx+ghostWidth-1, j, IU);
                Udata(i, j, IV) = ZERO_F;
                Udata(i, j, ITRAD) = Udata(nx+ghostWidth-1, j, ITRAD);
                Udata(i, j, IERAD) = Udata(nx+ghostWidth-1, j, IERAD);
                Udata(i, j, IO) = 3.1e-10;
                Udata(i, j, IFX) = ZERO_F;
                Udata(i, j, IFY) = ZERO_F;
              }

            }


            if(faceId == FACE_YMIN)

            {
              const int i = index / ghostWidth;

              if(i >= imin && i <= imax)
              {
                for (int j=jmin+ghostWidth-1; j>=jmin; j--)
                {
                  const real_t rho = Udata(i, ghostWidth, ID);
                  Udata(i, j, ID) = rho;
                  Udata(i, j, IE) = Udata(i, ghostWidth, IE);
                  Udata(i, j, IU) = ZERO_F;
                  Udata(i, j, IV) = - Udata(i, ghostWidth, IV); 
                  const real_t ekin = HALF_F*(Udata(i, j, IU)*Udata(i, j, IU) + Udata(i, j, IV)*Udata(i, j, IV))/rho;
                  Udata(i, j, ITRAD) = (Udata(i, j, IE)-ekin)/(cv*Udata(i, j, ID));
                  Udata(i, j, IERAD) = Udata(i,ghostWidth,IERAD);
                  Udata(i, j, IO) = 3.1e-10;
                  Udata(i, j, IFX) = ZERO_F;
                  Udata(i, j, IFY) = - Udata(i, ghostWidth, IFY);
                }
              }
            }

          }

        const HydroParams params;
        DataArray2d Udata;
    }; // MakeBoundariesFunctor2D_RadiativeShock

} // namespace ark_rt
