// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

/**
 * \file HydroParams.h
 * \brief Hydrodynamics solver parameters.
 *
 * \date April, 16 2016
 * \author P. Kestener
 */
#ifndef HYDRO_PARAMS_H_
#define HYDRO_PARAMS_H_

#include "shared/kokkos_shared.h"
#include "shared/real_type.h"
#include "utils/config/ConfigMap.h"

#include <vector>
#include <stdbool.h>
#include <string>

#include "shared/enums.h"

#ifdef USE_MPI
#include "utils/mpiUtils/MpiCommCart.h"
#endif // USE_MPI

struct HydroSettings
{
  // hydro (numerical scheme) parameters
  real_t cfl_hydro;   /*!< Courant-Friedrich-Lewy parameter for hydrodynamics.*/
  real_t cfl_rad;     /*!< Courant-Friedrich-Lewy parameter for radiative transfer.*/
  real_t slope_type;  /*!< type of slope computation (2 for second order scheme).*/
  int    iorder;      /*!< */
  real_t smallr;      /*!< small density cut-off*/
  real_t smallc;      /*!< small speed of sound cut-off*/
  real_t smallp;      /*!< small pressure cut-off*/
  real_t smallpp;     /*!< smallp times smallr*/
  real_t K;
  bool   low_mach_correction;
  bool   conservative;

  real_t gamma0;      /*!< specific heat capacity ratio (adiabatic index)*/

  real_t mmw;         /*!< mean molecular weight*/

  real_t speedOfLight; 
  real_t bc_value; 	/*!< used to set boundary conditions */
  real_t radiativeConstant; 	/*!< radiative constant */
  bool useExactEigenvalues; /*!< either use exact eigenvalues or set them to +/- c */
  real_t valpmin; /*!< extrem value for the eigenvalues */
  bool useAsymptoticCorrection; /*!< use or not the asymptotic correction for radiative transfer */
  bool useP1; 	/*!< use M1 or P1 model */
  bool wellBalancedScheme; /*!< center the radiative flux source term at the interfaces or center of a cell */
  bool restrictTimeStepRT; /*!< restrict or not the time step for radiative transfer */
  real_t vardt; /*!< the time step is increased by vardt */
  real_t varrel_max; /*!< maximum relative variations to increase the time step */
  bool allTermsCoupling; /*!< use or not all the comoving terms for coupling with the hydrodynamics */
  bool replaceEddingtonFactor; /*!< if the Eddington factor is larger than 1, replace it by 1 */
  bool rtM1_enabled;
  solverRT   implicit_enabled;
  bool   hydro_enabled;
  bool   ionization_enabled;
  bool   thermal_driving_enabled;

  int max_newton; 	/*!< maximal number of iteration for Newton-Raphson method */
  real_t tol_newton; 	/*!< tolerance for Newton-Raphson method */
  std::string linearSolverParams; /*!< name of the parameters file for linear solver using Trilinos */
  bool outputLinearAlgebra; /*!< print the matrix, the right hand side and the initial guess in file if needed */
  bool linearAlgebraVerbose; /*!< print the number of iterations of the linear solver, the tolerance of the Newton and the variation of the fields if needed */


  bool   rt_enabled;
  bool   chem_enabled;

  real_t mmw1;         /*!< mean molecular weight phase 1*/
  real_t mmw2;         /*!< mean molecular weight phase 2*/

  real_t kap1;         /*!< opacity phase 1*/
  real_t kap2;         /*!< opacity phase 2*/

  real_t mu;          /*!< dynamic viscosity */
  real_t kappa;       /*!< thermal conductivity */

  real_t gamma6;
  real_t Rstar;       /*!< specific gas constant */
  real_t cp;          /*!< specific heat (constant pressure) */
  real_t cv;          /*!< specific heat (constant volume) */

  real_t g_x;         /*!< gravity in x-direction */
  real_t g_y;         /*!< gravity in y-direction */
  real_t g_z;         /*!< gravity in z-direction */

  KOKKOS_INLINE_FUNCTION
    HydroSettings() : cfl_hydro(-1.0), slope_type(-1.0), iorder(-1),
    smallr(1e-8), smallc(1e-8), smallp(1e-6), smallpp(1e-6),
    K(-1.0), low_mach_correction(false), conservative(true),
    gamma0(-1.0), mmw(-1.0), mu(-1.0), kappa(-1.0),
    gamma6(-1.0), Rstar(-1.0), cp(-1.0), cv(-1.0),
    g_x(0.0), g_y(0.0), g_z(0.0) {}
}; // struct HydroSettings

/**
 * Hydro Parameters (declaration).
 */
struct HydroParams
{
#ifdef USE_MPI
  using MpiCommCart = hydroSimu::MpiCommCart;
#endif // USE_MPI

  // run parameters
  std::string solver_name;
  int    nStepmax;   /*!< maximun number of time steps. */
  real_t tEnd;       /*!< end of simulation time. */
  int    nOutput;    /*!< number of time steps between 2 consecutive outputs. */
  int    nlog;      /*!<  number of time steps between 2 consecutive logs. */

  // geometry parameters
  int nx;     /*!< logical size along X (without ghost cells).*/
  int ny;     /*!< logical size along Y (without ghost cells).*/
  int nz;     /*!< logical size along Z (without ghost cells).*/
  int ghostWidth;
  int nbvar;  /*!< number of variables in HydroState */
  DimensionType dimType; //!< 2D or 3D.

  int imin;   /*!< index minimum at X border*/
  int imax;   /*!< index maximum at X border*/
  int jmin;   /*!< index minimum at Y border*/
  int jmax;   /*!< index maximum at Y border*/
  int kmin;   /*!< index minimum at Z border*/
  int kmax;   /*!< index maximum at Z border*/

  int isize;  /*!< total size (in cell unit) along X direction with ghosts.*/
  int jsize;  /*!< total size (in cell unit) along Y direction with ghosts.*/
  int ksize;  /*!< total size (in cell unit) along Z direction with ghosts.*/

  real_t xmin; /*!< domain bound */
  real_t xmax; /*!< domain bound */
  real_t ymin; /*!< domain bound */
  real_t ymax; /*!< domain bound */
  real_t zmin; /*!< domain bound */
  real_t zmax; /*!< domain bound */
  real_t dx;   /*!< x resolution */
  real_t dy;   /*!< y resolution */
  real_t dz;   /*!< z resolution */

  BoundaryConditionType boundary_type_xmin; /*!< boundary condition */
  BoundaryConditionType boundary_type_xmax; /*!< boundary condition */
  BoundaryConditionType boundary_type_ymin; /*!< boundary condition */
  BoundaryConditionType boundary_type_ymax; /*!< boundary condition */
  BoundaryConditionType boundary_type_zmin; /*!< boundary condition */
  BoundaryConditionType boundary_type_zmax; /*!< boundary condition */

  // IO parameters
  bool ioVTK;   /*!< enable VTK  output file format (using VTI).*/
  bool ioHDF5;  /*!< enable HDF5 output file format.*/

  //! hydro settings (gamma0, ...) to be passed to Kokkos device functions
  HydroSettings settings;

  bool useAllRegimeTimeSteps;

  int niter_riemann;  /*!< number of iteration usd in quasi-exact riemann solver*/
  int riemannSolverType;

  // other parameters
  int implementationVersion=0; /*!< triggers which implementation to use (currently 3 versions)*/

#ifdef USE_MPI
  //! runtime determination if we are using float ou double (for MPI communication)
  //! initialized in constructor to either MpiComm::FLOAT or MpiComm::DOUBLE
  int data_type;

  //! size of the MPI cartesian grid
  int mx,my,mz;

  //! MPI communicator in a cartesian virtual topology
  MpiCommCart *communicator;

  //! number of dimension
  int nDim;

  //! MPI rank of current process
  int myRank;

  //! number of MPI processes
  int nProcs;

  //! MPI cartesian coordinates inside MPI topology
  Kokkos::Array<int,3> myMpiPos;

  //! number of MPI process neighbors (4 in 2D and 6 in 3D)
  int nNeighbors;

  //! MPI rank of adjacent MPI processes
  Kokkos::Array<int,6> neighborsRank;

  //! boundary condition type with adjacent domains (corresponding to
  //! neighbor MPI processes)
  Kokkos::Array<BoundaryConditionType,6> neighborsBC;

#endif // USE_MPI

  HydroParams() :
    solver_name("unknown"),
    nStepmax(0), tEnd(0.0), nOutput(0),
    nlog(10),
    nx(0), ny(0), nz(0), ghostWidth(2), nbvar(4), dimType(TWO_D),
    imin(0), imax(0), jmin(0), jmax(0), kmin(0), kmax(0),
    isize(0), jsize(0), ksize(0),
    xmin(0.0), xmax(1.0), ymin(0.0), ymax(1.0), zmin(0.0), zmax(1.0),
    dx(0.0), dy(0.0), dz(0.0),
    boundary_type_xmin(BC_UNDEFINED),
    boundary_type_xmax(BC_UNDEFINED),
    boundary_type_ymin(BC_UNDEFINED),
    boundary_type_ymax(BC_UNDEFINED),
    boundary_type_zmin(BC_UNDEFINED),
    boundary_type_zmax(BC_UNDEFINED),
    ioVTK(true), ioHDF5(false),
    settings(),
    niter_riemann(10), riemannSolverType(),
    implementationVersion(0)
#ifdef USE_MPI
    // init MPI-specific parameters...
#endif // USE_MPI
  {}

  virtual ~HydroParams() {}

  //! This is the genuine initialiation / setup (fed by parameter file)
  virtual void setup(ConfigMap& map);

#ifdef USE_MPI
  //! Initialize MPI-specific parameters
  void setup_mpi(ConfigMap& map);
#endif // USE_MPI

  void init();
  void print();
}; // struct HydroParams

#endif // HYDRO_PARAMS_H_
