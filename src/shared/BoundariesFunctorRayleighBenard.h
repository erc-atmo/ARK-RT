// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "all_regime/HydroBaseFunctor2D.h"
#include "all_regime/HydroBaseFunctor3D.h"
#include "shared/RayleighBenardParams.h"
#include "shared/HydroParams.h"    // for HydroParams
#include "shared/kokkos_shared.h"  // for Data arrays
#include "shared/units.h"

#include <iostream>

namespace ark_rt
{

/**
 * Functors to update ghost cells (Hydro 2D) for the test case
 */
template <FaceIdType faceId>
class MakeBoundariesFunctor2D_RayleighBenard : all_regime::HydroBaseFunctor2D
{
public:
    MakeBoundariesFunctor2D_RayleighBenard(HydroParams params_,
                                           RayleighBenardParams rayleighBenardParams_,
                                           DataArray2d Udata_)
        : all_regime::HydroBaseFunctor2D(params_)
        , rayleighBenardParams(rayleighBenardParams_)
        , Udata(Udata_) {};

    static void apply(HydroParams params,
                      RayleighBenardParams rayleighBenardParams,
                      DataArray2d Udata,
                      int nbCells)
    {
        MakeBoundariesFunctor2D_RayleighBenard<faceId> functor(params, rayleighBenardParams, Udata);
        Kokkos::parallel_for(nbCells, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int index) const
    {
        const int ny = params.ny;

        const int ghostWidth = params.ghostWidth;

        const int imin = params.imin;
        const int imax = params.imax;

        const int jmin = params.jmin;

        const real_t dy = params.dy;
        const real_t Rstar = params.settings.Rstar;

        const real_t beta  = rayleighBenardParams.temperature_gradient;
        const real_t T_top = rayleighBenardParams.temperature_top;
        const bool fixed_T_bot = rayleighBenardParams.fixed_temperature_bottom;
        const real_t T_bot_init = T_top - beta * (params.ymax - params.ymin);

        if (faceId == FACE_YMIN)
        {
            const int i = index / ghostWidth;

            if(i >= imin && i <= imax)
            {
                for (int j=jmin+ghostWidth-1; j>=jmin; j--)
                {
                    const int j0 = 2*ghostWidth-1-j;
                    const HydroState q_R = computePrimitives(getHydroState(Udata, i, j0));
                    const real_t T_R = computeTemperature(q_R);

                    const real_t Dphi = phi(i, j0) - phi(i, j);
                    const real_t T_L = fixed_T_bot ? T_bot_init : T_R - beta * (j0 - j)* dy;

                    HydroState q_L;
                    q_L[ID] = (q_R[IP] + HALF_F*q_R[ID]*Dphi) / (Rstar * T_L - HALF_F*Dphi);
                    q_L[IP] = q_L[ID] * Rstar * T_L;
                    q_L[IS] = q_R[IS];
                    q_L[IU] = + q_R[IU];
                    q_L[IV] = - q_R[IV];

                    setHydroState(Udata, computeConservatives(q_L), i, j);
                }
            }
        }

        if (faceId == FACE_YMAX)
        {
            const int i = index / ghostWidth;

            if(i >= imin && i <= imax)
            {
                for (int j=ny+ghostWidth; j<=ny+2*ghostWidth-1; j++)
                {
                    const int j0 = 2*ny+2*ghostWidth-1-j;
                    const HydroState q_L = computePrimitives(getHydroState(Udata, i, j0));

                    const real_t Dphi = phi(i, j) - phi(i, j0);
                    const real_t T_R = T_top;
                    HydroState q_R;
                    q_R[ID] = (q_L[IP] - HALF_F*q_L[ID]*Dphi) / (Rstar * T_R + HALF_F*Dphi);
                    q_R[IP] = q_R[ID] * Rstar * T_R;
                    q_R[IS] = q_L[IS];
                    q_R[IU] = + q_L[IU];
                    q_R[IV] = - q_L[IV];

                    setHydroState(Udata, computeConservatives(q_R), i, j);
                }
            }
        }
    }

    // HydroParams params;
    RayleighBenardParams rayleighBenardParams;
    DataArray2d Udata;
}; // MakeBoundariesFunctor2D_RayleighBenard

/**
 * Functors to update ghost cells (Hydro 3D) for the test case
 */
template <FaceIdType faceId>
class MakeBoundariesFunctor3D_RayleighBenard : all_regime::HydroBaseFunctor3D
{
public:
    MakeBoundariesFunctor3D_RayleighBenard(HydroParams params_,
                                           RayleighBenardParams rayleighBenardParams_,
                                           DataArray3d Udata_)
        : all_regime::HydroBaseFunctor3D(params_)
        , rayleighBenardParams(rayleighBenardParams_)
        , Udata(Udata_) {};

    static void apply(HydroParams params,
                      RayleighBenardParams rayleighBenardParams,
                      DataArray3d Udata,
                      int nbCells)
    {
        MakeBoundariesFunctor3D_RayleighBenard<faceId> functor(params, rayleighBenardParams, Udata);
        Kokkos::parallel_for(nbCells, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int index) const
    {
        const int nz = params.nz;

        const int ghostWidth = params.ghostWidth;

        const int imin = params.imin;
        const int imax = params.imax;

        const int jmin = params.jmin;
        const int jmax = params.jmax;

        const int kmin = params.kmin;

        const int isize = params.isize;
        const int jsize = params.jsize;

        const real_t dz = params.dz;
        const real_t Rstar = params.settings.Rstar;

        const real_t beta  = rayleighBenardParams.temperature_gradient;
        const real_t T_top = rayleighBenardParams.temperature_top;
        const bool fixed_T_bot = rayleighBenardParams.fixed_temperature_bottom;
        const real_t T_bot_init = T_top - beta * (params.zmax - params.zmin);

        if (faceId == FACE_ZMIN)
        {
            const int k_ = index / (isize*jsize);
            const int j = (index - k_*isize*jsize) / isize;
            const int i = index - j*isize - k_*isize*jsize;

            if(i >= imin && i <= imax &&
               j >= jmin && j <= jmax)
            {
                for (int k=kmin+ghostWidth-1; k>=kmin; k--)
                {
                    const int k0 = 2*ghostWidth-1-k;
                    const HydroState q_R = computePrimitives(getHydroState(Udata, i, j, k0));
                    const real_t T_R = computeTemperature(q_R);

                    const real_t Dphi = phi(i, j, k0) - phi(i, j, k);
                    const real_t T_L = fixed_T_bot ? T_bot_init : T_R - beta * (k0 - k)* dz;

                    HydroState q_L;
                    q_L[ID] = (q_R[IP] + HALF_F*q_R[ID]*Dphi) / (Rstar * T_L - HALF_F*Dphi);
                    q_L[IP] = q_L[ID] * Rstar * T_L;
                    q_L[IS] = q_R[IS];
                    q_L[IU] = + q_R[IU];
                    q_L[IV] = + q_R[IV];
                    q_L[IW] = - q_R[IW];

                    setHydroState(Udata, computeConservatives(q_L), i, j, k);
                }
            }
        }

        if (faceId == FACE_ZMAX)
        {
            int k_ = index / (isize*jsize);
            const int j = (index - k_*isize*jsize) / isize;
            const int i = index - j*isize - k_*isize*jsize;
            k_ += nz + ghostWidth;
            if(i >= imin && i <= imax &&
               j >= jmin && j <= jmax)
            {
                for (int k=nz+ghostWidth; k<=nz+2*ghostWidth-1; k++)
                {
                    const int k0 = 2*nz+2*ghostWidth-1-k;
                    const HydroState q_L = computePrimitives(getHydroState(Udata, i, j, k0));

                    const real_t Dphi = phi(i, j, k) - phi(i, j, k0);
                    const real_t T_R = T_top;
                    HydroState q_R;
                    q_R[ID] = (q_L[IP] - HALF_F*q_L[ID]*Dphi) / (Rstar * T_R + HALF_F*Dphi);
                    q_R[IP] = q_R[ID] * Rstar * T_R;
                    q_R[IS] = q_L[IS];
                    q_R[IU] = + q_L[IU];
                    q_R[IV] = + q_L[IV];
                    q_R[IW] = - q_L[IW];

                    setHydroState(Udata, computeConservatives(q_R), i, j, k);
                }
            }
        }
    }

    // HydroParams params;
    RayleighBenardParams rayleighBenardParams;
    DataArray3d Udata;
}; // MakeBoundariesFunctor3D_RayleighBenard

} // namespace ark_rt
