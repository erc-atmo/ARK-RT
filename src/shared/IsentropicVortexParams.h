// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#ifndef ISENTROPIC_VORTEX_PARAMS_H_
#define ISENTROPIC_VORTEX_PARAMS_H_

#include "real_type.h"
#include "utils/config/ConfigMap.h"

/**
 * isentropic vortex advection test parameters.
 */
struct IsentropicVortexParams
{
    // isentropic vortex ambient flow
    real_t rho_a;
    real_t p_a;
    real_t T_a;
    real_t u_a;
    real_t v_a;
    real_t w_a;

    // vortex center
    real_t vortex_x;
    real_t vortex_y;
    real_t vortex_z;

    // vortex strength
    real_t beta;

    // number of quadrature points (used to compute initial cell-averaged values)
    int nQuadPts;

    IsentropicVortexParams(ConfigMap& configMap)
    {
        real_t xmin = configMap.getFloat("mesh", "xmin", 0.0);
        real_t ymin = configMap.getFloat("mesh", "ymin", 0.0);
        real_t zmin = configMap.getFloat("mesh", "zmin", 0.0);

        real_t xmax = configMap.getFloat("mesh", "xmax", 1.0);
        real_t ymax = configMap.getFloat("mesh", "ymax", 1.0);
        real_t zmax = configMap.getFloat("mesh", "zmax", 1.0);

        rho_a  = configMap.getFloat("isentropic_vortex","density_ambient", 1.0);
        p_a  = configMap.getFloat("isentropic_vortex","pressure_ambient", 1.0);
        T_a  = configMap.getFloat("isentropic_vortex","temperature_ambient", 1.0);
        u_a  = configMap.getFloat("isentropic_vortex","vx_ambient", 1.0);
        v_a  = configMap.getFloat("isentropic_vortex","vy_ambient", 1.0);
        w_a  = configMap.getFloat("isentropic_vortex","vz_ambient", 1.0);

        vortex_x = configMap.getFloat("isentropic_vortex","center_x", (xmin+xmax)/2);
        vortex_y = configMap.getFloat("isentropic_vortex","center_y", (ymin+ymax)/2);
        vortex_z = configMap.getFloat("isentropic_vortex","center_z", (zmin+zmax)/2);

        beta = configMap.getFloat("isentropic_vortex","strength",5.0);

        nQuadPts = configMap.getInteger("isentropic_vortex", "num_quadrature_points",4);
    }
}; // struct IsentropicVortexParams

#endif // ISENTROPIC_VORTEX_PARAMS_H_
