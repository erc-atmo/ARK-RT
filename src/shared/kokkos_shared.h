// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#ifndef KOKKOS_SHARED_H_
#define KOKKOS_SHARED_H_

#include "Kokkos_Macros.hpp"

#include <Kokkos_Core.hpp>
#include <Kokkos_Parallel.hpp>
#include <Kokkos_View.hpp>
#include <Kokkos_Macros.hpp>
#include <impl/Kokkos_Error.hpp>

#include "shared/real_type.h"
#include "shared/utils.h"


#ifdef KOKKOS_ENABLE_CUDA
#include <cuda.h>
#endif

using device = Kokkos::DefaultExecutionSpace;

enum KokkosLayout
{
    KOKKOS_LAYOUT_LEFT,
    KOKKOS_LAYOUT_RIGHT
};

// last index is hydro variable
// n-1 first indexes are space (i,j,k,....)
using DataArray2d      = Kokkos::View<real_t***, device>;
using DataArray2dConst = DataArray2d::const_type;
using DataArray2dHost  = DataArray2d::HostMirror;

using DataArray3d      = Kokkos::View<real_t****, device>;
using DataArray3dConst = DataArray3d::const_type;
using DataArray3dHost  = DataArray3d::HostMirror;

template <int dim>
struct DataArrays
{
};

template <>
struct DataArrays<2>
{
    using DataArray      = Kokkos::View<real_t***, device>;
    using DataArrayConst = DataArray::const_type;
    using DataArrayHost  = DataArray::HostMirror;
};

template <>
struct DataArrays<3>
{
    using DataArray      = Kokkos::View<real_t****, device>;
    using DataArrayConst = DataArray::const_type;
    using DataArrayHost  = DataArray::HostMirror;
};

/**
 * Retrieve cartesian coordinate from index, using memory layout information.
 *
 * for each execution space define a prefered layout.
 * Prefer left layout  for CUDA execution space.
 * Prefer right layout for OpenMP execution space.
 *
 * These function will eventually disappear.
 * We still need then as long as parallel_reduce does not accept MDRange policy.
 */

/* 2D */

KOKKOS_INLINE_FUNCTION
void index2coord(int index, int &i, int &j, int Nx, int Ny)
{
    UNUSED(Nx);
    UNUSED(Ny);

#ifdef KOKKOS_ENABLE_CUDA
    j = index / Nx;
    i = index - j*Nx;
#else
    i = index / Ny;
    j = index - i*Ny;
#endif
}

KOKKOS_INLINE_FUNCTION
int coord2index(int i, int j, int Nx, int Ny)
{
    UNUSED(Nx);
    UNUSED(Ny);
#ifdef KOKKOS_ENABLE_CUDA
    return i + Nx*j; // left layout
#else
    return j + Ny*i; // right layout
#endif
}

/* 3D */

KOKKOS_INLINE_FUNCTION
void index2coord(int index,
                 int &i, int &j, int &k,
                 int Nx, int Ny, int Nz)
{
    UNUSED(Nx);
    UNUSED(Nz);
#ifdef KOKKOS_ENABLE_CUDA
    int NxNy = Nx*Ny;
    k = index / NxNy;
    j = (index - k*NxNy) / Nx;
    i = index - j*Nx - k*NxNy;
#else
    int NyNz = Ny*Nz;
    i = index / NyNz;
    j = (index - i*NyNz) / Nz;
    k = index - j*Nz - i*NyNz;
#endif
}

KOKKOS_INLINE_FUNCTION
int coord2index(int i,  int j,  int k,
                int Nx, int Ny, int Nz)
{
    UNUSED(Nx);
    UNUSED(Nz);
#ifdef KOKKOS_ENABLE_CUDA
    return i + Nx*j + Nx*Ny*k; // left layout
#else
    return k + Nz*j + Nz*Ny*i; // right layout
#endif
}


#endif // KOKKOS_SHARED_H_
