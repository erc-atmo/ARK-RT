// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#ifndef BOUNDARIES_FUNCTORS_H_
#define BOUNDARIES_FUNCTORS_H_

#include "HydroParams.h"    // for HydroParams
#include "kokkos_shared.h"  // for Data arrays

/**
 * Functors to update ghost cells (Hydro 2D).
 *
 */
template <FaceIdType faceId>
class MakeBoundariesFunctor2D
{
public:
    MakeBoundariesFunctor2D(HydroParams params,
                            DataArray2d Udata) :
        params(params), Udata(Udata) {};

    static void apply(HydroParams params, DataArray2d Udata, int nbCells)
    {
        MakeBoundariesFunctor2D<faceId> functor(params, Udata);
        Kokkos::parallel_for(nbCells, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int index) const
    {
        const int nx = params.nx;
        const int ny = params.ny;

        const int ghostWidth = params.ghostWidth;
        const int nbvar = params.nbvar;

        const int imin = params.imin;
        const int imax = params.imax;

        const int jmin = params.jmin;
        const int jmax = params.jmax;

        if (faceId == FACE_XMIN)
        {
            // boundary xmin
            const int boundary_type = params.boundary_type_xmin;

            int j = index / ghostWidth;
            int i = index - j*ghostWidth;

            if(j >= jmin && j <= jmax    &&
               i >= 0    && i <ghostWidth)
            {
                for (int iVar=0; iVar<nbvar; iVar++)
                {
                    int i0 = 0;
                    real_t sign = ONE_F;
                    if (boundary_type == BC_DIRICHLET)
                    {
                        i0 = 2*ghostWidth-1-i;
                        if (iVar==IU || iVar==IFX) sign = -ONE_F;
                        /* if (iVar==IU) sign = -ONE_F; */
                    }
                    else if(boundary_type == BC_NEUMANN)
                    {
                        i0 = ghostWidth;
                    }
                    else
                    {
                        // periodic
                        i0 = nx+i;
                    }

                    Udata(i  ,j  , iVar) = Udata(i0  ,j  , iVar)*sign;
                }
            }
        } // end FACE_XMIN

        if (faceId == FACE_XMAX)
        {
            // boundary xmax
            const int boundary_type = params.boundary_type_xmax;

            int j = index / ghostWidth;
            int i = index - j*ghostWidth;
            i += (nx+ghostWidth);

            if(j >= jmin          && j <= jmax             &&
               i >= nx+ghostWidth && i <= nx+2*ghostWidth-1)
            {
                int i0 = 0;
                real_t sign = ONE_F;
                for (int iVar=0; iVar<nbvar; iVar++)
                {
                    if (boundary_type == BC_DIRICHLET)
                    {
                        i0 = 2*nx+2*ghostWidth-1-i;
                        if (iVar==IU || iVar == IFX) sign = -ONE_F;
                        /* if (iVar==IU) sign = -ONE_F; */
                    }
                    else if (boundary_type == BC_NEUMANN)
                    {
                        i0 = nx+ghostWidth-1;
                    }
                    else
                    {
                        // periodic
                        i0 = i-nx;
                    }

                    Udata(i  ,j  , iVar) = Udata(i0 ,j  , iVar)*sign;
                }
            }
        } // end FACE_XMAX

        if (faceId == FACE_YMIN)
        {
            // boundary ymin
            const int boundary_type = params.boundary_type_ymin;

            int i = index / ghostWidth;
            int j = index - i*ghostWidth;

            if(i >= imin && i <= imax    &&
               j >= 0    && j <ghostWidth)
            {
                int j0 = 0;
                real_t sign = ONE_F;
                for (int iVar=0; iVar<nbvar; iVar++)
                {
                    if (boundary_type == BC_DIRICHLET)
                    {
                        j0 = 2*ghostWidth-1-j;
                        if (iVar==IV || iVar == IFY) sign = -ONE_F;
                        /* if (iVar==IV) sign = -ONE_F; */
                    }
                    else if (boundary_type == BC_NEUMANN)
                    {
                        j0 = ghostWidth;
                    }
                    else
                    {
                        // periodic
                        j0 = ny+j;
                    }

                    Udata(i  ,j  , iVar) = Udata(i  ,j0 , iVar)*sign;
                }
            }
        } // end FACE_YMIN

        if (faceId == FACE_YMAX)
        {
            // boundary ymax
            const int boundary_type = params.boundary_type_ymax;

            int i = index / ghostWidth;
            int j = index - i*ghostWidth;
            j += (ny+ghostWidth);
            if(i >= imin          && i <= imax              &&
               j >= ny+ghostWidth && j <= ny+2*ghostWidth-1)
            {
                int j0 = 0;
                real_t sign = ONE_F;
                for (int iVar=0; iVar<nbvar; iVar++)
                {
                    if (boundary_type == BC_DIRICHLET)
                    {
                        j0 = 2*ny+2*ghostWidth-1-j;
                        if (iVar==IV || iVar == IFY) sign =- ONE_F;
                        /* if (iVar==IV) sign =- ONE_F; */
                    }
                    else if (boundary_type == BC_NEUMANN)
                    {
                        j0 = ny+ghostWidth-1;
                    }
                    else
                    {
                        // periodic
                        j0 = j-ny;
                    }

                    Udata(i  ,j  , iVar) = Udata(i  ,j0  , iVar)*sign;
                }
            }
        } // end FACE_YMAX
    } // end operator ()

    HydroParams params;
    DataArray2d Udata;
}; // MakeBoundariesFunctor2D

/**
 * Functors to update ghost cells (Hydro 3D).
 *
 */
template <FaceIdType faceId>
class MakeBoundariesFunctor3D
{
public:
    MakeBoundariesFunctor3D(HydroParams params,
                            DataArray3d Udata) :
        params(params), Udata(Udata) {};

    static void apply(HydroParams params, DataArray3d Udata, int nbCells)
    {
        MakeBoundariesFunctor3D<faceId> functor(params, Udata);
        Kokkos::parallel_for(nbCells, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int index) const
    {
        const int nx = params.nx;
        const int ny = params.ny;
        const int nz = params.nz;

        const int isize = params.isize;
        const int jsize = params.jsize;
        //const int ksize = params.ksize;
        const int ghostWidth = params.ghostWidth;
        const int nbvar = params.nbvar;

        const int imin = params.imin;
        const int imax = params.imax;

        const int jmin = params.jmin;
        const int jmax = params.jmax;

        const int kmin = params.kmin;
        const int kmax = params.kmax;

        if (faceId == FACE_XMIN)
        {
            // boundary xmin (index = i + j * ghostWidth + k * ghostWidth*jsize)
            int k = index / (ghostWidth*jsize);
            int j = (index - k*ghostWidth*jsize) / ghostWidth;
            int i = index - j*ghostWidth - k*ghostWidth*jsize;

            const int boundary_type = params.boundary_type_xmin;

            if(k >= kmin && k <= kmax &&
               j >= jmin && j <= jmax &&
               i >= 0    && i <ghostWidth)
            {
                int i0 = 0;
                real_t sign = ONE_F;
                for (int iVar=0; iVar<nbvar; iVar++)
                {
                    if (boundary_type == BC_DIRICHLET)
                    {
                        i0 = 2*ghostWidth-1-i;
                        if (iVar==IU) sign = -ONE_F;
                    }
                    else if( boundary_type == BC_NEUMANN )
                    {
                        i0 = ghostWidth;
                    }
                    else
                    {
                        // periodic
                        i0 = nx+i;
                    }

                    Udata(i,j,k, iVar) = Udata(i0,j,k, iVar)*sign;

                }

            } // end xmin
        }

        if (faceId == FACE_XMAX)
        {

            // boundary xmax (index = i + j *ghostWidth + k * ghostWidth*jsize)
            // same i,j,k as xmin, except translation along x-axis
            int k = index / (ghostWidth*jsize);
            int j = (index - k*ghostWidth*jsize) / ghostWidth;
            int i = index - j*ghostWidth - k*ghostWidth*jsize;

            i += (nx+ghostWidth);

            const int boundary_type = params.boundary_type_xmax;

            if(k >= kmin          && k <= kmax &&
               j >= jmin          && j <= jmax &&
               i >= nx+ghostWidth && i <= nx+2*ghostWidth-1)
            {
                int i0 = 0;
                real_t sign = ONE_F;
                for (int iVar=0; iVar<nbvar; iVar++)
                {
                    if (boundary_type == BC_DIRICHLET)
                    {
                        i0 = 2*nx+2*ghostWidth-1-i;
                        if (iVar==IU) sign = -ONE_F;
                    }
                    else if (boundary_type == BC_NEUMANN)
                    {
                        i0 = nx+ghostWidth-1;
                    }
                    else
                    {
                        // periodic
                        i0 = i-nx;
                    }

                    Udata(i,j,k, iVar) = Udata(i0,j,k, iVar)*sign;
                }
            } // end xmax
        }

        if (faceId == FACE_YMIN)
        {
            // boundary ymin (index = i + j*isize + k*isize*ghostWidth)
            int k = index / (isize*ghostWidth);
            int j = (index - k*isize*ghostWidth) / isize;
            int i = index - j*isize - k*isize*ghostWidth;

            const int boundary_type = params.boundary_type_ymin;

            if(k >= kmin && k <= kmax       &&
               j >= 0    && j <  ghostWidth &&
               i >= imin && i <= imax)
            {
                int j0 = 0;
                real_t sign = ONE_F;
                for (int iVar=0; iVar<nbvar; iVar++)
                {
                    if (boundary_type == BC_DIRICHLET)
                    {
                        j0 = 2*ghostWidth-1-j;
                        if (iVar==IV) sign = -ONE_F;
                    }
                    else if (boundary_type == BC_NEUMANN)
                    {
                        j0 = ghostWidth;
                    }
                    else
                    {
                        // periodic
                        j0 = ny+j;
                    }

                    Udata(i,j,k, iVar) = Udata(i,j0,k, iVar)*sign;

                }
            } // end ymin
        }

        if (faceId == FACE_YMAX)
        {
            // boundary ymax (index = i + j*isize + k*isize*ghostWidth)
            // same i,j,k as ymin, except translation along y-axis
            int k = index / (isize*ghostWidth);
            int j = (index - k*isize*ghostWidth) / isize;
            int i = index - j*isize - k*isize*ghostWidth;

            j += (ny+ghostWidth);

            const int boundary_type = params.boundary_type_ymax;

            if(k >= kmin           && k <= kmax              &&
               j >= ny+ghostWidth  && j <= ny+2*ghostWidth-1 &&
               i >= imin           && i <= imax)
            {
                int j0 = 0;
                real_t sign = ONE_F;
                for (int iVar=0; iVar<nbvar; iVar++)
                {
                    if (boundary_type == BC_DIRICHLET)
                    {
                        j0 = 2*ny+2*ghostWidth-1-j;
                        if (iVar==IV) sign = -ONE_F;
                    }
                    else if (boundary_type == BC_NEUMANN)
                    {
                        j0 = ny+ghostWidth-1;
                    }
                    else
                    {
                        // periodic
                        j0 = j-ny;
                    }

                    Udata(i,j,k, iVar) = Udata(i,j0,k, iVar)*sign;
                }
            } // end ymax
        }

        if (faceId == FACE_ZMIN)
        {
            // boundary zmin (index = i + j*isize + k*isize*jsize)
            int k = index / (isize*jsize);
            int j = (index - k*isize*jsize) / isize;
            int i = index - j*isize - k*isize*jsize;

            const int boundary_type = params.boundary_type_zmin;

            if(k >= 0    && k <  ghostWidth &&
               j >= jmin && j <= jmax       &&
               i >= imin && i <= imax)
            {
                int k0 = 0;
                real_t sign = ONE_F;
                for (int iVar=0; iVar<nbvar; iVar++)
                {
                    if (boundary_type == BC_DIRICHLET)
                    {
                        k0 = 2*ghostWidth-1-k;
                        if (iVar==IW) sign= -ONE_F;
                    }
                    else if (boundary_type == BC_NEUMANN)
                    {
                        k0 = ghostWidth;
                    }
                    else
                    {
                        // periodic
                        k0 = nz+k;
                    }

                    Udata(i,j,k, iVar) = Udata(i,j,k0, iVar)*sign;
                }
            } // end zmin
        }

        if (faceId == FACE_ZMAX)
        {
            // boundary zmax (index = i + j*isize + k*isize*jsize)
            // same i,j,k as ymin, except translation along y-axis
            int k = index / (isize*jsize);
            int j = (index - k*isize*jsize) / isize;
            int i = index - j*isize - k*isize*jsize;

            k += (nz+ghostWidth);

            const int boundary_type = params.boundary_type_zmax;

            if(k >= nz+ghostWidth && k <= nz+2*ghostWidth-1 &&
               j >= jmin          && j <= jmax              &&
               i >= imin          && i <= imax)
            {
                int k0 = 0;
                real_t sign = ONE_F;
                for (int iVar=0; iVar<nbvar; iVar++)
                {
                    if (boundary_type == BC_DIRICHLET)
                    {
                        k0 = 2*nz+2*ghostWidth-1-k;
                        if (iVar==IW) sign = -ONE_F;
                    }
                    else if (boundary_type == BC_NEUMANN)
                    {
                        k0 = nz+ghostWidth-1;
                    }
                    else
                    {
                        // periodic
                        k0 = k-nz;
                    }

                    Udata(i,j,k, iVar) = Udata(i,j,k0, iVar)*sign;
                }
            } // end zmax
        }
    } // end operator ()

    HydroParams params;
    DataArray3d Udata;
}; // MakeBoundariesFunctor3D

#endif // BOUNDARIES_FUNCTORS_H_
