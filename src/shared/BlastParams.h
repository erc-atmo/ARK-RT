// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#ifndef BLAST_PARAMS_H_
#define BLAST_PARAMS_H_

#include "utils/config/ConfigMap.h"
#include "real_type.h"

struct BlastParams
{
    // blast problem parameters
    real_t blast_radius;
    real_t blast_center_x;
    real_t blast_center_y;
    real_t blast_center_z;
    real_t blast_density_in;
    real_t blast_density_out;
    real_t blast_pressure_in;
    real_t blast_pressure_out;

    BlastParams(ConfigMap& configMap)
    {
        real_t xmin = configMap.getFloat("mesh", "xmin", 0.0);
        real_t ymin = configMap.getFloat("mesh", "ymin", 0.0);
        real_t zmin = configMap.getFloat("mesh", "zmin", 0.0);

        real_t xmax = configMap.getFloat("mesh", "xmax", 1.0);
        real_t ymax = configMap.getFloat("mesh", "ymax", 1.0);
        real_t zmax = configMap.getFloat("mesh", "zmax", 1.0);

        blast_radius   = configMap.getFloat("blast","radius", (xmin+xmax)/2.0/10);
        blast_center_x = configMap.getFloat("blast","center_x", (xmin+xmax)/2);
        blast_center_y = configMap.getFloat("blast","center_y", (ymin+ymax)/2);
        blast_center_z = configMap.getFloat("blast","center_z", (zmin+zmax)/2);
        blast_density_in  = configMap.getFloat("blast","density_in", 1.0);
        blast_density_out = configMap.getFloat("blast","density_out", 1.2);
        blast_pressure_in  = configMap.getFloat("blast","pressure_in", 10.0);
        blast_pressure_out = configMap.getFloat("blast","pressure_out", 0.1);
    }
}; // struct BlastParams

#endif // BLAST_PARAMS_H_
