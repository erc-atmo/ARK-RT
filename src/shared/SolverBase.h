// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#ifndef SOLVER_BASE_H_
#define SOLVER_BASE_H_

#include "shared/HydroParams.h"
#include "utils/config/ConfigMap.h"
#include "shared/kokkos_shared.h"

#include <map>
#include <memory> // for std::unique_ptr / std::shared_ptr

// for timer
#if defined(KOKKOS_ENABLE_CUDA)
#include "utils/monitoring/CudaTimer.h"
#elif defined(KOKKOS_ENABLE_OPENMP)
#include "utils/monitoring/OpenMPTimer.h"
#else
#include "utils/monitoring/SimpleTimer.h"
#endif

#include "shared/MpiBorderComm.h"

//! this enum helps identifying the type of solver used
enum solver_type_t
{
    SOLVER_UNDEFINED=0,
    SOLVER_MUSCL_HANCOCK=1,
    SOLVER_ALL_REGIME=2
};

namespace ark_rt { namespace io
{

class IO_ReadWriteBase;

}

}

enum TimerIds
{
    TIMER_TOTAL = 0,
    TIMER_IO = 1,
    TIMER_DT = 2,
    TIMER_BOUNDARIES = 3,
    TIMER_NUM_SCHEME = 4,
    TIMER_LINEAR_ALGEBRA = 5
}; // enum TimerIds

namespace ark_rt
{

/**
 * Abstract base class for all our actual solvers.
 */
class SolverBase
{
public:
    SolverBase(HydroParams& params, ConfigMap& configMap);
    virtual ~SolverBase();

    // hydroParams
    HydroParams& params;
    ConfigMap& configMap;

    // MpiBorderComm
    MpiBorderComm mpiBorderComm;

    /* some common member data */
    solver_type_t solver_type;

    //! is this a restart run ?
    bool m_restart_run_enabled;

    //! make an upscale
    bool m_restart_run_upscale;

    //! filename containing data from a previous run.
    std::string m_restart_run_filename;

    int m_restart_iteration;

    // iteration info
    real_t               m_t;         //!< the time at the current iteration
    real_t               m_dt;        //!< the time step at the current iteration
    int                  m_iteration; //!< the current iteration (integer)
    real_t               m_tStart_global;    //!< initial time
    real_t               m_tStart_local;    //!< initial time
    real_t               m_tEnd;      //!< maximun time
    real_t               m_cfl;       //!< Courant number

    int                  m_nlog;      //!< number of steps between two monitoring print on screen

    long long int        m_nCells;       //!< number of cells
    long long int        m_nDofsPerCell; //!< number of degrees of freedom per cell

    //! init condition name (or problem)
    std::string          m_problem_name;

    //! solver name (use in output file).
    std::string          m_solver_name;

    /*
     *
     * Computation interface that may be overriden in a derived
     * concrete implementation.
     *
     */

    virtual void getDeviceData(DataArray2d& Udata);
    virtual void getDeviceData(DataArray3d& Udata);

    //! Read and parse the configuration file (ini format).
    virtual void read_config();

    //! Compute CFL condition (allowed time step), over all MPI process.
    virtual void compute_dt();

    //! Compute CFL condition local to current MPI process
    virtual real_t compute_dt_local();

    //! Check if current time is larger than end time.
    virtual int finished();

    //! This is where action takes place. Wrapper arround next_iteration_impl.
    virtual void next_iteration();

    //! This is the next iteration computation (application specific).
    virtual void next_iteration_impl();

    //! Decides if the current time step is eligible for dump data to file
    virtual int  should_save_solution();

    //! main routine to dump solution to file
    virtual void save_solution();

    //! main routine to dump solution to file
    virtual void save_solution_impl();

    //! read restart data
    virtual void read_restart_file();

    /* IO related */

    //! counter incremented each time an output is written 
    //! if the simulation is a restart, it is the total number of outputs
    int m_iOutput_global;

    //! counter incremented each time an output is written 
    //! if the simulation is a restart, it is the number of outputs for the simulation
    int m_iOutput_local;

    //! Number of variables to saved
    //int m_write_variables_number;

    //! names of variables to save
    std::map<int, std::string> m_variables_names;

    //! timers
#if defined(KOKKOS_ENABLE_CUDA)
    using Timer = CudaTimer;
#elif defined(KOKKOS_ENABLE_OPENMP)
    using Timer = OpenMPTimer;
#else
    using Timer = SimpleTimer;
#endif
    using TimerMap = std::map<int, std::shared_ptr<Timer> >;
    TimerMap timers;

    void save_data(DataArray2d             U,
                   DataArray2d::HostMirror Uh,
                   int iOutput,
                   int iStep,
                   real_t time);

    void save_data(DataArray3d             U,
                   DataArray3d::HostMirror Uh,
                   int iOutput,
                   int iStep,
                   real_t time);

    void save_data_debug(DataArray2d             U,
                         DataArray2d::HostMirror Uh,
                         int iOutput,
                         int iStep,
                         real_t time,
                         std::string debug_name);

    void save_data_debug(DataArray3d             U,
                         DataArray3d::HostMirror Uh,
                         int iOutput,
                         int iStep,
                         real_t time,
                         std::string debug_name);

    virtual void make_boundaries(DataArray2d Udata);
    virtual void make_boundaries(DataArray3d Udata);

    virtual void make_boundary(DataArray2d Udata, FaceIdType faceId);
    virtual void make_boundary(DataArray3d Udata, FaceIdType faceId);

    virtual void make_boundaries_serial(DataArray2d Udata);
    virtual void make_boundaries_serial(DataArray3d Udata);

#ifdef USE_MPI
    virtual void make_boundaries_mpi(DataArray2d Udata);
    virtual void make_boundaries_mpi(DataArray3d Udata);

#endif // USE_MPI

    //! initialize m_io_reader_writer (can be override in a derived class)
    virtual void init_io();

protected:

    //! io writer
    std::shared_ptr<io::IO_ReadWriteBase>  m_io_reader_writer;

}; // class SolverBase

} // namespace ark_rt

#endif // SOLVER_BASE_H_
