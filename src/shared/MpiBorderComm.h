// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "shared/kokkos_shared.h"
#include "shared/HydroParams.h"
#ifdef USE_MPI
#include "shared/mpiBorderUtils.h"
#endif

namespace ark_rt {

  /**
   * Class to handle MPI communications
   * The idea is to have (almost) the same routines 
   * for the boundaries in the explicit code
   * and for the linear algebra
   */
  class MpiBorderComm
  {
    public:
      MpiBorderComm(HydroParams& params);
      ~MpiBorderComm();

      HydroParams params;

#ifdef USE_MPI
      void copy_boundaries(DataArray2d Udata, Direction dir);
      void copy_boundaries(DataArray3d Udata, Direction dir);

      void transfert_boundaries_2d(Direction dir);
      void transfert_boundaries_3d(Direction dir);

      void copy_boundaries_back(DataArray2d Udata, BoundaryLocation loc);
      void copy_boundaries_back(DataArray3d Udata, BoundaryLocation loc);

#endif

    protected: 
#ifdef USE_MPI
      //! \defgroup BorderBuffer data arrays for border exchange handling
      //! we assume that we use a cuda-aware version of OpenMPI / MVAPICH
      //! @{
      DataArray2d borderBufSend_xmin_2d;
      DataArray2d borderBufSend_xmax_2d;
      DataArray2d borderBufSend_ymin_2d;
      DataArray2d borderBufSend_ymax_2d;

      DataArray2d borderBufRecv_xmin_2d;
      DataArray2d borderBufRecv_xmax_2d;
      DataArray2d borderBufRecv_ymin_2d;
      DataArray2d borderBufRecv_ymax_2d;

      DataArray3d borderBufSend_xmin_3d;
      DataArray3d borderBufSend_xmax_3d;
      DataArray3d borderBufSend_ymin_3d;
      DataArray3d borderBufSend_ymax_3d;
      DataArray3d borderBufSend_zmin_3d;
      DataArray3d borderBufSend_zmax_3d;

      DataArray3d borderBufRecv_xmin_3d;
      DataArray3d borderBufRecv_xmax_3d;
      DataArray3d borderBufRecv_ymin_3d;
      DataArray3d borderBufRecv_ymax_3d;
      DataArray3d borderBufRecv_zmin_3d;
      DataArray3d borderBufRecv_zmax_3d;
      //! @}
#endif
  };

} // namespace ark_rt
