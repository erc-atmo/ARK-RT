// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#ifndef RADIATIVE_SHOCK_PARAMS_H_
#define RADIATIVE_SHOCK_PARAMS_H_

#include "utils/config/ConfigMap.h"
#include "real_type.h"

struct RadiativeShockParams
{
  // radiative shock problem parameters
  real_t sigma;
  real_t rho;
  real_t u0;
  real_t T;
  real_t kappa;

  RadiativeShockParams(ConfigMap& configMap)
  {
    sigma = configMap.getFloat("radiativeShock", "sigma", 3.1e-10);
    rho = configMap.getFloat("radiativeShock", "rho", 7.78e-10);
    u0 = configMap.getFloat("radiativeShock", "u0", 0.);
    T = configMap.getFloat("radiativeShock", "T", 10.0);
    kappa = configMap.getFloat("hydro", "kap1", 4e4);
  }
}; // struct RadiativeShockParams

#endif // RADIATIVE_SHOCK_PARAMS_H_
