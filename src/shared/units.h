// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include <cmath>
#include <limits>
#include "shared/real_type.h"


namespace ark_rt
{

namespace
{

// sqrtNewtonRaphson and my_sqrt come from github :
// https://gist.github.com/alexshtf/eb5128b3e3e143187794
inline constexpr real_t sqrtNewtonRaphson(real_t x, real_t curr, real_t prev)
{
    return (curr == prev)
        ? curr
        : sqrtNewtonRaphson(x, HALF_F * (curr + x / curr), curr);
}

/*
 * Constexpr version of the square root
 * Return value:
 *      - For a finite and non-negative value of "x", returns an approximation for the square root of "x"
 *   - Otherwise, returns NaN
 */
inline constexpr real_t my_sqrt(real_t x)
{
    return (x >= ZERO_F && x < std::numeric_limits<real_t>::infinity())
        ? sqrtNewtonRaphson(x, x, ZERO_F)
        : std::numeric_limits<real_t>::quiet_NaN();
}

} // namespace anonymous

struct si_constants
{
    // Avogadro number in SI (mol^-1)
    static constexpr real_t Na      {6.022140857E+23};
    // Boltzmann constant in SI (J.K^-1)
    static constexpr real_t k_b     {1.380648520E-23};
    // Universal gas constant in SI (J.mol^-1.K^-1)
    static constexpr real_t R       {8.314459800E+00};
    // Mean molecular weight of hydrogen (no unit)
    static constexpr real_t mmw_h   {ONE_F};
    // Mass of hydrogen in SI (kg)
    static constexpr real_t m_h     {1.673723600E-27};
    // Molar mass of hydrogen in SI (kg.mol^-1)
    static constexpr real_t M_h     {m_h * Na};
    // Specific gas constant of hydrogen in SI (J.kg^-1.K^-1)
    static constexpr real_t Rstar_h {R / M_h};
    // Constant of stefan Boltzmann in SI (W.m^-2.K^-4)
    static constexpr real_t sigsb   {5.670373000E-08};
    // Speed of light in SI (m.s^-1)
    static constexpr real_t speedOfLight {299792458E+00};
};

namespace code_units
{

#if defined(CGS_UNITS)
static constexpr real_t length_u          {1.0E-2}; // in SI (m)
static constexpr real_t mass_u            {1.0E-3}; // in SI (kg
static constexpr real_t time_u            {ONE_F}; // in SI (s)
static constexpr real_t temperature_u     {ONE_F}; // in SI (K)
static constexpr real_t matter_quantity_u {ONE_F}; // in SI (mol)
#elif defined(ASTRO_UNITS)
static constexpr real_t length_u          {3.086E+16}; // in SI (m)
static constexpr real_t mass_u            {1E+18}; // in SI (kg
static constexpr real_t time_u            {length_u/si_constants::speedOfLight}; // in SI (s)
static constexpr real_t temperature_u     {ONE_F}; // in SI (K)
static constexpr real_t matter_quantity_u {ONE_F}; // in SI (mol)
#elif defined(RB_UNITS)
static constexpr real_t length_u          {ONE_F}; // in SI (m)
static constexpr real_t mass_u            {ONE_F}; // in SI (kg)
static constexpr real_t temperature_u     {ONE_F}; // in SI (K)
// the mean-molecular weight should appear in the unit time but it is a run-time parameter,
// so to be consistent with the set of parameter the mean molecular weight has to be equal to 1.0 in the parameter file (.ini)
static constexpr real_t time_u            {length_u / my_sqrt(si_constants::k_b /
                                                              (si_constants::mmw_h * si_constants::m_h) * temperature_u)}; // in SI (s)
static constexpr real_t matter_quantity_u {ONE_F}; // in SI (mol)
#elif defined(SI_UNITS)
static constexpr real_t length_u          {ONE_F}; // in SI (m)
static constexpr real_t mass_u            {ONE_F}; // in SI (kg)
static constexpr real_t time_u            {ONE_F}; // in SI (s)
static constexpr real_t temperature_u     {ONE_F}; // in SI (K)
static constexpr real_t matter_quantity_u {ONE_F}; // in SI (mol)
#else
static_assert(false, "You must define a system unit !");
#endif

// Derived units
static constexpr real_t volume_u   = length_u * length_u * length_u;
static constexpr real_t density_u  = mass_u / volume_u;
static constexpr real_t velocity_u = length_u / time_u;
static constexpr real_t energy_u   = mass_u * velocity_u * velocity_u;
static constexpr real_t pressure_u = energy_u / volume_u;

namespace constants
{

static constexpr real_t Na      = si_constants::Na  / (ONE_F / code_units::matter_quantity_u);
static constexpr real_t k_b     = si_constants::k_b / (code_units::energy_u / code_units::temperature_u);
static constexpr real_t R       = si_constants::R   / (code_units::energy_u / (code_units::matter_quantity_u * code_units::temperature_u));
static constexpr real_t mmw_h   = si_constants::mmw_h;
static constexpr real_t m_h     = si_constants::m_h / code_units::mass_u;
static constexpr real_t M_h     = m_h * Na;
static constexpr real_t Rstar_h = R / M_h;
static constexpr real_t sigsb   = si_constants::sigsb / (code_units::energy_u / (code_units::time_u *
      										 code_units::length_u * code_units::length_u *
                                                                                 code_units::temperature_u * code_units::temperature_u *
                                                                                 code_units::temperature_u * code_units::temperature_u ));
static constexpr real_t speedOfLight = si_constants::speedOfLight  / (code_units::length_u / code_units::time_u);

} // namespace constants

} // namespace code_units

} // namespace ark_rt
