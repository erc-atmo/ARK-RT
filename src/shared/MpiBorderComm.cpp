// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#include "MpiBorderComm.h"

namespace ark_rt{

  MpiBorderComm::MpiBorderComm (HydroParams& params) :
    params(params)
  {
#ifdef USE_MPI
    const int gw = params.ghostWidth;
    const int isize = params.isize;
    const int jsize = params.jsize;
    const int ksize = params.ksize;
    const int nbvar = params.nbvar;

    if (params.dimType == TWO_D)
    {
      borderBufSend_xmin_2d = DataArray2d("borderBufSend_xmin",    gw, jsize, nbvar);
      borderBufSend_xmax_2d = DataArray2d("borderBufSend_xmax",    gw, jsize, nbvar);
      borderBufSend_ymin_2d = DataArray2d("borderBufSend_ymin", isize,    gw, nbvar);
      borderBufSend_ymax_2d = DataArray2d("borderBufSend_ymax", isize,    gw, nbvar);

      borderBufRecv_xmin_2d = DataArray2d("borderBufRecv_xmin",    gw, jsize, nbvar);
      borderBufRecv_xmax_2d = DataArray2d("borderBufRecv_xmax",    gw, jsize, nbvar);
      borderBufRecv_ymin_2d = DataArray2d("borderBufRecv_ymin", isize,    gw, nbvar);
      borderBufRecv_ymax_2d = DataArray2d("borderBufRecv_ymax", isize,    gw, nbvar);
    }
    else
    {
      borderBufSend_xmin_3d = DataArray3d("borderBufSend_xmin",    gw, jsize, ksize, nbvar);
      borderBufSend_xmax_3d = DataArray3d("borderBufSend_xmax",    gw, jsize, ksize, nbvar);
      borderBufSend_ymin_3d = DataArray3d("borderBufSend_ymin", isize,    gw, ksize, nbvar);
      borderBufSend_ymax_3d = DataArray3d("borderBufSend_ymax", isize,    gw, ksize, nbvar);
      borderBufSend_zmin_3d = DataArray3d("borderBufSend_zmin", isize, jsize,    gw, nbvar);
      borderBufSend_zmax_3d = DataArray3d("borderBufSend_zmax", isize, jsize,    gw, nbvar);

      borderBufRecv_xmin_3d = DataArray3d("borderBufRecv_xmin",    gw, jsize, ksize, nbvar);
      borderBufRecv_xmax_3d = DataArray3d("borderBufRecv_xmax",    gw, jsize, ksize, nbvar);
      borderBufRecv_ymin_3d = DataArray3d("borderBufRecv_ymin", isize,    gw, ksize, nbvar);
      borderBufRecv_ymax_3d = DataArray3d("borderBufRecv_ymax", isize,    gw, ksize, nbvar);
      borderBufRecv_zmin_3d = DataArray3d("borderBufRecv_zmin", isize, jsize,    gw, nbvar);
      borderBufRecv_zmax_3d = DataArray3d("borderBufRecv_zmax", isize, jsize,    gw, nbvar);
    }
#endif
  } // MpiBorderComm::MpiBorderComm

  MpiBorderComm::~MpiBorderComm()
  {
  } // MpiBorderComm::~MpiBorderComm

#ifdef USE_MPI
  void
    MpiBorderComm::copy_boundaries(DataArray2d Udata, Direction dir)
    {
      const int isize = params.isize;
      const int jsize = params.jsize;
      const int gw    = params.ghostWidth;

      if (dir == XDIR)
      {
        const int nbIter = gw * jsize;

        CopyDataArray_To_BorderBuf<XMIN, TWO_D>::apply(borderBufSend_xmin_2d, Udata, gw, nbIter);
        CopyDataArray_To_BorderBuf<XMAX, TWO_D>::apply(borderBufSend_xmax_2d, Udata, gw, nbIter);
      }
      else if (dir == YDIR)
      {
        const int nbIter = isize * gw;

        CopyDataArray_To_BorderBuf<YMIN, TWO_D>::apply(borderBufSend_ymin_2d, Udata, gw, nbIter);
        CopyDataArray_To_BorderBuf<YMAX, TWO_D>::apply(borderBufSend_ymax_2d, Udata, gw, nbIter);
      }

      Kokkos::fence();
    } // MpiBorderComm::copy_boundaries - 2d

  // =======================================================
  // =======================================================
  void
    MpiBorderComm::copy_boundaries(DataArray3d Udata, Direction dir)
    {
      const int isize = params.isize;
      const int jsize = params.jsize;
      const int ksize = params.ksize;
      const int gw    = params.ghostWidth;

      if (dir == XDIR)
      {
        const int nbIter = gw * jsize * ksize;

        CopyDataArray_To_BorderBuf<XMIN, THREE_D>::apply(borderBufSend_xmin_3d, Udata, gw, nbIter);
        CopyDataArray_To_BorderBuf<XMAX, THREE_D>::apply(borderBufSend_xmax_3d, Udata, gw, nbIter);
      }
      else if (dir == YDIR)
      {
        const int nbIter = isize * gw * ksize;

        CopyDataArray_To_BorderBuf<YMIN, THREE_D>::apply(borderBufSend_ymin_3d, Udata, gw, nbIter);
        CopyDataArray_To_BorderBuf<YMAX, THREE_D>::apply(borderBufSend_ymax_3d, Udata, gw, nbIter);
      }
      else if (dir == ZDIR)
      {
        const int nbIter = isize * jsize * gw;

        CopyDataArray_To_BorderBuf<ZMIN, THREE_D>::apply(borderBufSend_zmin_3d, Udata, gw, nbIter);
        CopyDataArray_To_BorderBuf<ZMAX, THREE_D>::apply(borderBufSend_zmax_3d, Udata, gw, nbIter);
      }

      Kokkos::fence();
    } // MpiBorderComm::copy_boundaries - 3d

  void
    MpiBorderComm::transfert_boundaries_2d(Direction dir)
    {
      const int data_type = params.data_type;

      using namespace hydroSimu;

      /*
       * use MPI_Sendrecv
       */

      // two borders to send, two borders to receive

      if (dir == XDIR)
      {
        params.communicator->sendrecv(borderBufSend_xmin_2d.data(),
            borderBufSend_xmin_2d.size(),
            data_type, params.neighborsRank[X_MIN], 111,
            borderBufRecv_xmax_2d.data(),
            borderBufRecv_xmax_2d.size(),
            data_type, params.neighborsRank[X_MAX], 111);

        params.communicator->sendrecv(borderBufSend_xmax_2d.data(),
            borderBufSend_xmax_2d.size(),
            data_type, params.neighborsRank[X_MAX], 111,
            borderBufRecv_xmin_2d.data(),
            borderBufRecv_xmin_2d.size(),
            data_type, params.neighborsRank[X_MIN], 111);
      }
      else if (dir == YDIR)
      {
        params.communicator->sendrecv(borderBufSend_ymin_2d.data(),
            borderBufSend_ymin_2d.size(),
            data_type, params.neighborsRank[Y_MIN], 211,
            borderBufRecv_ymax_2d.data(),
            borderBufRecv_ymax_2d.size(),
            data_type, params.neighborsRank[Y_MAX], 211);

        params.communicator->sendrecv(borderBufSend_ymax_2d.data(),
            borderBufSend_ymax_2d.size(),
            data_type, params.neighborsRank[Y_MAX], 211,
            borderBufRecv_ymin_2d.data(),
            borderBufRecv_ymin_2d.size(),
            data_type, params.neighborsRank[Y_MIN], 211);
      }
    } // MpiBorderComm::transfert_boundaries_2d

  // =======================================================
  // =======================================================
  void
    MpiBorderComm::transfert_boundaries_3d(Direction dir)
    {
      const int data_type = params.data_type;

      using namespace hydroSimu;

      if (dir == XDIR)
      {
        params.communicator->sendrecv(borderBufSend_xmin_3d.data(),
            borderBufSend_xmin_3d.size(),
            data_type, params.neighborsRank[X_MIN], 111,
            borderBufRecv_xmax_3d.data(),
            borderBufRecv_xmax_3d.size(),
            data_type, params.neighborsRank[X_MAX], 111);

        params.communicator->sendrecv(borderBufSend_xmax_3d.data(),
            borderBufSend_xmax_3d.size(),
            data_type, params.neighborsRank[X_MAX], 111,
            borderBufRecv_xmin_3d.data(),
            borderBufRecv_xmin_3d.size(),
            data_type, params.neighborsRank[X_MIN], 111);
      }
      else if (dir == YDIR)
      {
        params.communicator->sendrecv(borderBufSend_ymin_3d.data(),
            borderBufSend_ymin_3d.size(),
            data_type, params.neighborsRank[Y_MIN], 211,
            borderBufRecv_ymax_3d.data(),
            borderBufRecv_ymax_3d.size(),
            data_type, params.neighborsRank[Y_MAX], 211);

        params.communicator->sendrecv(borderBufSend_ymax_3d.data(),
            borderBufSend_ymax_3d.size(),
            data_type, params.neighborsRank[Y_MAX], 211,
            borderBufRecv_ymin_3d.data(),
            borderBufRecv_ymin_3d.size(),
            data_type, params.neighborsRank[Y_MIN], 211);
      }
      else if (dir == ZDIR)
      {
        params.communicator->sendrecv(borderBufSend_zmin_3d.data(),
            borderBufSend_zmin_3d.size(),
            data_type, params.neighborsRank[Z_MIN], 311,
            borderBufRecv_zmax_3d.data(),
            borderBufRecv_zmax_3d.size(),
            data_type, params.neighborsRank[Z_MAX], 311);

        params.communicator->sendrecv(borderBufSend_zmax_3d.data(),
            borderBufSend_zmax_3d.size(),
            data_type, params.neighborsRank[Z_MAX], 311,
            borderBufRecv_zmin_3d.data(),
            borderBufRecv_zmin_3d.size(),
            data_type, params.neighborsRank[Z_MIN], 311);
      }
    } // MpiBorderComm::transfert_boundaries_3d

  void
    MpiBorderComm::copy_boundaries_back(DataArray2d Udata, BoundaryLocation loc)
    {
      const int isize = params.isize;
      const int jsize = params.jsize;
      //const int ksize = params.ksize;
      const int gw    = params.ghostWidth;

      if (loc == XMIN)
      {
        const int nbIter = gw * jsize;

        CopyBorderBuf_To_DataArray<XMIN, TWO_D>::apply(Udata, borderBufRecv_xmin_2d, gw, nbIter);
      }

      if (loc == XMAX)
      {
        const int nbIter = gw * jsize;

        CopyBorderBuf_To_DataArray<XMAX, TWO_D>::apply(Udata, borderBufRecv_xmax_2d, gw, nbIter);
      }

      if (loc == YMIN)
      {
        const int nbIter = isize * gw;

        CopyBorderBuf_To_DataArray<YMIN, TWO_D>::apply(Udata, borderBufRecv_ymin_2d, gw, nbIter);
      }

      if (loc == YMAX)
      {

        const int nbIter = isize * gw;

        CopyBorderBuf_To_DataArray<YMAX, TWO_D>::apply(Udata, borderBufRecv_ymax_2d, gw, nbIter);
      }
    } // MpiBorderComm::copy_boundaries_back - 2d

  // =======================================================
  // =======================================================
  void
    MpiBorderComm::copy_boundaries_back(DataArray3d Udata, BoundaryLocation loc)
    {
      const int isize = params.isize;
      const int jsize = params.jsize;
      const int ksize = params.ksize;
      const int gw    = params.ghostWidth;

      if (loc == XMIN)
      {
        const int nbIter = gw * jsize * ksize;

        CopyBorderBuf_To_DataArray<XMIN, THREE_D>::apply(Udata, borderBufRecv_xmin_3d, gw, nbIter);
      }

      if (loc == XMAX)
      {
        const int nbIter = gw * jsize * ksize;

        CopyBorderBuf_To_DataArray<XMAX, THREE_D>::apply(Udata, borderBufRecv_xmax_3d, gw, nbIter);
      }

      if (loc == YMIN)
      {
        const int nbIter = isize * gw * ksize;

        CopyBorderBuf_To_DataArray<YMIN, THREE_D>::apply(Udata, borderBufRecv_ymin_3d, gw, nbIter);
      }

      if (loc == YMAX)
      {
        const int nbIter = isize * gw * ksize;

        CopyBorderBuf_To_DataArray<YMAX, THREE_D>::apply(Udata, borderBufRecv_ymax_3d, gw, nbIter);
      }

      if (loc == ZMIN)
      {
        const int nbIter = isize * jsize * gw;

        CopyBorderBuf_To_DataArray<ZMIN, THREE_D>::apply(Udata, borderBufRecv_zmin_3d, gw, nbIter);
      }

      if (loc == ZMAX)
      {
        const int nbIter = isize * jsize * gw;

        CopyBorderBuf_To_DataArray<ZMAX, THREE_D>::apply(Udata, borderBufRecv_zmax_3d, gw, nbIter);
      }
    } // MpiBorderComm::copy_boundaries_back - 3d


#endif



} // namespace ark_rt
