// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "shared/HydroParams.h"    // for HydroParams
#include "shared/units.h"
#include "shared/kokkos_shared.h"  // for Data arrays
#include "shared/HydroState.h"
#include "shared/RadiativeConvectionParams.h"
#include <iostream>

namespace ark_rt
{

template <FaceIdType faceId>
class MakeBoundariesFunctor2D_RadiativeConvection
{
public:
    MakeBoundariesFunctor2D_RadiativeConvection(HydroParams params_,
                                                RadiativeConvectionParams rc_params_,
                                                DataArray2d Udata_)
        : params(params_)
        , rc_params(rc_params_)
        , Udata(Udata_)
    {
    };

    static void apply(HydroParams params,
                      RadiativeConvectionParams rc_params,
                      DataArray2d Udata,
                      int nbCells)
    {
        MakeBoundariesFunctor2D_RadiativeConvection functor(params, rc_params, Udata);
        Kokkos::parallel_for(nbCells, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int& index) const
    {
        const int ny = params.ny;

        const int ghostWidth = params.ghostWidth;

        const int imin = params.imin;
        const int imax = params.imax;

        const int jmin = params.jmin;

        const real_t dy = params.dy;
        const real_t gamma0 = params.settings.gamma0;
        const real_t k_b = code_units::constants::k_b;
        const real_t m_h = code_units::constants::m_h;
        const real_t mmw = params.settings.mmw;
        const real_t g_y = params.settings.g_y;

        if (faceId == FACE_YMIN)
        {
            const int i = index / ghostWidth;

            if(i >= imin && i <= imax)
            {
                const int j0 = jmin+ghostWidth;
                real_t ekin = 0.5*(Udata(i, j0, IU)*Udata(i, j0, IU)+Udata(i, j0, IV)*Udata(i, j0, IV))/Udata(i,j0,ID);
                real_t pp0 = (Udata(i, j0, IP)-ekin) * (gamma0-ONE_F);
                real_t T0  = pp0/(k_b*Udata(i,j0,ID))*mmw*m_h;

                const int j1 = jmin+ghostWidth+1;
                ekin = 0.5*(Udata(i, j1, IU)*Udata(i, j1, IU)+Udata(i, j1, IV)*Udata(i, j1, IV))/Udata(i,j1,ID);
                real_t pp1 = (Udata(i, j1, IP)-ekin) * (gamma0-ONE_F);
                real_t T1  = pp1/(k_b*Udata(i,j1,ID))*mmw*m_h;

                const real_t deltaT = T0-T1;

                for (int j=jmin+ghostWidth-1; j>=jmin; j--)
                {
                    const int jp1 = j+1;
                    real_t ekin = 0.5*(Udata(i, jp1, IU)*Udata(i, jp1, IU)+Udata(i, jp1, IV)*Udata(i, jp1, IV))/Udata(i,jp1,ID);
                    real_t ppp1 = (Udata(i, jp1, IP)-ekin) * (gamma0-ONE_F);
                    real_t Tp1  = ppp1/(k_b*Udata(i,jp1,ID))*mmw*m_h;

                    const real_t csp1  = sqrt(k_b*(Tp1)/(mmw*m_h));
                    const real_t cs  = sqrt(k_b*(Tp1+deltaT)/(mmw*m_h));

                    Udata(i, j, ID) = (csp1*csp1 - HALF_F*g_y*dy) / (cs*cs + HALF_F*g_y*dy) * Udata(i, j+1, ID);
                    Udata(i, j, IP) = Udata(i, j, ID) * cs*cs/ (gamma0-ONE_F);
                    Udata(i, j, IU) = ZERO_F;
                    Udata(i, j, IV) = ZERO_F;
                    Udata(i, j, IS) = Udata(i, j, ID) * ONE_F;
                }
            }
        }

        if (faceId == FACE_YMAX)
        {
            const int i = index / ghostWidth;

            if(i >= imin && i <= imax)
            {
                const int j0 = ny+ghostWidth-1;
                real_t ekin = 0.5*(Udata(i, j0, IU)*Udata(i, j0, IU)+Udata(i, j0, IV)*Udata(i, j0, IV))/Udata(i,j0,ID);
                real_t pp0 = (Udata(i, j0, IP)-ekin) * (gamma0-ONE_F);
                real_t T0  = pp0/(k_b*Udata(i,j0,ID))*mmw*m_h;

                const int j1 = ny+ghostWidth-2;
                ekin = 0.5*(Udata(i, j1, IU)*Udata(i, j1, IU)+Udata(i, j1, IV)*Udata(i, j1, IV))/Udata(i,j1,ID);
                real_t pp1 = (Udata(i, j1, IP)-ekin) * (gamma0-ONE_F);
                real_t T1  = pp1/(k_b*Udata(i,j1,ID))*mmw*m_h;

                const real_t deltaT = T0-T1;

                for (int j=ny+ghostWidth; j<=ny+2*ghostWidth-1; j++)
                {
                    // u[index + ID*ijSize] = exp((j-ghostWidth) * log((TWO_F - dy)/(TWO_F + dy)));
                    const int jm1 = j-1;
                    ekin = 0.5*(Udata(i, jm1, IU)*Udata(i, jm1, IU)+Udata(i, jm1, IV)*Udata(i, jm1, IV))/Udata(i,jm1,ID);
                    real_t ppm1 = (Udata(i, jm1, IP)-ekin) * (gamma0-ONE_F);
                    real_t Tm1  = ppm1/(k_b*Udata(i,jm1,ID))*mmw*m_h;

                    const real_t csm1  = sqrt(k_b*(Tm1)/(mmw*m_h));
                    const real_t cs  = sqrt(k_b*(Tm1+deltaT)/(mmw*m_h));

                    Udata(i, j, ID) = (csm1*csm1 + HALF_F*g_y*dy) / (cs*cs - HALF_F*g_y*dy)* Udata(i, j-1, ID);
                    Udata(i, j, IP) = Udata(i, j, ID) * cs*cs /(gamma0-ONE_F);
                    Udata(i, j, IU) = ZERO_F;
                    Udata(i, j, IV) = ZERO_F;
                    Udata(i, j, IS) = Udata(i, j, ID) * ZERO_F;
                }
            }
        }
    }

    const HydroParams params;
    const RadiativeConvectionParams rc_params;
    DataArray2d Udata;
}; // MakeBoundariesFunctor2D_RadiativeConvection

}
