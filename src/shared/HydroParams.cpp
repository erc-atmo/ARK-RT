// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#include "HydroParams.h"

#include "ark_rt.h"
#include "shared/units.h"

#include <cstdlib> // for exit
#include <cstdio>  // for fprintf
#include <cstring> // for strcmp
#include <iostream>

#include "config/inih/ini.h" // our INI file reader
#include "HydroState.h"

#ifdef USE_MPI
using namespace hydroSimu;
#endif // USE_MPI

// =======================================================
// =======================================================
/*
 * Hydro Parameters (read parameter file)
 */
void HydroParams::setup(ConfigMap &configMap)
{
  /* initialize RUN parameters */
  nStepmax = configMap.getInteger("run","nstepmax",1000);
  tEnd     = configMap.getFloat  ("run","tend",0.0);
  // nOutput is the number of outputs per run
  // if the simulation is a restart from output N,
  // the total number of outputs will be N+nOutputs
  nOutput  = configMap.getInteger("run","noutput",100);

  nlog        = configMap.getInteger("run","nlog",10);
  solver_name = configMap.getString("run", "solver_name", "unknown");

  if (solver_name == "Hydro_Muscl_2D")
  {
    dimType = TWO_D;
    nbvar = HYDRO_2D_NBVAR;
    ghostWidth = 2;
  }
  else if (solver_name == "Hydro_Muscl_3D")
  {
    dimType = THREE_D;
    nbvar = HYDRO_3D_NBVAR;
    ghostWidth = 2;
  }
  else if (solver_name == "Hydro_All_Regime_2D")
  {
    dimType = TWO_D;
    nbvar = HYDRO_2D_NBVAR;
    ghostWidth = 2;
  }
  else if (solver_name == "Hydro_All_Regime_3D")
  {
    dimType = THREE_D;
    nbvar = HYDRO_3D_NBVAR;
    ghostWidth = 2;
  }
  else if (solver_name == "Hydro_All_Regime_Radiative_Transfer_2D")
  {
    dimType = TWO_D;
    nbvar = HYDRO_2D_NBVAR;
    ghostWidth = 2;
  }
  else
  {
    ark_rt::abort("Solver \""+solver_name+"\" is not valid");
  }

  /* initialize MESH parameters */
  nx = configMap.getInteger("mesh","nx", 1);
  ny = configMap.getInteger("mesh","ny", 1);
  nz = configMap.getInteger("mesh","nz", 1);

  xmin = configMap.getFloat("mesh", "xmin", 0.0);
  ymin = configMap.getFloat("mesh", "ymin", 0.0);
  zmin = configMap.getFloat("mesh", "zmin", 0.0);

  xmax = configMap.getFloat("mesh", "xmax", 1.0);
  ymax = configMap.getFloat("mesh", "ymax", 1.0);
  zmax = configMap.getFloat("mesh", "zmax", 1.0);

  boundary_type_xmin  = static_cast<BoundaryConditionType>(configMap.getInteger("mesh","boundary_type_xmin", BC_DIRICHLET));
  boundary_type_xmax  = static_cast<BoundaryConditionType>(configMap.getInteger("mesh","boundary_type_xmax", BC_DIRICHLET));
  boundary_type_ymin  = static_cast<BoundaryConditionType>(configMap.getInteger("mesh","boundary_type_ymin", BC_DIRICHLET));
  boundary_type_ymax  = static_cast<BoundaryConditionType>(configMap.getInteger("mesh","boundary_type_ymax", BC_DIRICHLET));
  boundary_type_zmin  = static_cast<BoundaryConditionType>(configMap.getInteger("mesh","boundary_type_zmin", BC_DIRICHLET));
  boundary_type_zmax  = static_cast<BoundaryConditionType>(configMap.getInteger("mesh","boundary_type_zmax", BC_DIRICHLET));

  settings.cfl_hydro      = configMap.getFloat("hydro", "cfl_hydro", 0.5);
  settings.cfl_rad        = configMap.getFloat("radiative_transfer", "cfl_rad", 0.9);
  settings.iorder         = configMap.getInteger("hydro","iorder", 2);
  settings.slope_type     = configMap.getFloat("hydro","slope_type",1.0);
  settings.smallc         = configMap.getFloat("hydro","smallc", 1e-10);
  settings.smallr         = configMap.getFloat("hydro","smallr", 1e-10);

  settings.K                   = configMap.getFloat("hydro", "K", 1.1);
  settings.low_mach_correction = configMap.getBool("hydro", "low_mach_correction", true);
  settings.conservative        = configMap.getBool("hydro", "conservative", true);

  settings.gamma0         = configMap.getFloat("hydro","gamma0", 1.4);
  settings.mmw            = configMap.getFloat("hydro", "mmw", 1.0);
  settings.mmw1           = configMap.getFloat("hydro","mmw1",2.4);
  settings.mmw2           = configMap.getFloat("hydro","mmw2",2.4);

  settings.mu             = configMap.getFloat("hydro", "mu", 0.0);
  settings.kappa          = configMap.getFloat("hydro", "kappa", 0.0);

  settings.g_x            = configMap.getFloat("gravity", "g_x", 0.0);
  settings.g_y            = configMap.getFloat("gravity", "g_y", 0.0);
  settings.g_z            = configMap.getFloat("gravity", "g_z", 0.0);

  settings.rt_enabled     = configMap.getBool("hydro", "rt_enabled"  , false);
  settings.chem_enabled   = configMap.getBool("hydro", "chem_enabled", false);
  settings.kap1           = configMap.getFloat("hydro","kap1",1E-2);
  settings.kap2           = configMap.getFloat("hydro","kap2",1E-2);

  settings.rtM1_enabled	    = configMap.getBool("radiative_transfer", "rtM1_enabled"  , true);
  settings.implicit_enabled = static_cast<solverRT>(configMap.getInteger("radiative_transfer","implicit_enabled", IMPLICIT));
  settings.hydro_enabled  = configMap.getBool("radiative_transfer", "hydro_enabled"  , false);
  settings.ionization_enabled = configMap.getBool("radiative_transfer", "ionization_enabled"  , false);
  settings.thermal_driving_enabled = configMap.getBool("radiative_transfer", "thermal_driving_enabled"  , false);
  settings.bc_value       = configMap.getFloat("radiative_transfer", "bc_value", 0.0);
  settings.max_newton     = configMap.getInteger("radiative_transfer", "max_newton", 10);
  settings.tol_newton     = configMap.getFloat("radiative_transfer", "tol_newton", 0.000001);
  settings.useExactEigenvalues = configMap.getBool("radiative_transfer", "useExactEigenvalues", true);
  settings.valpmin        = configMap.getFloat("radiative_transfer", "valpmin", 0.0);
  settings.useAsymptoticCorrection = configMap.getBool("radiative_transfer", "useAsymptoticCorrection", true);
  settings.useP1 = configMap.getBool("radiative_transfer", "useP1", false);
  settings.wellBalancedScheme = configMap.getBool("radiative_transfer", "wellBalancedScheme", true);
  settings.restrictTimeStepRT = configMap.getBool("radiative_transfer", "restrictTimeStepRT", false);
  settings.vardt 	     = configMap.getFloat("radiative_transfer", "vardt", 1.0);
  settings.varrel_max	     = configMap.getFloat("radiative_transfer", "varrel_max", 1.0);
  settings.allTermsCoupling= configMap.getBool("radiative_transfer", "allTermsCoupling", false);
  settings.replaceEddingtonFactor = configMap.getBool("radiative_transfer", "replaceEddingtonFactor", false);
  settings.radiativeConstant = configMap.getFloat("radiative_transfer", "radiativeConstant", 4*ark_rt::code_units::constants::sigsb/ark_rt::code_units::constants::speedOfLight);

  settings.linearSolverParams = configMap.getString("linearAlgebra", "linearSolverParams", "inputs.yml");
  settings.outputLinearAlgebra = configMap.getBool("linearAlgebra", "outputLinearAlgebra", false);
  settings.linearAlgebraVerbose = configMap.getBool("linearAlgebra", "linearAlgebraVerbose", false);

  niter_riemann  = configMap.getInteger("hydro","niter_riemann", 10);
  std::string riemannSolverStr(configMap.getString("hydro","riemann", "approx"));
  if (riemannSolverStr == "approx")
  {
    riemannSolverType = RIEMANN_APPROX;
  }
  else if (riemannSolverStr == "llf")
  {
    riemannSolverType = RIEMANN_LLF;
  }
  else if (riemannSolverStr == "hll")
  {
    riemannSolverType = RIEMANN_HLL;
  }
  else if (riemannSolverStr == "hllc")
  {
    riemannSolverType = RIEMANN_HLLC;
  }
  else
  {
    ark_rt::abort("Riemann Solver \""+riemannSolverStr+"\" is invalid");
  }

  useAllRegimeTimeSteps = configMap.getBool("hydro", "useAllRegimeTimeSteps", false);

  implementationVersion = configMap.getInteger("other","implementationVersion", 0);
  if (implementationVersion != 0 && implementationVersion != 1)
  {
    ark_rt::abort("Implementation version is invalid (must be 0 or 1)\n"
        "Check your parameter file, section \"other\"");
  }

  init();

#ifdef USE_MPI
  setup_mpi(configMap);
#endif // USE_MPI
} // HydroParams::setup

#ifdef USE_MPI
// =======================================================
// =======================================================
void HydroParams::setup_mpi(ConfigMap& configMap)
{
  // runtime determination if we are using float ou double (for MPI communication)
  data_type = std::is_same<real_t, float>::value ?
    hydroSimu::MpiComm::FLOAT : hydroSimu::MpiComm::DOUBLE;

  // MPI parameters :
  mx = configMap.getInteger("mpi", "mx", 1);
  my = configMap.getInteger("mpi", "my", 1);
  mz = configMap.getInteger("mpi", "mz", 1);

  // check that parameters are consistent
  bool error = false;
  error |= (mx < 1);
  error |= (my < 1);
  error |= (mz < 1);

  // get world communicator size and check it is consistent with mesh grid sizes
  nProcs = MpiComm::world().getNProc();
  if (nProcs != mx*my*mz)
  {
    ark_rt::abort("Inconsistent MPI cartesian virtual topology geometry\n"
        "mx*my*mz must match with parameter given to mpirun !!!");
  }

  // create the MPI communicator for our cartesian mesh
  if (dimType == TWO_D)
  {
    communicator = new MpiCommCart(mx, my, MPI_CART_PERIODIC_TRUE, MPI_REORDER_TRUE);
    nDim = 2;
  }
  else
  {
    communicator = new MpiCommCart(mx, my, mz, MPI_CART_PERIODIC_TRUE, MPI_REORDER_TRUE);
    nDim = 3;
  }

  // get my MPI rank inside topology
  myRank = communicator->getRank();

  // get my coordinates inside topology
  // myMpiPos[0] is between 0 and mx-1
  // myMpiPos[1] is between 0 and my-1
  // myMpiPos[2] is between 0 and mz-1
  communicator->getMyCoords(myMpiPos.data());

  /*
   * compute MPI ranks of our neighbors and
   * set default boundary condition types
   */
  if (dimType == TWO_D)
  {
    nNeighbors = N_NEIGHBORS_2D;
    neighborsRank[X_MIN] = communicator->getNeighborRank<X_MIN>();
    neighborsRank[X_MAX] = communicator->getNeighborRank<X_MAX>();
    neighborsRank[Y_MIN] = communicator->getNeighborRank<Y_MIN>();
    neighborsRank[Y_MAX] = communicator->getNeighborRank<Y_MAX>();
    neighborsRank[Z_MIN] = 0;
    neighborsRank[Z_MAX] = 0;

    neighborsBC[X_MIN] = BC_COPY;
    neighborsBC[X_MAX] = BC_COPY;
    neighborsBC[Y_MIN] = BC_COPY;
    neighborsBC[Y_MAX] = BC_COPY;
    neighborsBC[Z_MIN] = BC_UNDEFINED;
    neighborsBC[Z_MAX] = BC_UNDEFINED;
  }
  else
  {
    nNeighbors = N_NEIGHBORS_3D;
    neighborsRank[X_MIN] = communicator->getNeighborRank<X_MIN>();
    neighborsRank[X_MAX] = communicator->getNeighborRank<X_MAX>();
    neighborsRank[Y_MIN] = communicator->getNeighborRank<Y_MIN>();
    neighborsRank[Y_MAX] = communicator->getNeighborRank<Y_MAX>();
    neighborsRank[Z_MIN] = communicator->getNeighborRank<Z_MIN>();
    neighborsRank[Z_MAX] = communicator->getNeighborRank<Z_MAX>();

    neighborsBC[X_MIN] = BC_COPY;
    neighborsBC[X_MAX] = BC_COPY;
    neighborsBC[Y_MIN] = BC_COPY;
    neighborsBC[Y_MAX] = BC_COPY;
    neighborsBC[Z_MIN] = BC_COPY;
    neighborsBC[Z_MAX] = BC_COPY;
  }

  /*
   * identify outside boundaries (no actual communication if we are
   * doing BC_DIRICHLET or BC_NEUMANN)
   *
   * Please notice the duality
   * XMIN -- boundary_xmax
   * XMAX -- boundary_xmin
   *
   */

  // X_MIN boundary
  if (myMpiPos[DIR_X] == 0)
  {
    neighborsBC[X_MIN] = boundary_type_xmin;
  }

  // X_MAX boundary
  if (myMpiPos[DIR_X] == mx-1)
  {
    neighborsBC[X_MAX] = boundary_type_xmax;
  }

  // Y_MIN boundary
  if (myMpiPos[DIR_Y] == 0)
  {
    neighborsBC[Y_MIN] = boundary_type_ymin;
  }

  // Y_MAX boundary
  if (myMpiPos[DIR_Y] == my-1)
  {
    neighborsBC[Y_MAX] = boundary_type_ymax;
  }

  if (dimType == THREE_D)
  {
    // Z_MIN boundary
    if (myMpiPos[DIR_Z] == 0)
    {
      neighborsBC[Z_MIN] = boundary_type_zmin;
    }

    // Z_MAX boundary
    if (myMpiPos[DIR_Z] == mz-1)
    {
      neighborsBC[Z_MAX] = boundary_type_zmax;
    }
  } // end THREE_D

  // fix space resolution :
  // need to take into account number of MPI process in each direction
  dx = (xmax - xmin)/(nx*mx);
  dy = (ymax - ymin)/(ny*my);
  dz = (zmax - zmin)/(nz*mz);

  // print information about current setup
  if (myRank == 0)
  {
    std::cout << "We are about to start simulation with the following characteristics\n";
    std::cout << "Global resolution : "
      << nx*mx << " x " << ny*my << " x " << nz*mz << "\n";
    std::cout << "Local  resolution : "
      << nx << " x " << ny << " x " << nz << "\n";
    std::cout << "MPI Cartesian topology : " << mx << "x" << my << "x" << mz << std::endl;
  }
} // HydroParams::setup_mpi

#endif // USE_MPI

// =======================================================
// =======================================================
void HydroParams::init()
{
  // set other parameters
  imin = 0;
  jmin = 0;
  kmin = 0;

  imax = nx - 1 + 2*ghostWidth;
  jmax = ny - 1 + 2*ghostWidth;
  kmax = nz - 1 + 2*ghostWidth;

  isize = imax - imin + 1;
  jsize = jmax - jmin + 1;
  ksize = kmax - kmin + 1;

  dx = (xmax - xmin) / nx;
  dy = (ymax - ymin) / ny;
  dz = (zmax - zmin) / nz;

  settings.smallp  = settings.smallc*settings.smallc/settings.gamma0;
  settings.smallpp = settings.smallr*settings.smallp;
  settings.gamma6  = (settings.gamma0 + ONE_F)/(TWO_F * settings.gamma0);
  settings.Rstar   = ark_rt::code_units::constants::Rstar_h / settings.mmw;
  settings.cp      = settings.gamma0 / (settings.gamma0-ONE_F) * settings.Rstar;
  settings.cv      = ONE_F / (settings.gamma0-ONE_F) * settings.Rstar;
  settings.speedOfLight = ark_rt::code_units::constants::speedOfLight;
} // HydroParams::init


// =======================================================
// =======================================================
void HydroParams::print()
{
  printf( "##########################\n");
  printf( "Simulation run parameters:\n");
  printf( "##########################\n");
  if (solver_name=="Hydro_Muscl_2D" || solver_name=="Hydro_Muscl_3D")
  {
    printf( "riemann       : %d\n", riemannSolverType);
    printf( "niter_riemann : %d\n", niter_riemann);
    printf( "iorder        : %d\n", settings.iorder);
    printf( "slope_type    : %f\n", settings.slope_type);
    printf( "implementation version : %d\n",implementationVersion);
  }
  if (solver_name=="Hydro_All_Regime_2D" || solver_name=="Hydro_All_Regime_3D")
  {
    printf( "low Mach correction: %s\n", settings.low_mach_correction ? "Enabled" : "Disabled");
    printf( "conservative scheme: %s\n", settings.conservative ? "Enabled" : "Disabled");
  }
  printf( "nx         : %d\n", nx);
  printf( "ny         : %d\n", ny);
  printf( "nz         : %d\n", nz);

  printf( "dx         : %f\n", dx);
  printf( "dy         : %f\n", dy);
  printf( "dz         : %f\n", dz);

  printf( "imin       : %d\n", imin);
  printf( "imax       : %d\n", imax);

  printf( "jmin       : %d\n", jmin);
  printf( "jmax       : %d\n", jmax);

  printf( "kmin       : %d\n", kmin);
  printf( "kmax       : %d\n", kmax);

  printf( "ghostWidth : %d\n", ghostWidth);
  printf( "nbvar      : %d\n", nbvar);
  printf( "nStepmax   : %d\n", nStepmax);
  printf( "tEnd       : %f\n", tEnd);
  printf( "nOutput    : %d\n", nOutput);
  printf( "cfl_hydro  : %f\n", settings.cfl_hydro);
  printf( "cfl_rad    : %f\n", settings.cfl_rad);
  printf( "smallr     : %12.10f\n", settings.smallr);
  printf( "smallc     : %12.10f\n", settings.smallc);
  printf( "smallp     : %12.10f\n", settings.smallp);
  printf( "smallpp    : %g\n", settings.smallpp);
  printf( "gamma0     : %f\n", settings.gamma0);
  printf( "gamma6     : %f\n", settings.gamma6);
  printf( "Rstar (specific gas constant) : %g\n", settings.Rstar);
  printf( "cp (specific heat)            : %f\n", settings.cp);
  printf( "cv (specific heat)            : %e\n", settings.cv);
  printf( "c (speed of light)            : %f\n", settings.speedOfLight);
  printf( "a (radiative consant )        : %g\n", settings.radiativeConstant);
  printf( "mu (dynamic visosity)         : %f\n", settings.mu);
  printf( "kappa (thermal diffusivity)   : %f\n", settings.kappa);
  printf( "g_x        : %f\n", settings.g_x);
  printf( "g_y        : %f\n", settings.g_y);
  printf( "g_z        : %f\n", settings.g_z);
  printf( "##########################\n");
} // HydroParams::print
