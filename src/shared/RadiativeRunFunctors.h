// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "all_regime/HydroBaseFunctor2D.h"
#include "all_regime/HydroBaseFunctor3D.h"
#include "shared/RadiativeConvectionParams.h"
#include "ark_rt.h"
#include "shared/Profile.h"

namespace ark_rt {

namespace chemistry {

class ComputeReactionRateStepFunctor2D : all_regime::HydroBaseFunctor2D
{
public:
    ComputeReactionRateStepFunctor2D(HydroParams params_,
                                     RadiativeConvectionParams rc_params_,
                                     DataArray Udata_,
                                     DataArray Qdata_,
                                     real_t dt_)
        : HydroBaseFunctor2D(params_), rc_params(rc_params_)
        , Udata(Udata_), Qdata(Qdata_)
        , profile(rc_params_.input_file)
        , dt(dt_)
    {
    };

    static void apply(HydroParams params,  RadiativeConvectionParams rc_params,
                      DataArray2d Udata, DataArray2d Qdata, real_t dt,int nbCells)
    {
        ComputeReactionRateStepFunctor2D functor(params, rc_params, Udata, Qdata, dt);
        Kokkos::parallel_for(nbCells, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(int index) const
    {

        const int    ghostWidth = params.ghostWidth;
        const real_t tau        = rc_params.tauchem;

        int i, j;
        index2coord(index, i, j, params.isize, params.jsize);

        if (j>=ghostWidth && j<=params.jmax-ghostWidth &&
            i>=ghostWidth && i<=params.imax-ghostWidth)
        {

            HydroState uLoc = getHydroState(Udata, i, j);

            const real_t rhoxeq  = uLoc[ID]*profile.d_data(j-ghostWidth, 2);
            const real_t rhoxnp1 = (rhoxeq*dt/tau+uLoc[IS])/(ONE_F+dt/tau);

            uLoc[IS] = rhoxnp1;
            setHydroState(Udata, uLoc, i, j);

        }
    }

    RadiativeConvectionParams rc_params;
    DataArray Udata;
    DataArray Qdata;
    Profile profile;
    real_t dt;
};

class ComputeReactionRateStepFunctor3D : all_regime::HydroBaseFunctor3D
{
public:
    ComputeReactionRateStepFunctor3D(HydroParams params_,
                                     RadiativeConvectionParams rc_params_,
                                     DataArray Udata_,
                                     DataArray Qdata_,
                                     real_t dt_)
        : HydroBaseFunctor3D(params_), rc_params(rc_params_)
        , Udata(Udata_), Qdata(Qdata_)
        , profile(rc_params_.input_file)
        , dt(dt_)
    {
    };

    static void apply(HydroParams params,  RadiativeConvectionParams rc_params,
                      DataArray3d Udata, DataArray3d Qdata, real_t dt,int nbCells)
    {
        ComputeReactionRateStepFunctor3D functor(params, rc_params, Udata, Qdata, dt);
        Kokkos::parallel_for(nbCells, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(int index) const
    {

        const int    ghostWidth = params.ghostWidth;
        const real_t tau        = rc_params.tauchem;

        int i, j, k;
        index2coord(index, i, j, k, params.isize, params.jsize, params.ksize);

        if (k>=ghostWidth-1 && k<=params.kmax-ghostWidth+1 &&
            j>=ghostWidth-1 && j<=params.jmax-ghostWidth+1 &&
            i>=ghostWidth-1 && i<=params.imax-ghostWidth+1)
        {

            HydroState uLoc = getHydroState(Udata, i, j, k);

            const real_t rhoxeq  = uLoc[ID]*profile.d_data(j-ghostWidth, 2);
            const real_t rhoxnp1 = (rhoxeq*dt/tau+uLoc[IS])/(ONE_F+dt/tau);

            uLoc[IS] = rhoxnp1;
            setHydroState(Udata, uLoc, i, j, k);

        }
    }

    RadiativeConvectionParams rc_params;
    DataArray Udata;
    DataArray Qdata;
    Profile profile;
    real_t dt;
};
}

namespace radiative_transfer
{

class ComputeRadiativeTransferStepFunctor2D : all_regime::HydroBaseFunctor2D
{
public:
    ComputeRadiativeTransferStepFunctor2D(HydroParams params_,
                                          RadiativeConvectionParams rc_params_,
                                          DataArray2d Udata_, DataArray2d Qdata_, real_t dt_)
        : HydroBaseFunctor2D(params_), rc_params(rc_params_)
        , Udata(Udata_), Qdata(Qdata_)
        , dt(dt_)
    {
    };

    static void apply(HydroParams params,  RadiativeConvectionParams rc_params,
                      DataArray2d Udata, DataArray2d Qdata, real_t dt, int isize)
    {
        ComputeRadiativeTransferStepFunctor2D functor(params, rc_params, Udata, Qdata, dt);
        Kokkos::parallel_for(isize, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int index) const
    {
        // radiative transfer has to be done by column intra-node parallelisation is only done in the x direction
        const int i = index;

        const int jmin  = params.jmin;
        const int jmax  = params.jmax;
        const int ghostWidth = params.ghostWidth;

        const real_t dy   = params.dy;

        const real_t sigsb = code_units::constants::sigsb;
        const real_t Trad_ymin  = rc_params.Trad_ymin;
        const real_t Trad_ymax  = rc_params.Trad_ymax;
        const real_t mubar = rc_params.mubar;

        const real_t Pi    = std::acos(-ONE_F);

        // downward flux
        real_t Idown = ZERO_F;

        {
            const int j = jmax-ghostWidth;
            const HydroState qi     = getHydroState(Qdata, i, j);
            const real_t     Ti     = computeTemperature(qi);
            const real_t     Bi     = sigsb*Ti*Ti*Ti*Ti*HALF_F/mubar;
            const real_t     kapi   = computeOpacity(qi);

            Idown = sigsb*Trad_ymax*Trad_ymax*Trad_ymax*Trad_ymax;
            Udata(i, j, IP) += dt*4.*Pi * qi[ID]*kapi*HALF_F*(Idown-Bi);
        }

        for (int j=jmax-ghostWidth-1; j>jmin+ghostWidth; --j)
        {
            const HydroState qim1   = getHydroState(Qdata, i, j+1);
            const HydroState qi     = getHydroState(Qdata, i, j);
            const HydroState qip1   = getHydroState(Qdata, i, j-1);

            const real_t     Tip1   = computeTemperature(qip1);
            const real_t     Ti     = computeTemperature(qi);
            const real_t     Tim1   = computeTemperature(qim1);

            const real_t     Bip1   = sigsb*Tip1*Tip1*Tip1*Tip1*HALF_F/mubar;
            const real_t     Bi     = sigsb*Ti*Ti*Ti*Ti*HALF_F/mubar;
            const real_t     Bim1   = sigsb*Tim1*Tim1*Tim1*Tim1*HALF_F/mubar;

            const real_t     kapip1 = computeOpacity(qip1);
            const real_t     kapi   = computeOpacity(qi);
            const real_t     kapim1 = computeOpacity(qim1);

            const real_t     dtaum  = HALF_F*(qi[ID]*kapi+qim1[ID]*kapim1) * dy / mubar;
            const real_t     dtaup  = HALF_F*(qi[ID]*kapi+qip1[ID]*kapip1) * dy / mubar;

            const real_t expmtau = std::exp(-dtaum);

            const Kokkos::Array<real_t, 3> psi = compute_integral_constant(expmtau, dtaum, dtaup);

            Idown = Idown * expmtau + psi[0]*Bim1 + psi[1]*Bi + psi[2]*Bip1;
            Udata(i, j, IP) += dt*4.*Pi * qi[ID]*kapi*HALF_F*(Idown-Bi);
        }

        {
            const int j = jmin+ghostWidth;
            const HydroState qim1   = getHydroState(Qdata, i, j+1);
            const HydroState qi     = getHydroState(Qdata, i, j);

            const real_t     Ti     = computeTemperature(qi);
            const real_t     Tim1   = computeTemperature(qim1);

            const real_t     Bi     = sigsb*Ti*Ti*Ti*Ti*HALF_F/mubar;
            const real_t     Bim1   = sigsb*Tim1*Tim1*Tim1*Tim1*HALF_F/mubar;

            const real_t     kapi   = computeOpacity(qi);
            const real_t     kapim1 = computeOpacity(qim1);

            const real_t     dtaum  = HALF_F*(qi[ID]*kapi+qim1[ID]*kapim1) * dy / mubar;
            const real_t     dtaup  = ZERO_F;

            const real_t expmtau = std::exp(-dtaum);

            const Kokkos::Array<real_t, 3> psi = compute_integral_constant(expmtau, dtaum, dtaup);

            Idown = Idown * expmtau + psi[0]*Bim1 + psi[1]*Bi;
            Udata(i, j, IP) += dt*4.*Pi * qi[ID]*kapi*HALF_F*(Idown-Bi);
        }

        // upward flux
        real_t Iup = ZERO_F;

        {
            const int j = jmin+ghostWidth;
            const HydroState qi     = getHydroState(Qdata, i, j);
            const real_t     Ti     = computeTemperature(qi);
            const real_t     kapi   = computeOpacity(qi);
            const real_t     Bi     = sigsb*Ti*Ti*Ti*Ti*HALF_F/mubar;

            Iup = Idown + sigsb * Trad_ymin*Trad_ymin*Trad_ymin*Trad_ymin;
            Udata(i, j, IP) += dt*4.*Pi * qi[ID]*kapi*HALF_F*(Iup-Bi);
        }

        for (int j=jmin+ghostWidth+1; j<jmax-ghostWidth; ++j)
        {
            const HydroState qim1   = getHydroState(Qdata, i, j-1);
            const HydroState qi     = getHydroState(Qdata, i, j);
            const HydroState qip1   = getHydroState(Qdata, i, j+1);

            const real_t     Tip1   = computeTemperature(qip1);
            const real_t     Ti     = computeTemperature(qi);
            const real_t     Tim1   = computeTemperature(qim1);

            const real_t     kapip1 = computeOpacity(qip1);
            const real_t     kapi   = computeOpacity(qi);
            const real_t     kapim1 = computeOpacity(qim1);

            const real_t     Bip1   = sigsb*Tip1*Tip1*Tip1*Tip1*HALF_F/mubar;
            const real_t     Bi     = sigsb*Ti*Ti*Ti*Ti*HALF_F/mubar;
            const real_t     Bim1   = sigsb*Tim1*Tim1*Tim1*Tim1*HALF_F/mubar;

            const real_t     dtaum  = HALF_F*(qi[ID]*kapi+qim1[ID]*kapim1) * dy / mubar;
            const real_t     dtaup  = HALF_F*(qi[ID]*kapi+qip1[ID]*kapip1) * dy / mubar;

            const real_t expmtau = std::exp(-dtaum);

            const Kokkos::Array<double, 3> psi = compute_integral_constant(expmtau, dtaum, dtaup);

            Iup = Iup * expmtau + psi[0]*Bim1 + psi[1]*Bi + psi[2]*Bip1;
            Udata(i, j, IP) += dt*4.*Pi * qi[ID]*kapi*HALF_F*(Iup-Bi);
        }

        {
            const int j = jmax-ghostWidth;
            const HydroState qim1   = getHydroState(Qdata, i, j-1);
            const HydroState qi     = getHydroState(Qdata, i, j);

            const real_t     Ti     = computeTemperature(qi);
            const real_t     Tim1   = computeTemperature(qim1);

            const real_t     kapi   = computeOpacity(qi);
            const real_t     kapim1 = computeOpacity(qim1);

            const real_t     Bi     = sigsb*Ti*Ti*Ti*Ti*HALF_F/mubar;
            const real_t     Bim1   = sigsb*Tim1*Tim1*Tim1*Tim1*HALF_F/mubar;

            const real_t     dtaum  = HALF_F*(qi[ID]*kapi+qim1[ID]*kapim1) * dy / mubar;
            const real_t     dtaup  = ZERO_F;

            const real_t expmtau = std::exp(-dtaum);

            const Kokkos::Array<real_t, 3> psi = compute_integral_constant(expmtau,dtaum,dtaup);

            Iup = Iup * expmtau + psi[0]*Bim1 + psi[1]*Bi;
            Udata(i, j, IP) += dt*4.*Pi * qi[ID]*kapi*HALF_F*(Iup-Bi);

        }
    }

    KOKKOS_INLINE_FUNCTION
    Kokkos::Array<double, 3> compute_integral_constant(real_t expmtau, real_t dtaum, real_t dtaup) const
    {
        const real_t U0 = (dtaum<1.0E-9 ? dtaum : ONE_F - expmtau);
        const real_t U1 = (dtaum<1.0E-6 ? HALF_F * dtaum * dtaum : dtaum - U0);
        const real_t U2 = (dtaum<1.0E-3 ? 1./3. * dtaum * dtaum * dtaum : dtaum * dtaum - TWO_F * U1);

        Kokkos::Array<double, 3> psi;
        if (dtaum == ZERO_F)
        {
            psi[0] = ZERO_F;
            psi[1] = ZERO_F;
            psi[2] = ZERO_F;
        }
        else if (dtaup == ZERO_F)
        {
            psi[0] = U0 - U1/dtaum;
            psi[1] = U0 - psi[0];
            psi[2] = ZERO_F;
        }
        else if (U2 - dtaum * U1 < ZERO_F)
        {
            psi[0] = U0 - U1/dtaum;
            psi[1] = U0 - psi[0];
            psi[2] = ZERO_F;
        }
        else
        {
            psi[0] = U0 + (U2-(TWO_F*dtaum+dtaup)*U1)/(dtaum*(dtaum+dtaup));
            psi[1] = ((dtaup+dtaum)*U1-U2)/(dtaum*dtaup);
            psi[2] = (U2-dtaum*U1)/(dtaup*(dtaum+dtaup));
        }

        return psi;
    }

    RadiativeConvectionParams rc_params;
    DataArray2d Udata;
    DataArray2d Qdata;
    real_t dt;
};


class ComputeRadiativeTransferStepFunctor3D : all_regime::HydroBaseFunctor3D
{
public:
    ComputeRadiativeTransferStepFunctor3D(HydroParams params_, RadiativeConvectionParams rc_params_,
                                          DataArray3d Udata_, DataArray3d Qdata_, real_t dt_)
        : HydroBaseFunctor3D(params_), rc_params(rc_params_)
        , Udata(Udata_), Qdata(Qdata_)
        , dt(dt_)
    {
    };

    static void apply(HydroParams params,  RadiativeConvectionParams rc_params,
                      DataArray3d Udata, DataArray3d Qdata, real_t dt, int ijsize)
    {
        abort("Kernel not ready");
        ComputeRadiativeTransferStepFunctor3D functor(params, rc_params, Udata, Qdata, dt);
        Kokkos::parallel_for(ijsize, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int index) const
    {
        // radiative transfer has to be done by column intra-node parallelisation is only done in the x direction
        int i, j;
        index2coord(index, i, j, params.isize, params.jsize);

        const int kmin  = params.kmin;
        const int kmax  = params.kmax;
        const int ghostWidth = params.ghostWidth;

        const real_t dy   = params.dy;

        const real_t sigsb = code_units::constants::sigsb;
        const real_t Trad_ymin  = rc_params.Trad_ymin;
        const real_t Trad_ymax  = rc_params.Trad_ymax;
        const real_t mubar = rc_params.mubar;

        const real_t Pi    = std::acos(-ONE_F);

        // downward flux
        real_t Idown = ZERO_F;

        {
            const int k = kmax-ghostWidth;
            const HydroState qi     = getHydroState(Qdata, i, j, k);
            const real_t     Ti     = computeTemperature(qi);
            const real_t     Bi     = sigsb*Ti*Ti*Ti*Ti*HALF_F/mubar;
            const real_t     kapi   = computeOpacity(qi);

            Idown = sigsb*Trad_ymax*Trad_ymax*Trad_ymax*Trad_ymax;
            Udata(i, j, k, IP) += dt*4.*Pi * qi[ID]*kapi*HALF_F*(Idown-Bi);
        }

        for (int k=kmax-ghostWidth-1; k>kmin+ghostWidth; --k)
        {
            const HydroState qim1   = getHydroState(Qdata, i, j, k+1);
            const HydroState qi     = getHydroState(Qdata, i, j, k);
            const HydroState qip1   = getHydroState(Qdata, i, j, k-1);

            const real_t     Tip1   = computeTemperature(qip1);
            const real_t     Ti     = computeTemperature(qi);
            const real_t     Tim1   = computeTemperature(qim1);

            const real_t     Bip1   = sigsb*Tip1*Tip1*Tip1*Tip1*HALF_F/mubar;
            const real_t     Bi     = sigsb*Ti*Ti*Ti*Ti*HALF_F/mubar;
            const real_t     Bim1   = sigsb*Tim1*Tim1*Tim1*Tim1*HALF_F/mubar;

            const real_t     kapip1 = computeOpacity(qip1);
            const real_t     kapi   = computeOpacity(qi);
            const real_t     kapim1 = computeOpacity(qim1);

            const real_t     dtaum  = HALF_F*(qi[ID]*kapi+qim1[ID]*kapim1) * dy / mubar;
            const real_t     dtaup  = HALF_F*(qi[ID]*kapi+qip1[ID]*kapip1) * dy / mubar;

            const real_t expmtau = std::exp(-dtaum);

            const Kokkos::Array<real_t, 3> psi = compute_integral_constant(expmtau, dtaum, dtaup);

            Idown = Idown * expmtau + psi[0]*Bim1 + psi[1]*Bi + psi[2]*Bip1;
            Udata(i, j, k, IP) += dt*4.*Pi * qi[ID]*kapi*HALF_F*(Idown-Bi);
        }

        {
            const int k = kmin+ghostWidth;
            const HydroState qim1   = getHydroState(Qdata, i, j, k+1);
            const HydroState qi     = getHydroState(Qdata, i, j, k);

            const real_t     Ti     = computeTemperature(qi);
            const real_t     Tim1   = computeTemperature(qim1);

            const real_t     Bi     = sigsb*Ti*Ti*Ti*Ti*HALF_F/mubar;
            const real_t     Bim1   = sigsb*Tim1*Tim1*Tim1*Tim1*HALF_F/mubar;

            const real_t     kapi   = computeOpacity(qi);
            const real_t     kapim1 = computeOpacity(qim1);

            const real_t     dtaum  = HALF_F*(qi[ID]*kapi+qim1[ID]*kapim1) * dy / mubar;
            const real_t     dtaup  = ZERO_F;

            const real_t expmtau = std::exp(-dtaum);

            const Kokkos::Array<real_t, 3> psi = compute_integral_constant(expmtau, dtaum, dtaup);

            Idown = Idown * expmtau + psi[0]*Bim1 + psi[1]*Bi;
            Udata(i, j, k, IP) += dt*4.*Pi * qi[ID]*kapi*HALF_F*(Idown-Bi);
        }

        // upward flux
        real_t Iup = ZERO_F;

        {
            const int k = kmin+ghostWidth;
            const HydroState qi     = getHydroState(Qdata, i, j, k);
            const real_t     Ti     = computeTemperature(qi);
            const real_t     kapi   = computeOpacity(qi);
            const real_t     Bi     = sigsb*Ti*Ti*Ti*Ti*HALF_F/mubar;

            Iup = Idown + sigsb * Trad_ymin*Trad_ymin*Trad_ymin*Trad_ymin;
            Udata(i, j, k, IP) += dt*4.*Pi * qi[ID]*kapi*HALF_F*(Iup-Bi);
        }

        for (int k=kmin+ghostWidth+1; k<kmax-ghostWidth; ++k)
        {
            const HydroState qim1   = getHydroState(Qdata, i, j, k-1);
            const HydroState qi     = getHydroState(Qdata, i, j, k);
            const HydroState qip1   = getHydroState(Qdata, i, j, k+1);

            const real_t     Tip1   = computeTemperature(qip1);
            const real_t     Ti     = computeTemperature(qi);
            const real_t     Tim1   = computeTemperature(qim1);

            const real_t     kapip1 = computeOpacity(qip1);
            const real_t     kapi   = computeOpacity(qi);
            const real_t     kapim1 = computeOpacity(qim1);

            const real_t     Bip1   = sigsb*Tip1*Tip1*Tip1*Tip1*HALF_F/mubar;
            const real_t     Bi     = sigsb*Ti*Ti*Ti*Ti*HALF_F/mubar;
            const real_t     Bim1   = sigsb*Tim1*Tim1*Tim1*Tim1*HALF_F/mubar;

            const real_t     dtaum  = HALF_F*(qi[ID]*kapi+qim1[ID]*kapim1) * dy / mubar;
            const real_t     dtaup  = HALF_F*(qi[ID]*kapi+qip1[ID]*kapip1) * dy / mubar;

            const real_t expmtau = std::exp(-dtaum);

            const Kokkos::Array<double, 3> psi = compute_integral_constant(expmtau, dtaum, dtaup);

            Iup = Iup * expmtau + psi[0]*Bim1 + psi[1]*Bi + psi[2]*Bip1;
            Udata(i, j, k, IP) += dt*4.*Pi * qi[ID]*kapi*HALF_F*(Iup-Bi);
        }

        {
            const int k = kmax-ghostWidth;
            const HydroState qim1   = getHydroState(Qdata, i, j, k-1);
            const HydroState qi     = getHydroState(Qdata, i, j, k);

            const real_t     Ti     = computeTemperature(qi);
            const real_t     Tim1   = computeTemperature(qim1);

            const real_t     kapi   = computeOpacity(qi);
            const real_t     kapim1 = computeOpacity(qim1);

            const real_t     Bi     = sigsb*Ti*Ti*Ti*Ti*HALF_F/mubar;
            const real_t     Bim1   = sigsb*Tim1*Tim1*Tim1*Tim1*HALF_F/mubar;

            const real_t     dtaum  = HALF_F*(qi[ID]*kapi+qim1[ID]*kapim1) * dy / mubar;
            const real_t     dtaup  = ZERO_F;

            const real_t expmtau = std::exp(-dtaum);

            const Kokkos::Array<real_t, 3> psi = compute_integral_constant(expmtau,dtaum,dtaup);

            Iup = Iup * expmtau + psi[0]*Bim1 + psi[1]*Bi;
            Udata(i, j, k, IP) += dt*4.*Pi * qi[ID]*kapi*HALF_F*(Iup-Bi);
        }
    }

    KOKKOS_INLINE_FUNCTION
    Kokkos::Array<double, 3> compute_integral_constant(real_t expmtau, real_t dtaum, real_t dtaup) const
    {
        const real_t U0 = (dtaum<1.0E-9 ? dtaum : ONE_F - expmtau);
        const real_t U1 = (dtaum<1.0E-6 ? HALF_F * dtaum * dtaum : dtaum - U0);
        const real_t U2 = (dtaum<1.0E-3 ? 1./3. * dtaum * dtaum * dtaum : dtaum * dtaum - TWO_F * U1);

        Kokkos::Array<double, 3> psi;
        if (dtaum == ZERO_F)
        {
            psi[0] = ZERO_F;
            psi[1] = ZERO_F;
            psi[2] = ZERO_F;
        }
        else if (dtaup == ZERO_F)
        {
            psi[0] = U0 - U1/dtaum;
            psi[1] = U0 - psi[0];
            psi[2] = ZERO_F;
        }
        else if (U2 - dtaum * U1 < ZERO_F)
        {
            psi[0] = U0 - U1/dtaum;
            psi[1] = U0 - psi[0];
            psi[2] = ZERO_F;
        }
        else
        {
            psi[0] = U0 + (U2-(TWO_F*dtaum+dtaup)*U1)/(dtaum*(dtaum+dtaup));
            psi[1] = ((dtaup+dtaum)*U1-U2)/(dtaum*dtaup);
            psi[2] = (U2-dtaum*U1)/(dtaup*(dtaum+dtaup));
        }

        return psi;
    }

    RadiativeConvectionParams rc_params;
    DataArray3d Udata;
    DataArray3d Qdata;
    real_t dt;
};

} // namespace radiative_transfer

} // namespace ark_rt
