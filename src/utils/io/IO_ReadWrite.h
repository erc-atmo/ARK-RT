// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#ifndef IO_READWRITE_H_
#define IO_READWRITE_H_

#include <map>
#include <string>
#include <vector>

#include <shared/kokkos_shared.h>
//class HydroParams;
//class ConfigMap;
#include <shared/HydroParams.h>
#include <utils/config/ConfigMap.h>

#include "IO_ReadWriteBase.h"

namespace ark_rt { namespace io
{

/**
 *
 */
class IO_ReadWrite : public IO_ReadWriteBase
{
public:
    IO_ReadWrite(HydroParams& params,
                 ConfigMap& configMap,
                 std::map<int, std::string>& variables_names);

    //! destructor
    virtual ~IO_ReadWrite() {};

    //! hydro parameters
    HydroParams& params;

    //! configuration file reader
    ConfigMap& configMap;

    //! override base class method
    virtual void save_data(DataArray2d             Udata,
                           DataArray2d::HostMirror Uhost,
                           int iOutput,
                           int iStep,
                           real_t time,
                           std::string debug_name);

    //! override base class method
    virtual void save_data(DataArray3d             Udata,
                           DataArray3d::HostMirror Uhost,
                           int iOutput,
                           int iStep,
                           real_t time,
                           std::string debug_name);

    //! public interface to save data.
    virtual void save_data_impl(DataArray2d             Udata,
                                DataArray2d::HostMirror Uhost,
                                int iOutput,
                                int iStep,
                                real_t time,
                                std::string debug_name);

    virtual void save_data_impl(DataArray3d             Udata,
                                DataArray3d::HostMirror Uhost,
                                int iOutput,
                                int iStep,
                                real_t time,
                                std::string debug_name);

    //! override base class method
    virtual void load_data(DataArray2d             Udata,
                           DataArray2d::HostMirror Uhost,
                           int& iOutput,
                           int& iStep,
                           real_t& time);

    //! override base class method
    virtual void load_data(DataArray3d             Udata,
                           DataArray3d::HostMirror Uhost,
                           int& iOutput,
                           int& iStep,
                           real_t& time);

    //! public interface to load data.
    virtual void load_data_impl(DataArray2d             Udata,
                                DataArray2d::HostMirror Uhost,
                                int& iOutput,
                                int& iStep,
                                real_t& time);

    virtual void load_data_impl(DataArray3d             Udata,
                                DataArray3d::HostMirror Uhost,
                                int& iOutput,
                                int& iStep,
                                real_t& time);

    std::vector<real_t> saved_times;

    //! names of variables to save (inherited from Solver)
    std::map<int, std::string>& variables_names;

    bool restart_enabled;

    bool vtk_enabled;
    bool hdf5_enabled;
    bool pnetcdf_enabled;
}; // class IO_ReadWrite

} // namespace io

} // namespace ark_rt

#endif // IO_READWRITE_H_
