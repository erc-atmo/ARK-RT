// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#ifndef IO_HDF5_H_
#define IO_HDF5_H_

#include <cstring>
#include <hdf5.h>
#include <iostream>
#include <map>
#include <string>
#include <type_traits>
#include <vector>

#include "ark_rt.h"
#include "ark_rt_version.h"
#include "shared/kokkos_shared.h"
#include "shared/HydroParams.h"
#include "shared/units.h"
#include "shared/utils.h"
#include "utils/config/ConfigMap.h"
#ifdef USE_MPI
#include "utils/mpiUtils/MpiComm.h"
#endif // USE_MPI

namespace ark_rt { namespace io
{

inline void hdf5_mesg(const std::string& mesg)
{
    std::cerr << "HDF5 :" << mesg << std::endl;
}

inline void hdf5_check(herr_t status, const std::string& mesg)
{
    if (status<0)
    {
        std::cerr << "*** HDF5 ERROR ***\n";
        std::cerr << "    hdf5_check (" << mesg << ") failed with status : "
                  << status << "\n";
    }
}

// =======================================================
// =======================================================
/**
 * Write a wrapper file using the Xmdf file format (XML) to allow
 * Paraview/Visit to read these h5 files as a time series.
 *
 * \param[in] params a HydroParams struct (to retrieve geometry).
 * \param[in] totalNumberOfSteps The number of time steps computed.
 * \param[in] singleStep boolean; if true we only write header for
 *  the last step.
 * \param[in] ghostIncluded boolean; if true include ghost cells
 *
 * If library HDF5 is not available, do nothing.
 */
void writeXdmfForHdf5Wrapper(HydroParams& params,
                             ConfigMap& configMap,
			     int nbvar,
                             std::map<int, std::string>& variables_names,
                             const std::vector<real_t>& saved_times);

// =======================================================
// =======================================================
/**
 *
 */
template<DimensionType d>
class Save_HDF5
{
public:
    //! Decide at compile-time which data array type to use
    using DataArray  = typename std::conditional<d==TWO_D,DataArray2d,DataArray3d>::type;
    using DataArrayHost  = typename std::conditional<d==TWO_D,DataArray2dHost,DataArray3dHost>::type;

    Save_HDF5(DataArray     Udata,
              DataArrayHost Uhost,
              HydroParams& params,
              ConfigMap& configMap,
              int nbvar,
              const std::map<int, std::string>& variables_names,
              int iOutput,
              int iStep,
              real_t totalTime,
              std::string debug_name) :
        Udata(Udata), Uhost(Uhost), params(params), configMap(configMap),
        nbvar(nbvar), variables_names(variables_names),
        iOutput(iOutput), iStep(iStep), totalTime(totalTime), debug_name(debug_name)
    {};
    ~Save_HDF5() {};

    template<DimensionType d_ = d>
    void copy_buffer(typename std::enable_if<d_==TWO_D, real_t>::type *& data,
                     int isize, int jsize, int ksize, int nvar, KokkosLayout layout)
    {
        if (layout == KOKKOS_LAYOUT_RIGHT) { // transpose array to make data contiguous in memory
            for (int j=0; j<jsize; ++j) {
                for (int i=0; i<isize; ++i) {
                    int index = i+isize*j;
                    data[index]=Uhost(i,j,nvar);
                }
            }
        } else {
            data = Uhost.data() + isize*jsize*nvar;
        }

    } // copy_buffer

    template<DimensionType d_=d>
    void copy_buffer(typename std::enable_if<d_==THREE_D, real_t>::type *& data,
                     int isize, int jsize, int ksize, int nvar, KokkosLayout layout)
    {
        if (layout == KOKKOS_LAYOUT_RIGHT) { // transpose array to make data contiguous in memory
            for (int k=0; k<ksize; ++k) {
                for (int j=0; j<jsize; ++j) {
                    for (int i=0; i<isize; ++i) {
                        int index = i+isize*j+isize*jsize*k;
                        data[index]=Uhost(i,j,k,nvar);
                    }
                }
            }

        } else {
            data = Uhost.data() + isize*jsize*ksize*nvar;
        }

    } // copy_buffer / 3D

    herr_t write_field(int varId, real_t* &data, hid_t& file_id,
                       hid_t& dataspace_memory,
                       hid_t& dataspace_file, hid_t& propList_create_id,
                       KokkosLayout& layout)
    {
        hid_t dataType = std::is_same<real_t, float>::value ? H5T_NATIVE_FLOAT : H5T_NATIVE_DOUBLE;
        const int isize = params.isize;
        const int jsize = params.jsize;
        const int ksize = params.ksize;

        const std::string varName = "/" + variables_names.at(varId);
        hid_t dataset_id = H5Dcreate2(file_id, varName.c_str(),
                                      dataType, dataspace_file,
                                      H5P_DEFAULT, propList_create_id, H5P_DEFAULT);
        copy_buffer(data, isize, jsize, ksize, varId, layout);
        herr_t status = H5Dwrite(dataset_id, dataType,
                                 dataspace_memory, dataspace_file,
                                 H5P_DEFAULT, data);
        H5Dclose(dataset_id);

        return status;
    } // write_field

    void write_scalar(double scalar, const std::string& name, const hid_t& file_id)
    {
        hid_t space = H5Screate(H5S_SCALAR);
        hid_t dset  = H5Dcreate2(file_id, name.c_str(), H5T_NATIVE_DOUBLE, space,
                                 H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        H5Dwrite(dset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &scalar);
        H5Sclose(space);
        H5Dclose(dset);
    }

    void write_vector(const std::vector<double>& vector, const std::string& name, const hid_t& file_id)
    {
        int rank = 1;
        hsize_t dims = vector.size();
        hid_t space = H5Screate_simple(rank, &dims, NULL);
        hid_t dset  = H5Dcreate2(file_id, name.c_str(), H5T_NATIVE_DOUBLE, space,
                                 H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        H5Dwrite(dset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, vector.data());
        H5Sclose(space);
        H5Dclose(dset);
    }

    void write_attribute(int data, const std::string& name, const hid_t& file_id)
    {
        // write time step as an attribute to root group
        hid_t ds_id = H5Screate(H5S_SCALAR);
        hid_t attr_id = H5Acreate2(file_id, name.c_str(), H5T_NATIVE_INT,
                                   ds_id, H5P_DEFAULT, H5P_DEFAULT);
        H5Awrite(attr_id, H5T_NATIVE_INT, &data);
        H5Aclose(attr_id);
        H5Sclose(ds_id);
    }

    void write_attribute(double data, const std::string& name, const hid_t& file_id)
    {
        // write time step as an attribute to root group
        hid_t ds_id = H5Screate(H5S_SCALAR);
        hid_t attr_id = H5Acreate2(file_id, name.c_str(), H5T_NATIVE_DOUBLE,
                                   ds_id, H5P_DEFAULT, H5P_DEFAULT);
        H5Awrite(attr_id, H5T_NATIVE_DOUBLE, &data);
        H5Aclose(attr_id);
        H5Sclose(ds_id);
    }

    void write_attribute(const std::string& data, const std::string& name, const hid_t& file_id)
    {
        hid_t type = H5Tcopy (H5T_C_S1);
        H5Tset_size (type, data.size()+1);

        hid_t ds_id = H5Screate(H5S_SCALAR);
        hid_t attr_id = H5Acreate2(file_id, name.c_str(), type,
                                   ds_id, H5P_DEFAULT, H5P_DEFAULT);
        const char* dataChar = data.c_str();
        H5Awrite(attr_id, type, dataChar);
        H5Aclose(attr_id);
        H5Sclose(ds_id);
        H5Tclose(type);
    }

    // =======================================================
    // =======================================================
    /**
     * Dump computation results (conservative variables) into a file
     * (HDF5 file format) file extension is h5. File can be viewed by
     * hdfview; see also h5dump.
     *
     * \sa writeXdmfForHdf5Wrapper this routine write a Xdmf wrapper file for paraview.
     *
     * If library HDF5 is not available, do nothing.
     * \param[in] Udata device data to save
     * \param[in,out] Uhost host data temporary array before saving to file
     */
    void save()
    {
        const int nx = params.nx;
        const int ny = params.ny;
        const int nz = params.nz;

        const int isize = params.isize;
        const int jsize = params.jsize;
        const int ksize = params.ksize;

        const int ghostWidth = params.ghostWidth;

        const int dimType = params.dimType;

        const bool ghostIncluded = configMap.getBool("output","ghostIncluded",false);

        // copy device data to host
        Kokkos::deep_copy(Uhost, Udata);

        // here we need to check Uhost memory layout
        KokkosLayout layout;
        if (std::is_same<typename DataArray::array_layout, Kokkos::LayoutLeft>::value)
            layout = KOKKOS_LAYOUT_LEFT;
        else
            layout = KOKKOS_LAYOUT_RIGHT;

        herr_t status = 0;
        UNUSED(status);

        // make filename string
        std::string outputDir    = configMap.getString("output", "outputDir", "./");
        std::string outputPrefix = configMap.getString("output", "outputPrefix", "output");

        std::ostringstream outNum;
        outNum.width(7);
        outNum.fill('0');
        outNum << iOutput;
        std::string baseName         = outputPrefix+"_"+outNum.str();
        std::string hdf5Filename     = baseName+".h5";
        std::string hdf5FilenameFull = outputDir+"/"+hdf5Filename;

        // data size actually written on disk
        int nxg = nx;
        int nyg = ny;
        int nzg = nz;
        if (ghostIncluded) {
            nxg += 2*ghostWidth;
            nyg += 2*ghostWidth;
            nzg += 2*ghostWidth;
        }

        /*
         * write HDF5 file
         */
        // Create a new file using default properties.
        hid_t file_id = H5Fcreate(hdf5FilenameFull.c_str(), H5F_ACC_TRUNC |  H5F_ACC_DEBUG, H5P_DEFAULT, H5P_DEFAULT);

        // Create the data space for the dataset in memory and in file.
        hsize_t  dims_memory[3];
        hsize_t  dims_file[3];
        hid_t dataspace_memory, dataspace_file;
        if (dimType == TWO_D) {
            dims_memory[0] = jsize;
            dims_memory[1] = isize;
            dims_file[0] = nyg;
            dims_file[1] = nxg;
            dataspace_memory = H5Screate_simple(2, dims_memory, NULL);
            dataspace_file   = H5Screate_simple(2, dims_file  , NULL);
        } else {
            dims_memory[0] = ksize;
            dims_memory[1] = jsize;
            dims_memory[2] = isize;
            dims_file[0] = nzg;
            dims_file[1] = nyg;
            dims_file[2] = nxg;
            dataspace_memory = H5Screate_simple(3, dims_memory, NULL);
            dataspace_file   = H5Screate_simple(3, dims_file  , NULL);
        }

        // select data with or without ghost zones
        if (ghostIncluded) {
            if (dimType == TWO_D) {
                hsize_t  start[2] = {0, 0}; // ghost zone width
                hsize_t stride[2] = {1, 1};
                hsize_t  count[2] = {(hsize_t) nyg, (hsize_t) nxg};
                hsize_t  block[2] = {1, 1}; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_memory, H5S_SELECT_SET, start, stride, count, block);
            } else {
                hsize_t  start[3] = {0, 0, 0}; // ghost zone width
                hsize_t stride[3] = {1, 1, 1};
                hsize_t  count[3] = {(hsize_t) nzg, (hsize_t) nyg, (hsize_t) nxg};
                hsize_t  block[3] = {1, 1, 1}; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_memory, H5S_SELECT_SET, start, stride, count, block);
            }
        } else {
            if (dimType == TWO_D) {
                hsize_t  start[2] = {(hsize_t) ghostWidth, (hsize_t) ghostWidth}; // ghost zone width
                hsize_t stride[2] = {1, 1};
                hsize_t  count[2] = {(hsize_t) ny, (hsize_t) nx};
                hsize_t  block[2] = {1, 1}; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_memory, H5S_SELECT_SET, start, stride, count, block);
            } else {
                hsize_t  start[3] = {(hsize_t) ghostWidth, (hsize_t) ghostWidth, (hsize_t) ghostWidth}; // ghost zone width
                hsize_t stride[3] = {1, 1, 1};
                hsize_t  count[3] = {(hsize_t) nz, (hsize_t) ny, (hsize_t) nx};
                hsize_t  block[3] = {1, 1, 1}; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_memory, H5S_SELECT_SET, start, stride, count, block);
            }
        }
        hdf5_check(status, "Problem in H5Sselect_hyperslab");

        /*
         * property list for compression
         */
        // get compression level (0=no compression; 9 is highest level of compression)
        int compressionLevel = configMap.getInteger("output", "outputHdf5CompressionLevel", 0);
        if (compressionLevel < 0 || compressionLevel > 9) {
            std::cerr << "Invalid value for compression level; must be an integer between 0 and 9 !!!" << std::endl;
            std::cerr << "compression level is then set to default value 0; i.e. no compression !!" << std::endl;
            compressionLevel = 0;
        }

        hid_t propList_create_id = H5Pcreate(H5P_DATASET_CREATE);

        if (dimType == TWO_D) {
            const hsize_t chunk_size2D[2] = {(hsize_t) ny, (hsize_t) nx};
            status = H5Pset_chunk (propList_create_id, 2, chunk_size2D);
        } else { // THREE_D
            const hsize_t chunk_size3D[3] = {(hsize_t) nz, (hsize_t) ny, (hsize_t) nx};
            status = H5Pset_chunk (propList_create_id, 3, chunk_size3D);
        }
        hdf5_check(status, "Problem in H5Pset_chunk");

        H5Pset_shuffle (propList_create_id);
        H5Pset_deflate (propList_create_id, compressionLevel);

        /*
         * write heavy data to HDF5 file
         */

        // Some adjustement needed to take into account that strides / layout need
        // to be checked at runtime
        // if memory layout is KOKKOS_LAYOUT_RIGHT, we need an extra buffer.
        real_t* data = nullptr;
        if (layout == KOKKOS_LAYOUT_RIGHT)
        {
            const int data_size = dimType == TWO_D ? isize*jsize : isize*jsize*ksize;
            data = new real_t[data_size];
        }

        for (int ivar=0; ivar<nbvar; ++ivar)
        {
            write_field(ivar, data, file_id, dataspace_memory, dataspace_file, propList_create_id, layout);
        }

        // free memory if necessary
        if (layout == KOKKOS_LAYOUT_RIGHT)
        {
            delete[] data;
        }

        write_scalar(params.settings.gamma0, "gamma", file_id);
        write_scalar(params.settings.mmw, "mmw", file_id);
        write_scalar(code_units::constants::Rstar_h, "Rstar_h", file_id);
        write_vector(std::vector<double>{params.zmin, params.ymin, params.xmin}, "origin", file_id);
        write_vector(std::vector<double>{params.dz, params.dy, params.dx}, "spacing", file_id);

        write_attribute(totalTime, "total time", file_id);
        write_attribute(nx, "nx", file_id);
        write_attribute(ny, "ny", file_id);
        write_attribute(nz, "nz", file_id);
        write_attribute(iOutput, "iOutput", file_id);
        write_attribute(iStep, "iStep", file_id);
        write_attribute((ghostIncluded ? 1 : 0), "ghost zone included", file_id);
        write_attribute(version::git_branch, "git branch", file_id);
        write_attribute(version::git_build_string, "git commit", file_id);

        // close/release resources.
        H5Pclose(propList_create_id);
        H5Sclose(dataspace_memory);
        H5Sclose(dataspace_file);
        H5Fflush(file_id, H5F_SCOPE_LOCAL);
        H5Fclose(file_id);
    } // save


    DataArray     Udata;
    DataArrayHost Uhost;
    HydroParams& params;
    ConfigMap& configMap;
    int nbvar;
    std::map<int, std::string> variables_names;
    int iOutput;
    int iStep;
    real_t totalTime;
    std::string debug_name;
}; // class Save_HDF5

#ifdef USE_MPI
// =======================================================
// =======================================================
/**
 *
 */
template<DimensionType d>
class Save_HDF5_mpi
{
public:
    //! Decide at compile-time which data array type to use
    using DataArray  = typename std::conditional<d==TWO_D,DataArray2d,DataArray3d>::type;
    using DataArrayHost  = typename std::conditional<d==TWO_D,DataArray2dHost,DataArray3dHost>::type;

    Save_HDF5_mpi(DataArray     Udata,
                  DataArrayHost Uhost,
                  HydroParams& params,
                  ConfigMap& configMap,
                  int nbvar,
                  const std::map<int, std::string>& variables_names,
                  int iOutput,
                  int iStep,
                  real_t totalTime,
                  std::string debug_name) :
        Udata(Udata), Uhost(Uhost), params(params), configMap(configMap),
        nbvar(nbvar), variables_names(variables_names),
        iOutput(iOutput), iStep(iStep), totalTime(totalTime), debug_name(debug_name)
    {};
    ~Save_HDF5_mpi() {};

    template<DimensionType d_ = d>
    void copy_buffer(typename std::enable_if<d_==TWO_D, real_t>::type *& data,
                     int isize, int jsize, int ksize, int nvar, KokkosLayout layout)
    {
        if (layout == KOKKOS_LAYOUT_RIGHT) { // transpose array to make data contiguous in memory
            for (int j=0; j<jsize; ++j) {
                for (int i=0; i<isize; ++i) {
                    int index = i+isize*j;
                    data[index]=Uhost(i,j,nvar);
                }
            }
        } else {
            data = Uhost.data() + isize*jsize*nvar;
        }

    } // copy_buffer

    template<DimensionType d_ = d>
    void copy_buffer(typename std::enable_if<d_==THREE_D, real_t>::type *& data,
                     int isize, int jsize, int ksize, int nvar, KokkosLayout layout)
    {
        if (layout == KOKKOS_LAYOUT_RIGHT) { // transpose array to make data contiguous in memory
            for (int k=0; k<ksize; ++k) {
                for (int j=0; j<jsize; ++j) {
                    for (int i=0; i<isize; ++i) {
                        int index = i+isize*j+isize*jsize*k;
                        data[index]=Uhost(i,j,k,nvar);
                    }
                }
            }

        } else {
            data = Uhost.data() + isize*jsize*ksize*nvar;
        }

    } // copy_buffer / 3D

    herr_t write_field(int varId, real_t* &data, hid_t& file_id,
                       hid_t& dataspace_memory,
                       hid_t& dataspace_file,
                       hid_t& propList_create_id,
                       hid_t& propList_xfer_id,
                       KokkosLayout& layout)
    {
        hid_t dataType = std::is_same<real_t, float>::value ? H5T_NATIVE_FLOAT : H5T_NATIVE_DOUBLE;
        const int isize = params.isize;
        const int jsize = params.jsize;
        const int ksize = params.ksize;

        const std::string varName = "/" + variables_names.at(varId);
        hid_t dataset_id = H5Dcreate2(file_id, varName.c_str(),
                                      dataType, dataspace_file,
                                      H5P_DEFAULT, propList_create_id, H5P_DEFAULT);
        copy_buffer(data, isize, jsize, ksize, varId, layout);
        herr_t status = H5Dwrite(dataset_id, dataType,
                                 dataspace_memory, dataspace_file,
                                 propList_xfer_id, data);
        hdf5_check(status, "Problem in H5Dwrite");
        H5Dclose(dataset_id);

        return status;
    } // write_field

    void write_scalar(double scalar, const std::string& name, const hid_t& file_id)
    {
        hid_t space = H5Screate(H5S_SCALAR);
        hid_t dset  = H5Dcreate2(file_id, name.c_str(), H5T_NATIVE_DOUBLE, space,
                                 H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        herr_t status = H5Dwrite(dset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &scalar);
        hdf5_check(status, "Problem in H5Dwrite");
        H5Sclose(space);
        H5Dclose(dset);
    }

    void write_vector(const std::vector<double>& vector, const std::string& name, const hid_t& file_id)
    {
        int rank = 1;
        hsize_t dims = vector.size();
        hid_t space = H5Screate_simple(rank, &dims, NULL);
        hid_t dset  = H5Dcreate2(file_id, name.c_str(), H5T_NATIVE_DOUBLE, space,
                                 H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        herr_t status = H5Dwrite(dset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, vector.data());
        hdf5_check(status, "Problem in H5Dwrite");
        H5Sclose(space);
        H5Dclose(dset);
    }

     void write_attribute(int data, const std::string& name, const hid_t& file_id)
    {
        // write time step as an attribute to root group
        hid_t ds_id = H5Screate(H5S_SCALAR);
        hid_t attr_id = H5Acreate2(file_id, name.c_str(), H5T_NATIVE_INT,
                                   ds_id, H5P_DEFAULT, H5P_DEFAULT);
        herr_t status = H5Awrite(attr_id, H5T_NATIVE_INT, &data);
        hdf5_check(status, "Problem in H5Awrite");
        H5Aclose(attr_id);
        H5Sclose(ds_id);
    }

    void write_attribute(double data, const std::string& name, const hid_t& file_id)
    {
        // write time step as an attribute to root group
        hid_t ds_id = H5Screate(H5S_SCALAR);
        hid_t attr_id = H5Acreate2(file_id, name.c_str(), H5T_NATIVE_DOUBLE,
                                   ds_id, H5P_DEFAULT, H5P_DEFAULT);
        herr_t status = H5Awrite(attr_id, H5T_NATIVE_DOUBLE, &data);
        hdf5_check(status, "Problem in H5Awrite");
        H5Aclose(attr_id);
        H5Sclose(ds_id);
    }

    void write_attribute(const std::string& data, const std::string& name, const hid_t& file_id)
    {
        hid_t type = H5Tcopy (H5T_C_S1);
        H5Tset_size (type, data.size()+1);

        hid_t ds_id = H5Screate(H5S_SCALAR);
        hid_t attr_id = H5Acreate2(file_id, name.c_str(), type,
                                   ds_id, H5P_DEFAULT, H5P_DEFAULT);
        const char* dataChar = data.c_str();
        herr_t status = H5Awrite(attr_id, type, dataChar);
        hdf5_check(status, "Problem in H5Awrite");
        H5Aclose(attr_id);
        H5Sclose(ds_id);
        H5Tclose(type);
    }

    // =======================================================
    // =======================================================
    /**
     * Dump computation results (conservative variables) into a file
     * (HDF5 file format) file extension is h5. File can be viewed by
     * hdfview; see also h5dump.
     *
     * \sa writeXdmfForHdf5Wrapper this routine write a Xdmf wrapper file for paraview.
     *
     * If library HDF5 is not available, do nothing.
     * \param[in] Udata device data to save
     * \param[in,out] Uhost host data temporary array before saving to file
     */
    void save()
    {
        using namespace hydroSimu; // for MpiComm (this namespace is tout-pourri)

        // sub-domain sizes
        const int nx = params.nx;
        const int ny = params.ny;
        const int nz = params.nz;

        // domain decomposition sizes
        const int mx = params.mx;
        const int my = params.my;
        const int mz = params.mz;

        // sub-domaine sizes with ghost cells
        const int isize = params.isize;
        const int jsize = params.jsize;
        const int ksize = params.ksize;

        const int ghostWidth = params.ghostWidth;

        const int dimType = params.dimType;

        const bool ghostIncluded = configMap.getBool("output","ghostIncluded",false);
        const bool allghostIncluded = configMap.getBool("output","allghostIncluded",false);

        const bool reassembleInFile = configMap.getBool("output", "reassembleInFile", true);

        const int myRank = params.myRank;

        // verbose log ?
        bool hdf5_verbose = configMap.getBool("output","hdf5_verbose",false);

        // copy device data to host
        Kokkos::deep_copy(Uhost, Udata);

        // here we need to check Uhost memory layout
        KokkosLayout layout;
        if (std::is_same<typename DataArray::array_layout, Kokkos::LayoutLeft>::value)
            layout = KOKKOS_LAYOUT_LEFT;
        else
            layout = KOKKOS_LAYOUT_RIGHT;

        /*
         * get MPI coords corresponding to MPI rank iPiece
         */
        int coords[3];
        if (dimType == TWO_D) {
            params.communicator->getCoords(myRank,2,coords);
        } else {
            params.communicator->getCoords(myRank,3,coords);
        }

        herr_t status = 0;
        UNUSED(status);

        // make filename string
        std::string outputDir    = configMap.getString("output", "outputDir", "./");
        std::string outputPrefix = configMap.getString("output", "outputPrefix", "output");

        std::ostringstream outNum;
        outNum.width(7);
        outNum.fill('0');
        outNum << iOutput;
        std::string baseName         = outputPrefix+"_"+outNum.str();
        std::string hdf5Filename     = outputPrefix+"_"+outNum.str()+".h5";
        std::string hdf5FilenameFull = outputDir+"/"+outputPrefix+"_"+outNum.str()+".h5";

        // measure time ??
        double write_timing;
        if (hdf5_verbose)
        {
            //MPI_Barrier(params.communicator->getComm());
            params.communicator->synchronize();
            write_timing = MPI_Wtime();
        }

        /*
         * write HDF5 file
         */
        // Create a new file using property list with parallel I/O access.
        MPI_Info mpi_info     = MPI_INFO_NULL;
        hid_t    propList_create_id = H5Pcreate(H5P_FILE_ACCESS);
        status = H5Pset_fapl_mpio(propList_create_id, params.communicator->getComm(), mpi_info);
        hdf5_check(status, "Can not access MPI IO parameters");

        hid_t    file_id  = H5Fcreate(hdf5FilenameFull.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, propList_create_id);
        H5Pclose(propList_create_id);

        // Create the data space for the dataset in memory and in file.
        hsize_t  dims_file[3];
        hsize_t  dims_memory[3];
        hsize_t  dims_chunk[3];
        hid_t dataspace_memory;
        //hid_t dataspace_chunk;
        hid_t dataspace_file;

        /*
         * reassembleInFile is false
         */
        if (!reassembleInFile) {

            if (allghostIncluded || ghostIncluded) {

                if (dimType == TWO_D) {

                    dims_file[0] = (ny+2*ghostWidth)*(mx*my);
                    dims_file[1] = (nx+2*ghostWidth);
                    dims_memory[0] = jsize;
                    dims_memory[1] = isize;
                    dims_chunk[0] = ny+2*ghostWidth;
                    dims_chunk[1] = nx+2*ghostWidth;
                    dataspace_memory = H5Screate_simple(2, dims_memory, NULL);
                    dataspace_file   = H5Screate_simple(2, dims_file  , NULL);

                } else { // THREE_D

                    dims_file[0] = (nz+2*ghostWidth)*(mx*my*mz);
                    dims_file[1] =  ny+2*ghostWidth;
                    dims_file[2] =  nx+2*ghostWidth;
                    dims_memory[0] = ksize;
                    dims_memory[1] = jsize;
                    dims_memory[2] = isize;
                    dims_chunk[0] = nz+2*ghostWidth;
                    dims_chunk[1] = ny+2*ghostWidth;
                    dims_chunk[2] = nx+2*ghostWidth;
                    dataspace_memory = H5Screate_simple(3, dims_memory, NULL);
                    dataspace_file   = H5Screate_simple(3, dims_file  , NULL);

                } // end THREE_D

            } else { // no ghost zones are saved

                if (dimType == TWO_D) {

                    dims_file[0] = (ny)*(mx*my);
                    dims_file[1] = nx;
                    dims_memory[0] = jsize;
                    dims_memory[1] = isize;
                    dims_chunk[0] = ny;
                    dims_chunk[1] = nx;
                    dataspace_memory = H5Screate_simple(2, dims_memory, NULL);
                    dataspace_file   = H5Screate_simple(2, dims_file  , NULL);

                } else {

                    dims_file[0] = (nz)*(mx*my*mz);
                    dims_file[1] = ny;
                    dims_file[2] = nx;
                    dims_memory[0] = ksize;
                    dims_memory[1] = jsize;
                    dims_memory[2] = isize;
                    dims_chunk[0] = nz;
                    dims_chunk[1] = ny;
                    dims_chunk[2] = nx;
                    dataspace_memory = H5Screate_simple(3, dims_memory, NULL);
                    dataspace_file   = H5Screate_simple(3, dims_file  , NULL);

                } // end THREE_D

            } // end - no ghost zones are saved

        } else {
            /*
             * reassembleInFile is true
             */

            if (allghostIncluded) {

                if (dimType == TWO_D) {

                    dims_file[0] = my*(ny+2*ghostWidth);
                    dims_file[1] = mx*(nx+2*ghostWidth);
                    dims_memory[0] = jsize;
                    dims_memory[1] = isize;
                    dims_chunk[0] = ny+2*ghostWidth;
                    dims_chunk[1] = nx+2*ghostWidth;
                    dataspace_memory = H5Screate_simple(2, dims_memory, NULL);
                    dataspace_file   = H5Screate_simple(2, dims_file  , NULL);

                } else {

                    dims_file[0] = mz*(nz+2*ghostWidth);
                    dims_file[1] = my*(ny+2*ghostWidth);
                    dims_file[2] = mx*(nx+2*ghostWidth);
                    dims_memory[0] = ksize;
                    dims_memory[1] = jsize;
                    dims_memory[2] = isize;
                    dims_chunk[0] = nz+2*ghostWidth;
                    dims_chunk[1] = ny+2*ghostWidth;
                    dims_chunk[2] = nx+2*ghostWidth;
                    dataspace_memory = H5Screate_simple(3, dims_memory, NULL);
                    dataspace_file   = H5Screate_simple(3, dims_file  , NULL);

                }

            } else if (ghostIncluded) { // only external ghost zones

                if (dimType == TWO_D) {

                    dims_file[0] = ny*my+2*ghostWidth;
                    dims_file[1] = nx*mx+2*ghostWidth;
                    dims_memory[0] = jsize;
                    dims_memory[1] = isize;
                    dims_chunk[0] = ny+2*ghostWidth;
                    dims_chunk[1] = nx+2*ghostWidth;
                    dataspace_memory = H5Screate_simple(2, dims_memory, NULL);
                    dataspace_file   = H5Screate_simple(2, dims_file  , NULL);

                } else {

                    dims_file[0] = nz*mz+2*ghostWidth;
                    dims_file[1] = ny*my+2*ghostWidth;
                    dims_file[2] = nx*mx+2*ghostWidth;
                    dims_memory[0] = ksize;
                    dims_memory[1] = jsize;
                    dims_memory[2] = isize;
                    dims_chunk[0] = nz+2*ghostWidth;
                    dims_chunk[1] = ny+2*ghostWidth;
                    dims_chunk[2] = nx+2*ghostWidth;
                    dataspace_memory = H5Screate_simple(3, dims_memory, NULL);
                    dataspace_file   = H5Screate_simple(3, dims_file  , NULL);

                }

            } else { // no ghost zones are saved

                if (dimType == TWO_D) {

                    dims_file[0] = ny*my;
                    dims_file[1] = nx*mx;
                    dims_memory[0] = jsize;
                    dims_memory[1] = isize;
                    dims_chunk[0] = ny;
                    dims_chunk[1] = nx;
                    dataspace_memory = H5Screate_simple(2, dims_memory, NULL);
                    dataspace_file   = H5Screate_simple(2, dims_file  , NULL);

                } else {

                    dims_file[0] = nz*mz;
                    dims_file[1] = ny*my;
                    dims_file[2] = nx*mx;
                    dims_memory[0] = ksize;
                    dims_memory[1] = jsize;
                    dims_memory[2] = isize;
                    dims_chunk[0] = nz;
                    dims_chunk[1] = ny;
                    dims_chunk[2] = nx;
                    dataspace_memory = H5Screate_simple(3, dims_memory, NULL);
                    dataspace_file   = H5Screate_simple(3, dims_file  , NULL);

                }

            } // end ghostIncluded / allghostIncluded

        } // end reassembleInFile is true

        /*
         * Memory space hyperslab :
         * select data with or without ghost zones
         */
        if (ghostIncluded || allghostIncluded) {

            if (dimType == TWO_D) {
                hsize_t  start[2] = { 0, 0 }; // no start offset
                hsize_t stride[2] = { 1, 1 };
                hsize_t  count[2] = { 1, 1 };
                hsize_t  block[2] = { dims_chunk[0], dims_chunk[1] }; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_memory, H5S_SELECT_SET, start, stride, count, block);
            } else {
                hsize_t  start[3] = { 0, 0, 0 }; // no start offset
                hsize_t stride[3] = { 1, 1, 1 };
                hsize_t  count[3] = { 1, 1, 1 };
                hsize_t  block[3] = { dims_chunk[0], dims_chunk[1], dims_chunk[2] }; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_memory, H5S_SELECT_SET, start, stride, count, block);
            }

        } else { // no ghost zones

            if (dimType == TWO_D) {
                hsize_t  start[2] = { (hsize_t) ghostWidth,  (hsize_t) ghostWidth }; // ghost zone width
                hsize_t stride[2] = {                    1,                     1 };
                hsize_t  count[2] = {                    1,                     1 };
                hsize_t  block[2] = {(hsize_t)           ny, (hsize_t)         nx }; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_memory, H5S_SELECT_SET, start, stride, count, block);
            } else {
                hsize_t  start[3] = { (hsize_t) ghostWidth,  (hsize_t) ghostWidth, (hsize_t) ghostWidth }; // ghost zone width
                hsize_t stride[3] = { 1,  1,  1 };
                hsize_t  count[3] = { 1,  1,  1 };
                hsize_t  block[3] = {(hsize_t) nz, (hsize_t) ny, (hsize_t) nx }; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_memory, H5S_SELECT_SET, start, stride, count, block);
            }

        } // end ghostIncluded or allghostIncluded
        hdf5_check(status, "Problem in H5Sselect_hyperslab");

        /*
         * File space hyperslab :
         * select where we want to write our own piece of the global data
         * according to MPI rank.
         */

        /*
         * reassembleInFile is false
         */
        if (!reassembleInFile) {

            if (dimType == TWO_D) {

                hsize_t  start[2] = { myRank*dims_chunk[0], 0 };
                //hsize_t  start[2] = { 0, myRank*dims_chunk[1]};
                hsize_t stride[2] = { 1,  1 };
                hsize_t  count[2] = { 1,  1 };
                hsize_t  block[2] = { dims_chunk[0], dims_chunk[1] }; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_file, H5S_SELECT_SET, start, stride, count, block);

            } else { // THREE_D

                hsize_t  start[3] = { myRank*dims_chunk[0], 0, 0 };
                hsize_t stride[3] = { 1,  1,  1 };
                hsize_t  count[3] = { 1,  1,  1 };
                hsize_t  block[3] = { dims_chunk[0], dims_chunk[1], dims_chunk[2] }; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_file, H5S_SELECT_SET, start, stride, count, block);

            } // end THREE_D -- allghostIncluded

        } else {

            /*
             * reassembleInFile is true
             */

            if (allghostIncluded) {

                if (dimType == TWO_D) {

                    hsize_t  start[2] = { coords[1]*dims_chunk[0], coords[0]*dims_chunk[1]};
                    hsize_t stride[2] = { 1,  1 };
                    hsize_t  count[2] = { 1,  1 };
                    hsize_t  block[2] = { dims_chunk[0], dims_chunk[1] }; // row-major instead of column-major here
                    status = H5Sselect_hyperslab(dataspace_file, H5S_SELECT_SET, start, stride, count, block);

                } else { // THREE_D

                    hsize_t  start[3] = { coords[2]*dims_chunk[0], coords[1]*dims_chunk[1], coords[0]*dims_chunk[2]};
                    hsize_t stride[3] = { 1,  1,  1 };
                    hsize_t  count[3] = { 1,  1,  1 };
                    hsize_t  block[3] = { dims_chunk[0], dims_chunk[1], dims_chunk[2] }; // row-major instead of column-major here
                    status = H5Sselect_hyperslab(dataspace_file, H5S_SELECT_SET, start, stride, count, block);

                }

            } else if (ghostIncluded) {

                // global offsets
                int gOffsetStartX, gOffsetStartY, gOffsetStartZ;

                if (dimType == TWO_D) {
                    gOffsetStartY  = coords[1]*ny;
                    gOffsetStartX  = coords[0]*nx;

                    hsize_t  start[2] = { (hsize_t) gOffsetStartY, (hsize_t) gOffsetStartX };
                    hsize_t stride[2] = { 1,  1 };
                    hsize_t  count[2] = { 1,  1 };
                    hsize_t  block[2] = { dims_chunk[0], dims_chunk[1] }; // row-major instead of column-major here
                    status = H5Sselect_hyperslab(dataspace_file, H5S_SELECT_SET, start, stride, count, block);

                } else { // THREE_D

                    gOffsetStartZ  = coords[2]*nz;
                    gOffsetStartY  = coords[1]*ny;
                    gOffsetStartX  = coords[0]*nx;

                    hsize_t  start[3] = { (hsize_t) gOffsetStartZ, (hsize_t) gOffsetStartY, (hsize_t) gOffsetStartX };
                    hsize_t stride[3] = { 1,  1,  1 };
                    hsize_t  count[3] = { 1,  1,  1 };
                    hsize_t  block[3] = { dims_chunk[0], dims_chunk[1], dims_chunk[2] }; // row-major instead of column-major here
                    status = H5Sselect_hyperslab(dataspace_file, H5S_SELECT_SET, start, stride, count, block);

                }

            } else { // no ghost zones

                if (dimType == TWO_D) {

                    hsize_t  start[2] = { coords[1]*dims_chunk[0], coords[0]*dims_chunk[1]};
                    hsize_t stride[2] = { 1,  1 };
                    hsize_t  count[2] = { 1,  1 };
                    hsize_t  block[2] = { dims_chunk[0], dims_chunk[1] }; // row-major instead of column-major here
                    status = H5Sselect_hyperslab(dataspace_file, H5S_SELECT_SET, start, stride, count, block);

                } else { // THREE_D

                    hsize_t  start[3] = { coords[2]*dims_chunk[0], coords[1]*dims_chunk[1], coords[0]*dims_chunk[2]};
                    hsize_t stride[3] = { 1,  1,  1 };
                    hsize_t  count[3] = { 1,  1,  1 };
                    hsize_t  block[3] = { dims_chunk[0], dims_chunk[1], dims_chunk[2] }; // row-major instead of column-major here
                    status = H5Sselect_hyperslab(dataspace_file, H5S_SELECT_SET, start, stride, count, block);

                } // end THREE_D

            } // end ghostIncluded / allghostIncluded

        } // end reassembleInFile is true
        hdf5_check(status, "Problem in H5Sselect_hyperslab");

        /*
         *
         * write heavy data to HDF5 file
         *
         */

        // Some adjustement needed to take into account that strides / layout need
        // to be checked at runtime
        // if memory layout is KOKKOS_LAYOUT_RIGHT, we need an extra buffer.
        real_t* data = nullptr;
        if (layout == KOKKOS_LAYOUT_RIGHT)
        {
            const int data_size = dimType == TWO_D ? isize*jsize : isize*jsize*ksize;
            data = new real_t[data_size];
        }

        propList_create_id = H5Pcreate(H5P_DATASET_CREATE);
        if (dimType == TWO_D)
        {
            status = H5Pset_chunk(propList_create_id, 2, dims_chunk);
        }
        else
        {
            status = H5Pset_chunk(propList_create_id, 3, dims_chunk);
        }
        hdf5_check(status, "Problem in H5Pset_chunk");

        // please note that HDF5 parallel I/O does not support yet filters
        // so we can't use here H5P_deflate to perform compression !!!
        // Weak solution : call h5repack after the file is created
        // (performance of that has not been tested)

        // take care not to use parallel specific features if the HDF5
        // library available does not support them !!
        hid_t propList_xfer_id = H5Pcreate(H5P_DATASET_XFER);
        status = H5Pset_dxpl_mpio(propList_xfer_id, H5FD_MPIO_COLLECTIVE);
        hdf5_check(status, "Problem in H5Pset_dxpl_mpio");

        for (int ivar=0; ivar<nbvar; ++ivar)
        {
            write_field(ivar, data, file_id, dataspace_memory, dataspace_file,
                        propList_create_id, propList_xfer_id, layout);
        }

        // free memory if necessary
        if (layout == KOKKOS_LAYOUT_RIGHT)
        {
            delete[] data;
        }

        write_scalar(params.settings.gamma0, "gamma", file_id);
        write_scalar(params.settings.mmw, "mmw", file_id);
        write_scalar(code_units::constants::Rstar_h, "Rstar_h", file_id);
        write_vector(std::vector<double>{params.zmin, params.ymin, params.xmin}, "origin", file_id);
        write_vector(std::vector<double>{params.dz, params.dy, params.dx}, "spacing", file_id);

        write_attribute(totalTime, "total time", file_id);
        write_attribute(nx*mx, "nx", file_id);
        write_attribute(ny*my, "ny", file_id);
        write_attribute(nz*mz, "nz", file_id);
        write_attribute(iOutput, "iOutput", file_id);
        write_attribute(iStep, "iStep", file_id);
        write_attribute((ghostIncluded ? 1 : 0), "external ghost zones only included", file_id);
        write_attribute((allghostIncluded ? 1 : 0), "all ghost zones included", file_id);
        write_attribute((ghostIncluded ? 1 : 0), "reassemble MPI pieces in file", file_id);
        write_attribute(version::git_branch, "git branch", file_id);
        write_attribute(version::git_build_string, "git commit", file_id);

        // close/release resources.
        H5Pclose(propList_create_id);
        H5Pclose(propList_xfer_id);
        H5Sclose(dataspace_memory);
        H5Sclose(dataspace_file);
        H5Fclose(file_id);

        // verbose log about memory bandwidth
        if (hdf5_verbose)
        {
            write_timing = MPI_Wtime() - write_timing;

            MPI_Offset write_size = dimType == TWO_D ? isize * jsize : isize * jsize * ksize;
            write_size *= nbvar;
            write_size *= sizeof(real_t);

            MPI_Offset sum_write_size = write_size *  params.nProcs;

            double max_write_timing;
            MPI_Reduce(&write_timing, &max_write_timing, 1, MPI_DOUBLE, MPI_MAX, 0, params.communicator->getComm());

            if (myRank==0)
            {
                printf("########################################################\n");
                printf("################### HDF5 bandwidth #####################\n");
                printf("########################################################\n");
                printf("Local  array size %d x %d x %d reals(%zu bytes), write size = %.2f MB\n",
                       nx+2*ghostWidth,
                       ny+2*ghostWidth,
                       nz+2*ghostWidth,
                       sizeof(real_t),
                       1.0*write_size/1048576.0);
                sum_write_size /= 1048576.0;
                printf("Global array size %d x %d x %d reals(%zu bytes), write size = %.2f GB\n",
                       mx*nx+2*ghostWidth,
                       my*ny+2*ghostWidth,
                       mz*nz+2*ghostWidth,
                       sizeof(real_t),
                       1.0*sum_write_size/1024);

                const double write_bw = sum_write_size/max_write_timing;
                printf(" procs    Global array size  exec(sec)  write(MB/s)\n");
                printf("-------  ------------------  ---------  -----------\n");
                printf(" %4d    %4d x %4d x %4d %8.2f  %10.2f\n", params.nProcs,
                       mx*nx+2*ghostWidth,
                       my*ny+2*ghostWidth,
                       mz*nz+2*ghostWidth,
                       max_write_timing, write_bw);
                printf("########################################################\n");
            } // end (myRank == 0)
        } // hdf5_verbose
    } // save

    DataArray     Udata;
    DataArrayHost Uhost;
    HydroParams& params;
    ConfigMap& configMap;
    int nbvar;
    std::map<int, std::string> variables_names;
    int iOutput;
    int iStep;
    real_t totalTime;
    std::string debug_name;

}; // class Save_HDF5_mpi

#endif // USE_MPI

// =======================================================
// =======================================================
/**
 * Serial version of load data from a HDF5 file (previously dumped
 *  with class Save_Hdf5).
 * Data are computation results (conservative variables)
 * in HDF5 format.
 *
 * \sa Save_HDF5 this class performs HDF5 output
 *
 * \note This input routine is designed for re-starting a simulation run.
 *
 * Uhost is allocated here; if halfResolution is activated, an addition
 * upscale is done on host before uploading to device memory.
 *
 * If library HDF5 is not available, do nothing, just print a warning message.
 *
 */
template<DimensionType d>
class Load_HDF5
{
public:
    //! Decide at compile-time which data array type to use
    using DataArray  = typename std::conditional<d==TWO_D,DataArray2d,DataArray3d>::type;
    using DataArrayHost  = typename std::conditional<d==TWO_D,DataArray2dHost,DataArray3dHost>::type;

    /**
     *
     * \param[out]    Udata A Kokkos::View to hydro simulation
     * \param[in,out] Uhost A host mirror to a Kokkos::View to hydro simulation
     * \param[in]  params
     * \param[in]  configMap
     * \param[in]  nbvar number of scalar fields to read
     * \param[in]  variable_names map scalar field name to a string
     * \param[]
     * \param[in]  filename Name of the input HDF5 file
     * \param[in]  halfResolution boolean, triggers reading half resolution data
     *
     */
    Load_HDF5(DataArray     Udata,
              DataArrayHost Uhost,
              HydroParams& params,
              ConfigMap& configMap,
              int nbvar,
              const std::map<int, std::string>& variables_names) :
        Udata(Udata), Uhost(Uhost), params(params), configMap(configMap),
        nbvar(nbvar), variables_names(variables_names),
        iOutput(0), iStep(0), totalTime(0.0)
    {
    }; // end constructor

    virtual ~Load_HDF5() {};

    int get_ioutput() {return iOutput;}
    int get_istep() {return iStep;}
    real_t get_totalTime() {return totalTime;}

    /**
     * copy buffered data (read from file) to host buffer.
     */
    template<DimensionType d_ = d>
    void copy_buffer(typename std::enable_if<d_==TWO_D, real_t>::type *& data,
                     int isize, int jsize, int ksize, int nvar, KokkosLayout layout)
    {
        bool halfResolution = configMap.getBool("run","restart_upscale",false);

        if (halfResolution)
        {
            const int nx = params.nx;
            const int ghostWidth = params.ghostWidth;

            const int iL = nx/2+2*ghostWidth;

            // loop at high resolution
            for (int j=0; j<jsize; ++j)
            {
                for (int i=0; i<isize; ++i)
                {
                    const int jLow = (j+ghostWidth)/2;
                    const int iLow = (i+ghostWidth)/2;

                    Uhost(i, j, nvar) = data[iLow+iL*jLow];
                } // end for i
            } // end for j
        }
        else
        {
            // regular copy - same size
            if (layout == KOKKOS_LAYOUT_RIGHT)
            {
                // transpose array to make data contiguous in memory
                for (int j=0; j<jsize; ++j)
                {
                    for (int i=0; i<isize; ++i)
                    {
                        const int index = i+isize*j;
                        Uhost(i,j,nvar) = data[index];
                    }
                }
            }
            else
            {
                // simple copy
                real_t* tmp = Uhost.data() + isize*jsize*nvar;
		std::memcpy(tmp,data,isize*jsize*sizeof(real_t));
            }
        }
    } // copy_buffer

    /**
     * copy buffered data (red from file) to host buffer.
     */
    template<DimensionType d_=d>
    void copy_buffer(typename std::enable_if<d_==THREE_D, real_t>::type *& data,
                     int isize, int jsize, int ksize, int nvar, KokkosLayout layout)
    {
        bool halfResolution = configMap.getBool("run","restart_upscale",false);

        if (halfResolution)
        {
            const int nx = params.nx;
            const int ny = params.ny;
            const int ghostWidth = params.ghostWidth;

            const int iL = nx/2+2*ghostWidth;
            const int jL = ny/2+2*ghostWidth;

            // loop at high resolution
            for (int k=0; k<ksize; ++k)
            {
                for (int j=0; j<jsize; ++j)
                {
                    for (int i=0; i<isize; ++i)
                    {
                        const int kLow = (k+ghostWidth)/2;
                        const int jLow = (j+ghostWidth)/2;
                        const int iLow = (i+ghostWidth)/2;

                        Uhost(i,j,k,nvar) = data[iLow+iL*jLow+iL*jL*kLow];
                    } // end for i
                } // end for j
            } // end for k
        }
        else
        {
            if (layout == KOKKOS_LAYOUT_RIGHT)
            {
                // transpose array to make data contiguous in memory
                for (int k=0; k<ksize; ++k)
                {
                    for (int j=0; j<jsize; ++j)
                    {
                        for (int i=0; i<isize; ++i)
                        {
                            const int index = i+isize*j+isize*jsize*k;
                            Uhost(i,j,k,nvar) = data[index];
                        }
                    }
                }
            }
            else
            {
                // simple copy
                real_t* tmp = Uhost.data() + isize*jsize*ksize*nvar;
		std::memcpy(tmp,data,isize*jsize*ksize*sizeof(real_t));
            }

        } // end halfResolution

    } // copy_buffer / 3D

    herr_t read_field(int varId, real_t* &data, hid_t& file_id,
                      hid_t& dataspace_memory,
                      hid_t& dataspace_file,
                      KokkosLayout& layout)
    {
        const int isize = params.isize;
        const int jsize = params.jsize;
        const int ksize = params.ksize;

        const std::string varName = "/" + variables_names.at(varId);
        hid_t dataset_id = H5Dopen2(file_id, varName.c_str(), H5P_DEFAULT);

        // cross check dataType
        hid_t dataType = H5Dget_type(dataset_id);
        H5T_class_t t_class = H5Tget_class(dataType);
        hid_t expectedDataType = std::is_same<real_t, float>::value ?
            H5T_NATIVE_FLOAT : H5T_NATIVE_DOUBLE;
        H5T_class_t t_class_expected = H5Tget_class(expectedDataType);
        if (t_class != t_class_expected)
        {
            std::cerr << "Wrong HDF5 datatype !!\n";
            std::cerr << "expected     : " << t_class_expected << std::endl;
            std::cerr << "but received : " << t_class          << std::endl;
        }
        herr_t status = H5Dread(dataset_id, dataType, dataspace_memory, dataspace_file,
                                H5P_DEFAULT, data);
        hdf5_check(status, "Problem in H5Dread");

        H5Dclose(dataset_id);
        copy_buffer(data, isize, jsize, ksize, varId, layout);

        return status;
    } // read_field

    void read_attribute(int& data, const std::string& name, const hid_t& file_id)
    {
        hid_t group_id = H5Gopen2(file_id, "/", H5P_DEFAULT);
        hid_t attr_id = H5Aopen(group_id, name.c_str(), H5P_DEFAULT);
        herr_t status = H5Aread(attr_id, H5T_NATIVE_INT, &data);
        hdf5_check(status, "Problem in H5Aread");
        H5Aclose(attr_id);
        H5Gclose(group_id);
    }

    void read_attribute(double& data, const std::string& name, const hid_t& file_id)
    {
        hid_t group_id = H5Gopen2(file_id, "/", H5P_DEFAULT);
        hid_t attr_id = H5Aopen(group_id, name.c_str(), H5P_DEFAULT);
        herr_t status = H5Aread(attr_id, H5T_NATIVE_DOUBLE, &data);
        hdf5_check(status, "Problem in H5Aread");
        H5Aclose(attr_id);
        H5Gclose(group_id);
    }

    /**
     * \param[in] filename of the restart file
     *
     * \note the restart filename could directly read from the input parameter
     * file, but we chose to pass it to this routine, so that the load function
     * could be used for other purposes.
     */
    virtual void load(const std::string& filename)
    {
        const int nx = params.nx;
        const int ny = params.ny;
        const int nz = params.nz;

        const int isize = params.isize;
        const int jsize = params.jsize;
        const int ksize = params.ksize;

        const int ghostWidth = params.ghostWidth;

        const int dimType = params.dimType;

        std::string outputPrefix  = configMap.getString("output", "outputPrefix", "output");

        bool ghostIncluded = configMap.getBool("output","ghostIncluded",false);

        // upscale init data from a file twice smaller
        // in this case we expected that ghost cells are present in input file
        bool halfResolution = configMap.getBool("run","restart_upscale",false);

        herr_t status = 0;
        UNUSED(status);

        if (halfResolution)
        {
            bool is_odd = nx%2 != 0 || ny%2 != 0;
            is_odd |= dimType == TWO_D ? false : nz%2 != 0;
            if (is_odd)
            {
                abort("Number of cells have to be even when upscaling");
            }

            std::string oldPrefix = filename.substr(0, filename.find('_'));
            if (outputPrefix == oldPrefix)
            {
                abort("Use a different output prefix when upscaling");
            }
        }

        // logical sizes
        const int nx_r = halfResolution ? nx/2 : nx;
        const int ny_r = halfResolution ? ny/2 : ny;
        const int nz_r = halfResolution ? nz/2 : nz;

        // sizes with ghost zones included
        const int nx_rg = nx_r+2*ghostWidth;
        const int ny_rg = ny_r+2*ghostWidth;
        const int nz_rg = nz_r+2*ghostWidth;

        /*
         * Try to read HDF5 file.
         */

        /* Open the file */
        hid_t file_id = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
        if (file_id<0)
        {
            abort("Could not open file "+filename);
        }

        if (dimType == TWO_D)
        {
            int nx_file, ny_file;
            read_attribute(nx_file, "nx", file_id);
            read_attribute(ny_file, "ny", file_id);
            if (nx_file != nx_r || ny_file != ny_r)
            {
                abort("Wrong number of cells");
            }
        }
        else
        {
            int nx_file, ny_file, nz_file;
            read_attribute(nx_file, "nx", file_id);
            read_attribute(ny_file, "ny", file_id);
            read_attribute(nz_file, "nz", file_id);
            if (nx_file != nx_r || ny_file != ny_r || nz_file != nz_r)
            {
                abort("Wrong number of cells");
            }
        }

        /* build hyperslab handles */
        /* for data in file */
        /* for layout in memory */
        hsize_t  dims_memory[3];
        hsize_t  dims_file[3];
        hid_t dataspace_memory, dataspace_file;

        if (ghostIncluded) {

            if (dimType == TWO_D) {
                dims_memory[0] = ny_rg;
                dims_memory[1] = nx_rg;

                dims_file[0]   = ny_rg;
                dims_file[1]   = nx_rg;

                dataspace_memory = H5Screate_simple(2, dims_memory, NULL);
                dataspace_file   = H5Screate_simple(2, dims_file  , NULL);
            } else {
                dims_memory[0] = nz_rg;
                dims_memory[1] = ny_rg;
                dims_memory[2] = nx_rg;

                dims_file[0]   = nz_rg;
                dims_file[1]   = ny_rg;
                dims_file[2]   = nx_rg;

                dataspace_memory = H5Screate_simple(3, dims_memory, NULL);
                dataspace_file   = H5Screate_simple(3, dims_file  , NULL);
            }

        } else { // no ghost zones

            if (dimType == TWO_D) {
                dims_memory[0] = ny_rg;
                dims_memory[1] = nx_rg;

                dims_file[0]   = ny_r;
                dims_file[1]   = nx_r;

                dataspace_memory = H5Screate_simple(2, dims_memory, NULL);
                dataspace_file   = H5Screate_simple(2, dims_file  , NULL);
            } else {
                dims_memory[0] = nz_rg;
                dims_memory[1] = ny_rg;
                dims_memory[2] = nx_rg;

                dims_file[0]   = nz_r;
                dims_file[1]   = ny_r;
                dims_file[2]   = nx_r;

                dataspace_memory = H5Screate_simple(3, dims_memory, NULL);
                dataspace_file   = H5Screate_simple(3, dims_file  , NULL);
            }

        }


        /* hyperslab parameters */
        if (ghostIncluded) {

            if (dimType == TWO_D) {
                hsize_t  start[2] = {0, 0}; // ghost zone included
                hsize_t stride[2] = {1, 1};
                hsize_t  count[2] = {(hsize_t) ny_rg, (hsize_t) nx_rg};
                hsize_t  block[2] = {1, 1}; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_memory, H5S_SELECT_SET, start, stride, count, block);
            } else {
                hsize_t  start[3] = {0, 0, 0}; // ghost zone included
                hsize_t stride[3] = {1, 1, 1};
                hsize_t  count[3] = {(hsize_t) nz_rg, (hsize_t) ny_rg, (hsize_t) nx_rg};
                hsize_t  block[3] = {1, 1, 1}; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_memory, H5S_SELECT_SET, start, stride, count, block);
            }

        } else {

            if (dimType == TWO_D) {
                hsize_t  start[2] = {(hsize_t) ghostWidth, (hsize_t) ghostWidth}; // ghost zone width
                hsize_t stride[2] = {1, 1};
                hsize_t  count[2] = {(hsize_t) ny_r, (hsize_t) nx_r};
                hsize_t  block[2] = {1, 1}; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_memory, H5S_SELECT_SET, start, stride, count, block);
            } else {
                hsize_t  start[3] = {(hsize_t) ghostWidth, (hsize_t) ghostWidth, (hsize_t) ghostWidth}; // ghost zone width
                hsize_t stride[3] = {1, 1, 1};
                hsize_t  count[3] = {(hsize_t) nz_r, (hsize_t) ny_r, (hsize_t) nx_r};
                hsize_t  block[3] = {1, 1, 1}; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_memory, H5S_SELECT_SET, start, stride, count, block);
            }

        }
        hdf5_check(status, "Problem in H5Sselect_hyperslab");

        // here we need to check Udata / Uhost memory layout
        // see https://github.com/kokkos/kokkos/wiki/View - section 6.3.4
        KokkosLayout layout;
        if (std::is_same<typename DataArray::array_layout, Kokkos::LayoutLeft>::value)
            layout = KOKKOS_LAYOUT_LEFT;
        else
            layout = KOKKOS_LAYOUT_RIGHT;

        // Some adjustement needed to take into account that strides / layout need
        // to be checked at runtime
        // if memory layout is KOKKOS_LAYOUT_RIGHT, we need an allocation.
        // if memory layout is KOKKOS_LAYOUT_LEFT, allocation not required
        // (we could just use Uhost), but since we may need to upscale,

        // pointer to data in memory buffer
        // must be allocated (TODO)
        const int data_size = dimType == TWO_D ? isize*jsize : isize*jsize*ksize;
        real_t* data = new real_t[data_size];

        /*
         * open data set and perform read
         */

        // read density
        for (int ivar=0; ivar<nbvar; ++ivar)
        {
            read_field(ivar, data, file_id, dataspace_memory, dataspace_file, layout);
        }

        // free temporary memory
        delete[] data;

        read_attribute(iOutput, "iOutput", file_id);
        read_attribute(iStep, "iStep", file_id);
        read_attribute(totalTime, "total time", file_id);

        // close/release resources.
        H5Sclose(dataspace_memory);
        H5Sclose(dataspace_file);
        H5Fclose(file_id);

        // copy host data to device
        Kokkos::deep_copy(Udata, Uhost);
    } // load

    DataArray     Udata;
    DataArrayHost Uhost;
    HydroParams& params;
    ConfigMap& configMap;
    int nbvar;
    std::map<int, std::string> variables_names;
    int iOutput;
    int iStep;
    real_t totalTime;
}; // class Load_HDF5

#ifdef USE_MPI

// =======================================================
// =======================================================
/**
 * Parallel version of load data from a HDF5 file (previously dumped
 *  with class Save_Hdf5).
 * Data are computation results (conservative variables)
 * in HDF5 format.
 *
 * When MPI is activated, all MPI tasks read the same file and
 * extract the corresponding sub-domain.
 *
 * \sa Save_HDF5_mpi this class performs HDF5 output
 *
 * \note This input routine is designed for re-starting a simulation run.
 *
 * If library HDF5 is not available, do nothing, just print a warning message.
 *
 */
template<DimensionType d>
class Load_HDF5_mpi : public Load_HDF5<d>
{
public:
    //! Decide at compile-time which data array type to use
    using DataArray  = typename std::conditional<d==TWO_D,DataArray2d,DataArray3d>::type;
    using DataArrayHost  = typename std::conditional<d==TWO_D,DataArray2dHost,DataArray3dHost>::type;

    /**
     *
     * \param[out]    Udata A Kokkos::View to hydro simulation
     * \param[in,out] Uhost A host mirror to a Kokkos::View to hydro simulation
     * \param[in]  params
     * \param[in]  configMap
     * \param[in]  nbvar number of scalar fields to read
     * \param[in]  variable_names map scalar field name to a string
     * \param[]
     * \param[in]  filename Name of the input HDF5 file
     * \param[in]  halfResolution boolean, triggers reading half resolution data
     *
     */
    Load_HDF5_mpi(DataArray     Udata,
                  DataArrayHost Uhost,
                  HydroParams& params,
                  ConfigMap& configMap,
                  int nbvar,
                  const std::map<int, std::string>& variables_names) :
        Load_HDF5<d>(Udata, Uhost, params, configMap, nbvar, variables_names)
    {}; // end constructor

    ~Load_HDF5_mpi() {};

    herr_t read_field(int varId, real_t* &data, hid_t& file_id,
                      hid_t& dataspace_memory,
                      hid_t& dataspace_file,
                      hid_t& propList_xfer_id,
                      KokkosLayout& layout)
    {
        const int isize = this->params.isize;
        const int jsize = this->params.jsize;
        const int ksize = this->params.ksize;
        hid_t dataType = std::is_same<real_t, float>::value ? H5T_NATIVE_FLOAT : H5T_NATIVE_DOUBLE;

        const std::string varName = "/" + this->variables_names.at(varId);
        hid_t dataset_id = H5Dopen2(file_id, varName.c_str(), H5P_DEFAULT);
        herr_t status = H5Dread(dataset_id, dataType, dataspace_memory,
                                dataspace_file, propList_xfer_id, data);
        hdf5_check(status, "Problem in H5Dread");

        H5Dclose(dataset_id);
        this->copy_buffer(data, isize, jsize, ksize, varId, layout);

        return status;
    } // read_field

    void read_attribute(int& data, const std::string& name, const hid_t& file_id)
    {
        hid_t group_id = H5Gopen2(file_id, "/", H5P_DEFAULT);
        hid_t attr_id = H5Aopen(group_id, name.c_str(), H5P_DEFAULT);
        herr_t status = H5Aread(attr_id, H5T_NATIVE_INT, &data);
        hdf5_check(status, "Problem in H5Aread");
        H5Aclose(attr_id);
        H5Gclose(group_id);
    }

    void read_attribute(double& data, const std::string& name, const hid_t& file_id)
    {
        hid_t group_id = H5Gopen2(file_id, "/", H5P_DEFAULT);
        hid_t attr_id = H5Aopen(group_id, name.c_str(), H5P_DEFAULT);
        herr_t status = H5Aread(attr_id, H5T_NATIVE_DOUBLE, &data);
        hdf5_check(status, "Problem in H5Aread");
        H5Aclose(attr_id);
        H5Gclose(group_id);
    }

    /**
     * \param[in] filename of the restart file
     *
     * \note the restart filename could directly read from the input parameter
     * file, but we chose to pass it to this routine, so that the load function
     * could be used for other purposes.
     */
    void load(const std::string& filename)
    {
        const int nx = this->params.nx;
        const int ny = this->params.ny;
        const int nz = this->params.nz;

        const int mx = this->params.mx;
        const int my = this->params.my;
        const int mz = this->params.mz;

        const int isize = this->params.isize;
        const int jsize = this->params.jsize;
        const int ksize = this->params.ksize;

        const int ghostWidth = this->params.ghostWidth;

        const int dimType = this->params.dimType;

        const int nbvar = this->params.nbvar;
        const int myRank = this->params.myRank;
        const int nProcs = this->params.nProcs;

        std::string outputPrefix = this->configMap.getString("output", "outputPrefix", "output");
        bool ghostIncluded = this->configMap.getBool("output","ghostIncluded",false);
        bool allghostIncluded = this->configMap.getBool("output","allghostIncluded",false);

        // upscale init data from a file twice smaller
        // in this case we expected that ghost cells are present in input file
        bool halfResolution = this->configMap.getBool("run","restart_upscale",false);

        // verbose log ?
        bool hdf5_verbose = this->configMap.getBool("run","hdf5_verbose",false);

        if (halfResolution)
        {
            bool is_odd = nx%2 != 0 || ny%2 != 0;
            is_odd |= dimType == TWO_D ? false : nz%2 != 0;
            if (is_odd)
            {
                abort("Number of cells have to be even when upscaling");
            }

            std::string oldPrefix = filename.substr(0, filename.find('_'));
            if (outputPrefix == oldPrefix)
            {
                abort("Use a different output prefix when upscaling");
            }
        }

        // logical sizes / per sub-domain
        const int nx_r = halfResolution ? nx/2 : nx;
        const int ny_r = halfResolution ? ny/2 : ny;
        const int nz_r = halfResolution ? nz/2 : nz;

        // sizes with ghost zones included / per sub-domain
        const int nx_rg = nx_r+2*ghostWidth;
        const int ny_rg = ny_r+2*ghostWidth;
        const int nz_rg = nz_r+2*ghostWidth;

        herr_t status = 0;
        UNUSED(status);

        /*
         * Try parallel read HDF5 file.
         */

        /* Set up MPIO file access property lists */
        //MPI_Info mpi_info   = MPI_INFO_NULL;
        hid_t access_plist  = H5Pcreate(H5P_FILE_ACCESS);
        status = H5Pset_fapl_mpio(access_plist, /*MPI_COMM_WORLD*/ this->params.communicator->getComm(), MPI_INFO_NULL);
        hdf5_check(status, "Problem in H5Pset_fapl_mpio");

        /* Open the file */
        hid_t file_id = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, access_plist);
        if (file_id<0)
        {
            abort("Could not open file "+filename);
        }

        if (dimType == TWO_D)
        {
            int nx_file, ny_file;
            read_attribute(nx_file, "nx", file_id);
            read_attribute(ny_file, "ny", file_id);
            if (nx_file != mx*nx_r || ny_file != my*ny_r)
            {
                abort("Wrong number of cells");
            }
        }
        else
        {
            int nx_file, ny_file, nz_file;
            read_attribute(nx_file, "nx", file_id);
            read_attribute(ny_file, "ny", file_id);
            read_attribute(nz_file, "nz", file_id);
            if (nx_file != mx*nx_r || ny_file != my*ny_r || nz_file != mz*nz_r)
            {
                abort("Wrong number of cells");
            }
        }

        // get MPI coords corresponding to MPI rank iPiece
        int coords[3];
        if (dimType == TWO_D) {
            this->params.communicator->getCoords(myRank,2,coords);
        } else {
            this->params.communicator->getCoords(myRank,3,coords);
        }

        /*
         * Create the data space for the dataset in memory and in file.
         */
        hsize_t  dims_file[3];
        hsize_t  dims_memory[3];
        hsize_t  dims_chunk[3];
        hid_t dataspace_memory;
        hid_t dataspace_file;

        if (allghostIncluded) {

            if (dimType == TWO_D) {

                dims_file[0] = my*ny_rg;
                dims_file[1] = mx*nx_rg;
                dims_memory[0] = ny_rg;
                dims_memory[1] = nx_rg;
                dims_chunk[0] = ny_rg;
                dims_chunk[1] = nx_rg;
                dataspace_memory = H5Screate_simple(2, dims_memory, NULL);
                dataspace_file   = H5Screate_simple(2, dims_file  , NULL);

            } else { // THREE_D

                dims_file[0] = mz*nz_rg;
                dims_file[1] = my*ny_rg;
                dims_file[2] = mx*nx_rg;
                dims_memory[0] = nz_rg;
                dims_memory[1] = ny_rg;
                dims_memory[2] = nx_rg;
                dims_chunk[0] = nz_rg;
                dims_chunk[1] = ny_rg;
                dims_chunk[2] = nx_rg;
                dataspace_memory = H5Screate_simple(3, dims_memory, NULL);
                dataspace_file   = H5Screate_simple(3, dims_file  , NULL);

            }

        } else if (ghostIncluded) {

            if (dimType == TWO_D) {

                dims_file[0] = ny_r*my+2*ghostWidth;
                dims_file[1] = nx_r*mx+2*ghostWidth;
                dims_memory[0] = ny_rg;
                dims_memory[1] = nx_rg;
                dims_chunk[0] = ny_rg;
                dims_chunk[1] = nx_rg;
                dataspace_memory = H5Screate_simple(2, dims_memory, NULL);
                dataspace_file   = H5Screate_simple(2, dims_file  , NULL);

            } else { // THREE_D

                dims_file[0] = nz_r*mz+2*ghostWidth;
                dims_file[1] = ny_r*my+2*ghostWidth;
                dims_file[2] = nx_r*mx+2*ghostWidth;
                dims_memory[0] = nz_rg;
                dims_memory[1] = ny_rg;
                dims_memory[2] = nx_rg;
                dims_chunk[0] = nz_rg;
                dims_chunk[1] = ny_rg;
                dims_chunk[2] = nx_rg;
                dataspace_memory = H5Screate_simple(3, dims_memory, NULL);
                dataspace_file   = H5Screate_simple(3, dims_file  , NULL);

            }

        } else { // no ghost zones

            if (dimType == TWO_D) {

                dims_file[0] = ny_r*my;
                dims_file[1] = nx_r*mx;

                dims_memory[0] = ny_rg;
                dims_memory[1] = nx_rg;

                dims_chunk[0] = ny_r;
                dims_chunk[1] = nx_r;

                dataspace_memory = H5Screate_simple(2, dims_memory, NULL);
                dataspace_file   = H5Screate_simple(2, dims_file  , NULL);

            } else {

                dims_file[0] = nz_r*mz;
                dims_file[1] = ny_r*my;
                dims_file[2] = nx_r*mx;

                dims_memory[0] = nz_rg;
                dims_memory[1] = ny_rg;
                dims_memory[2] = nx_rg;

                dims_chunk[0] = nz_r;
                dims_chunk[1] = ny_r;
                dims_chunk[2] = nx_r;

                dataspace_memory = H5Screate_simple(3, dims_memory, NULL);
                dataspace_file   = H5Screate_simple(3, dims_file  , NULL);

            }

        } // end ghostIncluded / allghostIncluded

        /*
         * Memory space hyperslab :
         * select data with or without ghost zones
         */
        if (ghostIncluded || allghostIncluded) {

            if (dimType == TWO_D) {
                hsize_t  start[2] = { 0, 0 }; // ghost zone included
                hsize_t stride[2] = { 1, 1 };
                hsize_t  count[2] = { 1, 1 };
                hsize_t  block[2] = { dims_chunk[0], dims_chunk[1] }; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_memory, H5S_SELECT_SET, start, stride, count, block);
            } else {
                hsize_t  start[3] = { 0, 0, 0 }; // ghost zone included
                hsize_t stride[3] = { 1, 1, 1 };
                hsize_t  count[3] = { 1, 1, 1 };
                hsize_t  block[3] = { dims_chunk[0], dims_chunk[1], dims_chunk[2] }; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_memory, H5S_SELECT_SET, start, stride, count, block);
            }

        } else { // no ghost zones

            if (dimType == TWO_D) {
                hsize_t  start[2] = { (hsize_t) ghostWidth,  (hsize_t) ghostWidth }; // ghost zone width
                hsize_t stride[2] = { 1,  1 };
                hsize_t  count[2] = { 1,  1 };
                hsize_t  block[2] = {(hsize_t) ny_r, (hsize_t) nx_r}; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_memory, H5S_SELECT_SET, start, stride, count, block);
            } else {
                hsize_t  start[3] = { (hsize_t) ghostWidth,  (hsize_t) ghostWidth, (hsize_t) ghostWidth }; // ghost zone width
                hsize_t stride[3] = { 1,  1,  1 };
                hsize_t  count[3] = { 1,  1,  1 };
                hsize_t  block[3] = {(hsize_t) nz_r, (hsize_t) ny_r, (hsize_t) nx_r }; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_memory, H5S_SELECT_SET, start, stride, count, block);
            }

        } // end ghostIncluded or allghostIncluded
        hdf5_check(status, "Problem in H5Sselect_hyperslab");

        /*
         * File space hyperslab :
         * select where we want to read our own piece of the global data
         * according to MPI rank.
         */
        if (allghostIncluded) {

            if (dimType == TWO_D) {

                hsize_t  start[2] = { coords[1]*dims_chunk[0], coords[0]*dims_chunk[1]};
                hsize_t stride[2] = { 1,  1 };
                hsize_t  count[2] = { 1,  1 };
                hsize_t  block[2] = { dims_chunk[0], dims_chunk[1] }; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_file, H5S_SELECT_SET, start, stride, count, block);

            } else { // THREE_D

                hsize_t  start[3] = { coords[2]*dims_chunk[0], coords[1]*dims_chunk[1], coords[0]*dims_chunk[2]};
                hsize_t stride[3] = { 1,  1,  1 };
                hsize_t  count[3] = { 1,  1,  1 };
                hsize_t  block[3] = { dims_chunk[0], dims_chunk[1], dims_chunk[2] }; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_file, H5S_SELECT_SET, start, stride, count, block);

            }

        } else if (ghostIncluded) {

            // global offsets
            int gOffsetStartX, gOffsetStartY, gOffsetStartZ;

            if (dimType == TWO_D) {
                gOffsetStartY  = coords[1]*ny_r;
                gOffsetStartX  = coords[0]*nx_r;

                hsize_t  start[2] = { (hsize_t) gOffsetStartY, (hsize_t) gOffsetStartX };
                hsize_t stride[2] = { 1,  1};
                hsize_t  count[2] = { 1,  1};
                hsize_t  block[2] = { dims_chunk[0], dims_chunk[1] }; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_file, H5S_SELECT_SET, start, stride, count, block);

            } else { // THREE_D

                gOffsetStartZ  = coords[2]*nz_r;
                gOffsetStartY  = coords[1]*ny_r;
                gOffsetStartX  = coords[0]*nx_r;

                hsize_t  start[3] = { (hsize_t) gOffsetStartZ, (hsize_t) gOffsetStartY, (hsize_t) gOffsetStartX };
                hsize_t stride[3] = { 1,  1,  1};
                hsize_t  count[3] = { 1,  1,  1};
                hsize_t  block[3] = { dims_chunk[0], dims_chunk[1], dims_chunk[2] }; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_file, H5S_SELECT_SET, start, stride, count, block);

            }

        } else { // no ghost zones

            if (dimType == TWO_D) {

                hsize_t  start[2] = { coords[1]*dims_chunk[0], coords[0]*dims_chunk[1]};
                hsize_t stride[2] = { 1,  1};
                hsize_t  count[2] = { 1,  1};
                hsize_t  block[2] = { dims_chunk[0], dims_chunk[1] }; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_file, H5S_SELECT_SET, start, stride, count, block);

            } else { // THREE_D

                hsize_t  start[3] = { coords[2]*dims_chunk[0], coords[1]*dims_chunk[1], coords[0]*dims_chunk[2]};
                hsize_t stride[3] = { 1,  1,  1};
                hsize_t  count[3] = { 1,  1,  1};
                hsize_t  block[3] = { dims_chunk[0], dims_chunk[1], dims_chunk[2] }; // row-major instead of column-major here
                status = H5Sselect_hyperslab(dataspace_file, H5S_SELECT_SET, start, stride, count, block);
            }
        } // end ghostIncluded / allghostIncluded
        hdf5_check(status, "Problem in H5Sselect_hyperslab");

        // measure time ??
        double read_timing;
        if (hdf5_verbose)
        {
            MPI_Barrier(this->params.communicator->getComm());
            read_timing = MPI_Wtime();
        }

        /*
         *
         * read heavy data from HDF5 file
         *
         */

        // here we need to check Udata / Uhost memory layout
        // see https://github.com/kokkos/kokkos/wiki/View - section 6.3.4
        KokkosLayout layout;
        if (std::is_same<typename DataArray::array_layout, Kokkos::LayoutLeft>::value)
            layout = KOKKOS_LAYOUT_LEFT;
        else
            layout = KOKKOS_LAYOUT_RIGHT;

        // Some adjustement needed to take into account that strides / layout need
        // to be checked at runtime
        // if memory layout is KOKKOS_LAYOUT_RIGHT, we need an allocation.
        // if memory layout is KOKKOS_LAYOUT_LEFT, allocation not required
        // (we could just use Uhost), but since we may need to upscale,

        // pointer to data in memory buffer
        const int data_size = dimType == TWO_D ? isize*jsize : isize*jsize*ksize;
        real_t* data = new real_t[data_size];

        hid_t propList_create_id = H5Pcreate(H5P_DATASET_CREATE);
        if (dimType == TWO_D)
        {
            status = H5Pset_chunk(propList_create_id, 2, dims_chunk);
        }
        else
        {
            status = H5Pset_chunk(propList_create_id, 3, dims_chunk);
        }
        hdf5_check(status, "Problem in H5Pset_chunk");

        // please note that HDF5 parallel I/O does not support yet filters
        // so we can't use here H5P_deflate to perform compression !!!
        // Weak solution : call h5repack after the file is created
        // (performance of that has not been tested)

        hid_t propList_xfer_id = H5Pcreate(H5P_DATASET_XFER);
        H5Pset_dxpl_mpio(propList_xfer_id, H5FD_MPIO_COLLECTIVE);

        for (int ivar=0; ivar<nbvar; ++ivar)
        {
            read_field(ivar, data, file_id, dataspace_memory, dataspace_file,
                       propList_xfer_id, layout);
        }

        // free temporary memory
        delete[] data;

        read_attribute(this->iOutput, "iOutput", file_id);
        read_attribute(this->iStep, "iStep", file_id);
        read_attribute(this->totalTime, "total time", file_id);

        /* release resources */
        H5Pclose (propList_create_id);
        H5Pclose (access_plist);
        H5Fclose (file_id);

        // copy host data to device
        Kokkos::deep_copy(this->Udata, this->Uhost);

        /*
         * verbose log about memory bandwidth
         */
        if (hdf5_verbose)
        {
            read_timing = MPI_Wtime() - read_timing;

            MPI_Offset read_size = dimType == TWO_D ? nx_rg*ny_rg : nx_rg*ny_rg*nz_rg;
            read_size *= nbvar;
            read_size *= sizeof(real_t);

            MPI_Offset sum_read_size = read_size *  nProcs;

            double max_read_timing;
            MPI_Reduce(&read_timing, &max_read_timing, 1, MPI_DOUBLE, MPI_MAX, 0,
                       this->params.communicator->getComm());

            if (myRank==0)
            {
                printf("########################################################\n");
                printf("################### HDF5 bandwidth #####################\n");
                printf("########################################################\n");
                printf("Local  array size %d x %d x %d reals(%zu bytes), read size = %.2f MB\n",
                       nx+2*ghostWidth,
                       ny+2*ghostWidth,
                       nz+2*ghostWidth,
                       sizeof(real_t),
                       1.0*read_size/1048576.0);
                sum_read_size /= 1048576.0;
                printf("Global array size %d x %d x %d reals(%zu bytes), read size = %.2f GB\n",
                       mx*nx+2*ghostWidth,
                       my*ny+2*ghostWidth,
                       mz*nz+2*ghostWidth,
                       sizeof(real_t),
                       1.0*sum_read_size/1024);

                const double read_bw = sum_read_size/max_read_timing;
                printf(" procs    Global array size  exec(sec)  read(MB/s)\n");
                printf("-------  ------------------  ---------  -----------\n");
                printf(" %4d    %4d x %4d x %4d %8.2f  %10.2f\n", nProcs,
                       mx*nx+2*ghostWidth,
                       my*ny+2*ghostWidth,
                       mz*nz+2*ghostWidth,
                       max_read_timing, read_bw);
                printf("########################################################\n");
            } // end (myRank==0)
        } // hdf5_verbose
    } // Load_HDF5_mpi::load
}; // class Load_HDF5_mpi

#endif // USE_MPI

} // namespace io

} // namespace ark_rt

#endif // IO_HDF5_H_
