// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#include "IO_HDF5.h"

#include "shared/HydroParams.h"
#include "utils/config/ConfigMap.h"

#include <fstream>
#include <vector>

namespace ark_rt { namespace io
{

// =======================================================
// =======================================================
void writeXdmfForHdf5Wrapper(HydroParams& params,
                             ConfigMap& configMap,
			     int nbvar,
                             std::map<int, std::string>& variables_names,
                             const std::vector<real_t>& saved_times)
{
    // domain (no-MPI) or sub-domain sizes (MPI)
    const int nx = params.nx;
    const int ny = params.ny;
    const int nz = params.nz;

#ifdef USE_MPI
    // sub-domain decomposition sizes
    const int mx = params.mx;
    const int my = params.my;
    const int mz = params.mz;
#endif

    const int ghostWidth = params.ghostWidth;

    const int dimType = params.dimType;

    const bool ghostIncluded = configMap.getBool("output","ghostIncluded",false);

#ifdef USE_MPI
    // global sizes
    int nxg = mx*nx;
    int nyg = my*ny;
    int nzg = mz*nz;
#else
    // data size actually written on disk
    int nxg = nx;
    int nyg = ny;
    int nzg = nz;
#endif // USE_MPI

    if (ghostIncluded)
    {
        nxg += (2*ghostWidth);
        nyg += (2*ghostWidth);
        nzg += (2*ghostWidth);
    }

#ifdef USE_MPI
    /*
     * The follwing only makes sense in MPI: is allghostIncluded is true,
     * every sub-domain dumps its own local ghosts (might be usefull for debug,
     * at least it was useful in ramsesGPU for the shearing box border condition
     * debug).
     */
    bool allghostIncluded = configMap.getBool("output","allghostIncluded",false);
    if (allghostIncluded)
    {
        nxg = mx*(nx+2*ghostWidth);
        nyg = my*(ny+2*ghostWidth);
        nzg = mz*(nz+2*ghostWidth);
    }

    /*
     * Let MPIIO underneath hdf5 re-assemble the pieces and provides a single
     * nice file. Thanks parallel HDF5 !
     */
    bool reassembleInFile = configMap.getBool("output", "reassembleInFile", true);
    if (!reassembleInFile)
    {
        if (dimType==TWO_D)
        {
            if (allghostIncluded || ghostIncluded)
            {
                nxg = (nx+2*ghostWidth);
                nyg = (ny+2*ghostWidth)*mx*my;
            }
            else
            {
                nxg = nx;
                nyg = ny*mx*my;
            }
        }
        else
        {
            if (allghostIncluded || ghostIncluded)
            {
                nxg = nx+2*ghostWidth;
                nyg = ny+2*ghostWidth;
                nzg = (nz+2*ghostWidth)*mx*my*mz;
            }
            else
            {
                nxg = nx;
                nyg = ny;
                nzg = nz*mx*my*mz;
            }
        }
    }
#endif // USE_MPI

    // get data type as a string for Xdmf
    std::string precision(std::is_same<real_t, float>::value ? "4" : "8");

    /*
     * 1. open XDMF and write header lines
     */
    std::string outputDir    = configMap.getString("output", "outputDir", "./");
    std::string outputPrefix = configMap.getString("output", "outputPrefix", "output");
    std::string xdmfFilename = outputDir+'/'+outputPrefix+".xmf";

    std::fstream xdmfFile(xdmfFilename.c_str(), std::fstream::out);

    xdmfFile << "<?xml version=\"1.0\"?>"                                                            << std::endl;
    xdmfFile << "<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>"                                             << std::endl;
    xdmfFile << "<Xdmf xmlns:xi=\"http://www.w3.org/2003/XInclude\" Version=\"2.2\">"                << std::endl;
    xdmfFile << "  <Domain>"                                                                         << std::endl;
    xdmfFile << "    <Grid Name=\"TimeSeries\" GridType=\"Collection\" CollectionType=\"Temporal\">" << std::endl;

    // for each time step write a <grid> </grid> item
    const int totalNumberOfSteps = static_cast<int>(saved_times.size());
    for (int iOutput=0; iOutput<totalNumberOfSteps; ++iOutput)
    {
        std::ostringstream outNum;
        outNum.width(7);
        outNum.fill('0');
        outNum << iOutput;

        // take care that the following filename must be exactly the same as in routine outputHdf5 !!!
        std::string baseName         = outputPrefix+"_"+outNum.str();
        std::string hdf5Filename     = outputPrefix+"_"+outNum.str()+".h5";
        std::string hdf5FilenameFull = outputDir+"/"+outputPrefix+"_"+outNum.str()+".h5";

        xdmfFile << "      <Grid Name=\"" << baseName << "\" GridType=\"Uniform\">"   << std::endl;
        xdmfFile << "        <Time Value=\"" << saved_times[iOutput] << "\"/>" << std::endl;

        // topology CoRectMesh
        // here Dimensions is the number of points
        xdmfFile << "        <Topology TopologyType=\"3DCoRectMesh\" Dimensions=\"";
        xdmfFile << (dimType==TWO_D ? 2 : nzg+1) << ' ' << nyg+1 << " " << nxg+1 << "\"/>" << std::endl;

        // geometry
        xdmfFile << "        <Geometry GeometryType=\"ORIGIN_DXDYDZ\">"           << std::endl;
        xdmfFile << "          <DataItem";
        xdmfFile << " Name=\"Origin\"";
        xdmfFile << " NumberType=\"Float\"";
        xdmfFile << " Precision=\"" << precision << '"';
        xdmfFile << " Dimensions=\"3\"";
        xdmfFile << " Format=\"HDF\">" << std::endl;
        xdmfFile << "            " << hdf5Filename<< ':' << "origin"           << std::endl;
        xdmfFile << "          </DataItem>"                               << std::endl;
        xdmfFile << "          <DataItem";
        xdmfFile << " Name=\"Spacing\"";
        xdmfFile << " NumberType=\"Float\"";
        xdmfFile << " Precision=\"" << precision << '"';
        xdmfFile << " Dimensions=\"3\"";
        xdmfFile << " Format=\"HDF\">" << std::endl;
        xdmfFile << "            " << hdf5Filename<< ':' << "spacing"           << std::endl;
        xdmfFile << "          </DataItem>"                               << std::endl;
        xdmfFile << "        </Geometry>"                                 << std::endl;

        xdmfFile << "        <Attribute Name=\"gamma\" AttributeType=\"Scalar\" Center=\"Grid\">" << std::endl;
        xdmfFile << "          <DataItem";
        xdmfFile << " NumberType=\"Float\"";
        xdmfFile << " Precision=\"" << precision << '"';
        xdmfFile << " Dimensions=\"1\"";
        xdmfFile << " Format=\"HDF\">" << std::endl;
        xdmfFile << "            " << hdf5Filename<< ':' << "gamma"           << std::endl;
        xdmfFile << "          </DataItem>"                                   << std::endl;
        xdmfFile << "        </Attribute>"                                    << std::endl;

        xdmfFile << "        <Attribute Name=\"mmw\" AttributeType=\"Scalar\" Center=\"Grid\">" << std::endl;
        xdmfFile << "          <DataItem";
        xdmfFile << " NumberType=\"Float\"";
        xdmfFile << " Precision=\"" << precision << '"';
        xdmfFile << " Dimensions=\"1\"";
        xdmfFile << " Format=\"HDF\">" << std::endl;
        xdmfFile << "            " << hdf5Filename<< ':' << "mmw"             << std::endl;
        xdmfFile << "          </DataItem>"                                   << std::endl;
        xdmfFile << "        </Attribute>"                                    << std::endl;

        xdmfFile << "        <Attribute Name=\"Rstar_h\" AttributeType=\"Scalar\" Center=\"Grid\">" << std::endl;
        xdmfFile << "          <DataItem";
        xdmfFile << " NumberType=\"Float\"";
        xdmfFile << " Precision=\"" << precision << '"';
        xdmfFile << " Dimensions=\"1\"";
        xdmfFile << " Format=\"HDF\">" << std::endl;
        xdmfFile << "            " << hdf5Filename<< ':' << "Rstar_h"         << std::endl;
        xdmfFile << "          </DataItem>"                                   << std::endl;
        xdmfFile << "        </Attribute>"                                    << std::endl;

        for (int ivar=0; ivar<nbvar; ++ivar)
        {
            xdmfFile << "        <Attribute Name=\"" << variables_names[ivar] << "\" AttributeType=\"Scalar\" Center=\"Cell\">" << std::endl;
            xdmfFile << "          <DataItem";
            xdmfFile << " NumberType=\"Float\"";
            xdmfFile << " Precision=\"" << precision << '"';
            xdmfFile << " Dimensions=\"" << (dimType==TWO_D ? "" : std::to_string(nzg)) << ' ' << nyg << ' ' << nxg << "\"";
            xdmfFile << " Format=\"HDF\">" << std::endl;
            xdmfFile << "            " << hdf5Filename<< ':' << variables_names[ivar] << std::endl;
            xdmfFile << "          </DataItem>"                                   << std::endl;
            xdmfFile << "        </Attribute>"                                    << std::endl;
        }

        // finalize grid file for the current time step
        xdmfFile << "      </Grid>" << std::endl;
    } // end for loop over time step

    // finalize Xdmf wrapper file
    xdmfFile << "    </Grid>" << std::endl;
    xdmfFile << "  </Domain>" << std::endl;
    xdmfFile << "</Xdmf>"     << std::endl;

    xdmfFile.close();
} // writeXdmfForHdf5Wrapper

} // namespace io

} // namespace ark_rt
