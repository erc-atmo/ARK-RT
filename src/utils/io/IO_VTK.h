// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#ifndef IO_VTK_H_
#define IO_VTK_H_

#include <map>
#include <string>
#include <vector>

#include <shared/kokkos_shared.h>
struct HydroParams;
class ConfigMap;

namespace ark_rt { namespace io
{

void write_pvd_header(HydroParams& params,
                      ConfigMap& configMap,
                      const std::vector<real_t>& saved_times);

// ///////////////////////////////////////////////////////
// output routine (VTK file format, ASCII, VtkImageData)
// Take care that VTK uses row major (i+j*nx)
// To make sure OpenMP and CUDA version give the same
// results, we transpose the OpenMP data.
// ///////////////////////////////////////////////////////
/**
 * \param[in] Udata device data to save
 * \param[in,out] Uhost host data temporary array before saving to file
 */
void save_VTK_2D(DataArray2d             Udata,
                 DataArray2d::HostMirror Uhost,
                 HydroParams& params,
                 ConfigMap& configMap,
                 int nbvar,
                 const std::map<int, std::string>& variables_names,
                 int iOutput,
                 std::string debug_name);

// ///////////////////////////////////////////////////////
// output routine (VTK file format, ASCII, VtkImageData)
// Take care that VTK uses row major (i+j*nx+k*nx*ny)
// To make sure OpenMP and CUDA version give the same
// results, we transpose the OpenMP data.
// ///////////////////////////////////////////////////////
void save_VTK_3D(DataArray3d             Udata,
                 DataArray3d::HostMirror Uhost,
                 HydroParams& params,
                 ConfigMap& configMap,
                 int nbvar,
                 const std::map<int, std::string>& variables_names,
                 int iOutput,
                 std::string debug_name);


#ifdef USE_MPI
/**
 * \param[in] Udata device data to save
 * \param[in,out] Uhost host data temporary array before saving to file
 */
void save_VTK_2D_mpi(DataArray2d             Udata,
                     DataArray2d::HostMirror Uhost,
                     HydroParams& params,
                     ConfigMap& configMap,
                     int nbvar,
                     const std::map<int, std::string>& variables_names,
                     int iOutput,
                     std::string debug_name);

/**
 * \param[in] Udata device data to save
 * \param[in,out] Uhost host data temporary array before saving to file
 */
void save_VTK_3D_mpi(DataArray3d             Udata,
                     DataArray3d::HostMirror Uhost,
                     HydroParams& params,
                     ConfigMap& configMap,
                     int nbvar,
                     const std::map<int, std::string>& variables_names,
                     int iOutput,
                     std::string debug_name);

/**
 * Write Parallel VTI header.
 * Must be done by a single MPI process.
 *
 */
void write_pvti_header(std::string headerFilename,
                       std::string outputPrefix,
                       HydroParams& params,
                       int nbvar,
                       const std::map<int, std::string>& varNames,
                       int iOutput);
#endif // USE_MPI

} // namespace io

} // namespace ark_rt

#endif // IO_VTK_H_
