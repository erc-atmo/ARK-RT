// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#include <ctime>   // for std::time_t, std::tm, std::localtime
#include <sstream>
#include <iomanip> // for std::put_time (only g++ >= 5)

#include "IO_common.h"

namespace ark_rt { namespace io
{

// =======================================================
// =======================================================
std::string current_date()
{
    /* get current time */
    std::time_t     now = std::time(nullptr);

    /* Format and print the time, "ddd yyyy-mm-dd hh:mm:ss zzz" */
    std::tm tm = *std::localtime(&now);

    // old versions of g++ don't have std::put_time,
    // so we provide a slight work arround
#if defined(__GNUC__) && (__GNUC__ < 5)
    char foo[64];

    std::strftime(foo, sizeof(foo), "%Y-%m-%d %H:%M:%S %Z", &tm);
    return std::string(foo);
#else
    std::stringstream ss;
    ss << std::put_time(&tm, "%Y-%m-%d %H:%M:%S %Z");

    return ss.str();
#endif
} // current_date

} // namespace io

} // namespace ark_rt
