// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#include "IO_ReadWrite.h"

#include <utility>

#include <shared/HydroParams.h>
#include <utils/config/ConfigMap.h>
#include <shared/HydroState.h>

#include "IO_VTK.h"

#ifdef USE_HDF5
#include "IO_HDF5.h"
#endif

#ifdef USE_PNETCDF
#include "IO_PNETCDF.h"
#endif // USE_PNETCDF

namespace ark_rt { namespace io
{

// =======================================================
// =======================================================
IO_ReadWrite::IO_ReadWrite(HydroParams& params,
                           ConfigMap& configMap,
                           std::map<int, std::string>& variables_names) :
    IO_ReadWriteBase(),
    params(params),
    configMap(configMap),
    variables_names(variables_names),
    restart_enabled(true),
    vtk_enabled(true),
    hdf5_enabled(false),
    pnetcdf_enabled(false)
{
    restart_enabled = configMap.getBool("run", "restart_enabled", false);

    // do we want VTK output ?
    vtk_enabled = configMap.getBool("output","vtk_enabled", true);

    // do we want HDF5 output ?
    hdf5_enabled = configMap.getBool("output","hdf5_enabled", false);

    // do we want Parallel NETCDF output ? Only valid/activated for MPI run
    pnetcdf_enabled = configMap.getBool("output","pnetcdf_enabled", false);
} // IO_ReadWrite::IO_ReadWrite

// =======================================================
// =======================================================
void IO_ReadWrite::save_data(DataArray2d             Udata,
                             DataArray2d::HostMirror Uhost,
                             int iOutput,
                             int iStep,
                             real_t time,
                             std::string debug_name)
{
    save_data_impl(Udata, Uhost, iOutput, iStep, time, debug_name);
} // IO_ReadWrite::save_data

// =======================================================
// =======================================================
void IO_ReadWrite::save_data(DataArray3d             Udata,
                             DataArray3d::HostMirror Uhost,
                             int iOutput,
                             int iStep,
                             real_t time,
                             std::string debug_name)
{
    save_data_impl(Udata, Uhost, iOutput, iStep, time, debug_name);
} // IO_ReadWrite::save_data

// =======================================================
// =======================================================
void IO_ReadWrite::save_data_impl(DataArray2d             Udata,
                                  DataArray2d::HostMirror Uhost,
                                  int iOutput,
                                  int iStep,
                                  real_t time,
                                  std::string debug_name)
{
    saved_times.push_back(time);

    if (vtk_enabled)
    {
#ifdef USE_MPI
        save_VTK_2D_mpi(Udata, Uhost, params, configMap, HYDRO_2D_NBVAR, variables_names, iOutput, debug_name);
        if (!restart_enabled && params.myRank == 0)
        {
            write_pvd_header(params, configMap, saved_times);
        }
#else
        save_VTK_2D(Udata, Uhost, params, configMap, HYDRO_2D_NBVAR, variables_names, iOutput, debug_name);
        if (!restart_enabled)
        {
            write_pvd_header(params, configMap, saved_times);
        }
#endif // USE_MPI
    }

#ifdef USE_HDF5
    if (hdf5_enabled)
    {
#ifdef USE_MPI
        ark_rt::io::Save_HDF5_mpi<TWO_D> writer(Udata, Uhost, params, configMap, HYDRO_2D_NBVAR, variables_names, iOutput, iStep, time, debug_name);
        writer.save();
        if (!restart_enabled && params.myRank == 0)
        {
            writeXdmfForHdf5Wrapper(params, configMap, HYDRO_2D_NBVAR, variables_names, saved_times);
        }
#else
        ark_rt::io::Save_HDF5<TWO_D> writer(Udata, Uhost, params, configMap, HYDRO_2D_NBVAR, variables_names, iOutput, iStep, time, debug_name);
        writer.save();
        if (!restart_enabled)
        {
            writeXdmfForHdf5Wrapper(params, configMap, HYDRO_2D_NBVAR, variables_names, saved_times);
        }
#endif // USE_MPI
    }
#endif // USE_HDF5

#ifdef USE_PNETCDF
    if (pnetcdf_enabled)
    {
        ark_rt::io::Save_PNETCDF<TWO_D> writer(Udata, Uhost, params, configMap, HYDRO_2D_NBVAR, variables_names, iOutput, iStep, time, debug_name);
        writer.save();
    }
#endif // USE_PNETCDF

} // IO_ReadWrite::save_data_impl

// =======================================================
// =======================================================
void IO_ReadWrite::save_data_impl(DataArray3d             Udata,
                                  DataArray3d::HostMirror Uhost,
                                  int iOutput,
                                  int iStep,
                                  real_t time,
                                  std::string debug_name)
{
    saved_times.push_back(time);

    if (vtk_enabled)
    {
#ifdef USE_MPI
        save_VTK_3D_mpi(Udata, Uhost, params, configMap, HYDRO_3D_NBVAR, variables_names, iOutput, debug_name);
        if (!restart_enabled && params.myRank == 0)
        {
            write_pvd_header(params, configMap, saved_times);
        }
#else
        save_VTK_3D(Udata, Uhost, params, configMap, HYDRO_3D_NBVAR, variables_names, iOutput, debug_name);
        if (!restart_enabled)
        {
            write_pvd_header(params, configMap, saved_times);
        }
#endif // USE_MPI
    }

#ifdef USE_HDF5
    if (hdf5_enabled)
    {
#ifdef USE_MPI
        ark_rt::io::Save_HDF5_mpi<THREE_D> writer(Udata, Uhost, params, configMap, HYDRO_3D_NBVAR, variables_names, iOutput, iStep, time, debug_name);
        writer.save();
        if (!restart_enabled && params.myRank == 0)
        {
            writeXdmfForHdf5Wrapper(params, configMap, HYDRO_3D_NBVAR, variables_names, saved_times);
        }
#else
        ark_rt::io::Save_HDF5<THREE_D> writer(Udata, Uhost, params, configMap, HYDRO_3D_NBVAR, variables_names, iOutput, iStep, time, debug_name);
        writer.save();
        if (!restart_enabled)
        {
            writeXdmfForHdf5Wrapper(params, configMap, HYDRO_3D_NBVAR, variables_names, saved_times);
        }
#endif // USE_MPI
    }
#endif // USE_HDF5

#ifdef USE_PNETCDF
    if (pnetcdf_enabled)
    {
        ark_rt::io::Save_PNETCDF<THREE_D> writer(Udata, Uhost, params, configMap, HYDRO_2D_NBVAR, variables_names, iOutput, iStep, time, debug_name);
        writer.save();
    }
#endif // USE_PNETCDF
} // IO_ReadWrite::save_data_impl

void IO_ReadWrite::load_data(DataArray2d             Udata,
                             DataArray2d::HostMirror Uhost,
                             int& iOutput,
                             int& iStep,
                             real_t& time)
{
    load_data_impl(Udata, Uhost, iOutput, iStep, time);
} // IO_ReadWrite::save_data

// =======================================================
// =======================================================
void IO_ReadWrite::load_data(DataArray3d             Udata,
                             DataArray3d::HostMirror Uhost,
                             int& iOutput,
                             int& iStep,
                             real_t& time)
{
    load_data_impl(Udata, Uhost, iOutput, iStep, time);
} // IO_ReadWrite::load_data

void IO_ReadWrite::load_data_impl(DataArray2d             Udata,
                                  DataArray2d::HostMirror Uhost,
                                  int& iOutput,
                                  int& iStep,
                                  real_t& time)
{
    std::string restart_filename = configMap.getString("run", "restart_filename", "");
#ifdef USE_HDF5
#ifdef USE_MPI
    Load_HDF5_mpi<TWO_D> reader(Udata, Uhost, params, configMap, HYDRO_2D_NBVAR, variables_names);
#else
    Load_HDF5<TWO_D>     reader(Udata, Uhost, params, configMap, HYDRO_2D_NBVAR, variables_names);
#endif // USE_MPI
    reader.load(restart_filename);
    iOutput = reader.get_ioutput();
    iStep   = reader.get_istep();
    time    = reader.get_totalTime();
#endif // USE_HDF5
} // IO_ReadWrite::load_data_impl - 2d

// =======================================================
// =======================================================
void IO_ReadWrite::load_data_impl(DataArray3d             Udata,
                                  DataArray3d::HostMirror Uhost,
                                  int& iOutput,
                                  int& iStep,
                                  real_t& time)
{
    std::string restart_filename = configMap.getString("run", "restart_filename", "");
#ifdef USE_HDF5
#ifdef USE_MPI
    Load_HDF5_mpi<THREE_D> reader(Udata, Uhost, params, configMap, HYDRO_3D_NBVAR, variables_names);
#else
    Load_HDF5<THREE_D>     reader(Udata, Uhost, params, configMap, HYDRO_3D_NBVAR, variables_names);
#endif // USE_MPI
    reader.load(restart_filename);
    iOutput = reader.get_ioutput();
    iStep   = reader.get_istep();
    time    = reader.get_totalTime();
#endif // USE_HDF5
} // IO_ReadWrite::load_data_impl - 3d

} // namespace io

} // namespace ark_rt
