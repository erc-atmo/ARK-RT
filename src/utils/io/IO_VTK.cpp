// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#include "IO_VTK.h"

#include "shared/HydroParams.h"
#include "shared/units.h"
#include "utils/config/ConfigMap.h"

#include <iomanip>
#include <fstream>
#include <limits>
#include <map>
#include <vector>

namespace ark_rt { namespace io
{

// =======================================================
// =======================================================
static bool isBigEndian()
{
    const int i = 1;
    return ( (*(char*)&i) == 0 );
}

void write_pvd_header(HydroParams& params,
                      ConfigMap& configMap,
                      const std::vector<real_t>& saved_times)
{

   // local variables
   std::string outputDir    = configMap.getString("output", "outputDir", "./");
   std::string outputPrefix = configMap.getString("output", "outputPrefix", "output");

   std::string pvdFilename = outputDir+'/'+outputPrefix+".pvd";
   std::fstream pvdFile(pvdFilename.c_str(), std::fstream::out);

   std::string endian_type = isBigEndian() ? "BigEndian" : "LittleEndian";

   pvdFile << "<?xml version=\"1.0\"?>\n";
   pvdFile << "<VTKFile type=\"Collection\" version=\"0.1\"";
   pvdFile << " byte_order=\"" << endian_type << "\">\n";
   pvdFile << "  <Collection>\n";

   const int totalNumberOfSteps = static_cast<int>(saved_times.size());
   for (int iOutput=0; iOutput<totalNumberOfSteps; ++iOutput)
   {
       // write iOutput in string stepNum
       std::ostringstream stepNum;
       stepNum.width(7);
       stepNum.fill('0');
       stepNum << iOutput;

#if USE_MPI
       std::string filename = outputDir+'/'+outputPrefix+'_'+stepNum.str()+".pvti";
#else
       std::string filename = outputDir+'/'+ outputPrefix+'_'+ stepNum.str()+".vti";;
#endif
       pvdFile << "    <DataSet";
       pvdFile << " timestep=" << '"' << saved_times[iOutput] << '"';
       pvdFile << " group="    << '"' << ""                 << '"';
       pvdFile << " part="     << '"' << 0                  << '"';
       pvdFile << " file="     << '"' << filename.c_str()   << '"';
       pvdFile << "/>\n";
   }

   pvdFile << "  </Collection>\n";
   pvdFile << "</VTKFile>";
   pvdFile.close();
}

// =======================================================
// =======================================================
void save_VTK_2D(DataArray2d             Udata,
                 DataArray2d::HostMirror Uhost,
                 HydroParams& params,
                 ConfigMap& configMap,
                 int nbvar,
                 const std::map<int, std::string>& variables_names,
                 int iOutput,
                 std::string debug_name)
{
    const int nx = params.nx;
    const int ny = params.ny;

    const real_t dx = params.dx;
    const real_t dy = params.dy;

    const real_t xmin = params.xmin;
    const real_t ymin = params.ymin;

    const int imin = params.imin;
    const int imax = params.imax;

    const int jmin = params.jmin;
    const int jmax = params.jmax;

    const int ghostWidth = params.ghostWidth;

    const int isize = params.isize;
    const int jsize = params.jsize;
    const int nbCells = isize * jsize;

    // copy device data to host
    Kokkos::deep_copy(Uhost, Udata);

    std::string outputDir    = configMap.getString("output", "outputDir", "./");
    std::string outputPrefix = configMap.getString("output", "outputPrefix", "output");

    bool outputVtkAscii = configMap.getBool("output", "outputVtkAscii", false);

    // check scalar data type
    std::string type = std::is_same<real_t, double>::value ? "Float64" : "Float32";

    // write iOutput in string stepNum
    std::ostringstream stepNum;
    stepNum.width(7);
    stepNum.fill('0');
    stepNum << iOutput;

    // concatenate file prefix + file number + suffix
    std::string filename;
    if ( debug_name.empty() )
        filename = outputDir + "/" + outputPrefix + "_" + stepNum.str() + ".vti";
    else
        filename = outputDir + "/" + outputPrefix + "_" + debug_name + "_" + stepNum.str() + ".vti";

    // open file
    std::fstream outFile(filename.c_str(), std::fstream::out);
    outFile << std::setprecision(std::numeric_limits<real_t>::max_digits10);

    // write header

    // if writing raw binary data (file does not respect XML standard)
    if (outputVtkAscii)
        outFile << "<?xml version=\"1.0\"?>\n";

    // write xml data header
    std::string endian_type = isBigEndian() ? "BigEndian" : "LittleEndian";
    outFile << "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"" << endian_type << "\">\n";

    // write mesh extent
    outFile << "  <ImageData WholeExtent=\""
            << 0 << " " << nx << " "
            << 0 << " " << ny << " "
            << 0 << " " << 0  << "\""
            << " Origin=\""
            << xmin << " " << ymin << " " << 0 << "\" Spacing=\""
            << dx << " " << dy << " " << ZERO_F << "\">\n";

    outFile << "    <FieldData>\n";
    outFile << "      <DataArray Name=\"gamma\" NumberOfTuples=\"1\" type=\"Float64\" format=\"ascii\">\n";
    outFile << "        " << params.settings.gamma0 << '\n';
    outFile << "      </DataArray>\n";
    outFile << "      <DataArray Name=\"mmw\" NumberOfTuples=\"1\" type=\"Float64\" format=\"ascii\">\n";
    outFile << "        " << params.settings.mmw << '\n';
    outFile << "      </DataArray>\n";
    outFile << "      <DataArray Name=\"Rstar_h\" NumberOfTuples=\"1\" type=\"Float64\" format=\"ascii\">\n";
    outFile << "        " << code_units::constants::Rstar_h << '\n';
    outFile << "      </DataArray>\n";
    outFile << "    </FieldData>\n";

    outFile << "    <Piece Extent=\""
            << 0 << " " << nx << " "
            << 0 << " " << ny << " "
            << 0 << " " << 0  << " "
            << "\">\n";

    outFile << "      <PointData>\n";
    outFile << "      </PointData>\n";

    if (outputVtkAscii)
    {
        outFile << "      <CellData Scalars=\"" << variables_names.at(0) << "\">\n";

        // write data array (ascii), remove ghost cells
        for (int iVar=0; iVar<nbvar; iVar++)
        {
            outFile << "        <DataArray type=\"" << type;
            outFile << "\" Name=\"" << variables_names.at(iVar) << "\" format=\"ascii\" >\n";
            outFile << "          ";
            for (int index=0; index<nbCells; ++index)
            {
                // enforce the use of left layout (Ok for CUDA)
                // but for OpenMP, we will need to transpose
                const int j = index / isize;
                const int i = index - j*isize;

                if (j>=jmin+ghostWidth && j<=jmax-ghostWidth &&
                    i>=imin+ghostWidth && i<=imax-ghostWidth)
                {
                    outFile << Uhost(i, j, iVar) << " ";
                }
            }
            outFile << "\n        </DataArray>\n";
        } // end for iVar

        outFile << "      </CellData>\n";

        // write footer
        outFile << "    </Piece>\n";
        outFile << "  </ImageData>\n";
        outFile << "</VTKFile>\n";
    }
    else
    { // write data in binary format
        outFile << "      <CellData Scalars=\"" << variables_names.at(0) << "\">\n";

        for (int iVar=0; iVar<nbvar; iVar++)
        {
            outFile << "     <DataArray type=\"" << type << "\" Name=\"" ;
            outFile << variables_names.at(iVar)
                    << "\" format=\"appended\" offset=\""
                    << iVar*nx*ny*sizeof(real_t)+iVar*sizeof(unsigned int)
                    <<"\" />" << std::endl;
        }

        outFile << "      </CellData>" << std::endl;
        outFile << "    </Piece>" << std::endl;
        outFile << "  </ImageData>" << std::endl;

        outFile << "  <AppendedData encoding=\"raw\">" << std::endl;

        // write the leading undescore
        outFile << "_";
        // then write heavy data (column major format)
        {
            unsigned int nbOfWords = nx*ny*sizeof(real_t);
            for (int iVar=0; iVar<nbvar; iVar++)
            {
                outFile.write((char *)&nbOfWords,sizeof(unsigned int));
                for (int j=jmin+ghostWidth; j<=jmax-ghostWidth; j++)
                {
                    for (int i=imin+ghostWidth; i<=imax-ghostWidth; i++)
                    {
                        real_t tmp = Uhost(i, j, iVar);
                        outFile.write((char *)&tmp,sizeof(real_t));
                    }
                }
            }
        }

        outFile << "  </AppendedData>" << std::endl;
        outFile << "</VTKFile>" << std::endl;
    } // end ascii/binary heavy data write

    outFile.close();
} // end save_VTK_2D

// =======================================================
// =======================================================
void save_VTK_3D(DataArray3d             Udata,
                 DataArray3d::HostMirror Uhost,
                 HydroParams& params,
                 ConfigMap& configMap,
                 int nbvar,
                 const std::map<int, std::string>& variables_names,
                 int iOutput,
                 std::string debug_name)
{

    const int nx = params.nx;
    const int ny = params.ny;
    const int nz = params.nz;

    const real_t dx = params.dx;
    const real_t dy = params.dy;
    const real_t dz = params.dz;

    const real_t xmin = params.xmin;
    const real_t ymin = params.ymin;
    const real_t zmin = params.zmin;

    const int imin = params.imin;
    const int imax = params.imax;

    const int jmin = params.jmin;
    const int jmax = params.jmax;

    const int kmin = params.kmin;
    const int kmax = params.kmax;

    const int isize = params.isize;
    const int jsize = params.jsize;
    const int ksize = params.ksize;
    const int ijsize = isize * jsize;
    const int nbCells = isize * jsize * ksize;


    const int ghostWidth = params.ghostWidth;

    // copy device data to host
    Kokkos::deep_copy(Uhost, Udata);

    // local variables
    std::string outputDir    = configMap.getString("output", "outputDir", "./");
    std::string outputPrefix = configMap.getString("output", "outputPrefix", "output");

    bool outputVtkAscii = configMap.getBool("output", "outputVtkAscii", false);

    // check scalar data type
    std::string type = std::is_same<real_t, double>::value ? "Float64" : "Float32";

    // write iOutput in string stepNum
    std::ostringstream stepNum;
    stepNum.width(7);
    stepNum.fill('0');
    stepNum << iOutput;

    // concatenate file prefix + file number + suffix
    std::string filename;
    if ( debug_name.empty() )
        filename = outputDir + "/" + outputPrefix + "_" + stepNum.str() + ".vti";
    else
        filename = outputDir + "/" + outputPrefix + "_" + debug_name + "_" + stepNum.str() + ".vti";

    // open file
    std::fstream outFile(filename.c_str(), std::fstream::out);
    outFile << std::setprecision(std::numeric_limits<real_t>::max_digits10);

    // write header

    // if writing raw binary data (file does not respect XML standard)
    if (outputVtkAscii)
        outFile << "<?xml version=\"1.0\"?>\n";

    // write xml data header
    std::string endian_type = isBigEndian() ? "BigEndian" : "LittleEndian";
    outFile << "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"" << endian_type << "\">\n";

    // write mesh extent
    outFile << "  <ImageData WholeExtent=\""
            << 0 << " " << nx << " "
            << 0 << " " << ny << " "
            << 0 << " " << nz << "\" "
            << " Origin=\""
            << xmin << " " << ymin << " " << zmin << "\" Spacing=\""
            << dx << " " << dy << " " << dz << "\">\n";

    outFile << "    <FieldData>\n";
    outFile << "      <DataArray Name=\"gamma\" NumberOfTuples=\"1\" type=\"Float64\" format=\"ascii\">\n";
    outFile << "        " << params.settings.gamma0 << '\n';
    outFile << "      </DataArray>\n";
    outFile << "      <DataArray Name=\"mmw\" NumberOfTuples=\"1\" type=\"Float64\" format=\"ascii\">\n";
    outFile << "        " << params.settings.mmw << '\n';
    outFile << "      </DataArray>\n";
    outFile << "      <DataArray Name=\"Rstar_h\" NumberOfTuples=\"1\" type=\"Float64\" format=\"ascii\">\n";
    outFile << "        " << code_units::constants::Rstar_h << '\n';
    outFile << "      </DataArray>\n";
    outFile << "    </FieldData>\n";

    outFile << "    <Piece Extent=\""
            << 0 << " " << nx << " "
            << 0 << " " << ny << " "
            << 0 << " " << nz << " "
            << "\">\n";

    outFile << "      <PointData>\n";
    outFile << "      </PointData>\n";

    if (outputVtkAscii)
    {
        outFile << "      <CellData Scalars=\"" << variables_names.at(0) << "\">\n";

        // write data array (ascii), remove ghost cells
        for (int iVar=0; iVar<nbvar; iVar++)
        {
            outFile << "        <DataArray type=\"" << type;
            outFile << "\" Name=\"" << variables_names.at(iVar) << "\" format=\"ascii\" >\n";
            outFile << "          ";
            for (int index=0; index<nbCells; ++index)
            {
                // enforce the use of left layout (Ok for CUDA)
                // but for OpenMP, we will need to transpose
                const int k = index / ijsize;
                const int j = (index - k*ijsize) / isize;
                const int i = index - j*isize - k*ijsize;

                if (k>=kmin+ghostWidth && k<=kmax-ghostWidth &&
                    j>=jmin+ghostWidth && j<=jmax-ghostWidth &&
                    i>=imin+ghostWidth && i<=imax-ghostWidth)
                {
                    outFile << Uhost(i,j,k,iVar) << " ";
                }
            }
            outFile << "\n        </DataArray>\n";
        } // end for iVar

        outFile << "      </CellData>\n";

        // write footer
        outFile << "    </Piece>\n";
        outFile << "  </ImageData>\n";
        outFile << "</VTKFile>\n";

    }
    else
    { // write data in binary format
        outFile << "      <CellData Scalars=\"" << variables_names.at(0) << "\">\n";

        for (int iVar=0; iVar<nbvar; iVar++)
        {
            outFile << "        <DataArray type=\"" << type << "\" Name=\"" ;
            outFile << variables_names.at(iVar)
                    << "\" format=\"appended\" offset=\""
                    << iVar*nx*ny*nz*sizeof(real_t)+iVar*sizeof(unsigned int)
                    <<"\" />" << std::endl;
        }

        outFile << "      </CellData>" << std::endl;
        outFile << "    </Piece>" << std::endl;
        outFile << "  </ImageData>" << std::endl;

        outFile << "  <AppendedData encoding=\"raw\">" << std::endl;

        // write the leading undescore
        outFile << "_";

        // then write heavy data (column major format)
        {
            unsigned int nbOfWords = nx*ny*nz*sizeof(real_t);
            for (int iVar=0; iVar<nbvar; iVar++)
            {
                outFile.write((char *)&nbOfWords,sizeof(unsigned int));
                for (int k=kmin+ghostWidth; k<=kmax-ghostWidth; k++)
                {
                    for (int j=jmin+ghostWidth; j<=jmax-ghostWidth; j++)
                    {
                        for (int i=imin+ghostWidth; i<=imax-ghostWidth; i++)
                        {
                            real_t tmp = Uhost(i, j, k, iVar);
                            outFile.write((char *)&tmp,sizeof(real_t));
                        }
                    }
                }
            }
        }

        outFile << "  </AppendedData>" << std::endl;
        outFile << "</VTKFile>" << std::endl;
    } // end ascii/binary heavy data write

    outFile.close();
} // end save_VTK_3D

#ifdef USE_MPI
// =======================================================
// =======================================================
void save_VTK_2D_mpi(DataArray2d             Udata,
                     DataArray2d::HostMirror Uhost,
                     HydroParams& params,
                     ConfigMap& configMap,
                     int nbvar,
                     const std::map<int, std::string>& variables_names,
                     int iOutput,
                     std::string debug_name)
{

    const int nx = params.nx;
    const int ny = params.ny;

    const int imin = params.imin;
    const int imax = params.imax;

    const int jmin = params.jmin;
    const int jmax = params.jmax;

    const int ghostWidth = params.ghostWidth;

    const real_t dx = params.dx;
    const real_t dy = params.dy;
    const real_t dz = dx;

    const int isize = params.isize;
    const int jsize = params.jsize;
    const int nbCells = isize*jsize;

    const Kokkos::Array<int, 3> coord {params.myMpiPos};

    // copy device data to host
    Kokkos::deep_copy(Uhost, Udata);

    std::string outputDir    = configMap.getString("output", "outputDir", "./");
    std::string outputPrefix = configMap.getString("output", "outputPrefix", "output");

    bool outputVtkAscii = configMap.getBool("output", "outputVtkAscii", false);

    // check scalar data type
    std::string type = std::is_same<real_t, double>::value ? "Float64" : "Float32";

    // write iOutput in string timeFormat
    std::ostringstream timeFormat;
    timeFormat.width(7);
    timeFormat.fill('0');
    timeFormat << iOutput;

    // write MPI rank in string rankFormat
    std::ostringstream rankFormat;
    rankFormat.width(5);
    rankFormat.fill('0');
    rankFormat << params.myRank;

    // concatenate file prefix + file number + suffix
    std::string filename;
    if ( debug_name.empty() )
        filename = outputDir+"/"+outputPrefix+"_"+timeFormat.str()+"_piece_"+rankFormat.str()+".vti";
    else
        filename = outputDir+"/"+outputPrefix+"_"+debug_name +"_"+timeFormat.str()+"_piece_"+rankFormat.str()+".vti";

    // open file
    std::fstream outFile(filename.c_str(), std::fstream::out);
    outFile << std::setprecision(std::numeric_limits<real_t>::max_digits10);

    // write header
    if (params.myRank == 0)
    {
        // header file : parallel vti format
        std::string headerFilename = outputDir+"/"+outputPrefix+"_"+timeFormat.str()+".pvti";
        write_pvti_header(headerFilename, outputPrefix, params,
                          nbvar, variables_names, iOutput);
    }

    // if writing raw binary data (file does not respect XML standard)
    if (outputVtkAscii)
        outFile << "<?xml version=\"1.0\"?>\n";

    // write xml data header
    std::string endian_type = isBigEndian() ? "BigEndian" : "LittleEndian";
    outFile << "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"" << endian_type << "\">\n";

    // write mesh extent
    outFile << "  <ImageData WholeExtent=\""
            << coord[0]*nx << ' ' << (coord[0]+1)*nx << ' '
            << coord[1]*ny << ' ' << (coord[1]+1)*ny << ' '
            << 0           << ' ' << 0               << '"'
            << " Origin=\""
            << params.xmin << " " << params.ymin << " " << ZERO_F << "\" Spacing=\""
            << dx << " " << dy << " " << dz << "\">\n";

    outFile << "    <FieldData>\n";
    outFile << "      <DataArray Name=\"gamma\" NumberOfTuples=\"1\" type=\"Float64\" format=\"ascii\">\n";
    outFile << "        " << params.settings.gamma0 << '\n';
    outFile << "      </DataArray>\n";
    outFile << "      <DataArray Name=\"mmw\" NumberOfTuples=\"1\" type=\"Float64\" format=\"ascii\">\n";
    outFile << "        " << params.settings.mmw << '\n';
    outFile << "      </DataArray>\n";
    outFile << "      <DataArray Name=\"Rstar_h\" NumberOfTuples=\"1\" type=\"Float64\" format=\"ascii\">\n";
    outFile << "        " << code_units::constants::Rstar_h << '\n';
    outFile << "      </DataArray>\n";
    outFile << "    </FieldData>\n";

    outFile << "    <Piece Extent=\""
            << coord[0]*nx << ' ' << (coord[0]+1)*nx << ' '
            << coord[1]*ny << ' ' << (coord[1]+1)*ny << ' '
            << 0           << ' ' << 0               << '"'
            << '>' << std::endl;

    outFile << "      <PointData>\n";
    outFile << "      </PointData>\n";

    if (outputVtkAscii)
    {
        outFile << "      <CellData>\n";

        // write data array (ascii), remove ghost cells
        for (int iVar=0; iVar<nbvar; iVar++)
        {
            outFile << "        <DataArray type=\"" << type;
            outFile << "\" Name=\"" << variables_names.at(iVar) << "\" format=\"ascii\" >\n";
            outFile << "          ";
            for (int index=0; index<nbCells; ++index)
            {
                // enforce the use of left layout (Ok for CUDA)
                // but for OpenMP, we will need to transpose
                const int j = index / isize;
                const int i = index - j*isize;

                if (j>=jmin+ghostWidth && j<=jmax-ghostWidth &&
                    i>=imin+ghostWidth && i<=imax-ghostWidth)
                {
                    outFile << Uhost(i, j, iVar) << " ";
                }
            }

            outFile << "\n        </DataArray>\n";
        } // end for iVar

        outFile << "      </CellData>\n";

        // write footer
        outFile << "    </Piece>\n";
        outFile << "  </ImageData>\n";
        outFile << "</VTKFile>\n";
    }
    else
    { // write data in binary format
        outFile << "      <CellData>" << std::endl;

        for (int iVar=0; iVar<nbvar; iVar++)
        {
            outFile << "         <DataArray type=\"" << type << "\" Name=\"" ;
            outFile << variables_names.at(iVar)
                    << "\" format=\"appended\" offset=\""
                    << iVar*nx*ny*sizeof(real_t)+iVar*sizeof(unsigned int)
                    <<"\" />" << std::endl;
        }

        outFile << "      </CellData>" << std::endl;
        outFile << "    </Piece>" << std::endl;
        outFile << "  </ImageData>" << std::endl;

        outFile << "  <AppendedData encoding=\"raw\">" << std::endl;

        // write the leading undescore
        outFile << "_";
        // then write heavy data (column major format)
        {
            unsigned int nbOfWords = nx*ny*sizeof(real_t);
            for (int iVar=0; iVar<nbvar; iVar++)
            {
                outFile.write((char *)&nbOfWords,sizeof(unsigned int));
                for (int j=jmin+ghostWidth; j<=jmax-ghostWidth; j++)
                {
                    for (int i=imin+ghostWidth; i<=imax-ghostWidth; i++)
                    {
                        real_t tmp = Uhost(i, j, iVar);
                        outFile.write((char *)&tmp, sizeof(real_t));
                    }
                }
            }
        }

        outFile << "  </AppendedData>" << std::endl;
        outFile << "</VTKFile>" << std::endl;
    } // end ascii/binary heavy data write

    outFile.close();
} // save_VTK_2D_mpi

// =======================================================
// =======================================================
/**
 * \param[in] Udata device data to save
 * \param[in,out] Uhost host data temporary array before saving to file
 */
void save_VTK_3D_mpi(DataArray3d             Udata,
                     DataArray3d::HostMirror Uhost,
                     HydroParams& params,
                     ConfigMap& configMap,
                     int nbvar,
                     const std::map<int, std::string>& variables_names,
                     int iOutput,
                     std::string debug_name)
{

    const int nx = params.nx;
    const int ny = params.ny;
    const int nz = params.nz;

    const int imin = params.imin;
    const int imax = params.imax;

    const int jmin = params.jmin;
    const int jmax = params.jmax;

    const int kmin = params.kmin;
    const int kmax = params.kmax;

    const int ghostWidth = params.ghostWidth;

    const real_t dx = params.dx;
    const real_t dy = params.dy;
    const real_t dz = params.dz;

    const int isize = params.isize;
    const int jsize = params.jsize;
    const int ksize = params.ksize;
    const int ijsize = isize*jsize;
    const int nbCells = isize*jsize*ksize;

    const Kokkos::Array<int, 3> coord {params.myMpiPos};

    // copy device data to host
    Kokkos::deep_copy(Uhost, Udata);

    std::string outputDir    = configMap.getString("output", "outputDir", "./");
    std::string outputPrefix = configMap.getString("output", "outputPrefix", "output");

    bool outputVtkAscii = configMap.getBool("output", "outputVtkAscii", false);

    // check scalar data type
    std::string type = std::is_same<real_t, double>::value ? "Float64" : "Float32";

    // write iOutput in string timeFormat
    std::ostringstream timeFormat;
    timeFormat.width(7);
    timeFormat.fill('0');
    timeFormat << iOutput;

    // write MPI rank in string rankFormat
    std::ostringstream rankFormat;
    rankFormat.width(5);
    rankFormat.fill('0');
    rankFormat << params.myRank;

    // concatenate file prefix + file number + suffix
    std::string filename;
    if ( debug_name.empty() )
        filename = outputDir+"/"+outputPrefix+"_"+timeFormat.str()+"_piece_"+rankFormat.str()+".vti";
    else
        filename = outputDir+"/"+outputPrefix+"_"+debug_name +"_"+timeFormat.str()+"_piece_"+rankFormat.str()+".vti";

    // open file
    std::fstream outFile(filename.c_str(), std::fstream::out);
    outFile << std::setprecision(std::numeric_limits<real_t>::max_digits10);

    // write header
    if (params.myRank == 0)
    {
        // header file : parallel vti format
        std::string headerFilename = outputDir+"/"+outputPrefix+"_"+timeFormat.str()+".pvti";
        write_pvti_header(headerFilename, outputPrefix, params,
                          nbvar, variables_names, iOutput);
    }

    // if writing raw binary data (file does not respect XML standard)
    if (outputVtkAscii)
        outFile << "<?xml version=\"1.0\"?>\n";

    // write xml data header
    std::string endian_type = isBigEndian() ? "BigEndian" : "LittleEndian";
    outFile << "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"" << endian_type << "\">\n";

    // write mesh extent
    outFile << "  <ImageData WholeExtent=\""
            << coord[0]*nx << ' ' << (coord[0]+1)*nx << ' '
            << coord[1]*ny << ' ' << (coord[1]+1)*ny << ' '
            << coord[2]*nz << ' ' << (coord[2]+1)*nz << '"'
            << " Origin=\""
            << params.xmin << " " << params.ymin << " " << params.zmin << "\" Spacing=\""
            << dx << " " << dy << " " << dz << "\">\n";

    outFile << "    <FieldData>\n";
    outFile << "      <DataArray Name=\"gamma\" NumberOfTuples=\"1\" type=\"Float64\" format=\"ascii\">\n";
    outFile << "        " << params.settings.gamma0 << '\n';
    outFile << "      </DataArray>\n";
    outFile << "      <DataArray Name=\"mmw\" NumberOfTuples=\"1\" type=\"Float64\" format=\"ascii\">\n";
    outFile << "        " << params.settings.mmw << '\n';
    outFile << "      </DataArray>\n";
    outFile << "      <DataArray Name=\"Rstar_h\" NumberOfTuples=\"1\" type=\"Float64\" format=\"ascii\">\n";
    outFile << "        " << code_units::constants::Rstar_h << '\n';
    outFile << "      </DataArray>\n";
    outFile << "    </FieldData>\n";

    outFile << "    <Piece Extent=\""
            << coord[0]*nx << ' ' << (coord[0]+1)*nx << ' '
            << coord[1]*ny << ' ' << (coord[1]+1)*ny << ' '
            << coord[2]*nz << ' ' << (coord[2]+1)*nz << '"'
            << '>' << std::endl;

    outFile << "      <PointData>\n";
    outFile << "      </PointData>\n";

    if (outputVtkAscii)
    {
        outFile << "      <CellData>\n";

        // write data array (ascii), remove ghost cells
        for (int iVar=0; iVar<nbvar; iVar++)
        {
            outFile << "        <DataArray type=\"" << type;
            outFile << "\" Name=\"" << variables_names.at(iVar) << "\" format=\"ascii\" >\n";
            outFile << "          ";
            for (int index=0; index<nbCells; ++index)
            {
                // enforce the use of left layout (Ok for CUDA)
                // but for OpenMP, we will need to transpose
                const int k = index / ijsize;
                const int j = (index - k*ijsize) / isize;
                const int i = index - j*isize - k*ijsize;

                if (k>=kmin+ghostWidth && k<=kmax-ghostWidth &&
                    j>=jmin+ghostWidth && j<=jmax-ghostWidth &&
                    i>=imin+ghostWidth && i<=imax-ghostWidth)
                {
                    outFile << Uhost(i,j,k,iVar) << " ";
                }
            }

            outFile << "\n        </DataArray>\n";
        } // end for iVar

        outFile << "      </CellData>\n";

        // write footer
        outFile << "    </Piece>\n";
        outFile << "  </ImageData>\n";
        outFile << "</VTKFile>\n";
    }
    else
    { // write data in binary format
        outFile << "      <CellData>" << std::endl;

        for (int iVar=0; iVar<nbvar; iVar++)
        {
            outFile << "         <DataArray type=\"" << type << "\" Name=\"" ;
            outFile << variables_names.at(iVar)
                    << "\" format=\"appended\" offset=\""
                    << iVar*nx*ny*nz*sizeof(real_t)+iVar*sizeof(unsigned int)
                    <<"\" />" << std::endl;
        }

        outFile << "      </CellData>" << std::endl;
        outFile << "    </Piece>" << std::endl;
        outFile << "  </ImageData>" << std::endl;

        outFile << "  <AppendedData encoding=\"raw\">" << std::endl;

        // write the leading undescore
        outFile << "_";
        // then write heavy data (column major format)
        {
            unsigned int nbOfWords = nx*ny*nz*sizeof(real_t);
            for (int iVar=0; iVar<nbvar; iVar++)
            {
                outFile.write((char *)&nbOfWords,sizeof(unsigned int));
                for (int k=kmin+ghostWidth; k<=kmax-ghostWidth; k++)
                {
                    for (int j=jmin+ghostWidth; j<=jmax-ghostWidth; j++)
                    {
                        for (int i=imin+ghostWidth; i<=imax-ghostWidth; i++)
                        {
                            real_t tmp = Uhost(i, j, k, iVar);
                            outFile.write((char *)&tmp,sizeof(real_t));
                        } // for i
                    } // for j
                } // for k
            } // for iVar
        }

        outFile << "  </AppendedData>" << std::endl;
        outFile << "</VTKFile>" << std::endl;
    } // end ascii/binary heavy data write

    outFile.close();
} // save_VTK_3D_mpi

/*
 * write pvti header in a separate file.
 */
// =======================================================
// =======================================================
void write_pvti_header(std::string headerFilename,
                       std::string outputPrefix,
                       HydroParams& params,
                       int nbvar,
                       const std::map<int, std::string>& varNames,
                       int iOutput)
{
    // check scalar data type
    std::string type = std::is_same<real_t, double>::value ? "Float64" : "Float32";

    const int dimType = params.dimType;
    const int nProcs = params.nProcs;

    // write iOutput in string timeFormat
    std::ostringstream timeFormat;
    timeFormat.width(7);
    timeFormat.fill('0');
    timeFormat << iOutput;

    // local sub-domain sizes
    const int nx = params.nx;
    const int ny = params.ny;
    const int nz = (dimType == THREE_D) ? params.nz : 0;

    // sizes of MPI Cartesian topology
    const int mx = params.mx;
    const int my = params.my;
    const int mz = (dimType == THREE_D) ? params.mz : 0;

    const real_t dx = params.dx;
    const real_t dy = params.dy;
    const real_t dz = (dimType == THREE_D) ? params.dz : ZERO_F;

    // open pvti header file
    std::fstream outHeader(headerFilename.c_str(), std::fstream::out);
    outHeader << std::setprecision(std::numeric_limits<real_t>::max_digits10);

    outHeader << "<?xml version=\"1.0\"?>" << std::endl;
    std::string endian_type = isBigEndian() ? "BigEndian" : "LittleEndian";
    outHeader << "<VTKFile type=\"PImageData\" version=\"0.1\" byte_order=\"" << endian_type << "\">\n";
    outHeader << "  <PImageData WholeExtent=\"";
    outHeader << 0 << " " << mx*nx << " ";
    outHeader << 0 << " " << my*ny << " ";
    outHeader << 0 << " " << mz*nz << "\" GhostLevel=\"0\" "
              << "Origin=\""
              << params.xmin << " " << params.ymin << " " << params.zmin << "\" Spacing=\""
              << dx << " " << dy << " " << dz << "\">\n";
    outHeader << "    <PCellData Scalars=\"Scalars_\">" << std::endl;
    for (int iVar=0; iVar<nbvar; iVar++)
    {
        outHeader << "      <PDataArray type=\"" << type << "\" Name=\""<< varNames.at(iVar)<<"\"/>" << std::endl;
    }
    outHeader << "    </PCellData>" << std::endl;

    // one piece per MPI process
    if (dimType == TWO_D)
    {
        for (int iPiece=0; iPiece<nProcs; ++iPiece)
        {
            std::ostringstream pieceFormat;
            pieceFormat.width(5);
            pieceFormat.fill('0');
            pieceFormat << iPiece;
            std::string pieceFilename   = outputPrefix+"_"+timeFormat.str()+"_piece_"+pieceFormat.str()+".vti";
            // get MPI coords corresponding to MPI rank iPiece
            int coords[2];
            params.communicator->getCoords(iPiece,2,coords);
            outHeader << "    <Piece Extent=\"";

            // pieces in first line of column are different (due to the special
            // pvti file format with overlapping by 1 cell)
            outHeader << coords[0]*nx << ' ' << (coords[0]+1)*nx << ' ';
            outHeader << coords[1]*ny << ' ' << (coords[1]+1)*ny << ' ';
            outHeader << 0            << ' ' << 1                << '"';

            outHeader << " Source=\"" << pieceFilename << "\"/>" << std::endl;
        }
    }
    else
    { // THREE_D
        for (int iPiece=0; iPiece<nProcs; ++iPiece)
        {
            std::ostringstream pieceFormat;
            pieceFormat.width(5);
            pieceFormat.fill('0');
            pieceFormat << iPiece;
            std::string pieceFilename   = outputPrefix+"_"+timeFormat.str()+"_piece_"+pieceFormat.str()+".vti";
            // get MPI coords corresponding to MPI rank iPiece
            int coords[3];
            params.communicator->getCoords(iPiece,3,coords);
            outHeader << "    <Piece Extent=\"";

            outHeader << coords[0]*nx << ' ' << (coords[0]+1)*nx << ' ';
            outHeader << coords[1]*ny << ' ' << (coords[1]+1)*ny << ' ';
            outHeader << coords[2]*nz << ' ' << (coords[2]+1)*nz << '"';

            outHeader << " Source=\"" << pieceFilename << "\"/>" << std::endl;
        }
    }
    outHeader << "  </PImageData>" << std::endl;
    outHeader << "</VTKFile>" << std::endl;

    // close header file
    outHeader.close();
    // end writing pvti header
} // write_pvti_header
#endif // USE_MPI

} // namespace io

} // namespace ark_rt
