// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

/**
 * \file CudaTimer.h
 * \brief A simple timer class for CUDA based on events.
 *
 * \author Pierre Kestener
 * \date 30 Oct 2010
 *
 */
#ifndef CUDA_TIMER_H_
#define CUDA_TIMER_H_

#include <cuda_runtime.h>

/**
 * \brief a simple timer for CUDA kernel.
 */
class CudaTimer
{
protected:
  cudaEvent_t startEv, stopEv;
  double total_time;
  
public:
  CudaTimer() {
    cudaEventCreate(&startEv);
    cudaEventCreate(&stopEv);
    total_time = 0.0;
  }
  
  ~CudaTimer() {
    cudaEventDestroy(startEv);
    cudaEventDestroy(stopEv);
  }
  
  void start() {
    cudaEventRecord(startEv, 0);
  }
  
  void reset() {
    total_time = 0.0;
  }
  
  /** stop timer and accumulate time in seconds */
  void stop() {
    float gpuTime;
    cudaEventRecord(stopEv, 0);
    cudaEventSynchronize(stopEv);
    cudaEventElapsedTime(&gpuTime, startEv, stopEv);
    total_time += (double)1e-3*gpuTime;
  }
  
  /** return elapsed time in seconds (as record in total_time) */
  double elapsed() const {
    return total_time;
  }
  
}; // class CudaTimer
  
#endif // CUDA_TIMER_H_
