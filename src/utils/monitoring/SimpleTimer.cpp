// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

/**
 * \file SimpleTimer.cpp
 * \brief a simpe Timer class implementation.
 * 
 * \author Pierre Kestener
 * \date 29 Oct 2010
 *
 */

#include "SimpleTimer.h"

#include <stdexcept>

////////////////////////////////////////////////////////////////////////////////
// SimpleTimer class methods body
////////////////////////////////////////////////////////////////////////////////

// =======================================================
// =======================================================
SimpleTimer::SimpleTimer() {
  start_time = 0.0;
  total_time = 0.0;
  start();
} // SimpleTimer::SimpleTimer

// =======================================================
// =======================================================
SimpleTimer::SimpleTimer(double t) 
{
    
  //start_time.tv_sec = time_t(t);
  //start_time.tv_usec = (t - start_time.tv_sec) * 1e6;
  // start_time.tv_sec = 0;
  // start_time.tv_usec = 0;
  start_time = 0;
  total_time = t;
    
} // SimpleTimer::SimpleTimer

  // =======================================================
  // =======================================================
SimpleTimer::SimpleTimer(SimpleTimer const& aTimer) : start_time(aTimer.start_time), total_time(aTimer.total_time)
{
} // SimpleTimer::SimpleTimer

  // =======================================================
  // =======================================================
SimpleTimer::~SimpleTimer()
{
} // SimpleTimer::~SimpleTimer

  // =======================================================
  // =======================================================
void SimpleTimer::start() 
{

  timeval_t now;
  if (-1 == gettimeofday(&now, 0))
    throw std::runtime_error("SimpleTimer: Couldn't initialize start_time time");

  start_time = double(now.tv_sec) +  (double(now.tv_usec) * 1e-6);
  
} // SimpleTimer::start
  
  // =======================================================
  // =======================================================
void SimpleTimer::stop()
{
  double now_d;
  timeval_t now;
  if (-1 == gettimeofday(&now, 0))
    throw std::runtime_error("Couldn't get current time");
    
  now_d = double(now.tv_sec) + (double(now.tv_usec) * 1e-6);
  
  total_time += (now_d-start_time);

} // SimpleTimer::stop

  // =======================================================
  // =======================================================
double SimpleTimer::elapsed() const
{

  return total_time;

} // SimpleTimer::elapsed
