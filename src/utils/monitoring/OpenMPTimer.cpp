// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

/**
 * \file OpenMPTimer.cpp
 * \brief a simpe Timer class implementation.
 * 
 * \author Pierre Kestener
 * \date 29 Oct 2010
 *
 */

#include "OpenMPTimer.h"

#include <stdexcept>

////////////////////////////////////////////////////////////////////////////////
// OpenMPTimer class methods body
////////////////////////////////////////////////////////////////////////////////

// =======================================================
// =======================================================
OpenMPTimer::OpenMPTimer() {
  start_time = 0.0;
  total_time = 0.0;
  start();
} // OpenMPTimer::OpenMPTimer

// =======================================================
// =======================================================
OpenMPTimer::OpenMPTimer(double t) 
{
    
  start_time = 0;
  total_time = t;
    
} // OpenMPTimer::OpenMPTimer

  // =======================================================
  // =======================================================
OpenMPTimer::OpenMPTimer(OpenMPTimer const& aTimer) : start_time(aTimer.start_time), total_time(aTimer.total_time)
{
} // OpenMPTimer::OpenMPTimer

  // =======================================================
  // =======================================================
OpenMPTimer::~OpenMPTimer()
{
} // OpenMPTimer::~OpenMPTimer

  // =======================================================
  // =======================================================
void OpenMPTimer::start() 
{

  start_time = omp_get_wtime();
  
} // OpenMPTimer::start
  
  // =======================================================
  // =======================================================
void OpenMPTimer::stop()
{
  double now = omp_get_wtime();
  
  total_time += (now-start_time);

} // OpenMPTimer::stop

  // =======================================================
  // =======================================================
double OpenMPTimer::elapsed() const
{

  return total_time;

} // OpenMPTimer::elapsed
