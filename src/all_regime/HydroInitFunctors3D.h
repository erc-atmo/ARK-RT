// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include <limits> // for std::numeric_limits
#ifdef __CUDA_ARCH__
#include <math_constants.h> // for cuda math constants, e.g. CUDART_INF
#endif // __CUDA_ARCH__

#include "shared/kokkos_shared.h"
#include "HydroBaseFunctor3D.h"

// init conditions
#include "shared/BlastParams.h"
#include "shared/PoiseuilleParams.h"
#include "shared/RadiativeConvectionParams.h"
#include "shared/RayleighBenardParams.h"

#include "shared/Profile.h"

// kokkos random numbers
#include <Kokkos_Random.hpp>

namespace ark_rt { namespace all_regime
{

class InitRadiativeConvectionFunctor3D : HydroBaseFunctor3D
{
public:
    InitRadiativeConvectionFunctor3D(HydroParams params_,
                                     RadiativeConvectionParams rc_params_,
                                     DataArray Udata_)
        : HydroBaseFunctor3D(params_)
        , params(params_)
        , rc_params(rc_params_)
        , Udata(Udata_)
        , profile(rc_params_.input_file)
    {
    }

    static void apply(HydroParams params,
                      RadiativeConvectionParams rc_params,
                      DataArray Udata, int nbcells)
    {
        InitRadiativeConvectionFunctor3D functor(params, rc_params, Udata);
        Kokkos::parallel_for(nbcells, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int index) const
    {
        int i, j;
        index2coord(index, i, j, params.isize, params.jsize);

        const real_t k_b = code_units::constants::k_b;
        const real_t m_h = code_units::constants::m_h;

        const int kmin       = params.kmin;
        const int kmax       = params.kmax;
        const int ghostWidth = params.ghostWidth;
        const real_t dz      = params.dz;
        const real_t z_mid   = HALF_F * (params.zmax + params.zmin);

        if(i >= params.imin && i <= params.imax &&
           j >= params.jmin && j <= params.jmax)
        {
            for (int k=kmin+ghostWidth; k<kmax-ghostWidth+1; ++k)
            {
                const real_t z = params.zmin + (HALF_F + k - ghostWidth)*dz;

                HydroState q;
                q[IP] = profile.d_data(k-ghostWidth, 0);
                q[IS] = (z < z_mid ? ONE_F : ZERO_F);

                //compute mmw
                real_t mmw = computeMmw(q);
                q[ID] = q[IP]/(k_b*profile.d_data(k-ghostWidth, 1))*mmw*m_h;

                q[IU] = ZERO_F;
                q[IV] = ZERO_F;
                q[IW] = ZERO_F;

                setHydroState(Udata, computeConservatives(q), i, j, k);
            }
        }
    };

    const HydroParams params;
    const RadiativeConvectionParams rc_params;
    DataArray Udata;
    Profile profile;
}; // InitRadiativeConvectionFunctor3D


class InitRayleighBenardFunctor3D : HydroBaseFunctor3D
{
public:
    InitRayleighBenardFunctor3D(HydroParams params_,
                                RayleighBenardParams rayleighBenardParams_,
                                DataArray Udata_):
        HydroBaseFunctor3D(params_), rayleighBenardParams(rayleighBenardParams_),
        Udata(Udata_), rand_pool(12.0)
    {
        int rank = 0;
#ifdef USE_MPI
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#endif
        rand_pool = Kokkos::Random_XorShift64_Pool<device> (50+rank);

        if (rank==0)
        {
            const real_t Rstar = params.settings.Rstar;
            const real_t gamma0 = params.settings.gamma0;
            const real_t cp = params.settings.cp;
            const real_t beta = rayleighBenardParams.temperature_gradient;
            const real_t d = params.zmax-params.zmin;
            const real_t z = HALF_F*(params.zmax+params.zmin);

            const real_t prandtl = params.settings.mu * cp / params.settings.kappa;
            const real_t m = params.settings.g_z / (Rstar * beta) - ONE_F;
            const real_t Q = std::sqrt(-Rstar*beta*d)*d / (params.settings.kappa/cp);
            const real_t rayleigh = Q*Q*(m+1)*(1-(m+1)*(gamma0-ONE_F)/gamma0)*std::pow(z, 2*m-1)/prandtl;

            std::cout << "Polytropic index : " << m << std::endl;
            std::cout << "Prandtl number   : " << prandtl << std::endl;
            std::cout << "Rayleigh number  : " << rayleigh << std::endl;
        }
    }

    static void apply(HydroParams params,
                      RayleighBenardParams rayleighBenardParams,
                      DataArray Udata, int nbCells)
    {
        InitRayleighBenardFunctor3D functor(params, rayleighBenardParams, Udata);
        Kokkos::parallel_for(nbCells, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int index) const
    {
        const int ghostWidth = params.ghostWidth;
        const int kmin = params.kmin;
        const int kmax = params.kmax;
        const int nx = params.nx;
        const int ny = params.ny;
        const int nz = params.nz;
        const real_t dx = params.dx;
        const real_t dy = params.dy;
        const real_t dz = params.dz;
        const real_t Lx = params.xmax - params.xmin;
        const real_t Ly = params.ymax - params.ymin;
        const real_t Lz = params.zmax - params.zmin;

        const real_t x_mid = HALF_F * (params.xmax + params.xmin);
        const real_t y_mid = HALF_F * (params.ymax + params.ymin);
        const real_t z_mid = HALF_F * (params.zmax + params.zmin);
        const real_t Pi = std::acos(-ONE_F);
        const real_t Rstar = params.settings.Rstar;

        const real_t rho_top = rayleighBenardParams.density_top;
        const real_t T_top   = rayleighBenardParams.temperature_top;
        const real_t beta    = rayleighBenardParams.temperature_gradient;
        const real_t T_bot   = T_top - beta * Lz;
        const real_t amp     = rayleighBenardParams.perturb_amplitude;
        const int    nmode   = rayleighBenardParams.nmode;

#ifdef USE_MPI
        const int i_mpi = params.myMpiPos[IX];
        const int j_mpi = params.myMpiPos[IY];
        const int k_mpi = params.myMpiPos[IZ];
#else
        const int i_mpi = 0;
        const int j_mpi = 0;
        const int k_mpi = 0;
#endif
        const real_t m = params.settings.g_z / (Rstar * beta) - ONE_F;

        int i, j, k_;
        index2coord(index, i, j, k_, params.isize, params.jsize, params.ksize);
        if(i >= params.imin && i <= params.imax &&
           j >= params.jmin && j <= params.jmax)
        {
            // To handle mpi dispatch
            real_t rho  = rho_top * std::pow(T_bot / T_top, m);
            real_t T    = T_bot;
            for (int k_glob=0; k_glob<nz*k_mpi; ++k_glob)
            {
                const real_t Dphi = phi(i, j, k_glob+1) - phi(i, j, k_glob);
                rho *= (TWO_F * Rstar * T - Dphi) / (TWO_F * Rstar * (T + beta * dz) + Dphi);
                T   += beta * dz;
            }

            for (int k=kmin+ghostWidth; k<=kmax-ghostWidth; ++k)
            {
                const real_t x = params.xmin + (HALF_F + i + nx*i_mpi - ghostWidth)*dx;
                const real_t y = params.ymin + (HALF_F + j + ny*j_mpi - ghostWidth)*dy;
                const real_t z = params.zmin + (HALF_F + k + nz*k_mpi - ghostWidth)*dz;

                const real_t Dphi = phi(i, j, k+1) - phi(i, j, k);

                HydroState q;
                q[ID] = rho;
                q[IP] = rho *  Rstar * T;
                q[IS] = ZERO_F;
                q[IU] = ZERO_F;
                q[IV] = ZERO_F;
                q[IW] = ZERO_F;

                if (rayleighBenardParams.perturb_temperature)
                {
                    // get random number state
                    GeneratorType rand_gen = rand_pool.get_state();

                    q[IP] *= (ONE_F + amp * rand_gen.drand());
                    // free random number
                    rand_pool.free_state(rand_gen);
                }

                if (rayleighBenardParams.perturb_velocity)
                {
                    q[IU] += amp * std::sin(nmode*TWO_F*Pi*(x-x_mid)/Lx) * std::cos(nmode*TWO_F*Pi*(y-y_mid)/Ly) * std::sin(Pi*(z-z_mid)/Lz);
                    q[IV] += amp * std::cos(nmode*TWO_F*Pi*(x-x_mid)/Lx) * std::sin(nmode*TWO_F*Pi*(y-y_mid)/Ly) * std::sin(Pi*(z-z_mid)/Lz);
                    q[IW] += amp * std::cos(nmode*TWO_F*Pi*(x-x_mid)/Lx) * std::cos(nmode*TWO_F*Pi*(y-y_mid)/Ly) * std::cos(Pi*(z-z_mid)/Lz);
                }

                setHydroState(Udata, computeConservatives(q), i, j, k);

                rho *= (TWO_F * Rstar * T - Dphi) / (TWO_F * Rstar * (T + beta * dz) + Dphi);
                T   += beta * dz;
            }
        }
    }

    const RayleighBenardParams rayleighBenardParams;
    DataArray Udata;
    // random number generator
    using GeneratorPool = Kokkos::Random_XorShift64_Pool<device>;
    using GeneratorType = GeneratorPool::generator_type;
    GeneratorPool rand_pool; // random number generator
}; // InitRayleighBenardFunctor3D


class InitPoiseuilleFunctor3D : HydroBaseFunctor3D
{
public:
    InitPoiseuilleFunctor3D(HydroParams params_,
                            PoiseuilleParams poiseuilleParams_,
                            DataArray Udata_):
        HydroBaseFunctor3D(params_),
        poiseuilleParams(poiseuilleParams_),
        Udata(Udata_) {};

    static void apply(HydroParams params, PoiseuilleParams poiseuilleParams,
                      DataArray Udata, int nbCells)
    {
        InitPoiseuilleFunctor3D functor(params, poiseuilleParams, Udata);
        Kokkos::parallel_for(nbCells, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int index) const
    {
        const real_t ghostWidth = params.ghostWidth;
        const real_t xmin = params.xmin;
        const real_t ymin = params.ymin;
        const real_t zmin = params.zmin;
        const real_t nx = params.nx;
        const real_t ny = params.ny;
        const real_t nz = params.nz;
        const real_t dx = params.dx;
        const real_t dy = params.dy;
        const real_t dz = params.dz;

        int i,j,k;
        index2coord(index, i, j, k, params.isize, params.jsize, params.ksize);
#ifdef USE_MPI
        const int i_mpi = params.myMpiPos[IX];
        const int j_mpi = params.myMpiPos[IY];
        const int k_mpi = params.myMpiPos[IZ];
#else
        const int i_mpi = 0;
        const int j_mpi = 0;
        const int k_mpi = 0;
#endif
        const real_t pressure_gradient = poiseuilleParams.poiseuille_pressure_gradient;
        const real_t p0 = poiseuilleParams.poiseuille_pressure0;
        const real_t gamma0 = params.settings.gamma0;
        const real_t x = xmin + (HALF_F + i + nx*i_mpi-ghostWidth)*dx;
        const real_t y = ymin + (HALF_F + j + ny*j_mpi-ghostWidth)*dy;
        const real_t z = zmin + (HALF_F + k + nz*k_mpi-ghostWidth)*dz;

        const real_t d = poiseuilleParams.poiseuille_density;
        const real_t u = ZERO_F;
        const real_t v = ZERO_F;
        const real_t w = ZERO_F;
        real_t p;
        if (poiseuilleParams.poiseuille_flow_direction == IX)
        {
            p = p0 + (x - xmin) * pressure_gradient;
        }
        else if (poiseuilleParams.poiseuille_flow_direction == IY)
        {
            p = p0 + (y - ymin) * pressure_gradient;
        }
        else
        {
            p = p0 + (z - zmin) * pressure_gradient;
        }

        Udata(i, j, k, ID) = d;
        Udata(i, j, k, IU) = d * u;
        Udata(i, j, k, IV) = d * v;
        Udata(i, j, k, IW) = d * w;
        Udata(i, j, k, IP) = p / (gamma0-ONE_F) + HALF_F * d * (u*u+v*v+w*w);
    }

    PoiseuilleParams poiseuilleParams;
    DataArray Udata;

}; // InitPoiseuilleFunctor3D

class InitRayleighTaylorFunctor3D : HydroBaseFunctor3D
{

public:
    InitRayleighTaylorFunctor3D(HydroParams params_, DataArray Udata_) :
        HydroBaseFunctor3D(params_), Udata(Udata_) {};

    static void apply(HydroParams params,
                      DataArray Udata, int nbCells)
    {
        InitRayleighTaylorFunctor3D functor(params, Udata);
        Kokkos::parallel_for(nbCells, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(int index) const
    {
        const real_t ghostWidth = params.ghostWidth;
        const real_t xmin = params.xmin;
        const real_t ymin = params.ymin;
        const real_t zmin = params.zmin;
        const real_t xmax = params.xmax;
        const real_t ymax = params.ymax;
        const real_t zmax = params.zmax;
        const real_t dx = params.dx;
        const real_t dy = params.dy;
        const real_t dz = params.dz;
        const real_t nx = params.nx;
        const real_t ny = params.ny;
        const real_t nz = params.nz;

        int i, j, k;
        index2coord(index, i, j, k, params.isize, params.jsize, params.ksize);
#ifdef USE_MPI
        const int i_mpi = params.myMpiPos[IX];
        const int j_mpi = params.myMpiPos[IY];
        const int k_mpi = params.myMpiPos[IZ];
#else
        const int i_mpi = 0;
        const int j_mpi = 0;
        const int k_mpi = 0;
#endif
        const real_t x = xmin + dx/2 + (i+nx*i_mpi-ghostWidth)*dx;
        const real_t y = ymin + dy/2 + (j+ny*j_mpi-ghostWidth)*dy;
        const real_t z = zmin + dz/2 + (k+nz*k_mpi-ghostWidth)*dz;
        const real_t Lx = xmax - xmin;
        const real_t Ly = ymax - ymin;
        const real_t Lz = zmax - zmin;
        const real_t Pi = std::acos(-ONE_F);

        const real_t A = 0.01;
        const real_t p0 = 2.5;
        const real_t rho = (z<=ZERO_F) ? ONE_F : TWO_F;
        const real_t v = A * (1.0+std::cos(TWO_F*Pi*x/Lx))*(1.0+std::cos(TWO_F*Pi*y/Ly))*(1.0+std::cos(TWO_F*Pi*z/Lz))/8.0;

        Udata(i, j, k, ID) = rho;
        Udata(i, j, k, IP) = (p0 + rho*params.settings.g_z*z)/(params.settings.gamma0-ONE_F) + HALF_F*rho*v*v;
        Udata(i, j, k, IU) = ZERO_F;
        Udata(i, j, k, IV) = ZERO_F;
        Udata(i, j, k, IW) = rho * v;
    }

    DataArray Udata;

}; // InitRayleighTaylorFunctor3D


class InitFakeFunctor3D : public HydroBaseFunctor3D
{

public:
    InitFakeFunctor3D(HydroParams params,
                      DataArray Udata) :
        HydroBaseFunctor3D(params), Udata(Udata) {};

    KOKKOS_INLINE_FUNCTION
    void operator()(const int index) const
    {

        const int isize = params.isize;
        const int jsize = params.jsize;
        const int ksize = params.ksize;

        int i,j,k;
        index2coord(index,i,j,k,isize,jsize,ksize);

        Udata(i  ,j  ,k  , ID) = 0.0;
        Udata(i  ,j  ,k  , IP) = 0.0;
        Udata(i  ,j  ,k  , IU) = 0.0;
        Udata(i  ,j  ,k  , IV) = 0.0;
        Udata(i  ,j  ,k  , IW) = 0.0;

    } // end operator ()

    DataArray Udata;

}; // InitFakeFunctor3D


class InitImplodeFunctor3D : public HydroBaseFunctor3D
{

public:
    InitImplodeFunctor3D(HydroParams params,
                         DataArray Udata) :
        HydroBaseFunctor3D(params), Udata(Udata) {};

    static void apply(HydroParams params,
                      DataArray Udata, int nbCells)
    {
        InitImplodeFunctor3D functor(params, Udata);
        Kokkos::parallel_for(nbCells, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int index) const
    {

        const int isize = params.isize;
        const int jsize = params.jsize;
        const int ksize = params.ksize;
        const int ghostWidth = params.ghostWidth;

#ifdef USE_MPI
        const int i_mpi = params.myMpiPos[IX];
        const int j_mpi = params.myMpiPos[IY];
        const int k_mpi = params.myMpiPos[IZ];
#else
        const int i_mpi = 0;
        const int j_mpi = 0;
        const int k_mpi = 0;
#endif

        const int nx = params.nx;
        const int ny = params.ny;
        const int nz = params.nz;

        const real_t xmin = params.xmin;
        const real_t ymin = params.ymin;
        const real_t zmin = params.zmin;

        const real_t dx = params.dx;
        const real_t dy = params.dy;
        const real_t dz = params.dz;

        const real_t gamma0 = params.settings.gamma0;

        int i,j,k;
        index2coord(index,i,j,k,isize,jsize,ksize);

        real_t x = xmin + dx/2 + (i+nx*i_mpi-ghostWidth)*dx;
        real_t y = ymin + dy/2 + (j+ny*j_mpi-ghostWidth)*dy;
        real_t z = zmin + dz/2 + (k+nz*k_mpi-ghostWidth)*dz;

        real_t tmp = x + y + z;
        if (tmp > 0.5 && tmp < 2.5)
        {
            Udata(i  ,j  ,k  , ID) = 1.0;
            Udata(i  ,j  ,k  , IP) = 1.0/(gamma0-1.0);
            Udata(i  ,j  ,k  , IU) = 0.0;
            Udata(i  ,j  ,k  , IV) = 0.0;
            Udata(i  ,j  ,k  , IW) = 0.0;
        }
        else
        {
            Udata(i  ,j  ,k  , ID) = 0.125;
            Udata(i  ,j  ,k  , IP) = 0.14/(gamma0-1.0);
            Udata(i  ,j  ,k  , IU) = 0.0;
            Udata(i  ,j  ,k  , IV) = 0.0;
            Udata(i  ,j  ,k  , IW) = 0.0;
        }

    } // end operator ()

    DataArray Udata;

}; // InitImplodeFunctor3D


class InitBlastFunctor3D : public HydroBaseFunctor3D
{

public:
    InitBlastFunctor3D(HydroParams params,
                       BlastParams bParams,
                       DataArray Udata) :
        HydroBaseFunctor3D(params), bParams(bParams), Udata(Udata) {};

    static void apply(HydroParams params, BlastParams blastParams,
                      DataArray Udata, int nbCells)
    {
        InitBlastFunctor3D functor(params, blastParams, Udata);
        Kokkos::parallel_for(nbCells, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int index) const
    {

        const int isize = params.isize;
        const int jsize = params.jsize;
        const int ksize = params.ksize;
        const int ghostWidth = params.ghostWidth;

#ifdef USE_MPI
        const int i_mpi = params.myMpiPos[IX];
        const int j_mpi = params.myMpiPos[IY];
        const int k_mpi = params.myMpiPos[IZ];
#else
        const int i_mpi = 0;
        const int j_mpi = 0;
        const int k_mpi = 0;
#endif

        const int nx = params.nx;
        const int ny = params.ny;
        const int nz = params.nz;

        const real_t xmin = params.xmin;
        const real_t ymin = params.ymin;
        const real_t zmin = params.zmin;

        const real_t dx = params.dx;
        const real_t dy = params.dy;
        const real_t dz = params.dz;

        const real_t gamma0 = params.settings.gamma0;

        // blast problem parameters
        const real_t blast_radius      = bParams.blast_radius;
        const real_t radius2           = blast_radius*blast_radius;
        const real_t blast_center_x    = bParams.blast_center_x;
        const real_t blast_center_y    = bParams.blast_center_y;
        const real_t blast_center_z    = bParams.blast_center_z;
        const real_t blast_density_in  = bParams.blast_density_in;
        const real_t blast_density_out = bParams.blast_density_out;
        const real_t blast_pressure_in = bParams.blast_pressure_in;
        const real_t blast_pressure_out= bParams.blast_pressure_out;


        int i,j,k;
        index2coord(index,i,j,k,isize,jsize,ksize);

        real_t x = xmin + dx/2 + (i+nx*i_mpi-ghostWidth)*dx;
        real_t y = ymin + dy/2 + (j+ny*j_mpi-ghostWidth)*dy;
        real_t z = zmin + dz/2 + (k+nz*k_mpi-ghostWidth)*dz;

        real_t d2 =
            (x-blast_center_x)*(x-blast_center_x)+
            (y-blast_center_y)*(y-blast_center_y)+
            (z-blast_center_z)*(z-blast_center_z);

        if (d2 < radius2)
        {
            Udata(i  ,j  ,k  , ID) = blast_density_in;
            Udata(i  ,j  ,k  , IP) = blast_pressure_in/(gamma0-1.0);
            Udata(i  ,j  ,k  , IU) = 0.0;
            Udata(i  ,j  ,k  , IV) = 0.0;
            Udata(i  ,j  ,k  , IW) = 0.0;
        }
        else
        {
            Udata(i  ,j  ,k  , ID) = blast_density_out;
            Udata(i  ,j  ,k  , IP) = blast_pressure_out/(gamma0-1.0);
            Udata(i  ,j  ,k  , IU) = 0.0;
            Udata(i  ,j  ,k  , IV) = 0.0;
            Udata(i  ,j  ,k  , IW) = 0.0;
        }

    } // end operator ()

    BlastParams bParams;
    DataArray Udata;
}; // InitBlastFunctor3D

} // namespace all_regime

} // namespace ark_rt
