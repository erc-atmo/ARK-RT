// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "shared/kokkos_shared.h"
#include "shared/HydroParams.h"
#include "shared/HydroState.h"
#include "shared/units.h"

namespace ark_rt { namespace all_regime
{

/**
 * Base class to derive actual kokkos functor for hydro 2D.
 * params is passed by copy.
 */
class HydroBaseFunctor2D
{
public:
    using HydroState     = HydroState2d;
    using DataArray      = DataArray2d;
    using DataArrayConst = DataArray2dConst;

    HydroBaseFunctor2D(HydroParams params_) :
        params(params_), nbvar(params_.nbvar) {};
    virtual ~HydroBaseFunctor2D() {};

    const HydroParams params;
    const int nbvar;

    /**
     * Compute gravitational potential at position (x, y)
     * @param[in]  x    x-coordinate
     * @param[in]  y    y-coordinate
     * @param[out] phi  gravitational potential
     */
    KOKKOS_INLINE_FUNCTION
    real_t phi(real_t x, real_t y) const
    {
        return - params.settings.g_x * x - params.settings.g_y * y;
    } // phi

    /**
     * Compute gravitational potential at the center
     * of the cell C(i, j)
     * @param[in]  i    logical x-coordinate of the cell C
     * @param[in]  j    logical y-coordinate of the cell C
     * @param[out] phi  gravitational potential
     */
    KOKKOS_INLINE_FUNCTION
    real_t phi(int i, int j) const
    {
#ifdef USE_MPI
        const int nx_mpi = params.nx * params.myMpiPos[IX];
        const int ny_mpi = params.ny * params.myMpiPos[IY];
        const real_t x = params.xmin + (HALF_F + i + nx_mpi - params.ghostWidth)*params.dx;
        const real_t y = params.ymin + (HALF_F + j + ny_mpi - params.ghostWidth)*params.dy;
#else
        const real_t x = params.xmin + (HALF_F + i - params.ghostWidth)*params.dx;
        const real_t y = params.ymin + (HALF_F + j - params.ghostWidth)*params.dy;
#endif

        return phi(x, y);
    } // phi

    /**
     * Compute \mathcal{M} = \rho * \Delta \phi between two cells
     * (to be renamed)
     */
    KOKKOS_INLINE_FUNCTION
    real_t computeM(const HydroState& qLoc, real_t phiLoc,
                    const HydroState& qNei, real_t phiNei) const
    {
        return HALF_F * (qLoc[ID]+qNei[ID]) * (phiNei - phiLoc);
    } // computeM

    /**
     * Get HydroState from global array (either conservative or primitive)
     * at cell C(i, j) (global to local)
     * @param[in]  array global array
     * @param[in]  i     logical x-coordinate of the cell C
     * @param[in]  j     logical y-coordinate of the cell C
     * @param[out] state HydroState of cell C(i, j)
     */
    KOKKOS_INLINE_FUNCTION
    HydroState getHydroState(DataArrayConst array, int i, int j) const
    {
        HydroState state;
        state[ID] = array(i, j, ID);
        state[IP] = array(i, j, IP);
        state[IS] = array(i, j, IS);
        state[IU] = array(i, j, IU);
        state[IV] = array(i, j, IV);
        return state;
    }

    /**
     * Set HydroState to global array (either conservative or primitive)
     * at cell C(i, j) (local to global)
     * @param[in, out]  array global array
     * @param[in]       state HydroState of cell C(i, j)
     * @param[in]       i     logical x-coordinate of the cell C
     * @param[in]       j     logical y-coordinate of the cell C
     */
    KOKKOS_INLINE_FUNCTION
    void setHydroState(DataArray array, const HydroState& state, int i, int j) const
    {
        array(i, j, ID) = state[ID];
        array(i, j, IP) = state[IP];
        array(i, j, IS) = state[IS];
        array(i, j, IU) = state[IU];
        array(i, j, IV) = state[IV];
    }

    /**
     * Compute mean molecular weight from passive scalar
     * @param[in]  q   primitive variables array
     * @param[out] mmw mean molecular weight
     */
    KOKKOS_INLINE_FUNCTION
    real_t computeMmw(const HydroState& q) const
    {
        return params.settings.mmw1*q[IS] + params.settings.mmw2*(ONE_F-q[IS]);
    }

    /**
     * Compute temperature using ideal gas law
     * @param[in]  q  primitive variables array
     * @param[out] T  temperature
     */
    KOKKOS_INLINE_FUNCTION
    real_t computeTemperature(const HydroState& q) const
    {
        const real_t mmw = computeMmw(q);
        return mmw * code_units::constants::m_h * q[IP] / (q[ID] * code_units::constants::k_b);
    }

    KOKKOS_INLINE_FUNCTION
    real_t computeOpacity(const HydroState& q) const
    {
        return params.settings.kap1*q[IS] + params.settings.kap2*(ONE_F-q[IS]);
    }

    /**
     * Compute speed of sound using ideal gas law
     * @param[in]  q  primitive variables array
     * @param[out] c  speed of sound
     */
    KOKKOS_INLINE_FUNCTION
    real_t computeSpeedSound(const HydroState& q) const
    {
        return SQRT(params.settings.gamma0 * q[IP] / q[ID]);
    } // computeSpeedSound

    /**
     * Convert conservative variables (rho, rho*u, rho*v, rho*E) to
     * primitive variables (rho, u, v, p) using ideal gas law
     * @param[in]  u  conservative variables array
     * @param[out] q  primitive    variables array
     */
    KOKKOS_INLINE_FUNCTION
    HydroState computePrimitives(const HydroState& u) const
    {
        const real_t gamma0 = params.settings.gamma0;
        const real_t invD = ONE_F / u[ID];
        const real_t ekin = HALF_F*(u[IU]*u[IU]+u[IV]*u[IV])*invD;

        HydroState q;
        q[ID] = u[ID];
        q[IP] = (gamma0-ONE_F) * (u[IE] - ekin);
        q[IS] = u[IS] * invD;
        q[IU] = u[IU] * invD;
        q[IV] = u[IV] * invD;

        return q;
    } // computePrimitives

    /**
     * Convert primitive variables (rho, p, u, v) to
     * conservative variables (rho, rho*E, rho*u, rho*v) using ideal gas law
     * @param[in]  q  primitive    variables array
     * @param[out] u  conservative variables array
     */
    KOKKOS_INLINE_FUNCTION
    HydroState computeConservatives(const HydroState& q) const
    {
        const real_t gamma0 = params.settings.gamma0;
        const real_t ekin = HALF_F*q[ID]*(q[IU]*q[IU]+q[IV]*q[IV]);

        HydroState u;
        u[ID] = q[ID];
        u[IE] = q[IP] / (gamma0-ONE_F) + ekin;
        u[IS] = q[ID] * q[IS];
        u[IU] = q[ID] * q[IU];
        u[IV] = q[ID] * q[IV];

        return u;
    } // computeConservatives
}; // class HydroBaseFunctor2D

} // namespace all_regime

} // namespace ark_rt
