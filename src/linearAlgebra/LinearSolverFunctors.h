// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "shared/kokkos_shared.h"
#include "shared/enums.h"
#include "shared/HydroParams.h"
#include "Trilinos_shared.h"

// kokkos random numbers
#include <Kokkos_Random.hpp>

#ifdef USE_MPI
#include <mpi.h>
#endif

namespace ark_rt{ namespace linearAlgebra{


  template<int dim>
    class UpdateUdataLinearSystemLinearSolverFunctors
    {
      public:
        using DataArray = typename Trilinos_type<dim>::DataArray;
        using vector_view = typename Trilinos_type<dim>::vector_view_type;
        UpdateUdataLinearSystemLinearSolverFunctors(DataArray Udata_, vector_view b_, HydroParams params_, size_t nbVar_):
          Udata(Udata_), b(b_), params(params_), nbVar(nbVar_) {};

        static void apply(DataArray Udata, 
            vector_view b,
            int nbCells,
            HydroParams params,
            size_t nbVar)
        {
          UpdateUdataLinearSystemLinearSolverFunctors functor(Udata, b, params, nbVar);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(int index) const
          {
            int i,j;
            linearAlgebra_index2coord(index, i,j, params.nx, params.ny);
            i += params.ghostWidth;
            j += params.ghostWidth;

            // Use local indices to access the entries of the vector
            Udata(i, j, IERAD) = b(nbVar*index);
            Udata(i, j, ITRAD) = b(nbVar*index+1);
            Udata(i, j, IFX) = b(nbVar*index+2);
            Udata(i, j, IFY) = b(nbVar*index+3);

          }

        DataArray Udata;
        vector_view b;
        HydroParams params;
        size_t nbVar;
    }; // UpdateUdataLinearSystemLinearSolverFunctor

  template<int dim>
    class Udata2vectorLinearSolverFunctors
    {
      public:
        using DataArray = typename Trilinos_type<dim>::DataArray;
        using vector_view = typename Trilinos_type<dim>::vector_view_type;
        Udata2vectorLinearSolverFunctors(DataArray Udata_, vector_view u_, HydroParams params_, size_t nbVar_):
          Udata(Udata_), u(u_), params(params_), nbVar(nbVar_) {};

        static void apply(DataArray Udata, 
            vector_view u,
            int nbCells,
            HydroParams params, 
            size_t nbVar)
        {
          Udata2vectorLinearSolverFunctors functor(Udata, u,params, nbVar);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(int index) const
          {
            int i,j;
            linearAlgebra_index2coord(index, i,j, params.nx, params.ny);
            i += params.ghostWidth;
            j += params.ghostWidth;

            // Use local indices to access the entries of the vector
            u(nbVar*index) = Udata(i, j, IERAD);
            u(nbVar*index+1) = Udata(i, j, ITRAD);
            u(nbVar*index+2) = Udata(i, j, IFX);
            u(nbVar*index+3) = Udata(i, j, IFY);

          }

        DataArray Udata;
        vector_view u;
        HydroParams params;
        size_t nbVar;
    }; // Udata2vectorLinearSolverFunctor

  template<int dim>
    class ComputeVarrelLinearSolverFunctors
    {
      public:
        using DataArray = typename Trilinos_type<dim>::DataArray;
        ComputeVarrelLinearSolverFunctors(const HydroParams& params_, DataArray Unp1_, DataArray Un_, VarIndex varToReduce_):
          params(params_), Unp1(Unp1_), Un(Un_), varToReduce(varToReduce_) {};

        static void apply(const HydroParams& params,
            DataArray Unp1,
            DataArray Un, 
            VarIndex varToReduce,
            real_t& varrel,
            int nbCells)
        {
          ComputeVarrelLinearSolverFunctors functor(params, Unp1, Un, varToReduce);
          Kokkos::parallel_reduce(nbCells, functor, varrel);
        }


        // Tell each thread how to initialize its reduction result.
        KOKKOS_INLINE_FUNCTION
          void init (real_t& dst) const
          {
            // The identity under max is -Inf.
            // Kokkos does not come with a portable way to access
            // floating-point Inf and NaN.
#ifdef __CUDA_ARCH__
            dst = -CUDART_INF;
#else
            dst = std::numeric_limits<real_t>::min();
#endif // __CUDA_ARCH__
          } // init

        /* this is a reduce (max) functor */
        KOKKOS_INLINE_FUNCTION
          void operator()(const int index, real_t& varrel) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;
            int i,j;
            index2coord(index,i,j,isize,jsize);

            varrel = FMAX(varrel, FABS((Unp1(i, j, varToReduce)-Un(i, j, varToReduce))/Un(i, j, varToReduce)));

          } // operator()

        // "Join" intermediate results from different threads.
        // This should normally implement the same reduction
        // operation as operator() above. Note that both input
        // arguments MUST be declared volatile.
        KOKKOS_INLINE_FUNCTION
          void join (volatile real_t& dst,
              const volatile real_t& src) const
          {
            // max reduce
            if (dst < src)
              dst = src;
          } // join

        HydroParams params;
        DataArray Unp1;
        DataArray Un;
        VarIndex varToReduce;
    }; // ComputeVarrelLinearSolverFunctors


  template<int dim>
    class ComputeMaxOpacityLinearSolverFunctors
    {
      public:
        using DataArray = typename Trilinos_type<dim>::DataArray;
        ComputeMaxOpacityLinearSolverFunctors(const HydroParams& params_, DataArray Udata_):
          params(params_), Udata(Udata_){};

        static void apply(const HydroParams& params,
            DataArray Udata,
            real_t& max_opacity,
            int nbCells)
        {
          ComputeMaxOpacityLinearSolverFunctors functor(params, Udata);
          Kokkos::parallel_reduce(nbCells, functor, max_opacity);
        }


        // Tell each thread how to initialize its reduction result.
        KOKKOS_INLINE_FUNCTION
          void init (real_t& dst) const
          {
            // The identity under max is -Inf.
            // Kokkos does not come with a portable way to access
            // floating-point Inf and NaN.
#ifdef __CUDA_ARCH__
            dst = -CUDART_INF;
#else
            dst = std::numeric_limits<real_t>::min();
#endif // __CUDA_ARCH__
          } // init

        /* this is a reduce (max) functor */
        KOKKOS_INLINE_FUNCTION
          void operator()(const int index, real_t& max_opacity) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;
            int i,j;
            index2coord(index,i,j,isize,jsize);

            max_opacity = FMAX(max_opacity, Udata(i, j, IO));

          } // operator()

        // "Join" intermediate results from different threads.
        // This should normally implement the same reduction
        // operation as operator() above. Note that both input
        // arguments MUST be declared volatile.
        KOKKOS_INLINE_FUNCTION
          void join (volatile real_t& dst,
              const volatile real_t& src) const
          {
            // max reduce
            if (dst < src)
              dst = src;
          } // join

        HydroParams params;
        DataArray Udata;
    }; // ComputeMaxOpacityLinearSolverFunctors

  struct LinearSolverFunctors
  {
    template <int dim>
      using UpdateUdataLinearSystem = UpdateUdataLinearSystemLinearSolverFunctors<dim>;
    template <int dim>
      using Udata2vector = Udata2vectorLinearSolverFunctors<dim>;
    template <int dim>
      using ComputeVarrel = ComputeVarrelLinearSolverFunctors<dim>;
    template <int dim>
      using ComputeMaxOpacity = ComputeMaxOpacityLinearSolverFunctors<dim>;
  };

} // namespace linearAlgebra
}//namespace ark_rt
