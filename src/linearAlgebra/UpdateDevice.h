// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "shared/kokkos_shared.h"
#include "shared/HydroParams.h"
#include "all_regime_radiative_transfer/eddingtonTensor.h"

#include "Trilinos_shared.h"

namespace ark_rt { namespace linearAlgebra{

  template<int dim>
    class UpdateLinearProblem
    {
      public:
        using local_ordinal_type = typename Trilinos_type<dim>::local_ordinal_type;
        using scalar_type = typename Trilinos_type<dim>::scalar_type;
        using vector_view_type = typename Trilinos_type<dim>::vector_view_type;
        using local_matrix_type = typename Trilinos_type<dim>::local_matrix_type;
        using local_map_type = typename Trilinos_type<dim>::local_map_type;
        using DataArray = typename Trilinos_type<dim>::DataArray;

        UpdateLinearProblem(const HydroParams& params_, local_matrix_type A_, local_map_type map_, vector_view_type b_, int nbVar_, DataArray Udata_, DataArray UdataOld_, real_t dt_, implicit_boundary problem_name_, bool isSorted_):
          params(params_), A(A_), map(map_), b(b_),nbVar(nbVar_), Udata(Udata_), UdataOld(UdataOld_), dt(dt_), problem_name(problem_name_), isSorted(isSorted_) {};

        static void apply(const HydroParams& params,
            int numMyElements,
            local_matrix_type A,
            local_map_type map,
            vector_view_type b,
            int nbVar,
            DataArray Udata,
            DataArray UdataOld,
            real_t dt,
            implicit_boundary problem_name,
            bool isSorted)
        {
          UpdateLinearProblem functor(params, A, map, b, nbVar, Udata, UdataOld, dt, problem_name, isSorted);
          Kokkos::parallel_for(numMyElements, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(int index) const
          {
            // 0 <= index < nx*ny
            const int nx = params.nx;
            const int ny = params.ny;
            const int ghostWidth = params.ghostWidth;
            const bool useAsymptoticCorrection = params.settings.useAsymptoticCorrection;
            const bool useP1 = params.settings.useP1;
            const bool wellBalancedScheme = params.settings.wellBalancedScheme;
            const real_t dx = params.dx;
            const real_t dy = params.dy;
            const real_t c = params.settings.speedOfLight;
            const real_t a = params.settings.radiativeConstant;
            const real_t c2 = c*c;
            int i,j;
            const int myRank = params.myRank;
            linearAlgebra_index2coord(index, i, j, nx, ny);

            const real_t rho = Udata(i+ghostWidth, j+ghostWidth, ID);
            const real_t rho_cv = rho*params.settings.cv;

            real_t lambda_ipj_p, lambda_ipj_m, alpha_ipj;
            real_t lambda_imj_p, lambda_imj_m, alpha_imj;
            real_t lambda_ijp_p, lambda_ijp_m, alpha_ijp;
            real_t lambda_ijm_p, lambda_ijm_m, alpha_ijm;

            real_t Dxx_ij, Dxy_ij, Dyx_ij, Dyy_ij;
            real_t Dxx_imj, Dxy_imj, Dyx_imj, Dyy_imj;
            real_t Dxx_ipj, Dxy_ipj, Dyx_ipj, Dyy_ipj;
            real_t Dxx_ijm, Dxy_ijm, Dyx_ijm, Dyy_ijm;
            real_t Dxx_ijp, Dxy_ijp, Dyx_ijp, Dyy_ijp;

            // 0 <= i < nx and 0 <= j < ny
            // because Udata has ghost cells
            // we have Udata(0:nx+2*ghostWidth, 0:ny+2*ghostWidth)
            // so we "rescale" the indices
            const real_t Eij = Udata(i+ghostWidth,j+ghostWidth, IERAD);
            const real_t Tij = Udata(i+ghostWidth,j+ghostWidth, ITRAD);
            const real_t Fxij = Udata(i+ghostWidth,j+ghostWidth, IFX);
            const real_t Fyij = Udata(i+ghostWidth,j+ghostWidth, IFY);

            const real_t EijTimePrev = UdataOld(i+ghostWidth, j+ghostWidth, IERAD);
            const real_t TijTimePrev = UdataOld(i+ghostWidth, j+ghostWidth, ITRAD);
            const real_t FxijTimePrev = UdataOld(i+ghostWidth, j+ghostWidth, IFX);
            const real_t FyijTimePrev = UdataOld(i+ghostWidth, j+ghostWidth, IFY);

            const real_t Eimj = Udata(i+ghostWidth-1, j+ghostWidth, IERAD);
            const real_t Fximj = Udata(i+ghostWidth-1, j+ghostWidth, IFX);
            const real_t Fyimj = Udata(i+ghostWidth-1, j+ghostWidth, IFY);

            const real_t Eipj = Udata(i+ghostWidth+1, j+ghostWidth, IERAD);
            const real_t Fxipj = Udata(i+ghostWidth+1, j+ghostWidth, IFX);
            const real_t Fyipj = Udata(i+ghostWidth+1, j+ghostWidth, IFY);

            const real_t Eijm = Udata(i+ghostWidth, j+ghostWidth-1, IERAD);
            const real_t Fxijm = Udata(i+ghostWidth, j+ghostWidth-1, IFX);
            const real_t Fyijm = Udata(i+ghostWidth, j+ghostWidth-1, IFY);

            const real_t Eijp = Udata(i+ghostWidth, j+ghostWidth+1, IERAD);
            const real_t Fxijp = Udata(i+ghostWidth, j+ghostWidth+1, IFX);
            const real_t Fyijp = Udata(i+ghostWidth, j+ghostWidth+1, IFY);

            const real_t EimjTimePrev = UdataOld(i+ghostWidth-1, j+ghostWidth, IERAD);
            const real_t FximjTimePrev = UdataOld(i+ghostWidth-1, j+ghostWidth, IFX);
            const real_t FyimjTimePrev = UdataOld(i+ghostWidth-1, j+ghostWidth, IFY);

            const real_t EipjTimePrev = UdataOld(i+ghostWidth+1, j+ghostWidth, IERAD);
            const real_t FxipjTimePrev = UdataOld(i+ghostWidth+1, j+ghostWidth, IFX);
            const real_t FyipjTimePrev = UdataOld(i+ghostWidth+1, j+ghostWidth, IFY);

            const real_t EijmTimePrev = UdataOld(i+ghostWidth, j+ghostWidth-1, IERAD);
            const real_t FxijmTimePrev = UdataOld(i+ghostWidth, j+ghostWidth-1, IFX);
            const real_t FyijmTimePrev = UdataOld(i+ghostWidth, j+ghostWidth-1, IFY);

            const real_t EijpTimePrev = UdataOld(i+ghostWidth, j+ghostWidth+1, IERAD);
            const real_t FxijpTimePrev = UdataOld(i+ghostWidth, j+ghostWidth+1, IFX);
            const real_t FyijpTimePrev = UdataOld(i+ghostWidth, j+ghostWidth+1, IFY);

            const real_t sigma = Udata(i+ghostWidth, j+ghostWidth, IO);
            const real_t sigma_ipj = HALF_F*(sigma+Udata(i+ghostWidth+1, j+ghostWidth, IO));
            const real_t sigma_imj = HALF_F*(sigma+Udata(i+ghostWidth-1, j+ghostWidth, IO));
            const real_t sigma_ijp = HALF_F*(sigma+Udata(i+ghostWidth, j+ghostWidth+1, IO));
            const real_t sigma_ijm = HALF_F*(sigma+Udata(i+ghostWidth, j+ghostWidth-1, IO));

            real_t dPxxdE_ij, dPxxdFx_ij, dPxxdFy_ij, dPxydE_ij, dPxydFx_ij, dPxydFy_ij, dPyxdE_ij, dPyxdFx_ij, dPyxdFy_ij, dPyydE_ij, dPyydFx_ij, dPyydFy_ij;
            real_t dPxxdE_imj, dPxxdFx_imj, dPxxdFy_imj, dPxydE_imj, dPxydFx_imj, dPxydFy_imj, dPyxdE_imj, dPyxdFx_imj, dPyxdFy_imj, dPyydE_imj, dPyydFx_imj, dPyydFy_imj;
            real_t dPxxdE_ipj, dPxxdFx_ipj, dPxxdFy_ipj, dPxydE_ipj, dPxydFx_ipj, dPxydFy_ipj, dPyxdE_ipj, dPyxdFx_ipj, dPyxdFy_ipj, dPyydE_ipj, dPyydFx_ipj, dPyydFy_ipj;
            real_t dPxxdE_ijm, dPxxdFx_ijm, dPxxdFy_ijm, dPxydE_ijm, dPxydFx_ijm, dPxydFy_ijm, dPyxdE_ijm, dPyxdFx_ijm, dPyxdFy_ijm, dPyydE_ijm, dPyydFx_ijm, dPyydFy_ijm;
            real_t dPxxdE_ijp, dPxxdFx_ijp, dPxxdFy_ijp, dPxydE_ijp, dPxydFx_ijp, dPxydFy_ijp, dPyxdE_ijp, dPyxdFx_ijp, dPyxdFy_ijp, dPyydE_ijp, dPyydFx_ijp, dPyydFy_ijp;

            if(!useP1) // M1 model
            {
              all_regime_radiative_transfer::eddingtonTensor(Eij, Fxij, Fyij, Dxx_ij, Dxy_ij, Dyx_ij, Dyy_ij, c, params.settings.replaceEddingtonFactor);
              all_regime_radiative_transfer::eddingtonTensor(Eimj, Fximj, Fyimj, Dxx_imj, Dxy_imj, Dyx_imj, Dyy_imj, c, params.settings.replaceEddingtonFactor);
              all_regime_radiative_transfer::eddingtonTensor(Eipj, Fxipj, Fyipj, Dxx_ipj, Dxy_ipj, Dyx_ipj, Dyy_ipj, c, params.settings.replaceEddingtonFactor);
              all_regime_radiative_transfer::eddingtonTensor(Eijm, Fxijm, Fyijm, Dxx_ijm, Dxy_ijm, Dyx_ijm, Dyy_ijm, c, params.settings.replaceEddingtonFactor);
              all_regime_radiative_transfer::eddingtonTensor(Eijp, Fxijp, Fyijp, Dxx_ijp, Dxy_ijp, Dyx_ijp, Dyy_ijp, c, params.settings.replaceEddingtonFactor);
              all_regime_radiative_transfer::eddingtonTensor_derivatives(Eij, Fxij, Fyij, dPxxdE_ij, dPxxdFx_ij, dPxxdFy_ij, dPxydE_ij, dPxydFx_ij, dPxydFy_ij, dPyxdE_ij, dPyxdFx_ij, dPyxdFy_ij, dPyydE_ij, dPyydFx_ij, dPyydFy_ij, c, params.settings.replaceEddingtonFactor);
              all_regime_radiative_transfer::eddingtonTensor_derivatives(Eimj, Fximj, Fyimj, dPxxdE_imj, dPxxdFx_imj, dPxxdFy_imj, dPxydE_imj, dPxydFx_imj, dPxydFy_imj, dPyxdE_imj, dPyxdFx_imj, dPyxdFy_imj, dPyydE_imj, dPyydFx_imj, dPyydFy_imj, c, params.settings.replaceEddingtonFactor);
              all_regime_radiative_transfer::eddingtonTensor_derivatives(Eipj, Fxipj, Fyipj, dPxxdE_ipj, dPxxdFx_ipj, dPxxdFy_ipj, dPxydE_ipj, dPxydFx_ipj, dPxydFy_ipj, dPyxdE_ipj, dPyxdFx_ipj, dPyxdFy_ipj, dPyydE_ipj, dPyydFx_ipj, dPyydFy_ipj, c, params.settings.replaceEddingtonFactor);
              all_regime_radiative_transfer::eddingtonTensor_derivatives(Eijm, Fxijm, Fyijm, dPxxdE_ijm, dPxxdFx_ijm, dPxxdFy_ijm, dPxydE_ijm, dPxydFx_ijm, dPxydFy_ijm, dPyxdE_ijm, dPyxdFx_ijm, dPyxdFy_ijm, dPyydE_ijm, dPyydFx_ijm, dPyydFy_ijm, c, params.settings.replaceEddingtonFactor);
              all_regime_radiative_transfer::eddingtonTensor_derivatives(Eijp, Fxijp, Fyijp, dPxxdE_ijp, dPxxdFx_ijp, dPxxdFy_ijp, dPxydE_ijp, dPxydFx_ijp, dPxydFy_ijp, dPyxdE_ijp, dPyxdFx_ijp, dPyxdFy_ijp, dPyydE_ijp, dPyydFx_ijp, dPyydFy_ijp, c, params.settings.replaceEddingtonFactor);

              all_regime_radiative_transfer::eigenvaluesRT(lambda_ipj_m, lambda_ipj_p, c, EijTimePrev, FxijTimePrev, FyijTimePrev, EipjTimePrev, FxipjTimePrev, FyipjTimePrev, params.settings.useExactEigenvalues, params.settings.replaceEddingtonFactor, params.settings.valpmin);
              all_regime_radiative_transfer::eigenvaluesRT(lambda_imj_m, lambda_imj_p, c, EimjTimePrev, FximjTimePrev, FyimjTimePrev, EijTimePrev, FxijTimePrev, FyijTimePrev, params.settings.useExactEigenvalues, params.settings.replaceEddingtonFactor, params.settings.valpmin);
              all_regime_radiative_transfer::eigenvaluesRT(lambda_ijp_m, lambda_ijp_p, c, EijTimePrev, FyijTimePrev, FxijTimePrev, EijpTimePrev, FyijpTimePrev, FxijpTimePrev, params.settings.useExactEigenvalues, params.settings.replaceEddingtonFactor, params.settings.valpmin); // swap Fx and Fy
              all_regime_radiative_transfer::eigenvaluesRT(lambda_ijm_m, lambda_ijm_p, c, EijmTimePrev, FyijmTimePrev, FxijmTimePrev, EijTimePrev, FyijTimePrev, FxijTimePrev, params.settings.useExactEigenvalues, params.settings.replaceEddingtonFactor, params.settings.valpmin); // swap Fx and Fy
            }
            else // P1 model
            {
              Dxx_ij = c2*Eij/3.; Dxy_ij = ZERO_F; Dyx_ij = ZERO_F; Dyy_ij = c2*Eij/3.;
              Dxx_ipj = c2*Eipj/3.; Dxy_ipj = ZERO_F; Dyx_ipj = ZERO_F; Dyy_ipj = c2*Eipj/3.;
              Dxx_imj = c2*Eimj/3.; Dxy_imj = ZERO_F; Dyx_imj = ZERO_F; Dyy_imj = c2*Eimj/3.;
              Dxx_ijp = c2*Eijp/3.; Dxy_ijp = ZERO_F; Dyx_ijp = ZERO_F; Dyy_ijp = c2*Eijp/3.;
              Dxx_ijm = c2*Eijm/3.; Dxy_ijm = ZERO_F; Dyx_ijm = ZERO_F; Dyy_ijm = c2*Eijm/3.;
              dPxxdE_ij = c2/3.; dPxxdFx_ij = ZERO_F; dPxxdFy_ij = ZERO_F; dPxydE_ij = ZERO_F; dPxydFx_ij = ZERO_F; dPxydFy_ij = ZERO_F; dPyxdE_ij = ZERO_F; dPyxdFx_ij = ZERO_F; dPyxdFy_ij = ZERO_F; dPyydE_ij = c2/3.; dPyydFx_ij = ZERO_F; dPyydFy_ij = ZERO_F; 
              dPxxdE_ipj = c2/3.; dPxxdFx_ipj = ZERO_F; dPxxdFy_ipj = ZERO_F; dPxydE_ipj = ZERO_F; dPxydFx_ipj = ZERO_F; dPxydFy_ipj = ZERO_F; dPyxdE_ipj = ZERO_F; dPyxdFx_ipj = ZERO_F; dPyxdFy_ipj = ZERO_F; dPyydE_ipj = c2/3.; dPyydFx_ipj = ZERO_F; dPyydFy_ipj = ZERO_F; 
              dPxxdE_imj = c2/3.; dPxxdFx_imj = ZERO_F; dPxxdFy_imj = ZERO_F; dPxydE_imj = ZERO_F; dPxydFx_imj = ZERO_F; dPxydFy_imj = ZERO_F; dPyxdE_imj = ZERO_F; dPyxdFx_imj = ZERO_F; dPyxdFy_imj = ZERO_F; dPyydE_imj = c2/3.; dPyydFx_imj = ZERO_F; dPyydFy_imj = ZERO_F; 
              dPxxdE_ijp = c2/3.; dPxxdFx_ijp = ZERO_F; dPxxdFy_ijp = ZERO_F; dPxydE_ijp = ZERO_F; dPxydFx_ijp = ZERO_F; dPxydFy_ijp = ZERO_F; dPyxdE_ijp = ZERO_F; dPyxdFx_ijp = ZERO_F; dPyxdFy_ijp = ZERO_F; dPyydE_ijp = c2/3.; dPyydFx_ijp = ZERO_F; dPyydFy_ijp = ZERO_F; 
              dPxxdE_ijm = c2/3.; dPxxdFx_ijm = ZERO_F; dPxxdFy_ijm = ZERO_F; dPxydE_ijm = ZERO_F; dPxydFx_ijm = ZERO_F; dPxydFy_ijm = ZERO_F; dPyxdE_ijm = ZERO_F; dPyxdFx_ijm = ZERO_F; dPyxdFy_ijm = ZERO_F; dPyydE_ijm = c2/3.; dPyydFx_ijm = ZERO_F; dPyydFy_ijm = ZERO_F; 
              lambda_ipj_p = c/SQRT(3.); lambda_ipj_m = -c/SQRT(3.);
              lambda_imj_p = c/SQRT(3.); lambda_imj_m = -c/SQRT(3.);
              lambda_ijp_p = c/SQRT(3.); lambda_ijp_m = -c/SQRT(3.);
              lambda_ijm_p = c/SQRT(3.); lambda_ijm_m = -c/SQRT(3.);
            }
            if(useAsymptoticCorrection)
            {
              real_t f2;
              const real_t f_ij = SQRT(all_regime_radiative_transfer::computeReducedFluxSquare(EijTimePrev, FxijTimePrev, FyijTimePrev, c));
              const real_t f_ipj = SQRT(all_regime_radiative_transfer::computeReducedFluxSquare(EipjTimePrev, FxipjTimePrev, FyipjTimePrev, c));
              const real_t f_imj = SQRT(all_regime_radiative_transfer::computeReducedFluxSquare(EimjTimePrev, FximjTimePrev, FyimjTimePrev, c));
              const real_t f_ijp = SQRT(all_regime_radiative_transfer::computeReducedFluxSquare(EijpTimePrev, FxijpTimePrev, FyijpTimePrev, c));
              const real_t f_ijm = SQRT(all_regime_radiative_transfer::computeReducedFluxSquare(EijmTimePrev, FxijmTimePrev, FyijmTimePrev, c));
              f2 = HALF_F*HALF_F*(f_ij+f_ipj)*(f_ij+f_ipj);
              alpha_ipj = ONE_F/(ONE_F-(ONE_F-f2)*3.*sigma_ipj*dx*lambda_ipj_p*lambda_ipj_m/(c*(lambda_ipj_p-lambda_ipj_m)));
              f2 = HALF_F*HALF_F*(f_ij+f_imj)*(f_ij+f_imj);
              alpha_imj = ONE_F/(ONE_F-(ONE_F-f2)*3.*sigma_imj*dx*lambda_imj_p*lambda_imj_m/(c*(lambda_imj_p-lambda_imj_m)));
              f2 = HALF_F*HALF_F*(f_ij+f_ijp)*(f_ij+f_ijp);
              alpha_ijp = ONE_F/(ONE_F-(ONE_F-f2)*3.*sigma_ijp*dy*lambda_ijp_p*lambda_ijp_m/(c*(lambda_ijp_p-lambda_ijp_m)));
              f2 = HALF_F*HALF_F*(f_ij+f_ijm)*(f_ij+f_ijm);
              alpha_ijm = ONE_F/(ONE_F-(ONE_F-f2)*3.*sigma_ijm*dy*lambda_ijm_p*lambda_ijm_m/(c*(lambda_ijm_p-lambda_ijm_m)));
            }
            else
            {
              alpha_ipj = ONE_F;
              alpha_imj = ONE_F;
              alpha_ijp = ONE_F;
              alpha_ijm = ONE_F;
            }

            const real_t dtdx_lplm_ipj = dt/dx*lambda_ipj_p*lambda_ipj_m/(lambda_ipj_p-lambda_ipj_m);
            const real_t dtdx_lplm_imj = dt/dx*lambda_imj_p*lambda_imj_m/(lambda_imj_p-lambda_imj_m);
            const real_t dtdy_lplm_ijp = dt/dy*lambda_ijp_p*lambda_ijp_m/(lambda_ijp_p-lambda_ijp_m);
            const real_t dtdy_lplm_ijm = dt/dy*lambda_ijm_p*lambda_ijm_m/(lambda_ijm_p-lambda_ijm_m);
            const real_t dtdx_lp_ipj   = dt/dx*lambda_ipj_p		/(lambda_ipj_p-lambda_ipj_m);
            const real_t dtdx_lm_ipj   = dt/dx		   *lambda_ipj_m/(lambda_ipj_p-lambda_ipj_m);
            const real_t dtdx_lp_imj   = dt/dx*lambda_imj_p		/(lambda_imj_p-lambda_imj_m);
            const real_t dtdx_lm_imj   = dt/dx		   *lambda_imj_m/(lambda_imj_p-lambda_imj_m);
            const real_t dtdy_lp_ijp   = dt/dy*lambda_ijp_p		/(lambda_ijp_p-lambda_ijp_m);
            const real_t dtdy_lm_ijp   = dt/dy		   *lambda_ijp_m/(lambda_ijp_p-lambda_ijp_m);
            const real_t dtdy_lp_ijm   = dt/dy*lambda_ijm_p		/(lambda_ijm_p-lambda_ijm_m);
            const real_t dtdy_lm_ijm   = dt/dy		   *lambda_ijm_m/(lambda_ijm_p-lambda_ijm_m);

            // RHS
            b(nbVar*index) = -Eij + EijTimePrev
              -alpha_ipj*(dtdx_lp_ipj*Fxij  - dtdx_lm_ipj*Fxipj + dtdx_lplm_ipj*(Eipj-Eij ))
              +alpha_imj*(dtdx_lp_imj*Fximj - dtdx_lm_imj*Fxij  + dtdx_lplm_imj*(Eij -Eimj))
              -alpha_ijp*(dtdy_lp_ijp*Fyij  - dtdy_lm_ijp*Fyijp + dtdy_lplm_ijp*(Eijp-Eij ))
              +alpha_ijm*(dtdy_lp_ijm*Fyijm - dtdy_lm_ijm*Fyij  + dtdy_lplm_ijm*(Eij -Eijm))
              +c*sigma*dt*(a*Tij*Tij*Tij*Tij-Eij);

            b(index*nbVar+1) = -rho_cv*Tij + rho_cv*TijTimePrev
              -c*sigma*dt*(a*Tij*Tij*Tij*Tij-Eij);

            b(nbVar*index+2) = -Fxij+FxijTimePrev
              - dtdx_lp_ipj*Dxx_ij  + dtdx_lm_ipj*Dxx_ipj - dtdx_lplm_ipj*(Fxipj-Fxij )
              + dtdx_lp_imj*Dxx_imj - dtdx_lm_imj*Dxx_ij  + dtdx_lplm_imj*(Fxij -Fximj)
              - dtdy_lp_ijp*Dxy_ij  + dtdy_lm_ijp*Dxy_ijp - dtdy_lplm_ijp*(Fxijp-Fxij )
              + dtdy_lp_ijm*Dxy_ijm - dtdy_lm_ijm*Dxy_ij  + dtdy_lplm_ijm*(Fxij -Fxijm);

            b(index*nbVar+3) = -Fyij+FyijTimePrev
              - dtdx_lp_ipj*Dyx_ij  + dtdx_lm_ipj*Dyx_ipj - dtdx_lplm_ipj*(Fyipj-Fyij )
              + dtdx_lp_imj*Dyx_imj - dtdx_lm_imj*Dyx_ij  + dtdx_lplm_imj*(Fyij -Fyimj)
              - dtdy_lp_ijp*Dyy_ij  + dtdy_lm_ijp*Dyy_ijp - dtdy_lplm_ijp*(Fyijp-Fyij )
              + dtdy_lp_ijm*Dyy_ijm - dtdy_lm_ijm*Dyy_ij  + dtdy_lplm_ijm*(Fyij -Fyijm);

            if(wellBalancedScheme)
            {
              b(index*nbVar+2) -= HALF_F*HALF_F*c*dt*(sigma_ipj*(Fxij+Fxipj)+sigma_imj*(Fxij+Fximj));
              b(index*nbVar+3) -= HALF_F*HALF_F*c*dt*(sigma_ijp*(Fyij+Fyijp)+sigma_ijm*(Fyij+Fyijm));
            }
            else
            {
              b(index*nbVar+2) -= c*dt*sigma*Fxij;
              b(index*nbVar+3) -= c*dt*sigma*Fyij;
            }

            // elements i,j
            {
              const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny));
              const local_ordinal_type myLocEl_T = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+1);
              const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+2);
              const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+3);
              // row E, element i,j
              {
                local_ordinal_type col[4];
                scalar_type val[4];
                col[0] = myLocEl_E;
                val[0] = ONE_F - alpha_ipj*dtdx_lplm_ipj - alpha_imj*dtdx_lplm_imj - alpha_ijp*dtdy_lplm_ijp - alpha_ijm*dtdy_lplm_ijm + c*dt*sigma; // E i,j
                col[1] = myLocEl_T;
                val[1] = -dt*c*sigma*a*4.*Tij*Tij*Tij; // T i,j
                col[2] = myLocEl_Fx;
                val[2] = alpha_ipj*dtdx_lp_ipj   + alpha_imj*dtdx_lm_imj; // Fx i,j
                col[3] = myLocEl_Fy;
                val[3] = alpha_ijp*dtdy_lp_ijp   + alpha_ijm*dtdy_lm_ijm; // Fy i,j
                A.replaceValues(nbVar*index, col, 4, val, isSorted);
              }
              // row T, element i,j
              {
                local_ordinal_type col[2];
                scalar_type val[2];
                col[0] = myLocEl_E;
                val[0] = -dt * c * sigma; // E i,j
                col[1] = myLocEl_T;
                val[1] = rho_cv + dt * c * sigma * a * 4.*Tij*Tij*Tij; // T i,j
                A.replaceValues(nbVar*index+1, col, 2, val, isSorted);
              }
              // row Fx, element i,j
              {
                local_ordinal_type col[3];
                scalar_type val[3];
                col[0] = myLocEl_E;
                val[0] = dtdx_lp_ipj*dPxxdE_ij  + dtdx_lm_imj*dPxxdE_ij  + dtdy_lp_ijp*dPxydE_ij  + dtdy_lm_ijm*dPxydE_ij; // E i,j
                col[1] = myLocEl_Fx;
                val[1] = ONE_F + 	dtdx_lp_ipj*dPxxdFx_ij + dtdx_lm_imj*dPxxdFx_ij + dtdy_lp_ijp*dPxydFx_ij + dtdy_lm_ijm*dPxydFx_ij 		- dtdx_lplm_ipj - dtdx_lplm_imj - dtdy_lplm_ijp - dtdy_lplm_ijm; // Fx i,j
                col[2] = myLocEl_Fy;
                val[2] = dtdx_lp_ipj*dPxxdFy_ij + dtdx_lm_imj*dPxxdFy_ij + dtdy_lp_ijp*dPxydFy_ij + dtdy_lm_ijm*dPxydFy_ij; // Fy i,j
                A.replaceValues(nbVar*index+2, col, 3, val, isSorted);
              }
              if(wellBalancedScheme)
              {
                local_ordinal_type col[1];
                scalar_type val[1];
                col[0] = myLocEl_Fx;
                val[0] = HALF_F*HALF_F*c*dt*(sigma_ipj+sigma_imj); // Fx i,j
                A.sumIntoValues(nbVar*index+2, col, 1, val, isSorted);
              }
              else
              {
                local_ordinal_type col[1];
                scalar_type val[1];
                col[0] = myLocEl_Fx;
                val[0] = c*dt*sigma; // Fx i,j
                A.sumIntoValues(nbVar*index+2, col, 1, val, isSorted);
              }
              // row Fy, element i,j
              {
                local_ordinal_type col[3];
                scalar_type val[3];
                col[0] = myLocEl_E;
                val[0] = dtdx_lp_ipj*dPyxdE_ij  + dtdx_lm_imj*dPyxdE_ij  + dtdy_lp_ijp*dPyydE_ij  + dtdy_lm_ijm*dPyydE_ij; // E i,j
                col[1] = myLocEl_Fx;
                val[1] = dtdx_lp_ipj*dPyxdFx_ij + dtdx_lm_imj*dPyxdFx_ij + dtdy_lp_ijp*dPyydFx_ij + dtdy_lm_ijm*dPyydFx_ij; // Fx i,j
                col[2] = myLocEl_Fy;
                val[2] = ONE_F + 		dtdx_lp_ipj*dPyxdFy_ij + dtdx_lm_imj*dPyxdFy_ij + dtdy_lp_ijp*dPyydFy_ij + dtdy_lm_ijm*dPyydFy_ij 		- dtdx_lplm_ipj - dtdx_lplm_imj - dtdy_lplm_ijp - dtdy_lplm_ijm; // Fy i,j
                A.replaceValues(nbVar*index+3, col, 3, val, isSorted);
              }
              if(wellBalancedScheme)
              {
                local_ordinal_type col[1];
                scalar_type val[1];
                col[0] = myLocEl_Fy;
                val[0] = HALF_F*HALF_F*c*dt*(sigma_ijp+sigma_ijm); // Fy i,j
                A.sumIntoValues(nbVar*index+3, col, 1, val, isSorted);
              }
              else
              {
                local_ordinal_type col[1];
                scalar_type val[1];
                col[0] = myLocEl_Fy;
                val[0] = c*dt*sigma; // Fy i,j
                A.sumIntoValues(nbVar*index+3, col, 1, val, isSorted);
              }
            }


            // elements i-1,j
            if(i > 0)
            {
              const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(myRank, i-1, j, nx, ny));
              const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(myRank, i-1, j, nx, ny)+2);
              const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(myRank, i-1, j, nx, ny)+3);
              // row E, element i-1, j
              {
                local_ordinal_type col[2];
                scalar_type val[2];
                col[0] = myLocEl_E;
                val[0] = alpha_imj*dtdx_lplm_imj; // E i-1,j
                col[1] = myLocEl_Fx;
                val[1] = -alpha_imj*dtdx_lp_imj; // Fx i-1,j
                A.replaceValues(nbVar*index, col, 2, val, isSorted);
              }
              // row Fx, element i-1, j
              {
                local_ordinal_type col[3];
                scalar_type val[3];
                col[0] = myLocEl_E;
                val[0] = - dtdx_lp_imj*dPxxdE_imj; // E i-1,j
                col[1] = myLocEl_Fx;
                val[1] = - dtdx_lp_imj*dPxxdFx_imj + dtdx_lplm_imj; // Fx i-1,j
                col[2] = myLocEl_Fy;
                val[2] = - dtdx_lp_imj*dPxxdFy_imj; // Fy i-1,j
                A.replaceValues(nbVar*index+2, col, 3, val, isSorted);
              }
              if(wellBalancedScheme)
              {
                local_ordinal_type col[1];
                scalar_type val[1];
                col[0] = myLocEl_Fx;
                val[0] = HALF_F*HALF_F*c*dt*sigma_imj; // Fx i-1,j
                A.sumIntoValues(nbVar*index+2, col, 1, val, isSorted);
              }
              // row Fy, element i-1, j
              {
                local_ordinal_type col[3];
                scalar_type val[3];
                col[0] = myLocEl_E;
                val[0] = - dtdx_lp_imj*dPyxdE_imj; // E i-1,j
                col[1] = myLocEl_Fx;
                val[1] = - dtdx_lp_imj*dPyxdFx_imj; // Fx i-1,j
                col[2] = myLocEl_Fy;
                val[2] = - dtdx_lp_imj*dPyxdFy_imj + dtdx_lplm_imj; // Fy i-1,j
                A.replaceValues(nbVar*index+3, col, 3, val, isSorted);
              }
            } // if i > 0
            else
            {
#ifdef USE_MPI
              if((params.neighborsBC[XMIN] == BC_COPY) || (params.neighborsBC[XMIN] == BC_PERIODIC))
              {
                const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(params.neighborsRank[XMIN], nx-1, j, nx, ny));
                const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(params.neighborsRank[XMIN], nx-1, j, nx, ny)+2);
                const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(params.neighborsRank[XMIN], nx-1, j, nx, ny)+3);
                // row E, element i-1, j
                {
                  local_ordinal_type col[2];
                  scalar_type val[2];
                  col[0] = myLocEl_E;
                  val[0] = alpha_imj*dtdx_lplm_imj; // E i-1,j
                  col[1] = myLocEl_Fx;
                  val[1] = -alpha_imj*dtdx_lp_imj; // Fx i-1,j
                  A.replaceValues(nbVar*index, col, 2, val, isSorted);
                }
                // row Fx, element i-1, j
                {
                  local_ordinal_type col[3];
                  scalar_type val[3];
                  col[0] = myLocEl_E;
                  val[0] = - dtdx_lp_imj*dPxxdE_imj; // E i-1,j
                  col[1] = myLocEl_Fx;
                  val[1] = - dtdx_lp_imj*dPxxdFx_imj + dtdx_lplm_imj; // Fx i-1,j
                  col[2] = myLocEl_Fy;
                  val[2] = - dtdx_lp_imj*dPxxdFy_imj; // Fy i-1,j
                  A.replaceValues(nbVar*index+2, col, 3, val, isSorted);
                }
                // row Fy, element i-1, j
                {
                  local_ordinal_type col[3];
                  scalar_type val[3];
                  col[0] = myLocEl_E;
                  val[0] = - dtdx_lp_imj*dPyxdE_imj; // E i-1,j
                  col[1] = myLocEl_Fx;
                  val[1] = - dtdx_lp_imj*dPyxdFx_imj; // Fx i-1,j
                  col[2] = myLocEl_Fy;
                  val[2] = - dtdx_lp_imj*dPyxdFy_imj + dtdx_lplm_imj; // Fy i-1,j
                  A.replaceValues(nbVar*index+3, col, 3, val, isSorted);
                }
                if(wellBalancedScheme)
                {
                  local_ordinal_type col[1];
                  scalar_type val[1];
                  col[0] = myLocEl_Fx;
                  val[0] = HALF_F*HALF_F*c*dt*sigma_imj; // Fx i-1,j
                  A.sumIntoValues(nbVar*index+2, col, 1, val, isSorted);
                }
              } // neighborsBC[XMIN] == BC_COPY || BC_PERIODIC
              else if(params.neighborsBC[XMIN] == BC_NEUMANN)
              {
                const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny));
                const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+2);
                const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+3);
                // row E, element i-1, j
                {
                  local_ordinal_type col[2];
                  scalar_type val[2];
                  col[0] = myLocEl_E;
                  val[0] = alpha_imj*dtdx_lplm_imj; // E i-1,j
                  col[1] = myLocEl_Fx;
                  val[1] = -alpha_imj*dtdx_lp_imj; // Fx i-1,j
                  A.sumIntoValues(nbVar*index, col, 2, val, isSorted);
                }
                // row Fx, element i-1, j
                {
                  local_ordinal_type col[3];
                  scalar_type val[3];
                  col[0] = myLocEl_E;
                  val[0] = - dtdx_lp_imj*dPxxdE_ij; // E i-1,j
                  col[1] = myLocEl_Fx;
                  val[1] = - dtdx_lp_imj*dPxxdFx_ij + dtdx_lplm_imj; // Fx i-1,j
                  col[2] = myLocEl_Fy;
                  val[2] = - dtdx_lp_imj*dPxxdFy_ij; // Fy i-1,j
                  A.sumIntoValues(nbVar*index+2, col, 3, val, isSorted);
                }
                if(wellBalancedScheme)
                {
                  local_ordinal_type col[1];
                  scalar_type val[1];
                  col[0] = myLocEl_Fx;
                  val[0] = HALF_F*HALF_F*c*dt*sigma_imj; // Fx i-1,j
                  A.sumIntoValues(nbVar*index+2, col, 1, val, isSorted);
                }
                // row Fy, element i-1, j
                {
                  local_ordinal_type col[3];
                  scalar_type val[3];
                  col[0] = myLocEl_E;
                  val[0] =  dtdx_lp_imj*dPyxdE_ij; // E i-1,j
                  col[1] = myLocEl_Fx;
                  val[1] = - dtdx_lp_imj*dPyxdFx_ij, // Fx i-1,j
                    col[2] = myLocEl_Fy;
                  val[2] = - dtdx_lp_imj*dPyxdFy_ij + dtdx_lplm_imj; // Fy i-1,j
                  A.sumIntoValues(nbVar*index+3, col, 3, val, isSorted);
                }
              } // neighborsBC[XMIN] == BC_NEUMANN
              else if(params.neighborsBC[XMIN] == BC_PROBLEM_DEFINED)
#endif
              {
                const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny));
                const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+2);
                const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+3);
                if(problem_name == marshak_wave_1d)
                {
                  // row E, element i-1, j
                  {
                    local_ordinal_type col[2];
                    scalar_type val[2];
                    col[0] = myLocEl_E;
                    val[0] = -alpha_imj*dtdx_lp_imj; // E i-1,j
                    val[0] *= (-2.*c/(3.*dx*sigma_imj));
                    col[1] = myLocEl_Fx;
                    val[1] = -alpha_imj*dtdx_lp_imj; // Fx i-1,j
                    val[1] *= -1.;
                    A.sumIntoValues(nbVar*index, col, 2, val, isSorted);
                  }
                  // row Fx, element i-1, j
                  {
                    local_ordinal_type col[2];
                    scalar_type val[2];
                    col[0] = myLocEl_E;
                    val[0] = - dtdx_lp_imj*dPxxdFx_imj + dtdx_lplm_imj; // E i-1,j
                    val[0] *= (-2.*c/(3.*dx*sigma_imj));
                    col[1] = myLocEl_Fx;
                    val[1] = - dtdx_lp_imj*dPxxdFx_imj + dtdx_lplm_imj; // Fx i-1,j
                    val[1] *= -1.;
                    A.sumIntoValues(nbVar*index+2, col, 2, val, isSorted);
                  }
                  // row Fy, element i-1, j
                  {
                    local_ordinal_type col[2];
                    scalar_type val[2];
                    col[0] = myLocEl_E;
                    val[0] = - dtdx_lp_imj*dPyxdFx_imj; // E i-1,j
                    val[0] *= (-2.*c/(3.*dx*sigma_imj));
                    col[1] = myLocEl_Fx;
                    val[1] = - dtdx_lp_imj*dPyxdFx_imj; // Fx i-1,j
                    val[1] *= -1.;
                    A.sumIntoValues(nbVar*index+3, col, 2, val, isSorted);
                  }
                  if(wellBalancedScheme)
                  {
                    local_ordinal_type col[2];
                    scalar_type val[2];
                    col[0] = myLocEl_E;
                    val[0] = HALF_F*HALF_F*c*dt*sigma_imj; // E i-1,j
                    val[0] *= (-2.*c/(3.*dx*sigma_imj));
                    col[1] = myLocEl_Fx;
                    val[1] = HALF_F*HALF_F*c*dt*sigma_imj; // Fx i-1,j
                    val[1] *= -1.;
                    A.sumIntoValues(nbVar*index+2, col, 2, val, isSorted);
                  }
                } // problem_name == marshak_wave_1d
                else if(problem_name == marshak_wave_2d)
                {
#ifdef USE_MPI
                  const int jGlob = j + params.myMpiPos[IY]*ny;
#else
                  const int jGlob = j;
#endif
                  const int my = params.my;
                  if(jGlob == ny*my/2)
                  {
                    // row E, element i-1, j
                    {
                      local_ordinal_type col[1];
                      scalar_type val[1];
                      col[0] = myLocEl_Fx;
                      val[0] =  -alpha_imj*dtdx_lp_imj; // Fx i-1,j
                      A.sumIntoValues(nbVar*index, col, 1, val, isSorted);
                    }
                    // row Fx, element i-1, j
                    {
                      local_ordinal_type col[2];
                      scalar_type val[2];
                      col[0] = myLocEl_Fx;
                      val[0] = - dtdx_lp_imj*dPxxdFx_ij + dtdx_lplm_imj; // Fx i-1,j
                      col[1] = myLocEl_Fy;
                      val[1] = - dtdx_lp_imj*dPxxdFy_ij; // Fy i-1,j
                      A.sumIntoValues(nbVar*index+2, col, 2, val, isSorted);
                    }
                    if(wellBalancedScheme)
                    {
                      local_ordinal_type col[1];
                      scalar_type val[1];
                      col[0] = myLocEl_Fx;
                      val[0] = HALF_F*HALF_F*c*dt*sigma_imj; // Fx i-1,j
                      A.sumIntoValues(nbVar*index+2, col, 1, val, isSorted);
                    }
                    // row Fy, element i-1, j
                    {
                      local_ordinal_type col[2];
                      scalar_type val[2];
                      col[0] = myLocEl_Fx;
                      val[0] = - dtdx_lp_imj*dPyxdFx_ij; // Fx i-1,j
                      col[1] = myLocEl_Fy;
                      val[1] = - dtdx_lp_imj*dPyxdFy_ij + dtdx_lplm_imj; // Fy i-1,j
                      A.sumIntoValues(nbVar*index+3, col, 2, val, isSorted);
                    }
                  } // jGblob == nx*my/2
                  else
                  {
                    // row E, element i-1, j
                    {
                      local_ordinal_type col[2];
                      scalar_type val[2];
                      col[0] = myLocEl_E;
                      val[0] =  alpha_imj*dtdx_lplm_imj; // E i-1,j
                      col[1] = myLocEl_Fx;
                      val[1] = -alpha_imj*dtdx_lp_imj; // Fx i-1,j
                      A.sumIntoValues(nbVar*index, col, 2, val, isSorted);
                    }
                    // row Fx, element i-1, j
                    {
                      local_ordinal_type col[3];
                      scalar_type val[3];
                      col[0] = myLocEl_E;
                      val[0] = - dtdx_lp_imj*dPxxdE_ij; // E i-1,j */
                      col[1] = myLocEl_Fx;
                      val[1] = - dtdx_lp_imj*dPxxdFx_ij + dtdx_lplm_imj; // Fx i-1,j */
                      col[2] = myLocEl_Fy;
                      val[2] = - dtdx_lp_imj*dPxxdFy_ij; // Fy i-1,j */
                      A.sumIntoValues(nbVar*index+2, col, 3, val, isSorted);
                    }
                    if(wellBalancedScheme)
                    {
                      local_ordinal_type col[1];
                      scalar_type val[1];
                      col[0] = myLocEl_Fx;
                      val[0] = HALF_F*HALF_F*c*dt*sigma_imj; // Fx i-1,j
                      A.sumIntoValues(nbVar*index+2, col, 1, val, isSorted);
                    }
                    // row Fy, element i-1, j
                    {
                      local_ordinal_type col[3];
                      scalar_type val[3];
                      col[0] = myLocEl_E;
                      val[0] = - dtdx_lp_imj*dPyxdE_ij; // E i-1,j
                      col[1] = myLocEl_Fx;
                      val[1] = - dtdx_lp_imj*dPyxdFx_ij; // Fx i-1,j
                      col[2] = myLocEl_Fy;
                      val[2] = - dtdx_lp_imj*dPyxdFy_ij + dtdx_lplm_imj; // Fy i-1,j
                      A.sumIntoValues(nbVar*index+3, col, 3, val, isSorted);
                    }
                  } // jGblob == nx*my/2
                } // problem_name == marshak_wave_2d
                else if(problem_name  == shadow) {}
                else if(problem_name  == beam) {}
                else if(problem_name  == radiative_shock)
                {
                  // row E, element i-1, j
                  {
                    local_ordinal_type col[2];
                    scalar_type val[2];
                    col[0] = myLocEl_E;
                    val[0] =  alpha_imj*dtdx_lplm_imj; // E i-1,j
                    col[1] = myLocEl_Fx;
                    val[1] = +alpha_imj*dtdx_lp_imj; // Fx i-1,j
                    A.sumIntoValues(nbVar*index, col, 2, val, isSorted);
                  }
                  // row Fx, element i-1, j
                  {
                    local_ordinal_type col[2];
                    scalar_type val[2];
                    col[0] = myLocEl_E;
                    val[0] = - dtdx_lp_imj*dPxxdE_imj; // E i-1,j
                    col[1] = myLocEl_Fx;
                    val[1] = - dtdx_lp_imj*dPxxdFx_imj - dtdx_lplm_imj; // Fx i-1,j
                    A.sumIntoValues(nbVar*index+2, col, 2, val, isSorted);
                  }
                  if(wellBalancedScheme)
                  {
                    local_ordinal_type col[1];
                    scalar_type val[1];
                    col[0] = myLocEl_Fx;
                    val[0] = -HALF_F*HALF_F*c*dt*sigma_imj; // Fx i-1,j
                    A.sumIntoValues(nbVar*index+2, col, 1, val, isSorted);
                  }
                  // row Fy, element i-1, j
                  {
                    local_ordinal_type col[2];
                    scalar_type val[2];
                    col[0] = myLocEl_E;
                    val[0] = - dtdx_lp_imj*dPyxdE_imj; // E i-1,j
                    col[1] = myLocEl_Fx;
                    val[1] = - dtdx_lp_imj*dPyxdFx_imj; // Fx i-1,j
                    A.sumIntoValues(nbVar*index+3, col, 2, val, isSorted);
                  }
                } // problem_name == radiative_shock
                else if(problem_name == HII_region) {}


              } // neighborsBC[XMIN] == BC_PROBLEM_DEFINED
            } // i == 0

            // elements i+1,j
            if(i < nx-1)
            {
              const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(myRank, i+1, j, nx, ny));
              const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(myRank, i+1, j, nx, ny)+2);
              const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(myRank, i+1, j, nx, ny)+3);
              // row E, element i+1, j
              {
                local_ordinal_type col[2];
                scalar_type val[2];
                col[0] = myLocEl_E;
                val[0] = alpha_ipj*dtdx_lplm_ipj; // E i+1,j
                col[1] = myLocEl_Fx;
                val[1] = -alpha_ipj*dtdx_lm_ipj; // Fx i+1,j
                A.replaceValues(nbVar*index, col, 2, val, isSorted);
              }
              // row Fx, element i+1, j
              {
                local_ordinal_type col[3];
                scalar_type val[3];
                col[0] = myLocEl_E;
                val[0] = - dtdx_lm_ipj*dPxxdE_ipj; // E i+1,j
                col[1] = myLocEl_Fx;
                val[1] = - dtdx_lm_ipj*dPxxdFx_ipj + dtdx_lplm_ipj; // Fx i+1,j
                col[2] = myLocEl_Fy;
                val[2] = - dtdx_lm_ipj*dPxxdFy_ipj; // Fy i+1,j
                A.replaceValues(nbVar*index+2, col, 3, val, isSorted);
              }
              if(wellBalancedScheme)
              {
                local_ordinal_type col[1];
                scalar_type val[1];
                col[0] = myLocEl_Fx;
                val[0] = HALF_F*HALF_F*c*dt*sigma_ipj; // Fx i+1,j
                A.sumIntoValues(nbVar*index+2, col, 1, val, isSorted);
              }
              // row Fy, element i+1, j
              {
                local_ordinal_type col[3];
                scalar_type val[3];
                col[0] = myLocEl_E;
                val[0] = - dtdx_lm_ipj*dPyxdE_ipj; // E i+1,j
                col[1] = myLocEl_Fx;
                val[1] = - dtdx_lm_ipj*dPyxdFx_ipj; // Fx i+1,j
                col[2] = myLocEl_Fy;
                val[2] = - dtdx_lm_ipj*dPyxdFy_ipj + dtdx_lplm_ipj; // Fy i+1,j
                A.replaceValues(nbVar*index+3, col, 3, val, isSorted);
              }
            } // i < nx-1
            else
            {
#ifdef USE_MPI
              if((params.neighborsBC[XMAX] == BC_COPY) || (params.neighborsBC[XMAX] == BC_PERIODIC))
              { 
                const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(params.neighborsRank[XMAX], 0, j, nx, ny));
                const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(params.neighborsRank[XMAX], 0, j, nx, ny)+2);
                const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(params.neighborsRank[XMAX], 0, j, nx, ny)+3);
                // row E, element i+1, j
                {
                  local_ordinal_type col[2];
                  scalar_type val[2];
                  col[0] = myLocEl_E;
                  val[0] = alpha_ipj*dtdx_lplm_ipj; // E i+1,j
                  col[1] = myLocEl_Fx;
                  val[1] = -alpha_ipj*dtdx_lm_ipj; // Fx i+1,j
                  A.replaceValues(nbVar*index, col, 2, val, isSorted);
                }
                // row Fx, element i+1, j
                {
                  local_ordinal_type col[3];
                  scalar_type val[3];
                  col[0] = myLocEl_E;
                  val[0] = - dtdx_lm_ipj*dPxxdE_ipj; // E i+1,j
                  col[1] = myLocEl_Fx;
                  val[1] = - dtdx_lm_ipj*dPxxdFx_ipj + dtdx_lplm_ipj; // Fx i+1,j
                  col[2] = myLocEl_Fy;
                  val[2] = - dtdx_lm_ipj*dPxxdFy_ipj; // Fy i+1,j
                  A.replaceValues(nbVar*index+2, col, 3, val, isSorted);
                }
                if(wellBalancedScheme)
                {
                  local_ordinal_type col[1];
                  scalar_type val[1];
                  col[0] = myLocEl_Fx;
                  val[0] = HALF_F*HALF_F*c*dt*sigma_ipj; // Fx i+1,j
                  A.sumIntoValues(nbVar*index+2, col, 1, val, isSorted);
                }
                // row Fy, element i+1, j
                {
                  local_ordinal_type col[3];
                  scalar_type val[3];
                  col[0] = myLocEl_E;
                  val[0] = - dtdx_lm_ipj*dPyxdE_ipj; // E i+1,j
                  col[1] = myLocEl_Fx;
                  val[1] = - dtdx_lm_ipj*dPyxdFx_ipj; // Fx i+1,j
                  col[2] = myLocEl_Fy;
                  val[2] = - dtdx_lm_ipj*dPyxdFy_ipj + dtdx_lplm_ipj; // Fy i+1,j
                  A.replaceValues(nbVar*index+3, col, 3, val, isSorted);
                }
              } // neighborsBC[XMAX] == BC_COPY || BC_PERIODIC
              else if(params.neighborsBC[XMAX] == BC_NEUMANN)
              {
                const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny));
                const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+2);
                const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+3);
                // row E, element i+1, j
                {
                  local_ordinal_type col[2];
                  scalar_type val[2];
                  col[0] = myLocEl_E;
                  val[0] = alpha_ipj*dtdx_lplm_ipj; // E i+1,j
                  col[1] = myLocEl_Fx;
                  val[1] = -alpha_ipj*dtdx_lm_ipj; // Fx i+1,j
                  A.sumIntoValues(nbVar*index, col, 2, val, isSorted);
                }
                // row Fx, element i+1, j
                {
                  local_ordinal_type col[3];
                  scalar_type val[3];
                  col[0] = myLocEl_E;
                  val[0] = - dtdx_lm_ipj*dPxxdE_ij; // E i+1,j
                  col[1] = myLocEl_Fx;
                  val[1] = - dtdx_lm_ipj*dPxxdFx_ij + dtdx_lplm_ipj; // Fx i+1,j
                  col[2] = myLocEl_Fy;
                  val[2] = - dtdx_lm_ipj*dPxxdFy_ij; // Fy i+1,j
                  A.sumIntoValues(nbVar*index+2, col, 3, val, isSorted);
                }
                if(wellBalancedScheme)
                {
                  local_ordinal_type col[1];
                  scalar_type val[1];
                  col[0] = myLocEl_Fx;
                  val[0] = HALF_F*HALF_F*c*dt*sigma_ipj; // Fx i+1,j
                  A.sumIntoValues(nbVar*index+2, col, 1, val, isSorted);
                }
                // row Fy, element i+1, j
                {
                  local_ordinal_type col[3];
                  scalar_type val[3];
                  col[0] = myLocEl_E;
                  val[0] = - dtdx_lm_ipj*dPyxdE_ij; // E i+1,j
                  col[1] = myLocEl_Fx;
                  val[1] = - dtdx_lm_ipj*dPyxdFx_ij; // Fx i+1,j
                  col[2] = myLocEl_Fy;
                  val[2] = - dtdx_lm_ipj*dPyxdFy_ij + dtdx_lplm_ipj; // Fy i+1,j
                  A.sumIntoValues(nbVar*index+3, col, 3, val, isSorted);
                }
              } // neighborsBC[XMAX] == BC_NEUMANN
              else if(params.neighborsBC[XMAX] == BC_PROBLEM_DEFINED)
#endif
              {
                /* const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)); */
                /* const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+2); */
                /* const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+3); */
              } // neighborsBC[XMAX] == BC_PROBLEM_DEFINED
            } // i == nx-1

            // elements i,j-1
            if(j > 0)
            {
              const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(myRank, i, j-1, nx, ny));
              const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(myRank, i, j-1, nx, ny)+2);
              const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(myRank, i, j-1, nx, ny)+3);
              // row E, element i, j-1
              {
                local_ordinal_type col[2];
                scalar_type val[2];
                col[0] = myLocEl_E;
                val[0] = alpha_ijm*dtdy_lplm_ijm; // E i,j-1
                col[1] = myLocEl_Fy;
                val[1] = -alpha_ijm*dtdy_lp_ijm; // Fy i,j-1
                A.replaceValues(nbVar*index, col, 2, val, isSorted);
              }
              // row Fx, element i, j-1
              {
                local_ordinal_type col[3];
                scalar_type val[3];
                col[0] = myLocEl_E;
                val[0] = - dtdy_lp_ijm*dPxydE_ijm; // E i,j-1
                col[1] = myLocEl_Fx;
                val[1] = - dtdy_lp_ijm*dPxydFx_ijm + dtdy_lplm_ijm; // Fx i,j-1
                col[2] = myLocEl_Fy;
                val[2] = - dtdy_lp_ijm*dPxydFy_ijm; // Fy i,j-1
                A.replaceValues(nbVar*index+2, col, 3, val, isSorted);
              }
              // row Fy, element i, j-1
              {
                local_ordinal_type col[3];
                scalar_type val[3];
                col[0] = myLocEl_E;
                val[0] = - dtdy_lp_ijm*dPyydE_ijm; // E i,j-1
                col[1] = myLocEl_Fx;
                val[1] = - dtdy_lp_ijm*dPyydFx_ijm; // Fx i,j-1
                col[2] = myLocEl_Fy;
                val[2] = - dtdy_lp_ijm*dPyydFy_ijm + dtdy_lplm_ijm; // Fy i,j-1
                A.replaceValues(nbVar*index+3, col, 3, val, isSorted);
              }
              if(wellBalancedScheme)
              {
                local_ordinal_type col[1];
                scalar_type val[1];
                col[0] = myLocEl_Fy;
                val[0] = HALF_F*HALF_F*c*dt*sigma_ijm; // Fy i,j-1
                A.sumIntoValues(nbVar*index+3, col, 1, val, isSorted);
              }
            } // j  > 0
            else
            {
#ifdef USE_MPI
              if((params.neighborsBC[YMIN] == BC_COPY) || (params.neighborsBC[YMIN] == BC_PERIODIC))
              {
                const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(params.neighborsRank[YMIN], i, ny-1, nx, ny));
                const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(params.neighborsRank[YMIN], i, ny-1, nx, ny)+2);
                const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(params.neighborsRank[YMIN], i, ny-1, nx, ny)+3);
                // row E, element i, j-1
                {
                  local_ordinal_type col[2];
                  scalar_type val[2];
                  col[0] = myLocEl_E;
                  val[0] = alpha_ijm*dtdy_lplm_ijm; // E i,j-1
                  col[1] = myLocEl_Fy;
                  val[1] = -alpha_ijm*dtdy_lp_ijm; // Fy i,j-1
                  A.replaceValues(nbVar*index, col, 2, val, isSorted);
                }
                // row Fx, element i, j-1
                {
                  local_ordinal_type col[3];
                  scalar_type val[3];
                  col[0] = myLocEl_E;
                  val[0] = - dtdy_lp_ijm*dPxydE_ijm; // E i,j-1
                  col[1] = myLocEl_Fx;
                  val[1] = - dtdy_lp_ijm*dPxydFx_ijm + dtdy_lplm_ijm; // Fx i,j-1
                  col[2] = myLocEl_Fy;
                  val[2] = - dtdy_lp_ijm*dPxydFy_ijm; // Fy i,j-1
                  A.replaceValues(nbVar*index+2, col, 3, val, isSorted);
                }
                // row Fy, element i, j-1
                {
                  local_ordinal_type col[3];
                  scalar_type val[3];
                  col[0] = myLocEl_E;
                  val[0] = - dtdy_lp_ijm*dPyydE_ijm; // E i,j-1
                  col[1] = myLocEl_Fx;
                  val[1] = - dtdy_lp_ijm*dPyydFx_ijm; // Fx i,j-1
                  col[2] = myLocEl_Fy;
                  val[2] = - dtdy_lp_ijm*dPyydFy_ijm + dtdy_lplm_ijm; // Fy i,j-1
                  A.replaceValues(nbVar*index+3, col, 3, val, isSorted);
                }
                if(wellBalancedScheme)
                {
                  local_ordinal_type col[1];
                  scalar_type val[1];
                  col[0] = myLocEl_Fy;
                  val[0] = HALF_F*HALF_F*c*dt*sigma_ijm; // Fy i,j-1
                  A.sumIntoValues(nbVar*index+3, col, 1, val, isSorted);
                }
              } // neighborsBC[YMIN] == BC_COPY || BC_PERIODIC
              else if(params.neighborsBC[YMIN] == BC_NEUMANN)
              {
                const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny));
                const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+2);
                const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+3);
                // row E, element i, j-1
                {
                  local_ordinal_type col[2];
                  scalar_type val[2];
                  col[0] = myLocEl_E;
                  val[0] = alpha_ijm*dtdy_lplm_ijm; // E i,j-1
                  col[1] = myLocEl_Fy;
                  val[1] = -alpha_ijm*dtdy_lp_ijm; // Fy i,j-1
                  A.sumIntoValues(nbVar*index, col, 2, val, isSorted);
                }
                // row Fx, element i, j-1
                {
                  local_ordinal_type col[3];
                  scalar_type val[3];
                  col[0] = myLocEl_E;
                  val[0] = - dtdy_lp_ijm*dPxydE_ij; // E i,j-1
                  col[1] = myLocEl_Fx;
                  val[1] = - dtdy_lp_ijm*dPxydFx_ij + dtdy_lplm_ijm; // Fx i,j-1
                  col[2] = myLocEl_Fy;
                  val[2] = - dtdy_lp_ijm*dPxydFy_ij; // Fy i,j-1 
                  A.sumIntoValues(nbVar*index+2, col, 3, val, isSorted);
                }
                // row Fy, element i, j-1
                {
                  local_ordinal_type col[3];
                  scalar_type val[3];
                  col[0] = myLocEl_E;
                  val[0] = - dtdy_lp_ijm*dPyydE_ij; // E i,j-1
                  col[1] = myLocEl_Fx;
                  val[1] = - dtdy_lp_ijm*dPyydFx_ij; // Fx i,j-1
                  col[2] = myLocEl_Fy;
                  val[2] = - dtdy_lp_ijm*dPyydFy_ij + dtdy_lplm_ijm; // Fy i,j-1
                  A.sumIntoValues(nbVar*index+3, col, 3, val, isSorted);
                }
                if(wellBalancedScheme)
                {
                  local_ordinal_type col[1];
                  scalar_type val[1];
                  col[0] = myLocEl_Fy;
                  val[0] = HALF_F*HALF_F*c*dt*sigma_ijm; // Fy i,j-1
                  A.sumIntoValues(nbVar*index+3, col, 1, val, isSorted);
                }
              } // neighborsBC[YMIN] == BC_NEUMANN
              else if(params.neighborsBC[YMIN] == BC_PROBLEM_DEFINED)
#endif
              {
                const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny));
                const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+2);
                const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+3);
                if(problem_name == marshak_wave_1d)
                {
                  // row E, element i, j-1
                  {
                    local_ordinal_type col[2];
                    scalar_type val[2];
                    col[0] = myLocEl_E;
                    val[0] = -alpha_ijm*dtdy_lp_ijm; // E i,j-1
                    val[0] *= (-2.*c/(3.*dy*sigma_ijm));
                    col[1] = myLocEl_Fy;
                    val[1] = -alpha_ijm*dtdy_lp_ijm; // Fy i,j-1
                    val[1] *= -1.;
                    A.sumIntoValues(nbVar*index, col, 2, val, isSorted);
                  }
                  // row Fx, element i, j-1
                  {
                    local_ordinal_type col[2];
                    scalar_type val[2];
                    col[0] = myLocEl_E;
                    val[0] = - dtdy_lp_ijm*dPxydFy_ijm; // E i,j-1
                    val[0] *= (-2.*c/(3.*dy*sigma_ijm));
                    col[1] = myLocEl_Fy;
                    val[1] = - -dtdy_lp_ijm*dPxydFy_ijm; // Fy i,j-1
                    val[1] *= -1.;
                    A.sumIntoValues(nbVar*index+2, col, 2, val, isSorted);
                  }
                  // row Fy, element i, j-1
                  {
                    local_ordinal_type col[2];
                    scalar_type val[2];
                    col[0] = myLocEl_E;
                    val[0] = - dtdy_lp_ijm*dPyydFy_ijm + dtdy_lplm_ijm; // E i,j-1
                    val[0] *= (-2.*c/(3.*dy*sigma_ijm));
                    col[1] = myLocEl_Fy;
                    val[1] = - dtdy_lp_ijm*dPyydFy_ijm + dtdy_lplm_ijm; // Fy i,j-1
                    val[1] *= -1.;
                    A.sumIntoValues(nbVar*index+3, col, 2, val, isSorted);
                  }
                  if(wellBalancedScheme)
                  {
                    local_ordinal_type col[2];
                    scalar_type val[2];
                    col[0] = myLocEl_E;
                    val[0] = HALF_F*HALF_F*c*dt*sigma_ijm; // E i,j-1
                    val[0] *= (-2.*c/(3.*dy*sigma_ijm));
                    col[1] = myLocEl_Fy;
                    val[1] = HALF_F*HALF_F*c*dt*sigma_ijm; // Fy i,j-1
                    val[1] *= -1.;
                    A.sumIntoValues(nbVar*index+3, col, 2, val, isSorted);
                  }
                } // problem_name == marshak_wave_1d
                if(problem_name == radiative_transfer_well_balanced)
                {
                  // row E, element i, j-1
                  {
                    local_ordinal_type col[2];
                    scalar_type val[2];
                    col[0] = myLocEl_E;
                    val[0] = -alpha_ijm*dtdy_lp_ijm; // E i,j-1
                    val[0] *= (-2.*c/(3.*dy*sigma_ijm));
                    col[1] = myLocEl_Fy;
                    val[1] = -alpha_ijm*dtdy_lp_ijm; // Fy i,j-1
                    val[1] *= -1.;
                    A.sumIntoValues(nbVar*index, col, 2, val, isSorted);
                  }
                  // row Fx, element i, j-1
                  {
                    local_ordinal_type col[2];
                    scalar_type val[2];
                    col[0] = myLocEl_E;
                    val[0] = - dtdy_lp_ijm*dPxydFy_ijm; // E i,j-1
                    val[0] *= (-2.*c/(3.*dy*sigma_ijm));
                    col[1] = myLocEl_Fy;
                    val[1] = - dtdy_lp_ijm*dPxydFy_ijm; // Fy i,j-1
                    val[1] *= -1.;
                    A.sumIntoValues(nbVar*index+2, col, 2, val, isSorted);
                  }
                  // row Fy, element i, j-1
                  {
                    local_ordinal_type col[2];
                    scalar_type val[2];
                    col[0] = myLocEl_E;
                    val[0] = - dtdy_lp_ijm*dPyydFy_ijm + dtdy_lplm_ijm; // E i,j-1
                    val[0] *= (-2.*c/(3.*dy*sigma_ijm));
                    col[1] = myLocEl_Fy;
                    val[1] = - dtdy_lp_ijm*dPyydFy_ijm + dtdy_lplm_ijm; // Fy i,j-1
                    val[1] *= -1.;
                    A.sumIntoValues(nbVar*index+3, col, 2, val, isSorted);
                  }
                  if(wellBalancedScheme)
                  {
                    local_ordinal_type col[2];
                    scalar_type val[2];
                    col[0] = myLocEl_E;
                    val[0] = HALF_F*HALF_F*c*dt*sigma_ijm; // E i,j-1
                    val[0] *= (-2.*c/(3.*dy*sigma_ijm));
                    col[1] = myLocEl_Fy;
                    val[1] = HALF_F*HALF_F*c*dt*sigma_ijm; // Fy i,j-1
                    val[1] *= -1.;
                    A.sumIntoValues(nbVar*index+3, col, 2, val, isSorted);
                  }
                } // problem_name == radiative_transfer_well_balanced
                else if(problem_name == marshak_wave_2d)
                {
#ifdef USE_MPI
                  const int iGlob = i + params.myMpiPos[IX]*nx;
#else
                  const int iGlob = i;
#endif
                  const int mx = params.mx;
                  if(iGlob == nx*mx/2)
                  {
                    // row E, element i, j-1
                    {
                      local_ordinal_type col[1];
                      scalar_type val[1];
                      col[0] = myLocEl_Fy;
                      val[0] = -alpha_ijm*dtdy_lp_ijm; // Fy i,j-1
                      A.sumIntoValues(nbVar*index, col, 1, val, isSorted);
                    }
                    // row Fx, element i, j-1
                    {
                      local_ordinal_type col[2];
                      scalar_type val[2];
                      col[0] = myLocEl_Fx;
                      val[0] = - dtdy_lp_ijm*dPxydFx_ij + dtdy_lplm_ijm; // Fx i,j-1
                      col[1] = myLocEl_Fy;
                      val[1] = - dtdy_lp_ijm*dPxydFy_ij; // Fy i,j-1
                      A.sumIntoValues(nbVar*index+2, col, 2, val, isSorted);
                    }
                    // row Fy, element i, j-1
                    {
                      local_ordinal_type col[2];
                      scalar_type val[2];
                      col[0] = myLocEl_Fx;
                      val[0] = - dtdy_lp_ijm*dPyydFx_ij; // Fx i,j-1
                      col[1] = myLocEl_Fy;
                      val[1] = - dtdy_lp_ijm*dPyydFy_ij + dtdy_lplm_ijm; // Fy i,j-1
                      A.sumIntoValues(nbVar*index+3, col, 2, val, isSorted);
                    }
                    if(wellBalancedScheme)
                    {
                      local_ordinal_type col[1];
                      scalar_type val[1];
                      col[0] = myLocEl_Fy;
                      val[0] = HALF_F*HALF_F*c*dt*sigma_ijm; // Fy i,j-1
                      A.sumIntoValues(nbVar*index+3, col, 1, val, isSorted);
                    }
                  } // iGlob == nx*mx/2
                  else
                  {
                    // row E, element i, j-1
                    {
                      local_ordinal_type col[2];
                      scalar_type val[2];
                      col[0] = myLocEl_E;
                      val[0] = alpha_ijm*dtdy_lplm_ijm; // E i,j-1
                      col[1] = myLocEl_Fy;
                      val[1] = -alpha_ijm*dtdy_lp_ijm; // Fy i,j-1
                      A.sumIntoValues(nbVar*index, col, 2, val, isSorted);
                    }
                    // row Fx, element i, j-1
                    {
                      local_ordinal_type col[3];
                      scalar_type val[3];
                      col[0] = myLocEl_E;
                      val[0] =  - dtdy_lp_ijm*dPxydE_ij; // E i,j-1
                      col[1] = myLocEl_Fx;
                      val[1] = - dtdy_lp_ijm*dPxydFx_ij + dtdy_lplm_ijm; // Fx i,j-1
                      col[2] = myLocEl_Fy;
                      val[2] = - dtdy_lp_ijm*dPxydFy_ij; // Fy i,j-1
                      A.sumIntoValues(nbVar*index+2, col, 3, val, isSorted);
                    }
                    // row Fy, element i, j-1
                    {
                      local_ordinal_type col[3];
                      scalar_type val[3];
                      col[0] = myLocEl_E;
                      val[0] = - dtdy_lp_ijm*dPyydE_ij; // E i,j-1
                      col[1] = myLocEl_Fx;
                      val[1] = - dtdy_lp_ijm*dPyydFx_ij; // Fx i,j-1
                      col[2] = myLocEl_Fy;
                      val[2] = - dtdy_lp_ijm*dPyydFy_ij + dtdy_lplm_ijm; // Fy i,j-1
                      A.sumIntoValues(nbVar*index+3, col, 3, val, isSorted);
                    }
                    if(wellBalancedScheme)
                    {
                      local_ordinal_type col[1];
                      scalar_type val[1];
                      col[0] = myLocEl_Fy;
                      val[0] = HALF_F*HALF_F*c*dt*sigma_ijm; // Fy i,j-1
                      A.sumIntoValues(nbVar*index+3, col, 1, val, isSorted);
                    }
                  } // iGlob == nx*mx/2
                } // problem_name == marshak_wave_2d
                else if(problem_name == HII_conv) {}
              } // neighborsBC[YMIN] == BC_PROBLEM_DEFINED
            } // j == 0

            // elements i,j+1
            if(j < ny-1)
            {
              const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(myRank, i, j+1, nx, ny));
              const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(myRank, i, j+1, nx, ny)+2);
              const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(myRank, i, j+1, nx, ny)+3);
              // row E, element i, j+1
              {
                local_ordinal_type col[2];
                scalar_type val[2];
                col[0] = myLocEl_E;
                val[0] = alpha_ijp*dtdy_lplm_ijp; // E i,j+1
                col[1] = myLocEl_Fy;
                val[1] = -alpha_ijp*dtdy_lm_ijp; // Fy i,j+1
                A.replaceValues(nbVar*index, col, 2, val, isSorted);
              }
              // row Fx, element i, j+1
              {
                local_ordinal_type col[3];
                scalar_type val[3];
                col[0] = myLocEl_E;
                val[0] = - dtdy_lm_ijp*dPxydE_ijm; // E i,j+1
                col[1] = myLocEl_Fx;
                val[1] = - dtdy_lm_ijp*dPxydFx_ijp + dtdy_lplm_ijp; // Fx i,j+1
                col[2] = myLocEl_Fy;
                val[2] = - dtdy_lm_ijp*dPxydFy_ijp; // Fy i,j+1
                A.replaceValues(nbVar*index+2, col, 3, val, isSorted);
              }
              // row Fy, element i, j+1
              {
                local_ordinal_type col[3];
                scalar_type val[3];
                col[0] = myLocEl_E;
                val[0] = - dtdy_lm_ijp*dPyydE_ijp; // E i,j+1
                col[1] = myLocEl_Fx;
                val[1] = - dtdy_lm_ijp*dPyydFx_ijp; // Fx i,j+1
                col[2] = myLocEl_Fy;
                val[2] = - dtdy_lm_ijp*dPyydFy_ijp + dtdy_lplm_ijp; // Fy i,j+1
                A.replaceValues(nbVar*index+3, col, 3, val, isSorted);
              }
              if(wellBalancedScheme)
              {
                local_ordinal_type col[1];
                scalar_type val[1];
                col[0] = myLocEl_Fy;
                val[0] = HALF_F*HALF_F*c*dt*sigma_ijp; // Fy i,j+1
                A.sumIntoValues(nbVar*index+3, col, 1, val, isSorted);
              }
            } // j < ny-1
            else
            {
#ifdef USE_MPI
              if((params.neighborsBC[YMAX] == BC_COPY) || (params.neighborsBC[YMAX] == BC_PERIODIC))
              {
                const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(params.neighborsRank[YMAX], i, 0, nx, ny));
                const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(params.neighborsRank[YMAX], i, 0, nx, ny)+2);
                const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(params.neighborsRank[YMAX], i, 0, nx, ny)+3);
                // row E, element i, j+1
                {
                  local_ordinal_type col[2];
                  scalar_type val[2];
                  col[0] = myLocEl_E;
                  val[0] = alpha_ijp*dtdy_lplm_ijp; // E i,j+1
                  col[1] = myLocEl_Fy;
                  val[1] = -alpha_ijp*dtdy_lm_ijp; // Fy i,j+1
                  A.replaceValues(nbVar*index, col, 2, val, isSorted);
                }
                // row Fx, element i, j+1
                {
                  local_ordinal_type col[3];
                  scalar_type val[3];
                  col[0] = myLocEl_E;
                  val[0] = - dtdy_lm_ijp*dPxydE_ijm; // E i,j+1
                  col[1] = myLocEl_Fx;
                  val[1] = - dtdy_lm_ijp*dPxydFx_ijp + dtdy_lplm_ijp; // Fx i,j+1
                  col[2] = myLocEl_Fy;
                  val[2] = - dtdy_lm_ijp*dPxydFy_ijp; // Fy i,j+1
                  A.replaceValues(nbVar*index+2, col, 3, val, isSorted);
                }
                // row Fy, element i, j+1
                {
                  local_ordinal_type col[3];
                  scalar_type val[3];
                  col[0] = myLocEl_E;
                  val[0] = - dtdy_lm_ijp*dPyydE_ijp; // E i,j+1
                  col[1] = myLocEl_Fx;
                  val[1] = - dtdy_lm_ijp*dPyydFx_ijp; // Fx i,j+1
                  col[2] = myLocEl_Fy;
                  val[2] = - dtdy_lm_ijp*dPyydFy_ijp + dtdy_lplm_ijp; // Fy i,j+1
                  A.replaceValues(nbVar*index+3, col, 3, val, isSorted);
                }
                if(wellBalancedScheme)
                {
                  local_ordinal_type col[1];
                  scalar_type val[1];
                  col[0] = myLocEl_Fy;
                  val[0] = HALF_F*HALF_F*c*dt*sigma_ijp; // Fy i,j+1
                  A.sumIntoValues(nbVar*index+3, col, 1, val, isSorted);
                }
              } // neighborsBC[YMAX] == BC_COPY || BC_PERIODIC
              else if(params.neighborsBC[YMAX] == BC_NEUMANN)
              {
                const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny));
                const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+2);
                const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+3);
                // row E, element i, j+1
                {
                  local_ordinal_type col[2];
                  scalar_type val[2];
                  col[0] = myLocEl_E;
                  val[0] = alpha_ijp*dtdy_lplm_ijp; // E i,j+1
                  col[1] = myLocEl_Fy;
                  val[1] = -alpha_ijp*dtdy_lm_ijp; // Fy i,j+1
                  A.sumIntoValues(nbVar*index, col, 2, val, isSorted);
                }
                // row Fx, element i, j+1
                {
                  local_ordinal_type col[3];
                  scalar_type val[3];
                  col[0] = myLocEl_E;
                  val[0] = - dtdy_lm_ijp*dPxydE_ij; // E i,j+1
                  col[1] = myLocEl_Fx;
                  val[1] = - dtdy_lm_ijp*dPxydFx_ij + dtdy_lplm_ijp; // Fx i,j+1
                  col[2] = myLocEl_Fy;
                  val[2] = - dtdy_lm_ijp*dPxydFy_ij; // Fy i,j+1
                  A.sumIntoValues(nbVar*index+2, col, 3, val, isSorted);
                }
                // row Fy, element i, j+1
                {
                  local_ordinal_type col[3];
                  scalar_type val[3];
                  col[0] = myLocEl_E;
                  val[0] = - dtdy_lm_ijp*dPyydE_ij; // E i,j+1
                  col[1] = myLocEl_Fx;
                  val[1] = - dtdy_lm_ijp*dPyydFx_ij; // Fx i,j+1
                  col[2] = myLocEl_Fy;
                  val[2] = - dtdy_lm_ijp*dPyydFy_ij + dtdy_lplm_ijp; // Fy i,j+1
                  A.sumIntoValues(nbVar*index+3, col, 3, val, isSorted);
                }
                if(wellBalancedScheme)
                {
                  local_ordinal_type col[1];
                  scalar_type val[1];
                  col[0] = myLocEl_Fy;
                  val[0] = HALF_F*HALF_F*c*dt*sigma_ijp; // Fy i,j+1
                  A.sumIntoValues(nbVar*index+3, col, 1, val, isSorted);
                }
              } // neighborsBC[YMAX] == BC_NEUMANN
              else if(params.neighborsBC[YMAX] == BC_PROBLEM_DEFINED)
#endif
              {
                /* const local_ordinal_type myLocEl_E = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)); */
                /* const local_ordinal_type myLocEl_Fx = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+2); */
                /* const local_ordinal_type myLocEl_Fy = map.getLocalElement(nbVar*global_coord2index(myRank, i, j, nx, ny)+3); */
                if(problem_name == HII_conv) {}
              } // neighborsBC[YMAX] == BC_PROBLEM_DEFINED
            } // j == ny-1

          } // operator()


        HydroParams params;
        local_matrix_type A;
        local_map_type map;
        vector_view_type b;
        int nbVar;
        DataArray Udata;
        DataArray UdataOld;
        real_t dt;
        implicit_boundary problem_name;
        bool isSorted;
    }; // UpdateLinearProblem

} // namespace linearAlgebra
} // namespace ark_rt
