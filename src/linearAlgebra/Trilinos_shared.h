// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include <Kokkos_DualView.hpp>

#include <Teuchos_Tuple.hpp>
#include <Teuchos_ArrayRCPDecl.hpp>
#include <Teuchos_ArrayViewDecl.hpp>
#include <Teuchos_RCPDecl.hpp>
#include <Teuchos_ParameterList.hpp>
#include <Teuchos_Comm.hpp>
#include <Teuchos_YamlParameterListCoreHelpers.hpp>
#include <Teuchos_TimeMonitor.hpp>

#include <Tpetra_CrsMatrix_decl.hpp>
#include <Tpetra_CrsMatrix_def.hpp>
#include <Tpetra_CrsGraph_decl.hpp>
#include <Tpetra_CrsGraph_def.hpp>
#include <Tpetra_Map_decl.hpp>
#include <Tpetra_ConfigDefs.hpp>
#include <Tpetra_ConfigDefs.hpp>
#include <MatrixMarket_Tpetra.hpp>

#include <BelosTpetraAdapter.hpp>
#include <BelosSolverFactory.hpp>
#include <BelosMueLuAdapter.hpp>

#include <MueLu.hpp>
#include <MueLu_CreateTpetraPreconditioner.hpp>

#include <Ifpack2_Factory.hpp>

namespace ark_rt { namespace linearAlgebra{
  template<int dim>
  class Trilinos_type
  {
    public: 
      using node_type = Tpetra::Map<>::node_type;
      using local_ordinal_type = Tpetra::Map<>::local_ordinal_type;
      using global_ordinal_type = Tpetra::Map<>::global_ordinal_type;
      using scalar_type = Tpetra::Vector<>::scalar_type;
      using vec_type =  Tpetra::MultiVector<scalar_type, local_ordinal_type, global_ordinal_type, node_type>;
      using map_type = Tpetra::Map<local_ordinal_type, global_ordinal_type, node_type>;
      using graph_type =  Tpetra::CrsGraph<local_ordinal_type, global_ordinal_type, node_type>;
      using matrix_type = Tpetra::CrsMatrix<scalar_type, local_ordinal_type, global_ordinal_type, node_type>;
      using op_type = Tpetra::Operator<scalar_type,local_ordinal_type,global_ordinal_type,node_type>;
      using problem_type = Belos::LinearProblem<scalar_type, vec_type, op_type>;
      using muelu_op_type = MueLu::TpetraOperator<scalar_type,local_ordinal_type,global_ordinal_type,node_type>;
      using ifpack2_op_type = Ifpack2::Preconditioner<scalar_type,local_ordinal_type,global_ordinal_type,node_type>;
      using local_matrix_type = matrix_type::local_matrix_type;
      using local_map_type = map_type::local_map_type;
      using vector_view_type = Kokkos::View<scalar_type*, device>;
      using dual_view = Kokkos::DualView<size_t*>;
      using DataArray = typename DataArrays<dim>::DataArray;
      using DataArrayHost = typename DataArrays<dim>::DataArrayHost;
  }; // class Trilinos_type

/**
 * Retrieve cartesian mapping from index
 * for linear algebra
 */

/* 2D */

KOKKOS_INLINE_FUNCTION
void linearAlgebra_index2coord(int index, int &i, int &j, int Nx, int Ny)
{
    UNUSED(Nx);
    UNUSED(Ny);

    j = index / Nx;
    i = index - j*Nx;
}

KOKKOS_INLINE_FUNCTION
int linearAlgebra_coord2index(int i, int j, int Nx, int Ny)
{
    UNUSED(Nx);
    UNUSED(Ny);
    return i + Nx*j; // left layout
}

/* 3D */

KOKKOS_INLINE_FUNCTION
void linearAlgebra_index2coord(int index,
                 int &i, int &j, int &k,
                 int Nx, int Ny, int Nz)
{
    UNUSED(Nx);
    UNUSED(Nz);
    int NxNy = Nx*Ny;
    k = index / NxNy;
    j = (index - k*NxNy) / Nx;
    i = index - j*Nx - k*NxNy;
}

KOKKOS_INLINE_FUNCTION
int linearAlgebra_coord2index(int i,  int j,  int k,
                int Nx, int Ny, int Nz)
{
    UNUSED(Nx);
    UNUSED(Nz);
    return i + Nx*j + Nx*Ny*k; // left layout
}

KOKKOS_INLINE_FUNCTION
int global_coord2index(int rank, int i, int j, int nx, int ny)
{
  const int nbCells = nx*ny;
  return rank*nbCells+linearAlgebra_coord2index(i, j, nx, ny);
} // LinearSolver::global_coord2index

// list boundary condition for the implicit solver
// this is different from params.neighborsBC
// because it lists the problem_name
// if a new problem_name is added, this list must be updated
enum implicit_boundary
{
  marshak_wave_1d,
  radiative_transfer_well_balanced,
  marshak_wave_2d,
  radiative_transfer_perf_2d,
  shadow,
  beam,
  radiative_shock,
  HII_region,
  HII_conv
};

} // namespace linearAlgebra
} // namespace ark_rt
