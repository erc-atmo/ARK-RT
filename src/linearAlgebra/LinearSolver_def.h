// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

  template<int dim>
void LinearSolver<dim>::create(typename Trilinos_type<dim>::DataArray Udata, const HydroParams params, const int nbCells, const std::string problem_name, const real_t dt, SolverBase* solverHydro)
{
  // timers
  Teuchos::RCP<Teuchos::Time> initializationTimer = Teuchos::TimeMonitor::getNewCounter("ek: Initizlization");
  initializationTimer->start();
  initializationTimer->incrementNumCalls();

  this->solverHydro = solverHydro;
  // set m_problem_name
  if(problem_name == "marshak_wave_1d")
    m_problem_name = marshak_wave_1d;
  else if(problem_name == "radiative_transfer_well_balanced")
    m_problem_name = radiative_transfer_well_balanced;
  else if(problem_name == "marshak_wave_2d")
    m_problem_name = marshak_wave_2d;
  else if(problem_name == "radiative_transfer_perf_2d")
    m_problem_name = radiative_transfer_perf_2d;
  else if(problem_name == "shadow")
    m_problem_name = shadow;
  else if(problem_name == "beam")
    m_problem_name = beam;
  else if(problem_name == "radiative_shock")
    m_problem_name = radiative_shock;
  else if(problem_name == "HII_region")
    m_problem_name = HII_region;
  else if(problem_name == "HII_conv")
    m_problem_name = HII_conv;
  else
  {
    std::cout << "Problem : " << problem_name
      << " has no boundary conditions implemented in the matrix"
      << std::endl;
    std::cout << "Exiting..." << std::endl;
    std::exit(EXIT_FAILURE);
  }

  this->dt_implicit = params.settings.cfl_rad / params.settings.speedOfLight * FMIN(params.dx, params.dy);
  this->useImplicit = false;


  // The global number of rows in the matrix  to create.  
#ifdef USE_MPI
  const Tpetra::global_size_t numGlobalElements = nbVar*params.nx*params.ny*params.nProcs;
#else
  const Tpetra::global_size_t numGlobalElements = nbVar*params.nx*params.ny;
#endif
  // Construct a Map
  const global_ordinal_type indexBase = 0;
  // use the commmunicator from params
#ifdef USE_MPI
  Teuchos::RCP<const Teuchos::Comm<int> > comm = Teuchos::rcp(new Teuchos::MpiComm<int> (params.communicator->getComm()));
#else
  Teuchos::RCP<const Teuchos::SerialComm<local_ordinal_type> > comm = Teuchos::rcp(new Teuchos::SerialComm<local_ordinal_type>());
#endif
  Teuchos::RCP<const map_type > map =
    Teuchos::rcp (new map_type (numGlobalElements, indexBase, comm,
          Tpetra::GloballyDistributed));
  {
    auto out = Teuchos::getFancyOStream (Teuchos::rcpFromRef (std::cout));
    map->describe(*out);
  }

  // Get update list and the number of equations that this MPI process owns
  numMyElements = map->getNodeNumElements();

  // NumNz[i] will be the number of nonzero entries for the i-th
  // global equation on this MPI process.
  // numNz is a Kokkos::DualView because the argument of the graph constructor 
  // can either be a Kokkos::DualView or a Teuchos::ArrayRCP
  // the Kokkos::DualView can be used with and whithout Trilinos
  // numNz is filled on the host because:
  //    - it is done only once, so it is not a performance issue
  //    - the tests about the boundary conditions can only be done on the host
  dual_view numNz = createNumNz(params);


  // Create a Tpetra::Graph using the Map, with a static allocation
  // dictated by NumNz.  (We know exactly how many elements there will
  // be in each row, so we use static profile for efficiency.)
  // The most efficient but least flexible fill method is to create the CrsMatrix with a constant graph
  // https://trilinos.org/docs/dev/packages/tpetra/doc/html/Tpetra_Lesson04.html
  Teuchos::RCP<graph_type> G = Teuchos::rcp (new graph_type (map, numNz, Tpetra::StaticProfile));
  createGraph(params, G);
  G->fillComplete();
  {
    auto out = Teuchos::getFancyOStream (Teuchos::rcpFromRef (std::cout));
    G->describe(*out);
  }
  // Create vectors ("multivectors" may store one or more vectors)  Set to zeros by default.
  // fillComplete() must have been called before getDomainMap
  X = Teuchos::rcp(new vec_type (G->getDomainMap() ,1));
  B = Teuchos::rcp(new vec_type (G->getDomainMap(), 1));
  U = Teuchos::rcp(new vec_type (G->getDomainMap(), 1));
  {
    // Get a nonconst persisting view of the entries in the Vector.
    // "Nonconst" means that you may modify the entries.  "Persisting"
    // means that the view persists beyond the lifetime of the Vector.
    // Even after the Vector's destructor is called, the view won't go
    // away.  If you create two nonconst persisting views of the same
    // Vector, and modify the entries of one view during the lifetime
    // of the other view, the entries of the other view are undefined.
    // https://trilinos.org/docs/dev/packages/tpetra/doc/html/Tpetra_Lesson02.html
    // https://github.com/trilinos/Trilinos/issues/2845
    if (U->template need_sync<device> ())
    {
      U->template sync<device> (); // sync'ing isn't free; don't do unless needed
    }
    U->template modify<device> ();
    auto u_2d = U->template getLocalView<device> (); // rank-2 Kokkos::View
    auto u_1d = Kokkos::subview (u_2d, Kokkos::ALL (), 0); // just the first column
    LinearSolverFunctors::Udata2vector<dim>::apply(Udata, u_1d, params.nx*params.ny, params, nbVar);
  }

  // Create the sparse matrix from the graph
  A = Teuchos::rcp( new matrix_type (G));

  A->fillComplete();
  {
    auto out = Teuchos::getFancyOStream (Teuchos::rcpFromRef (std::cout));
    A->describe(*out);
    A->getColMap()->describe(*out);
  }
  // deal with the solver and the preconditioner
  // get the parameters from the yaml file
  Teuchos::RCP<Teuchos::ParameterList> AllParam = Teuchos::getParametersFromYamlFile(params.settings.linearSolverParams);
  Teuchos::ParameterList& InputsParam = AllParam->get<Teuchos::ParameterList>("Inputs");
  Teuchos::ParameterList& BelosParam  = AllParam->get<Teuchos::ParameterList>("Belos");
  MueluParam  = AllParam->get<Teuchos::ParameterList>("MueLu");
  const std::string ifpack2_type = InputsParam.get<std::string>("ifpack2_type","RELAXATION");
  const std::string solver_type = InputsParam.get<std::string>("solver_type");
  const std::string precond = InputsParam.get<std::string>("precond","none");
  useMueLu = InputsParam.get<bool>("useMueLu", true);
  updateDevice = InputsParam.get<bool>("updateDevice", true);


  if(updateDevice)
  {
    if (B->template need_sync<device> ())
    {
      B->template sync<device> (); // sync'ing isn't free; don't do unless needed
    }
    B->template modify<device> ();
    auto b_2d = B->template getLocalView<device> ();
    auto b_1d = Kokkos::subview (b_2d, Kokkos::ALL (), 0);
    UpdateLinearProblem<dim>::apply(params, params.nx*params.ny, A->getLocalMatrix(), A->getColMap()->getLocalMap(), b_1d, nbVar, Udata, Udata, dt, m_problem_name, true);
    Kokkos::fence();
  }
  else
  {
    A->resumeFill();
    {
      if (B->template need_sync<device> ())
      {
        B->template sync<device> (); // sync'ing isn't free; don't do unless needed
      }
      B->template modify<device> ();
      auto b_2d = B->template getLocalView<device> ();
      auto b_1d = Kokkos::subview (b_2d, Kokkos::ALL (), 0);

      UpdateRhs<dim>::apply(params, params.nx*params.ny, b_1d, nbVar, Udata, Udata, dt);
    }
    UdataHost = Kokkos::create_mirror(Udata);
    Kokkos::deep_copy(UdataHost, Udata); // copy Udata into UdataHost
    updateMatrix<dim>(UdataHost, UdataHost, params, dt, A, nbVar, m_problem_name);
    // Tell the matrix that we are done changing its values
    A->fillComplete();
  }


  // get factory
  Belos::SolverFactory<scalar_type, vec_type, op_type> factory;
  // Create, configure, and return the specified solver. 
  solver = factory.create(solver_type, Teuchos::rcpFromRef(BelosParam));
  // Create a LinearProblem struct with the problem to solve.
  // A, X, B, and M are passed by (smart) pointer, not copied.
  // problem_type encapsulates the general information needed for solving a linear system of equations using an iterative method.
  Teuchos::RCP<problem_type> problem = Teuchos::rcp(new problem_type(A, X, B));
  if(useMueLu)
  {
    // Create AMG Preconditionner.
    // the method that take a matrix_type as argument is deprecated
    M = MueLu::CreateTpetraPreconditioner<scalar_type,local_ordinal_type,global_ordinal_type,node_type>((Teuchos::RCP<op_type>)A,MueluParam);
    if (precond=="left") {
      // You don't have to call this if you don't have a preconditioner.
      // If M is null, then Belos won't use a (left) preconditioner.
      problem->setLeftPrec(M);
    } else if (precond=="right") {
      // You don't have to call this if you don't have a preconditioner.
      // If M is null, then Belos won't use a (left) preconditioner.
      problem->setRightPrec(M);
    }
  }
  else
  {
    // use ifpack2 preconditioner
    Ifpack2::Factory ifpack2_factory;
    Teuchos::ParameterList& Ifpack2Param = AllParam->get<Teuchos::ParameterList>("Ifpack2");
    M_ifpack2 = ifpack2_factory.create<matrix_type>(ifpack2_type, A);
    M_ifpack2->setParameters(Ifpack2Param);
    M_ifpack2->initialize();
    M_ifpack2->compute();
    if (precond=="left") {
      // You don't have to call this if you don't have a preconditioner.
      // If M is null, then Belos won't use a (left) preconditioner.
      problem->setLeftPrec(M_ifpack2);
    } else if (precond=="right") {
      // You don't have to call this if you don't have a preconditioner.
      // If M is null, then Belos won't use a (left) preconditioner.
      problem->setRightPrec(M_ifpack2);
    }
  }

  // Tell the LinearProblem to make itself ready to solve.
  // Set up the linear problem manager.
  problem->setProblem ();
  // Tell the solver what problem you want to solve.
  // Set the linear problem that needs to be solved.
  solver->setProblem (problem);
  initializationTimer->stop();
}; // LinearSolver::create



  template<int dim>
void LinearSolver<dim>::solve(DataArray Udata, const HydroParams params, const int nbCells, const real_t dt)
{
  // solving f(x) = 0,
  // Newton-Raphson method can be written as
  // x_{k+1} = x_k - (Df(x_k))^{-1}f(x_k)
  // or
  // 1. solve A X = B
  //    with A = Df(x_k)
  //         B = f(x_k)
  // 2. U = U - X

  // timers
  Teuchos::RCP<Teuchos::Time> vector2UdataTimer = Teuchos::TimeMonitor::getNewCounter("ek: Time change data structures (1D or 2D)");
  Teuchos::RCP<Teuchos::Time> commTimer = Teuchos::TimeMonitor::getNewCounter("ek: Newton-Raphson communications");
  Teuchos::RCP<Teuchos::Time> linearSolverTimer = Teuchos::TimeMonitor::getNewCounter("ek: Call Belos solve");
  Teuchos::RCP<Teuchos::Time> preconditionerTimer = Teuchos::TimeMonitor::getNewCounter("ek: Call MueLu preconditioner");
  Teuchos::RCP<Teuchos::Time> updateMatrixTimer = Teuchos::TimeMonitor::getNewCounter("ek: Update matrix");
  Teuchos::RCP<Teuchos::Time> updateRhsTimer = Teuchos::TimeMonitor::getNewCounter("ek: Update RHS");
  Teuchos::RCP<Teuchos::Time> newtonTimer = Teuchos::TimeMonitor::getNewCounter("ek: Other Newton-Raphson operations");

  vector2UdataTimer->start();
  vector2UdataTimer->incrementNumCalls();
  {
    if (U->template need_sync<device> ())
    {
      U->template sync<device> (); // sync'ing isn't free; don't do unless needed
    }
    U->template modify<device> ();
    auto u_2d = U->template getLocalView<device> (); // rank-2 Kokkos::View
    auto u_1d = Kokkos::subview (u_2d, Kokkos::ALL (), 0); // just the first column
    LinearSolverFunctors::Udata2vector< dim>::apply(Udata, u_1d, params.nx*params.ny, params, nbVar);
  }
  vector2UdataTimer->stop();

  // declare norms
  Teuchos::Array<scalar_type> normXArray(1);
  newtonTimer->start();
  newtonTimer->incrementNumCalls();
  Teuchos::Array<scalar_type> normUArray(1);
  X->normInf(normXArray);
  real_t normX = normXArray[0];
  U->normInf(normUArray);
  real_t normU = normUArray[0];

  // we need to copy Udata the update the RHS of the system
  DataArray UdataOld = Kokkos::create_mirror(Udata);
  Kokkos::deep_copy(UdataOld, Udata); // copy Udata into UdataOld, 
  DataArrayHost UdataHost = Kokkos::create_mirror(Udata);
  DataArray UdataOldHost = Kokkos::create_mirror(Udata);
  Kokkos::deep_copy(UdataOldHost, Udata); // copy Udata into UdataOld, 
  newtonTimer->stop();

  for(int it_newton = 0; it_newton < params.settings.max_newton; it_newton++)
  {
    // update values inside the matrix, the right hand side and the initial guess
    if(updateDevice)
    {
      updateMatrixTimer->start();
      updateMatrixTimer->incrementNumCalls();
      if (B->template need_sync<device> ())
      {
        B->template sync<device> (); // sync'ing isn't free; don't do unless needed
      }
      B->template modify<device> ();
      auto b_2d = B->template getLocalView<device> ();
      auto b_1d = Kokkos::subview (b_2d, Kokkos::ALL (), 0);
      UpdateLinearProblem<dim>::apply(params, params.nx*params.ny, A->getLocalMatrix(), A->getColMap()->getLocalMap(), b_1d, nbVar, Udata, UdataOld, dt, m_problem_name, true);
      Kokkos::fence();
      updateMatrixTimer->stop();
    }
    else
    {
      updateMatrixTimer->start();
      A->resumeFill();
      updateMatrixTimer->stop();
      updateRhsTimer->start();
      updateRhsTimer->incrementNumCalls();
      {
        if (B->template need_sync<device> ())
        {
          B->template sync<device> (); // sync'ing isn't free; don't do unless needed
        }
        B->template modify<device> ();
        auto b_2d = B->template getLocalView<device> ();
        auto b_1d = Kokkos::subview (b_2d, Kokkos::ALL (), 0);

        UpdateRhs<dim>::apply(params, params.nx*params.ny, b_1d, nbVar, Udata, UdataOld, dt);
      }
      updateRhsTimer->stop();
      newtonTimer->start();
      newtonTimer->incrementNumCalls();
      Kokkos::deep_copy(UdataHost, Udata); // copy Udata into UdataHost
      newtonTimer->stop();
      updateMatrixTimer->start();
      updateMatrixTimer->incrementNumCalls();
      updateMatrix<dim>(UdataHost, UdataOldHost, params, dt, A, nbVar, m_problem_name);
      // Tell the matrix that we are done changing its values
      A->fillComplete();
      updateMatrixTimer->stop();
    }

    // for debugging purpose, write the matrix, the RHS and the initial guess in the matrix
    // for simplicity, overwrite the previous files
    if(params.settings.outputLinearAlgebra)
    {
      if(updateDevice)
      {
        Tpetra::MatrixMarket::Writer<vec_type>::writeDenseFile("initial_guess_device.txt", *X);
        Tpetra::MatrixMarket::Writer<matrix_type>::writeSparseFile("matrice_device.txt", A);
        Tpetra::MatrixMarket::Writer<vec_type>::writeDenseFile("rhs_device.txt", *B);
      }
      else
      {
        Tpetra::MatrixMarket::Writer<vec_type>::writeDenseFile("initial_guess_host.txt", *X);
        Tpetra::MatrixMarket::Writer<matrix_type>::writeSparseFile("matrice_host.txt", A);
        Tpetra::MatrixMarket::Writer<vec_type>::writeDenseFile("rhs_host.txt", *B);
      }
    }

    // compute the preconditiner with the new problem
    preconditionerTimer->start();
    preconditionerTimer->incrementNumCalls();
    if(useMueLu)
    {
      /* MueLu::ReuseTpetraPreconditioner(A, *M); */
      // in some tests, MueLu crashes in we just "reuse" the precondtioner
      // so we always "create" it 
      // it doesn't seems to impact the performances
      M = MueLu::CreateTpetraPreconditioner<scalar_type,local_ordinal_type,global_ordinal_type,node_type>((Teuchos::RCP<op_type>)A,MueluParam);
    }
    else
      M_ifpack2->compute();
    preconditionerTimer->stop();

    // solve the linear system
    // Performs a reset of the solver manager specified by the ResetType. 
    // This informs the solver manager that the solver should prepare for the next call to solve by resetting certain elements of the iterative solver strategy.
    linearSolverTimer->start();
    linearSolverTimer->incrementNumCalls();
    solver->reset(Belos::Problem);
    // Attempt to solve the linear system.  result == Belos::Converged 
    // means that it was solved to the desired tolerance.  This call 
    // overwrites X with the computed approximate solution.
    Belos::ReturnType result = solver->solve();
    // Ask the solver how many iterations the last solve() took.
    const int numIters = solver->getNumIters();
    const scalar_type achievedTol = solver->achievedTol();

#ifdef USE_MPI
    if(params.myRank == 0)
#endif
    {
      if (result == Belos::Converged) {
        if(params.settings.linearAlgebraVerbose)
          std::cout << "The Belos solve took " << numIters << " iteration(s) to reach "
            "a relative residual tolerance of " << achievedTol << "." << std::endl;
      } else {
        std::cout << "The Belos solve took " << numIters << " iteration(s), but did not reach " 
          "convergence, relative residual tolerance of " << solver->achievedTol() 
          << ", loss of accuracy: "<<  solver->isLOADetected() << "."  << std::endl;
        std::exit(EXIT_FAILURE);
      }
    }
    linearSolverTimer->stop();

    newtonTimer->start();
    newtonTimer->incrementNumCalls();
    U->update(ONE_F, *X, ONE_F); // U = U + X
    newtonTimer->stop();
    commTimer->start();
    commTimer->incrementNumCalls();

    // For the next iteration, we need the values of the neighbor cells
    // Use Udata and the communication routines to do it
    {
      if (U->template need_sync<device> ())
      {
        U->template sync<device> (); // sync'ing isn't free; don't do unless needed
      }
      auto u_2d = U->template getLocalView<device> (); // rank-2 Kokkos::View
      auto u_1d = Kokkos::subview (u_2d, Kokkos::ALL (), 0); // just the first column
      LinearSolverFunctors::UpdateUdataLinearSystem< dim>::apply(Udata, u_1d, params.nx*params.ny, params, nbVar);
    }
    solverHydro->make_boundaries(Udata);
    {
      if (U->template need_sync<device> ())
      {
        U->template sync<device> (); // sync'ing isn't free; don't do unless needed
      }
      U->template modify<device> ();
      auto u_2d = U->template getLocalView<device> (); // rank-2 Kokkos::View
      auto u_1d = Kokkos::subview (u_2d, Kokkos::ALL (), 0); // just the first column
      LinearSolverFunctors::Udata2vector< dim>::apply(Udata, u_1d, params.nx*params.ny, params, nbVar);
    }
    commTimer->stop();

    // check is convergence of the Newton-Raphson algorithm is reached
    newtonTimer->start();
    newtonTimer->incrementNumCalls();
    X->normInf(normXArray);
    normX = normXArray[0];
    U->normInf(normUArray);
    normU = normUArray[0];
    if(params.settings.linearAlgebraVerbose)
    {
#ifdef USE_MPI	
      if(params.myRank == 0)
#endif
        std::cout << "relative resildual tolerance = " << normX/normU << "   " << normX << "   " << normU << std::endl;
    }
    newtonTimer->stop();
    if(normX / normU < params.settings.tol_newton)
    {
      vector2UdataTimer->start();
      vector2UdataTimer->incrementNumCalls();
      // update Udata with the solution of the linear system
      {
        if (U->template need_sync<device> ())
        {
          U->template sync<device> (); // sync'ing isn't free; don't do unless needed
        }
        auto u_2d = U->template getLocalView<device> (); // rank-2 Kokkos::View
        auto u_1d = Kokkos::subview (u_2d, Kokkos::ALL (), 0); // just the first column
        LinearSolverFunctors::UpdateUdataLinearSystem< dim>::apply(Udata, u_1d, params.nx*params.ny, params, nbVar);
      }
      vector2UdataTimer->stop();
      if(params.settings.linearAlgebraVerbose)
      {
#ifdef USE_MPI
        if(params.myRank == 0)
#endif
          std::cout << "Newton-Raphson took " << it_newton+1 << " iteration(s) to reach a relative residual tolerance of " << normX/normU << '\n';
      }
      return;
    }
  } // Newton-Raphson

  // update Udata with the solution of the linear system
  {
    vector2UdataTimer->start();
    vector2UdataTimer->incrementNumCalls();
    if (U->template need_sync<device> ())
    {
      U->template sync<device> (); // sync'ing isn't free; don't do unless needed
    }
    auto u_2d = U->template getLocalView<device> (); // rank-2 Kokkos::View
    auto u_1d = Kokkos::subview (u_2d, Kokkos::ALL (), 0); // just the first column
    LinearSolverFunctors::UpdateUdataLinearSystem< dim>::apply(Udata, u_1d, params.nx*params.ny, params, nbVar);
    vector2UdataTimer->stop();
  }
#ifdef USE_MPI	
  if(params.myRank == 0)
#endif
    std::cout << "Newton-Raphson took " << params.settings.max_newton << " iterations but did not reach convergence, relative residual tolerance of " << normX/normU << '\n';
  std::exit(EXIT_FAILURE);
}; // LinearSolver::solve



  template<int dim>
typename LinearSolver<dim>::dual_view LinearSolver<dim>::createNumNz(HydroParams params)
{
  dual_view numNz("numNz", numMyElements);
  typename dual_view::t_host h_numNz = numNz.h_view;
  const int nx = params.nx;
  const int ny = params.ny;
  int i, j;

  //numNz.template modify<Kokkos::HostSpace> ();
  numNz.modify_host();
  for (int index = 0; index < nx*ny; ++index)
  {
    linearAlgebra_index2coord(index, i, j, nx, ny);
    h_numNz(nbVar*index) = 4; // E_ij
    h_numNz(nbVar*index+1) = 2; // T_ij
    h_numNz(nbVar*index+2) = 3; // Fx_ij
    h_numNz(nbVar*index+3) = 3; // Fy_ij
    if(i == 0) // xmin
    {
#ifdef USE_MPI
      if((params.neighborsBC[XMIN] == BC_COPY) || (params.neighborsBC[XMIN] == BC_PERIODIC))
      {
        h_numNz(nbVar*index) += 2; // E_i-1j
        h_numNz(nbVar*index+2) += 3; // Fx_i-1j
        h_numNz(nbVar*index+3) += 3; // Fy_i-1j
      }
      else if(params.neighborsBC[XMIN] == BC_PROBLEM_DEFINED)
#endif
      {
      }
    } // if i == 0
    else
    {
      h_numNz(nbVar*index) += 2; // E_i-1j
      h_numNz(nbVar*index+2) += 3; // Fx_i-1j
      h_numNz(nbVar*index+3) += 3; // Fy_i-1j
    }

    if(i == nx-1) // xmax
    {
#ifdef USE_MPI
      if((params.neighborsBC[XMAX] == BC_COPY) || (params.neighborsBC[XMAX] == BC_PERIODIC))
      {
        h_numNz(nbVar*index) += 2; // E_i+1j
        h_numNz(nbVar*index+2) += 3; // Fx_i+1j
        h_numNz(nbVar*index+3) += 3; // Fy_i+1j
      }
      else if(params.neighborsBC[XMAX] == BC_PROBLEM_DEFINED)
#endif
      {
      }
    } // if i == nx-1
    else
    {
      h_numNz(nbVar*index) += 2; // E_i+1j
      h_numNz(nbVar*index+2) += 3; // Fx_i+1j
      h_numNz(nbVar*index+3) += 3; // Fy_i+1j
    }

    if(j == 0) // ymin
    {
#ifdef USE_MPI
      if((params.neighborsBC[YMIN] == BC_COPY) || (params.neighborsBC[YMIN] == BC_PERIODIC))
      {
        h_numNz(nbVar*index) += 2; // E_ij-1
        h_numNz(nbVar*index+2) += 3; // Fx_ij-1
        h_numNz(nbVar*index+3) += 3; // Fy_ij-1
      }
      else if(params.neighborsBC[YMIN] == BC_PROBLEM_DEFINED)
#endif
      {
      }
    } // if j == 0
    else
    {
      h_numNz(nbVar*index) += 2; // E_ij-1
      h_numNz(nbVar*index+2) += 3; // Fx_ij-1
      h_numNz(nbVar*index+3) += 3; // Fy_ij-1
    }

    if(j == ny-1) // ymax
    {
#ifdef USE_MPI
      if((params.neighborsBC[YMAX] == BC_COPY) || (params.neighborsBC[YMAX] == BC_PERIODIC))
      {
        h_numNz(nbVar*index) += 2; // E_ij+1
        h_numNz(nbVar*index+2) += 3; // Fx_ij+1
        h_numNz(nbVar*index+3) += 3; // Fy_ij+1
      }
      else if(params.neighborsBC[YMAX] == BC_PROBLEM_DEFINED)
#endif
      {
      }
    } // if j == ny-1
    else
    {
      h_numNz(nbVar*index) += 2; // E_ij+1
      h_numNz(nbVar*index+2) += 3; // Fx_ij+1
      h_numNz(nbVar*index+3) += 3; // Fy_ij+1
    }

  }

  numNz.sync_device ();
  return numNz;
} // LinearSolver::createNumNz

  template <int dim>
void LinearSolver<dim>::createGraph(HydroParams params, Teuchos::RCP<graph_type> G)
{
  const int nx = params.nx;
  const int ny = params.ny;
  int i,j;
  global_ordinal_type myGlobEl_nei;
  auto myGlobalElements = G->getRowMap()->getMyGlobalIndices();
  for(int index = 0; index < nx*ny; index++)
  {
    linearAlgebra_index2coord(index, i, j, nx, ny);
    // row E, element i,j
    G->insertGlobalIndices(myGlobalElements[nbVar*index], Teuchos::tuple(myGlobalElements[nbVar*index], myGlobalElements[nbVar*index+1], myGlobalElements[nbVar*index+2], myGlobalElements[nbVar*index+3])); // E, T, Fx, Fy
    // row T, element i,j
    G->insertGlobalIndices(myGlobalElements[nbVar*index+1], Teuchos::tuple(myGlobalElements[nbVar*index], myGlobalElements[nbVar*index+1])); // E, T
    // row Fx, element i,j
    G->insertGlobalIndices(myGlobalElements[nbVar*index+2], Teuchos::tuple(myGlobalElements[nbVar*index], myGlobalElements[nbVar*index+2], myGlobalElements[nbVar*index+3])); // E, Fx, Fy
    // row Fy, element i,j
    G->insertGlobalIndices(myGlobalElements[nbVar*index+3], Teuchos::tuple(myGlobalElements[nbVar*index], myGlobalElements[nbVar*index+2], myGlobalElements[nbVar*index+3])); // E, Fx, Fy

    if(i == 0) // xmin
    {
#ifdef USE_MPI
      if((params.neighborsBC[XMIN] == BC_COPY) || (params.neighborsBC[XMIN] == BC_PERIODIC))
      {
        myGlobEl_nei = global_coord2index(params.neighborsRank[XMIN], nx-1, j, nx, ny);
        // row E, element i-1, j
        G->insertGlobalIndices(myGlobalElements[nbVar*index], Teuchos::tuple(nbVar*myGlobEl_nei, nbVar*myGlobEl_nei+2)); // E, Fx
        // row Fx, element i-1, j
        G->insertGlobalIndices(myGlobalElements[nbVar*index+2], Teuchos::tuple(nbVar*myGlobEl_nei, nbVar*myGlobEl_nei+2, nbVar*myGlobEl_nei+3)); // E, Fx, Fy
        // row Fy, element i-1, j
        G->insertGlobalIndices(myGlobalElements[nbVar*index+3], Teuchos::tuple(nbVar*myGlobEl_nei, nbVar*myGlobEl_nei+2, nbVar*myGlobEl_nei+3)); // E, Fx, Fy
      }
      else if(params.neighborsBC[XMIN] == BC_PROBLEM_DEFINED)
#endif
      {
      }
    }
    else
    {
      myGlobEl_nei = linearAlgebra_coord2index(i-1,j,nx,ny);
      // row E, element i-1, j
      G->insertGlobalIndices(myGlobalElements[nbVar*index], Teuchos::tuple(myGlobalElements[nbVar*myGlobEl_nei], myGlobalElements[nbVar*myGlobEl_nei+2])); // E, Fx
      // row Fx, element i-1, j
      G->insertGlobalIndices(myGlobalElements[nbVar*index+2], Teuchos::tuple(myGlobalElements[nbVar*myGlobEl_nei], myGlobalElements[nbVar*myGlobEl_nei+2], myGlobalElements[nbVar*myGlobEl_nei+3])); // E, Fx, Fy
      // row Fy, element i-1, j
      G->insertGlobalIndices(myGlobalElements[nbVar*index+3], Teuchos::tuple(myGlobalElements[nbVar*myGlobEl_nei], myGlobalElements[nbVar*myGlobEl_nei+2], myGlobalElements[nbVar*myGlobEl_nei+3])); // E, Fx, Fy
    }

    if(i == nx-1) // xmax
    {
#ifdef USE_MPI
      if((params.neighborsBC[XMAX] == BC_COPY) || (params.neighborsBC[XMAX] == BC_PERIODIC))
      {
        myGlobEl_nei = global_coord2index(params.neighborsRank[XMAX], 0, j, nx, ny);
        // row E, element i+1, j
        G->insertGlobalIndices(myGlobalElements[nbVar*index], Teuchos::tuple(nbVar*myGlobEl_nei, nbVar*myGlobEl_nei+2)); // E, Fx
        // row Fx, element i+1, j
        G->insertGlobalIndices(myGlobalElements[nbVar*index+2], Teuchos::tuple(nbVar*myGlobEl_nei, nbVar*myGlobEl_nei+2, nbVar*myGlobEl_nei+3)); // E, Fx, Fy
        // row Fy, element i+1, j
        G->insertGlobalIndices(myGlobalElements[nbVar*index+3], Teuchos::tuple(nbVar*myGlobEl_nei, nbVar*myGlobEl_nei+2, nbVar*myGlobEl_nei+3)); // E, Fx, Fy
      }
      else if(params.neighborsBC[XMAX] == BC_PROBLEM_DEFINED)
#endif
      {
      }
    }
    else
    {
      myGlobEl_nei = linearAlgebra_coord2index(i+1,j,nx,ny);
      // row E, element i+1, j
      G->insertGlobalIndices(myGlobalElements[nbVar*index], Teuchos::tuple(myGlobalElements[nbVar*myGlobEl_nei], myGlobalElements[nbVar*myGlobEl_nei+2])); // E, Fx
      // row Fx, element i+1, j
      G->insertGlobalIndices(myGlobalElements[nbVar*index+2], Teuchos::tuple(myGlobalElements[nbVar*myGlobEl_nei], myGlobalElements[nbVar*myGlobEl_nei+2], myGlobalElements[nbVar*myGlobEl_nei+3])); // E, Fx, Fy
      // row Fy, element i+1, j
      G->insertGlobalIndices(myGlobalElements[nbVar*index+3], Teuchos::tuple(myGlobalElements[nbVar*myGlobEl_nei], myGlobalElements[nbVar*myGlobEl_nei+2], myGlobalElements[nbVar*myGlobEl_nei+3])); // E, Fx, Fy
    }

    if(j == 0) // ymin
    {
#ifdef USE_MPI
      if((params.neighborsBC[YMIN] == BC_COPY) || (params.neighborsBC[YMIN] == BC_PERIODIC))
      {
        myGlobEl_nei = global_coord2index(params.neighborsRank[YMIN], i, ny-1, nx, ny);
        // row E, element i, j-1
        G->insertGlobalIndices(myGlobalElements[nbVar*index], Teuchos::tuple(nbVar*myGlobEl_nei, nbVar*myGlobEl_nei+3)); // E, Fy
        // row Fx, element i, j-1
        G->insertGlobalIndices(myGlobalElements[nbVar*index+2], Teuchos::tuple(nbVar*myGlobEl_nei, nbVar*myGlobEl_nei+2, nbVar*myGlobEl_nei+3)); // E, Fx, Fy
        // row Fy, element i, j-1
        G->insertGlobalIndices(myGlobalElements[nbVar*index+3], Teuchos::tuple(nbVar*myGlobEl_nei, nbVar*myGlobEl_nei+2, nbVar*myGlobEl_nei+3)); // E, Fx, Fy
      }
      else if(params.neighborsBC[YMIN] == BC_PROBLEM_DEFINED)
#endif
      {
      }
    }
    else
    {
      myGlobEl_nei = linearAlgebra_coord2index(i,j-1,nx,ny);
      // row E, element i, j-1
      G->insertGlobalIndices(myGlobalElements[nbVar*index], Teuchos::tuple(myGlobalElements[nbVar*myGlobEl_nei], myGlobalElements[nbVar*myGlobEl_nei+3])); // E, Fy
      // row Fx, element i, j-1
      G->insertGlobalIndices(myGlobalElements[nbVar*index+2], Teuchos::tuple(myGlobalElements[nbVar*myGlobEl_nei], myGlobalElements[nbVar*myGlobEl_nei+2], myGlobalElements[nbVar*myGlobEl_nei+3])); // E, Fx, Fy
      // row Fy, element i, j-1
      G->insertGlobalIndices(myGlobalElements[nbVar*index+3], Teuchos::tuple(myGlobalElements[nbVar*myGlobEl_nei], myGlobalElements[nbVar*myGlobEl_nei+2], myGlobalElements[nbVar*myGlobEl_nei+3])); // E, Fx, Fy
    }

    if(j == ny-1) // ymax
    {
#ifdef USE_MPI
      if((params.neighborsBC[YMAX] == BC_COPY) || (params.neighborsBC[YMAX] == BC_PERIODIC))
      {
        myGlobEl_nei = global_coord2index(params.neighborsRank[YMAX], i, 0, nx, ny);
        // row E, element i, j+1
        G->insertGlobalIndices(myGlobalElements[nbVar*index], Teuchos::tuple(nbVar*myGlobEl_nei, nbVar*myGlobEl_nei+3)); // E, Fy
        // row Fx, element i, j-1
        G->insertGlobalIndices(myGlobalElements[nbVar*index+2], Teuchos::tuple(nbVar*myGlobEl_nei, nbVar*myGlobEl_nei+2, nbVar*myGlobEl_nei+3)); // E, Fx, Fy
        // row Fy, element i, j-1
        G->insertGlobalIndices(myGlobalElements[nbVar*index+3], Teuchos::tuple(nbVar*myGlobEl_nei, nbVar*myGlobEl_nei+2, nbVar*myGlobEl_nei+3)); // E, Fx, Fy
      }
      else if(params.neighborsBC[YMAX] == BC_PROBLEM_DEFINED)
#endif
      {
      }
    }
    else
    {
      myGlobEl_nei = linearAlgebra_coord2index(i,j+1,nx,ny);
      // row E, element i, j+1
      G->insertGlobalIndices(myGlobalElements[nbVar*index], Teuchos::tuple(myGlobalElements[nbVar*myGlobEl_nei], myGlobalElements[nbVar*myGlobEl_nei+3])); // E, Fy
      // row Fx, element i, j+1
      G->insertGlobalIndices(myGlobalElements[nbVar*index+2], Teuchos::tuple(myGlobalElements[nbVar*myGlobEl_nei], myGlobalElements[nbVar*myGlobEl_nei+2], myGlobalElements[nbVar*myGlobEl_nei+3])); // E, Fx, Fy
      // row Fy, element i, j+1
      G->insertGlobalIndices(myGlobalElements[nbVar*index+3], Teuchos::tuple(myGlobalElements[nbVar*myGlobEl_nei], myGlobalElements[nbVar*myGlobEl_nei+2], myGlobalElements[nbVar*myGlobEl_nei+3])); // E, Fx, Fy
    }
  } // for
} // LinearSolver::createGraph



template <int dim>
real_t LinearSolver<dim>::compute_max_opacity(HydroParams params, DataArray U, int nbCells) const
{
  real_t max_opacity_local = ZERO_F;
  LinearSolverFunctors::ComputeMaxOpacity<dim>::apply(params, U, max_opacity_local, nbCells);
#ifdef USE_MPI
  // synchronize all MPI processes
  params.communicator->synchronize();

  // perform MPI_Reduceall to get global time step
  real_t max_opacity_global;
  params.communicator->allReduce(&max_opacity_local, &max_opacity_global, 1, params.data_type, hydroSimu::MpiComm::MAX);
  return max_opacity_global;
#else
  return max_opacity_local;
#endif

}

template <int dim>
real_t LinearSolver<dim>::compute_max_relative_values(HydroParams params, DataArray Unp1, DataArray Un, int nbCells, VarIndex varToReduce) const
{
  real_t varrel_loc = ZERO_F;
  LinearSolverFunctors::ComputeVarrel<dim>::apply(params, Unp1, Un, varToReduce, varrel_loc, nbCells);

#ifdef USE_MPI
  // synchronize all MPI processes
  params.communicator->synchronize();

  // perform MPI_Reduceall to get global time step
  real_t varrel_global;
  params.communicator->allReduce(&varrel_loc, &varrel_global, 1, params.data_type, hydroSimu::MpiComm::MAX);
  return varrel_global;
#else
  return varrel_loc;
#endif

}; // LinearSolver::compute_max_relative_values

  template <int dim>
real_t LinearSolver<dim>::compute_dt_rad(HydroParams params, DataArray Unp1, DataArray Un, int nbCells, const real_t dt_hydro)
{
  // mimic the subroutine pasdt_ray from Heracles
  // Computes the maximum allowed implicit timestep for the M1
  // radiative transfer.

  // always use the hydro time step
  if(params.settings.cfl_rad < ZERO_F)
    return dt_hydro;

  // Computes the maximum allowed implicit timestep for the M1 radiative transfer.
  real_t dt_rad;
  if(params.settings.wellBalancedScheme)
  {
    const real_t dt_rad_wellBalanced = TWO_F / params.settings.speedOfLight / compute_max_opacity(params, Unp1, nbCells);
    const real_t dt_rad_hll = FMIN(params.dx, params.dy) / params.settings.speedOfLight;
    dt_rad = params.settings.cfl_rad*FMIN(dt_rad_wellBalanced, dt_rad_hll);
  }
  else
    dt_rad = params.settings.cfl_rad / params.settings.speedOfLight * FMIN(params.dx, params.dy);
  if(params.settings.implicit_enabled == EXPLICIT)
  {
    return FMIN(dt_rad, dt_hydro);
  }
  else // implicit or ray tracing
  {
    if(!params.settings.restrictTimeStepRT)
    {
      return FMIN(dt_rad, dt_hydro);
    }
    else
    {
      const real_t vardt = params.settings.vardt;
      const real_t varrel_max = params.settings.varrel_max;

      const real_t varrel_erad = compute_max_relative_values(params, Unp1, Un, nbCells, IERAD);
      const real_t varrel_gastemp = compute_max_relative_values(params, Unp1, Un, nbCells, ITRAD);
      const real_t varrel_fx = compute_max_relative_values(params, Unp1, Un, nbCells, IFY);

      const real_t dt_erad = vardt*this->dt_implicit*FMIN(varrel_erad, varrel_max/vardt)/varrel_erad;
      const real_t dt_gastemp = vardt*this->dt_implicit*FMIN(varrel_gastemp, varrel_max/vardt)/varrel_gastemp;
      const real_t dt_fx = vardt*this->dt_implicit*FMIN(varrel_fx, varrel_max/vardt)/varrel_fx;
      this->dt_implicit = FMAX(dt_implicit, FMIN(dt_erad, FMIN(dt_gastemp, dt_fx))); // if the computed dt is smaller than dt_implicit, let's keep dt_implicit

      if(this->dt_implicit < 10.0*dt_rad)
      {
        this->useImplicit = false;
        return FMIN(dt_rad, dt_hydro);
      }
      else
      {
        this->useImplicit = true;
        return FMIN(this->dt_implicit, dt_hydro);
      }
    }
  }
}; // LinearSolver::compute_dt_rad
