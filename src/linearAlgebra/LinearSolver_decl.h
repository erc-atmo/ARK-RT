// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "shared/kokkos_shared.h"
#include "shared/HydroParams.h"
#include "UpdateDevice.h"
#include "UpdateHost.h"

#include "all_regime_radiative_transfer/SolverHydroAllRegimeRT.h"

// linear algebra
#include "LinearSolverFunctors.h"

// Trilinos
#include "Trilinos_shared.h"

namespace ark_rt { namespace linearAlgebra{


  template<int dim>
    class LinearSolver{
      public: 
        using DataArray = typename Trilinos_type<dim>::DataArray;
        using DataArrayHost = typename Trilinos_type<dim>::DataArrayHost;
        using node_type = typename Trilinos_type<dim>::node_type;
        using global_ordinal_type = typename Trilinos_type<dim>::global_ordinal_type;
        using local_ordinal_type = typename Trilinos_type<dim>::local_ordinal_type;
        using scalar_type = typename Trilinos_type<dim>::scalar_type;
        using map_type = typename Trilinos_type<dim>::map_type;
        using vec_type = typename Trilinos_type<dim>::vec_type;
        using matrix_type = typename Trilinos_type<dim>::matrix_type;
        using graph_type = typename Trilinos_type<dim>::graph_type;
        using problem_type = typename Trilinos_type<dim>::problem_type;
        using op_type = typename Trilinos_type<dim>::op_type;
        using muelu_op_type = typename Trilinos_type<dim>::muelu_op_type;
        using ifpack2_op_type = typename Trilinos_type<dim>::ifpack2_op_type;
        using dual_view = typename Trilinos_type<dim>::dual_view;

      private:
        Teuchos::RCP<matrix_type> A  = Teuchos::null; /*!< matrix */
        Teuchos::RCP<vec_type> X = Teuchos::null; /*!< initial guess and solution */
        Teuchos::RCP<vec_type> B = Teuchos::null; /*!< right hand side */
        Teuchos::RCP<Belos::SolverManager<scalar_type, vec_type, op_type> > solver = Teuchos::null; /*!< linear solver */
        Teuchos::RCP<muelu_op_type> M = Teuchos::null; /*!< AMG preconditioner */
        Teuchos::RCP<ifpack2_op_type> M_ifpack2 = Teuchos::null; /*!< Ifpack2 preconditioner */
        Teuchos::RCP<vec_type> U = Teuchos::null; /*!< need one more vector using Newton-Raphson method */
        SolverBase* solverHydro = NULL; /*!< store a pointer to the hydro solver to call the function make_boundaries */
        DataArrayHost UdataHost;
        Teuchos::ParameterList MueluParam;

        bool useMueLu = true;
        size_t numMyElements; /*!< number of elements in the matrix per MPI process */
        const int nbVar = 4; /*!< number of variables used in the linear system. For example, if nbVar = 2, the system is solved for radiative energy and radiative temperature */
        implicit_boundary m_problem_name;

        real_t dt_implicit; /*!< implicit time step with the variations are limited */
        bool useImplicit;   /*!< use or not the implicit solver if the variations are limited */

        bool updateDevice; /*!< update the matrix from the host or the device */

      public:
        LinearSolver() {};

        ~LinearSolver()
        {
          // Summarize global performance timing results, for all timers
          // created using TimeMonitor::getNewCounter().
          Teuchos::TimeMonitor::summarize ();
        };

        bool useImplicitSolver() const
        {
          return this->useImplicit;
        }

        std::string getSolverType(HydroParams params) const
        {
          if(params.settings.implicit_enabled == RAY_TRACING)
            return "Ray tracing";
          else if(params.settings.implicit_enabled == EXPLICIT || (params.settings.restrictTimeStepRT && useImplicit))
            return "Explicit";
          else
            return "Implicit";
        }

        void create(DataArray Udata, const HydroParams params, const int nbCells, const std::string problem_name, const real_t dt, SolverBase* solverHydro);
        void solve(DataArray Udata, const HydroParams params, const int nbCells, const real_t dt);

      private:
        real_t compute_max_opacity(HydroParams params, DataArray U, int nbCells) const;

        
        real_t compute_max_relative_values(HydroParams params, DataArray Unp1, DataArray Un, int nbCells, VarIndex varToReduce) const;


      public:
        real_t compute_dt_rad(HydroParams params, DataArray Unp1, DataArray Un, int nbCells, const real_t dt_hydro);



      private:

        dual_view createNumNz(HydroParams params);
        void createGraph(HydroParams params, Teuchos::RCP<graph_type> G);



    }; // class LinearSolver

#include "LinearSolver_def.h"

} // namespace linearAlgebra
} // namespace ark_rt
