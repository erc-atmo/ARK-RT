// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "HydroRunFunctors2D.h"
#include "HydroRunFunctors3D.h"

namespace ark_rt { namespace muscl
{

template<int dim>
struct RunFunctors
{
};

template<>
struct RunFunctors<2>
{
    using ComputeGravityStep    = ComputeGravityStep2D;
    using ComputeAndStoreFluxes = ComputeAndStoreFluxesFunctor2D;
    using ComputeSlopes         = ComputeSlopesFunctor2D;
    template <Direction dir>
    using ComputeTraceAndFluxes = ComputeTraceAndFluxes_Functor2D<dir>;
    using Update                = UpdateFunctor2D;
    template <Direction dir>
    using UpdateDir             = UpdateDirFunctor2D<dir>;
    using TimeStep              = ComputeDtFunctor2D;
    using ConvertToPrimitives   = ConvertToPrimitivesFunctor2D;
};

template<>
struct RunFunctors<3>
{
    using ComputeGravityStep    = ComputeGravityStep3D;
    using ComputeAndStoreFluxes = ComputeAndStoreFluxesFunctor3D;
    using ComputeSlopes         = ComputeSlopesFunctor3D;
    template <Direction dir>
    using ComputeTraceAndFluxes = ComputeTraceAndFluxes_Functor3D<dir>;
    template <Direction dir>
    using UpdateDir             = UpdateDirFunctor3D<dir>;
    using Update                = UpdateFunctor3D;
    using TimeStep              = ComputeDtFunctor3D;
    using ConvertToPrimitives   = ConvertToPrimitivesFunctor3D;
};

} // namespace muscl

} // namespace ark_rt
