// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#include <cstdlib>
#include <cstdio>
#include <cerrno>
#include "ark_rt.h"

#include "shared/real_type.h"    // choose between single and double precision
#include "shared/HydroParams.h"  // read parameter file
#include "shared/solver_utils.h" // print monitoring information

// solver
#include "shared/SolverFactory.h"

#ifdef USE_MPI
#include "utils/mpiUtils/GlobalMpiSession.h"
#endif // USE_MPI

// ===============================================================
// ===============================================================
// ===============================================================
int main(int argc, char* argv[])
{

    namespace ek = ark_rt;

    // Create MPI session if MPI enabled
#ifdef USE_MPI
    hydroSimu::GlobalMpiSession mpiSession(&argc, &argv);
    const int rank = hydroSimu::GlobalMpiSession::getRank();
#else
    const int rank = 0;
#endif // USE_MPI

    ek::initialize(argc, argv);

    std::atexit(ek::finalize);

    if (argc < 2)
    {
        ek::abort("Wrong number of arguments");
    }

    ek::print_kokkos_configuration();

    {
        // read parameter file and initialize parameter
        // parse parameters from input file
        const std::string filename(argv[1]);
        ConfigMap configMap(filename);
        if (configMap.ParseError() < 0)
        {
            ek::abort("Something went wrong when parsing file \""+filename+'"');
        }

        // test: create a HydroParams object
        HydroParams params = HydroParams();
        params.setup(configMap);

        // retrieve solver name from settings
        const std::string solver_name = configMap.getString("run", "solver_name", "Unknown");

        // initialize workspace memory (U, U2, ...)
        ek::SolverBase* solver = ek::SolverFactory::Instance().create(solver_name,
                                                                      params,
                                                                      configMap);

        // start computation
        if (rank == 0) std::cout << "Starting computation...\n";
        solver->timers[TIMER_TOTAL]->start();

        // output
        if (solver->should_save_solution())
        {
            solver->save_solution();
        } // end enable output

        // Hydrodynamics solver loop
        while (!solver->finished())
        {
            solver->next_iteration();

            if (solver->should_save_solution())
            {
                solver->save_solution();
            }
        } // end solver loop

        if (rank == 0) std::cout << "final time is " << solver->m_t << std::endl;

        // end of computation
        solver->timers[TIMER_TOTAL]->stop();
        if (rank == 0) std::cout << "...end of computation\n";

        ek::print_solver_monitoring_info(solver);

        delete solver;
    }

    return EXIT_SUCCESS;

} // end main
