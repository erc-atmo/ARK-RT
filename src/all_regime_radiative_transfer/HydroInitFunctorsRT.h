// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "HydroInitFunctorsRT2D.h"
#include "HydroInitFunctorsRT3D.h"

namespace ark_rt{ namespace all_regime_radiative_transfer
{

template <int dim>
struct InitFunctors
{
};

template <>
struct InitFunctors<2>
{
    using AtmosphereAtRest = InitAtmosphereAtRestFunctor2D;
    using Blast            = InitBlastFunctor2D;
    using DamBreak         = InitDamBreakFunctor2D;
    using FourQuadrant     = InitFourQuadrantFunctor2D;
    using Gresho           = InitGreshoFunctor2D;
    using Implode          = InitImplodeFunctor2D;
    using IsentropicVortex = InitIsentropicVortexFunctor2D;
    using Poiseuille       = InitPoiseuilleFunctor2D;
    using RadiativeConvection = InitRadiativeConvectionFunctor2D;
    using RayleighBenard   = InitRayleighBenardFunctor2D;
    using RayleighTaylor   = InitRayleighTaylorFunctor2D;
    using RiemannProblem   = InitRiemannProblemFunctor2D;
    using RadiativeTransferM1 = InitRadiativeTransferM1Functor2D;
    using RTPerf2D         = InitRTPerf2DFunctor2D;
    using RT2DShadow 	   = InitRT2DShadowFunctor2D;
    using RT2DBeam 	   = InitRT2DBeamFunctor2D;
    using RadiativeShock   = InitRadiativeShockFunctor2D;
    using HIIRegion 	   = InitHIIRegionFunctor2D;
    using HIIConv 	   = InitHIIConvFunctor2D;
};

template <>
struct InitFunctors<3>
{
    using Blast          = InitBlastFunctor3D;
    using Implode        = InitImplodeFunctor3D;
    using Poiseuille     = InitPoiseuilleFunctor3D;
    using RadiativeConvection = InitRadiativeConvectionFunctor3D;
    using RayleighBenard = InitRayleighBenardFunctor3D;
    using RayleighTaylor = InitRayleighTaylorFunctor3D;
};

} // namespace all_regime_radiative_transfer

} // namespace ark_rt
