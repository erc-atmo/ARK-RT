// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include <limits> // for std::numeric_limits
#ifdef __CUDA_ARCH__
#include <math_constants.h> // for cuda math constants, e.g. CUDART_INF
#endif // __CUDA_ARCH__

#include "shared/kokkos_shared.h"
#include "HydroBaseFunctorRT2D.h"

// init conditions
#include "shared/BlastParams.h"
#include "shared/DamBreakParams.h"
#include "shared/GreshoParams.h"
#include "shared/IsentropicVortexParams.h"
#include "shared/PoiseuilleParams.h"
#include "shared/RadiativeConvectionParams.h"
#include "shared/RayleighBenardParams.h"
#include "shared/RiemannProblemParams.h"
#include "shared/RadiativeShockParams.h"
#include "shared/RadiativeTransferParams.h"

#include "shared/Profile.h"

// kokkos random numbers
#include <Kokkos_Random.hpp>

#ifdef USE_TRILINOS
#include <Teuchos_Tuple.hpp>
#include <Teuchos_ArrayRCPDecl.hpp>
#include <Tpetra_Map_decl.hpp>
#endif

#include <algorithm>
extern "C" void  dgeev_(char *jobvl, char *jobvr, int *n, double *a, int *lda, double *wr, double *wi, double *vl, int *ldvl, double *vr, int *ldvr, double *work, int *lwork, int *info);

namespace ark_rt { namespace all_regime_radiative_transfer
  {

    class InitRadiativeConvectionFunctor2D : HydroBaseFunctor2D
    {
      public:
        InitRadiativeConvectionFunctor2D(HydroParams params_,
            RadiativeConvectionParams rc_params_,
            DataArray Udata_)
          : HydroBaseFunctor2D(params_)
            , params(params_)
            , rc_params(rc_params_)
            , Udata(Udata_)
            , profile(rc_params_.input_file)
      {
      }

        static void apply(HydroParams params,
            RadiativeConvectionParams rc_params,
            DataArray Udata, int nbcells)
        {
          InitRadiativeConvectionFunctor2D functor(params, rc_params, Udata);
          Kokkos::parallel_for(nbcells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const int i = index;
            const real_t k_b = code_units::constants::k_b;
            const real_t m_h = code_units::constants::m_h;

            const real_t Pi      = acos(-ONE_F);
            const int jmin       = params.jmin;
            const int jmax       = params.jmax;
            const int ghostWidth = params.ghostWidth;
            const real_t dy      = params.dy;
            const real_t dx      = params.dx;
            const real_t xmax    = params.xmax;
            const real_t xmin    = params.xmin;
            const real_t ymax    = params.ymax;
            const real_t ymin    = params.ymin;

            const real_t amp    = rc_params.amp;
            const real_t mode   = rc_params.mode;
            const real_t width  = rc_params.width;

            if(i >= params.imin && i <= params.imax)
            {
              const real_t x = xmin + (HALF_F+ i - ghostWidth)*dx;

              for (int j=jmin+ghostWidth; j<jmax-ghostWidth+1; ++j)
              {
                const real_t y = ymin + (HALF_F + j - ghostWidth)*dy;

                HydroState q;
                q[IP] = profile.d_data(j-ghostWidth, 0);
                q[IS] = profile.d_data(j-ghostWidth, 2);

                //compute mmw
                const real_t tt  = profile.d_data(j-ghostWidth, 1);
                const real_t mmw = computeMmw(q);
                const real_t cs  = sqrt(k_b*tt/(mmw*m_h));

                q[ID] = q[IP]/(k_b*tt)*mmw*m_h;

                if(rc_params.perturb_velocity)
                {
                  q[IU] = ZERO_F;
                  q[IV] = cs*amp*sin(x/xmax*mode*Pi)*exp(-(y-0.5*ymax)*(y-0.5*ymax)/((width*ymax)*(width*ymax)));
                }
                else
                {
                  q[IU] = ZERO_F;
                  q[IV] = ZERO_F;
                }

                q[IO] = ZERO_F;
                q[IERAD] = ONE_F;
                q[ITRAD] = ONE_F;
                q[IFX] = ZERO_F;
                q[IFY] = ZERO_F;

                setHydroState(Udata, computeConservatives(q), i, j);
              }
            }
          };

        const HydroParams params;
        const RadiativeConvectionParams rc_params;
        DataArray Udata;
        Profile profile;
    }; // InitRadiativeConvectionFunctor2D


    class InitDamBreakFunctor2D : HydroBaseFunctor2D
    {
      public:
        InitDamBreakFunctor2D(HydroParams params_,
            DamBreakParams dbParams_,
            DataArray Udata_):
          HydroBaseFunctor2D(params_), dbParams(dbParams_), Udata(Udata_) {}

        static void apply(HydroParams params, DamBreakParams dbParams,
            DataArray Udata, int nbCells)
        {
          InitDamBreakFunctor2D functor(params, dbParams, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int i) const
          {
            const real_t ghostWidth = params.ghostWidth;
            const real_t jmin = params.jmin;
            const real_t jmax = params.jmax;
            const real_t nx = params.nx;
            const real_t dx = params.dx;

            const real_t x_int = dbParams.interface_position;
            const real_t Rstar = params.settings.Rstar;

#ifdef USE_MPI
            const int i_mpi = params.myMpiPos[IX];
#else
            const int i_mpi = 0;
#endif

            if (i >= params.imin && i <= params.imax)
            {
              const real_t x = params.xmin + (HALF_F + i + nx*i_mpi - ghostWidth)*dx;

              real_t rho    = (x < x_int ? 100.0 :  10.0);
              real_t T      = (x < x_int ?  10.0 : 100.0);
              real_t scalar = (x < x_int ?   1.0 :   0.0);

              for (int j=jmax-ghostWidth; j>=jmin+ghostWidth; --j)
              {
                const real_t Dphi = phi(i, j) - phi(i, j-1);
                HydroState q;
                q[ID] = rho;
                q[IP] = rho * Rstar * T;
                q[IS] = scalar;
                q[IU] = ZERO_F;
                q[IV] = ZERO_F;

                q[IO] = ZERO_F;
                q[IERAD] = ONE_F;
                q[ITRAD] = ONE_F;
                q[IFX] = ZERO_F;
                q[IFY] = ZERO_F;

                setHydroState(Udata, computeConservatives(q), i, j);

                rho *= (TWO_F * Rstar * T + Dphi) / (TWO_F * Rstar * T - Dphi);
              }
            }
          }

        DamBreakParams dbParams;
        DataArray Udata;
    }; // InitDamBreakFunctor2D


    class InitPoiseuilleFunctor2D : HydroBaseFunctor2D
    {
      public:
        InitPoiseuilleFunctor2D(HydroParams params_,
            PoiseuilleParams poiseuilleParams_,
            DataArray Udata_):
          HydroBaseFunctor2D(params_),
          poiseuilleParams(poiseuilleParams_),
          Udata(Udata_) {};

        static void apply(HydroParams params,
            PoiseuilleParams poiseuilleParams,
            DataArray Udata, int nbCells)
        {
          InitPoiseuilleFunctor2D functor(params, poiseuilleParams, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const real_t ghostWidth = params.ghostWidth;
            const real_t xmin = params.xmin;
            const real_t ymin = params.ymin;
            const real_t nx = params.nx;
            const real_t ny = params.ny;
            const real_t dx = params.dx;
            const real_t dy = params.dy;

            int i,j;
            index2coord(index, i, j, params.isize, params.jsize);
#ifdef USE_MPI
            const int i_mpi = params.myMpiPos[IX];
            const int j_mpi = params.myMpiPos[IY];
#else
            const int i_mpi = 0;
            const int j_mpi = 0;
#endif
            const real_t pressure_gradient = poiseuilleParams.poiseuille_pressure_gradient;
            const real_t p0 = poiseuilleParams.poiseuille_pressure0;
            const real_t gamma0 = params.settings.gamma0;
            const real_t x = xmin + (HALF_F + i + nx*i_mpi-ghostWidth)*dx;
            const real_t y = ymin + (HALF_F + j + ny*j_mpi-ghostWidth)*dy;

            const real_t d = poiseuilleParams.poiseuille_density;
            const real_t u = ZERO_F;
            const real_t v = ZERO_F;
            real_t p;
            if (poiseuilleParams.poiseuille_flow_direction == IX)
            {
              p = p0 + (x - xmin) * pressure_gradient;
            }
            else
            {
              p = p0 + (y - ymin) * pressure_gradient;
            }

            Udata(i, j, ID) = d;
            Udata(i, j, IU) = d * u;
            Udata(i, j, IV) = d * v;
            Udata(i, j, IP) = p / (gamma0-ONE_F) + HALF_F * d * (u*u+v*v);
          }

        PoiseuilleParams poiseuilleParams;
        DataArray Udata;
    }; // InitPoiseuilleFunctor2D


    class InitRayleighBenardFunctor2D : HydroBaseFunctor2D
    {
      public:
        InitRayleighBenardFunctor2D(HydroParams params_,
            RayleighBenardParams rayleighBenardParams_,
            DataArray Udata_):
          HydroBaseFunctor2D(params_), rayleighBenardParams(rayleighBenardParams_),
          Udata(Udata_)
      {
        int rank = 0;
#ifdef USE_MPI
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#endif
        rand_pool = Kokkos::Random_XorShift64_Pool<device> (50+rank);

        if (rank==0)
        {
          const real_t Rstar = params.settings.Rstar;
          const real_t gamma0 = params.settings.gamma0;
          const real_t cp = params.settings.cp;
          const real_t beta = rayleighBenardParams.temperature_gradient;
          const real_t d = params.ymax-params.ymin;
          const real_t z = HALF_F*(params.ymax+params.ymin);

          const real_t prandtl = params.settings.mu * cp / params.settings.kappa;
          const real_t m = params.settings.g_y / (Rstar * beta) - ONE_F;
          const real_t Q = std::sqrt(-Rstar*beta*d)*d / (params.settings.kappa/cp);
          const real_t rayleigh = Q*Q*(m+1)*(1-(m+1)*(gamma0-ONE_F)/gamma0)*std::pow(z, 2*m-1)/prandtl;

          std::cout << "Polytropic index : " << m << std::endl;
          std::cout << "Prandtl number   : " << prandtl << std::endl;
          std::cout << "Rayleigh number  : " << rayleigh << std::endl;
        }
      }

        static void apply(HydroParams params,
            RayleighBenardParams rayleighBenardParams,
            DataArray Udata, int nbCells)
        {
          InitRayleighBenardFunctor2D functor(params, rayleighBenardParams, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int i) const
          {
            const real_t ghostWidth = params.ghostWidth;
            const real_t jmin = params.jmin;
            const real_t jmax = params.jmax;
            const real_t nx = params.nx;
            const real_t ny = params.ny;
            const real_t dx = params.dx;
            const real_t dy = params.dy;
            const real_t Lx = params.xmax - params.xmin;
            const real_t Ly = params.ymax - params.ymin;

            const real_t x_mid = HALF_F * (params.xmax + params.xmin);
            const real_t y_mid = HALF_F * (params.ymax + params.ymin);
            const real_t Pi = std::acos(-ONE_F);
            const real_t Rstar = params.settings.Rstar;

            const real_t rho_top = rayleighBenardParams.density_top;
            const real_t T_top   = rayleighBenardParams.temperature_top;
            const real_t beta    = rayleighBenardParams.temperature_gradient;
            const real_t T_bot   = T_top - beta * Ly;
            const real_t amp     = rayleighBenardParams.perturb_amplitude;
            const int    nmode   = rayleighBenardParams.nmode;

#ifdef USE_MPI
            const int i_mpi = params.myMpiPos[IX];
            const int j_mpi = params.myMpiPos[IY];
#else
            const int i_mpi = 0;
            const int j_mpi = 0;
#endif
            const real_t m = params.settings.g_y / (Rstar * beta) - ONE_F;

            if(i >= params.imin && i <= params.imax)
            {
              // To handle mpi dispatch
              real_t rho  = rho_top * std::pow(T_bot / T_top, m);
              real_t T    = T_bot;
              for (int j_glob=0; j_glob<ny*j_mpi; ++j_glob)
              {
                const real_t Dphi = phi(i, j_glob+1) - phi(i, j_glob);
                rho *= (TWO_F * Rstar * T - Dphi) / (TWO_F * Rstar * (T + beta * dy) + Dphi);
                T   += beta * dy;
              }

              for (int j=jmin+ghostWidth; j<=jmax-ghostWidth; ++j)
              {
                const real_t x = params.xmin + (HALF_F + i + nx*i_mpi - ghostWidth)*dx;
                const real_t y = params.ymin + (HALF_F + j + ny*j_mpi - ghostWidth)*dy;

                const real_t Dphi = phi(i, j+1) - phi(i, j);

                HydroState q;
                q[ID] = rho;
                q[IP] = rho *  Rstar * T;
                q[IS] = ZERO_F;
                q[IU] = ZERO_F;
                q[IV] = ZERO_F;

                if (rayleighBenardParams.perturb_temperature)
                {
                  // get random number state
                  GeneratorType rand_gen = rand_pool.get_state();

                  q[IP] *= (ONE_F + amp * rand_gen.drand());
                  // free random number
                  rand_pool.free_state(rand_gen);
                }

                if (rayleighBenardParams.perturb_velocity)
                {
                  q[IU] += amp * std::sin(nmode*TWO_F*Pi*(x-x_mid)/Lx) * std::sin(Pi*(y-y_mid)/Ly);
                  q[IV] += amp * std::cos(nmode*TWO_F*Pi*(x-x_mid)/Lx) * std::cos(Pi*(y-y_mid)/Ly);
                }

                q[IO] = ZERO_F;
                q[IERAD] = ONE_F;
                q[ITRAD] = ONE_F;
                q[IFX] = ZERO_F;
                q[IFY] = ZERO_F;

                setHydroState(Udata, computeConservatives(q), i, j);

                rho *= (TWO_F * Rstar * T - Dphi) / (TWO_F * Rstar * (T + beta * dy) + Dphi);
                T   += beta * dy;
              }
            }
          }

        const RayleighBenardParams rayleighBenardParams;
        DataArray Udata;
        using GeneratorPool = Kokkos::Random_XorShift64_Pool<device>;
        using GeneratorType = GeneratorPool::generator_type;
        GeneratorPool rand_pool; // random number generator
    }; // InitRayleighBenardFunctor2D


    class InitRayleighTaylorFunctor2D : HydroBaseFunctor2D
    {
      public:
        InitRayleighTaylorFunctor2D(HydroParams params_, DataArray Udata_) :
          HydroBaseFunctor2D(params_), Udata(Udata_) {};

        static void apply(HydroParams params,
            DataArray Udata, int nbCells)
        {
          InitRayleighTaylorFunctor2D functor(params, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const real_t ghostWidth = params.ghostWidth;
            const real_t xmin = params.xmin;
            const real_t ymin = params.ymin;
            const real_t xmax = params.xmax;
            const real_t ymax = params.ymax;
            const real_t dx = params.dx;
            const real_t dy = params.dy;
            const real_t nx = params.nx;
            const real_t ny = params.ny;

            int i,j;
            index2coord(index, i, j, params.isize, params.jsize);
#ifdef USE_MPI
            const int i_mpi = params.myMpiPos[IX];
            const int j_mpi = params.myMpiPos[IY];
#else
            const int i_mpi = 0;
            const int j_mpi = 0;
#endif
            const real_t x = xmin + (HALF_F + i + nx*i_mpi - ghostWidth)*dx;
            const real_t y = ymin + (HALF_F + j + ny*j_mpi - ghostWidth)*dy;
            const real_t Lx = xmax - xmin;
            const real_t Ly = ymax - ymin;
            const real_t Pi = std::acos(-ONE_F);

            const real_t A = 0.01;
            const real_t p0 = 2.5;
            const real_t rho = (y<=ZERO_F) ? ONE_F : TWO_F;
            const real_t v = A * (1.0+std::cos(TWO_F*Pi*x/Lx))*(1.0+std::cos(TWO_F*Pi*y/Ly))/4.0;

            Udata(i, j, ID) = rho;
            Udata(i, j, IP) = (p0 + rho*params.settings.g_y*y)/(params.settings.gamma0-ONE_F) + HALF_F*rho*v*v;
            Udata(i, j, IU) = ZERO_F;
            Udata(i, j, IV) = rho * v;
            Udata(i, j, IS) = rho * (y <= ZERO_F ? ONE_F : ZERO_F);
          }

        DataArray Udata;

    }; // InitRayleighTaylorFunctor2D


    class InitAtmosphereAtRestFunctor2D : HydroBaseFunctor2D
    {
      public:
        InitAtmosphereAtRestFunctor2D(HydroParams params_, DataArray Udata_) :
          HydroBaseFunctor2D(params_),
          params(params_), Udata(Udata_) {};

        static void apply(HydroParams params,
            DataArray Udata, int nbCells)
        {
          InitAtmosphereAtRestFunctor2D functor(params, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int i) const
          {
            const int jsize = params.jsize;
            const int jmin  = params.jmin;
            const int ghostWidth = params.ghostWidth;
            const real_t dy = params.dy;
            const real_t gamma0 = params.settings.gamma0;
            const real_t ny = params.ny;

#ifdef USE_MPI
            const int j_mpi = params.myMpiPos[IY];
#else
            const int j_mpi = 0;
#endif

            // To handle mpi dispatch
            real_t rho = ONE_F;
            for (int j_glob=1; j_glob<=ny*j_mpi; ++j_glob)
            {
              rho *= (TWO_F - dy) / (TWO_F + dy);
            }

            if(i >= params.imin && i <= params.imax)
            {
              for (int j=jmin+ghostWidth; j<jsize-ghostWidth; ++j)
              {
                rho *= (TWO_F - dy) / (TWO_F + dy);
                Udata(i, j, ID) = rho;
                Udata(i, j, IP) = rho / (gamma0-ONE_F);
                Udata(i, j, IU) = ZERO_F;
                Udata(i, j, IV) = ZERO_F;
              }
            }
          };
        const HydroParams params;
        DataArray Udata;
    }; // InitAtmosphereAtRestFunctor2D


    class InitGreshoFunctor2D : HydroBaseFunctor2D
    {
      public:
        InitGreshoFunctor2D(HydroParams params_, GreshoParams greshoParams_, DataArray Udata_):
          HydroBaseFunctor2D(params_), Udata(Udata_), greshoParams(greshoParams_) {};

        static void apply(HydroParams params, GreshoParams greshoParams,
            DataArray Udata, int nbCells)
        {
          InitGreshoFunctor2D functor(params, greshoParams, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const real_t ghostWidth = params.ghostWidth;
            const real_t xmin = params.xmin;
            const real_t ymin = params.ymin;
            const real_t xmax = params.xmax;
            const real_t ymax = params.ymax;
            const real_t dx = params.dx;
            const real_t dy = params.dy;
            const real_t nx = params.nx;
            const real_t ny = params.ny;

            const real_t gamma0 = params.settings.gamma0;
            const real_t Mach = greshoParams.gresho_mach;

            const real_t rho = ONE_F;
            const real_t p0 = rho / (gamma0 * Mach * Mach);

            int i,j;
            index2coord(index, i, j, params.isize, params.jsize);


#ifdef USE_MPI
            const int i_mpi = params.myMpiPos[IX];
            const int j_mpi = params.myMpiPos[IY];
#else
            const int i_mpi = 0;
            const int j_mpi = 0;
#endif

            real_t y = ymin + (ymax-ymin)*(j+ny*j_mpi-ghostWidth)*dy + dy/2.0;
            y -= greshoParams.gresho_center_y;

            real_t x = xmin + (xmax-xmin)*(i+nx*i_mpi-ghostWidth)*dx + dx/2.0;
            x -= greshoParams.gresho_center_x;
            const real_t r = std::sqrt(x*x + y*y);
            const real_t theta = std::atan2(y, x);

            real_t vtheta;
            real_t p;
            if (r <= 0.2)
            {
              vtheta = 5.0 * r;
              p  = p0 + 12.5 * r * r;
            }
            else if (r <= 0.4)
            {
              vtheta = 2.0 - 5.0 * r;
              p  = p0 + 12.5 * r * r + 4.0 * (1.0 - 5.0 * r + std::log(5.0*r));
            }
            else
            {
              vtheta = 0.0;
              p  = p0 - 2.0 + 4.0*std::log(2.0);
            }

            const real_t vx = - vtheta * std::sin(theta);
            const real_t vy =   vtheta * std::cos(theta);

            Udata(i, j, ID) = rho;
            Udata(i, j, IU) = rho * vx;
            Udata(i, j, IV) = rho * vy;
            Udata(i, j, IP) = p/(gamma0-1.0) + HALF_F*rho*(vx*vx + vy*vy);

          };

        DataArray Udata;
        GreshoParams greshoParams;
    }; // InitGreshoFunctor2D


    class InitImplodeFunctor2D : public HydroBaseFunctor2D
    {
      public:
        InitImplodeFunctor2D(HydroParams params_, DataArray Udata_) :
          HydroBaseFunctor2D(params_), Udata(Udata_)  {};

        static void apply(HydroParams params,
            DataArray Udata, int nbCells)
        {
          InitImplodeFunctor2D functor(params, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;
            const int ghostWidth = params.ghostWidth;

#ifdef USE_MPI
            const int i_mpi = params.myMpiPos[IX];
            const int j_mpi = params.myMpiPos[IY];
#else
            const int i_mpi = 0;
            const int j_mpi = 0;
#endif

            const int nx = params.nx;
            const int ny = params.ny;

            const real_t xmin = params.xmin;
            const real_t ymin = params.ymin;
            const real_t dx = params.dx;
            const real_t dy = params.dy;

            const real_t gamma0 = params.settings.gamma0;

            int i,j;
            index2coord(index,i,j,isize,jsize);

            real_t x = xmin + dx/2 + (i+nx*i_mpi-ghostWidth)*dx;
            real_t y = ymin + dy/2 + (j+ny*j_mpi-ghostWidth)*dy;

            real_t tmp = x+y*y;
            if (tmp > 0.5 && tmp < 1.5)
            {
              Udata(i  ,j  , ID) = 1.0;
              Udata(i  ,j  , IP) = 1.0/(gamma0-1.0);
              Udata(i  ,j  , IU) = 0.0;
              Udata(i  ,j  , IV) = 0.0;
            }
            else
            {
              Udata(i  ,j  , ID) = 0.125;
              Udata(i  ,j  , IP) = 0.14/(gamma0-1.0);
              Udata(i  ,j  , IU) = 0.0;
              Udata(i  ,j  , IV) = 0.0;
            }

          } // end operator ()

        DataArray Udata;
    }; // InitImplodeFunctor2D


    class InitBlastFunctor2D : public HydroBaseFunctor2D
    {
      public:
        InitBlastFunctor2D(HydroParams params_, BlastParams bParams_, DataArray Udata_) :
          HydroBaseFunctor2D(params_), bParams(bParams_), Udata(Udata_)  {};

        static void apply(HydroParams params, BlastParams blastParams,
            DataArray Udata, int nbCells)
        {
          InitBlastFunctor2D functor(params, blastParams, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;
            const int ghostWidth = params.ghostWidth;

#ifdef USE_MPI
            const int i_mpi = params.myMpiPos[IX];
            const int j_mpi = params.myMpiPos[IY];
#else
            const int i_mpi = 0;
            const int j_mpi = 0;
#endif

            const int nx = params.nx;
            const int ny = params.ny;

            const real_t xmin = params.xmin;
            const real_t ymin = params.ymin;
            const real_t dx = params.dx;
            const real_t dy = params.dy;

            const real_t gamma0 = params.settings.gamma0;

            // blast problem parameters
            const real_t blast_radius      = bParams.blast_radius;
            const real_t radius2           = blast_radius*blast_radius;
            const real_t blast_center_x    = bParams.blast_center_x;
            const real_t blast_center_y    = bParams.blast_center_y;
            const real_t blast_density_in  = bParams.blast_density_in;
            const real_t blast_density_out = bParams.blast_density_out;
            const real_t blast_pressure_in = bParams.blast_pressure_in;
            const real_t blast_pressure_out= bParams.blast_pressure_out;

            int i,j;
            index2coord(index,i,j,isize,jsize);

            real_t x = xmin + dx/2 + (i+nx*i_mpi-ghostWidth)*dx;
            real_t y = ymin + dy/2 + (j+ny*j_mpi-ghostWidth)*dy;

            real_t d2 =
              (x-blast_center_x)*(x-blast_center_x)+
              (y-blast_center_y)*(y-blast_center_y);

            if (d2 < radius2)
            {
              Udata(i  ,j  , ID) = blast_density_in;
              Udata(i  ,j  , IP) = blast_pressure_in/(gamma0-1.0);
              Udata(i  ,j  , IU) = 0.0;
              Udata(i  ,j  , IV) = 0.0;
            }
            else
            {
              Udata(i  ,j  , ID) = blast_density_out;
              Udata(i  ,j  , IP) = blast_pressure_out/(gamma0-1.0);
              Udata(i  ,j  , IU) = 0.0;
              Udata(i  ,j  , IV) = 0.0;
            }

          } // end operator ()

        BlastParams bParams;
        DataArray Udata;
    }; // InitBlastFunctor2D

    class InitFourQuadrantFunctor2D : public HydroBaseFunctor2D
    {
      public:
        InitFourQuadrantFunctor2D(HydroParams params_,
            DataArray Udata_,
            int configNumber_,
            HydroState U0_,
            HydroState U1_,
            HydroState U2_,
            HydroState U3_,
            real_t xt_,
            real_t yt_) :
          HydroBaseFunctor2D(params_), Udata(Udata_),
          U0(U0_), U1(U1_), U2(U2_), U3(U3_), xt(xt_), yt(yt_) {};

        static void apply(HydroParams params,
            DataArray Udata,
            int configNumber,
            HydroState U0,
            HydroState U1,
            HydroState U2,
            HydroState U3,
            real_t xt,
            real_t yt,
            int nbCells)
        {
          InitFourQuadrantFunctor2D functor(params, Udata, configNumber,
              U0, U1, U2, U3, xt, yt);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;
            const int ghostWidth = params.ghostWidth;

#ifdef USE_MPI
            const int i_mpi = params.myMpiPos[IX];
            const int j_mpi = params.myMpiPos[IY];
#else
            const int i_mpi = 0;
            const int j_mpi = 0;
#endif

            const int nx = params.nx;
            const int ny = params.ny;

            const real_t xmin = params.xmin;
            const real_t ymin = params.ymin;
            const real_t dx = params.dx;
            const real_t dy = params.dy;

            int i,j;
            index2coord(index,i,j,isize,jsize);

            real_t x = xmin + dx/2 + (i+nx*i_mpi-ghostWidth)*dx;
            real_t y = ymin + dy/2 + (j+ny*j_mpi-ghostWidth)*dy;

            if (x<xt)
            {
              if (y<yt)
              {
                // quarter 2
                Udata(i  ,j  , ID) = U2[ID];
                Udata(i  ,j  , IP) = U2[IP];
                Udata(i  ,j  , IU) = U2[IU];
                Udata(i  ,j  , IV) = U2[IV];
              }
              else
              {
                // quarter 1
                Udata(i  ,j  , ID) = U1[ID];
                Udata(i  ,j  , IP) = U1[IP];
                Udata(i  ,j  , IU) = U1[IU];
                Udata(i  ,j  , IV) = U1[IV];
              }
            }
            else
            {
              if (y<yt)
              {
                // quarter 3
                Udata(i  ,j  , ID) = U3[ID];
                Udata(i  ,j  , IP) = U3[IP];
                Udata(i  ,j  , IU) = U3[IU];
                Udata(i  ,j  , IV) = U3[IV];
              }
              else
              {
                // quarter 0
                Udata(i  ,j  , ID) = U0[ID];
                Udata(i  ,j  , IP) = U0[IP];
                Udata(i  ,j  , IU) = U0[IU];
                Udata(i  ,j  , IV) = U0[IV];
              }
            }

          } // end operator ()

        DataArray Udata;
        HydroState2d U0, U1, U2, U3;
        real_t xt, yt;
    }; // InitFourQuadrantFunctor2D


    class InitIsentropicVortexFunctor2D : public HydroBaseFunctor2D
    {
      public:
        InitIsentropicVortexFunctor2D(HydroParams params_,
            IsentropicVortexParams iparams_,
            DataArray Udata_) :
          HydroBaseFunctor2D(params_), iparams(iparams_), Udata(Udata_)  {};

        static void apply(HydroParams params, IsentropicVortexParams isentropicVortexParams,
            DataArray Udata, int nbCells)
        {
          InitIsentropicVortexFunctor2D functor(params, isentropicVortexParams, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;
            const int ghostWidth = params.ghostWidth;

#ifdef USE_MPI
            const int i_mpi = params.myMpiPos[IX];
            const int j_mpi = params.myMpiPos[IY];
#else
            const int i_mpi = 0;
            const int j_mpi = 0;
#endif

            const int nx = params.nx;
            const int ny = params.ny;

            const real_t xmin = params.xmin;
            const real_t ymin = params.ymin;
            const real_t dx = params.dx;
            const real_t dy = params.dy;

            const real_t gamma0 = params.settings.gamma0;

            int i,j;
            index2coord(index,i,j,isize,jsize);

            real_t x = xmin + dx/2 + (i+nx*i_mpi-ghostWidth)*dx;
            real_t y = ymin + dy/2 + (j+ny*j_mpi-ghostWidth)*dy;

            // ambient flow
            const real_t rho_a = this->iparams.rho_a;
            //const real_t p_a   = this->iparams.p_a;
            const real_t T_a   = this->iparams.T_a;
            const real_t u_a   = this->iparams.u_a;
            const real_t v_a   = this->iparams.v_a;
            //const real_t w_a   = this->iparams.w_a;

            // vortex center
            const real_t vortex_x = this->iparams.vortex_x;
            const real_t vortex_y = this->iparams.vortex_y;

            // relative coordinates versus vortex center
            real_t xp = x - vortex_x;
            real_t yp = y - vortex_y;
            real_t r  = std::sqrt(xp*xp + yp*yp);

            const real_t beta = this->iparams.beta;

            real_t du = - yp * beta / (2 * M_PI) * std::exp(0.5*(1.0-r*r));
            real_t dv =   xp * beta / (2 * M_PI) * std::exp(0.5*(1.0-r*r));

            real_t T = T_a - (gamma0-1)*beta*beta/(8*gamma0*M_PI*M_PI)*std::exp(1.0-r*r);
            real_t rho = rho_a*std::pow(T/T_a,1.0/(gamma0-1));

            Udata(i  ,j  , ID) = rho;
            Udata(i  ,j  , IU) = rho*(u_a + du);
            Udata(i  ,j  , IV) = rho*(v_a + dv);
            //Udata(i  ,j  , IP) = std::pow(rho,gamma0)/(gamma0-1.0) +
            Udata(i  ,j  , IP) = rho*T/(gamma0-1.0) +
              0.5*rho*(u_a + du)*(u_a + du) +
              0.5*rho*(v_a + dv)*(v_a + dv) ;

          } // end operator ()

        IsentropicVortexParams iparams;
        DataArray Udata;
    }; // InitIsentropicVortexFunctor2D


    class InitRiemannProblemFunctor2D : public HydroBaseFunctor2D
    {
      public:
        InitRiemannProblemFunctor2D(HydroParams params_,
            RiemannProblemParams rp_params_,
            DataArray Udata_) :
          HydroBaseFunctor2D(params_), rp_params(rp_params_),
          Udata(Udata_)  {};

        static void apply(HydroParams params,
            RiemannProblemParams rp_params,
            DataArray Udata, int nbCells)
        {
          InitRiemannProblemFunctor2D functor(params, rp_params, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;
            const int ghostWidth = params.ghostWidth;

#ifdef USE_MPI
            const int i_mpi = params.myMpiPos[IX];
#else
            const int i_mpi = 0;
#endif

            const int nx = params.nx;

            const real_t xmin = params.xmin;
            const real_t xmax = params.xmax;
            const real_t dx = params.dx;

            int i,j;
            index2coord(index,i,j,isize,jsize);

            real_t x = xmin + dx/2 + (i+nx*i_mpi-ghostWidth)*dx;
            real_t x_middle = HALF_F * (xmin + xmax);

            HydroState q;
            if (x < x_middle)
            {
              q[ID] = rp_params.density_left;
              q[IP] = rp_params.pressure_left;
              q[IS] = ZERO_F;
              q[IU] = rp_params.velocity_left;
              q[IV] = ZERO_F;
            }
            else
            {
              q[ID] = rp_params.density_right;
              q[IP] = rp_params.pressure_right;
              q[IS] = ZERO_F;
              q[IU] = rp_params.velocity_right;
              q[IV] = ZERO_F;
            }

            q[IO] = ZERO_F;
            q[IERAD] = ONE_F;
            q[ITRAD] = ONE_F;
            q[IFX] = ZERO_F;
            q[IFY] = ZERO_F;

            setHydroState(Udata, computeConservatives(q), i, j);

          } // end operator ()

        RiemannProblemParams rp_params;
        DataArray Udata;
    }; // InitRiemannProblemFunctor2D

    class InitRadiativeTransferM1Functor2D : HydroBaseFunctor2D
    {
      public:
        InitRadiativeTransferM1Functor2D(HydroParams params_, RadiativeTransferParams rtParams_, DataArray Udata_) :
          HydroBaseFunctor2D(params_), 
          params(params_), rtParams(rtParams_), Udata(Udata_) {};

        static void apply(HydroParams params, RadiativeTransferParams rtParams,
            DataArray Udata, int nbCells)
        {
          InitRadiativeTransferM1Functor2D functor(params, rtParams, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int i) const
          {
            const int jsize = params.jsize;
            const int jmin  = params.jmin;
            const int ghostWidth = params.ghostWidth;
            const real_t dy = params.dy;
            const real_t ny = params.ny;
            const real_t T0 = 300.0;
            const real_t a = params.settings.radiativeConstant;
            const real_t cv = params.settings.cv;

#ifdef USE_MPI
            const int j_mpi = params.myMpiPos[IY];
#else
            const int j_mpi = 0;
#endif

            if(i >= params.imin && i <= params.imax)
            {
              for (int j=jmin+ghostWidth; j<jsize-ghostWidth; ++j)
              {
                const real_t rho = ONE_F/cv;
                Udata(i, j, ID) = rho;
                Udata(i, j, IE) = rho*cv*T0;
                Udata(i, j, IU) = ZERO_F;
                Udata(i, j, IV) = ZERO_F;
                Udata(i, j, ITRAD) = T0;
                Udata(i, j, IERAD) = a*T0*T0*T0*T0;
                Udata(i, j, IFX) = ZERO_F;
                Udata(i, j, IFY) = ZERO_F;
                if ((j+ny*j_mpi)*dy < 0.5)
                  Udata(i, j, IO) = rtParams.sigma1;
                else
                  Udata(i, j, IO) = rtParams.sigma2;
              }
            }
          };
        const HydroParams params;
        const RadiativeTransferParams rtParams;
        DataArray Udata;
    }; // InitRadiativeTransferM1Functor2D


    class InitRTPerf2DFunctor2D : HydroBaseFunctor2D
    {
      public:
        InitRTPerf2DFunctor2D(HydroParams params_, RadiativeTransferParams rtParams_, DataArray Udata_) :
          HydroBaseFunctor2D(params_), 
          params(params_), rtParams(rtParams_), Udata(Udata_) {};

        static void apply(HydroParams params, RadiativeTransferParams rtParams,
            DataArray Udata, int nbCells)
        {
          InitRTPerf2DFunctor2D functor(params, rtParams, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int i) const
          {
            const int jsize = params.jsize;
            const int jmin  = params.jmin;
            const int ghostWidth = params.ghostWidth;
            const real_t nx = params.nx;
            const real_t a = params.settings.radiativeConstant;
            const real_t cv = params.settings.cv;

            if(i >= params.imin && i <= params.imax)
            {
              for (int j=jmin+ghostWidth; j<jsize-ghostWidth; ++j)
              {
                const real_t rho = ONE_F/cv;
                Udata(i, j, ID) = rho;
                Udata(i, j, IU) = ZERO_F;
                Udata(i, j, IV) = ZERO_F;
                Udata(i, j, IFX) = ZERO_F;
                Udata(i, j, IFY) = ZERO_F;
                Udata(i, j, IO) = rtParams.sigma1;
                if((i-nx/2)*(i-nx/2) + j*j < nx/10*nx/10)
                  Udata(i, j, ITRAD) = 1000.;
                else
                  Udata(i, j, ITRAD) = 300.;
                Udata(i, j, IERAD) = a*Udata(i, j, ITRAD)*Udata(i, j, ITRAD)*Udata(i, j, ITRAD)*Udata(i, j, ITRAD);
                Udata(i, j, IE) = rho*cv*Udata(i, j, ITRAD);
              }
            }
          };
        const HydroParams params;
        const RadiativeTransferParams rtParams;
        DataArray Udata;
    }; // InitRTPerf2DFunctor2D

    class InitRT2DShadowFunctor2D : HydroBaseFunctor2D
    {
      public:
        InitRT2DShadowFunctor2D(HydroParams params_, DataArray Udata_) :
          HydroBaseFunctor2D(params_),
          params(params_), Udata(Udata_) {};

        static void apply(HydroParams params,
            DataArray Udata, int nbCells)
        {
          InitRT2DShadowFunctor2D functor(params, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int i) const
          {
            const int jsize = params.jsize;
            const int jmin  = params.jmin;
            const int ghostWidth = params.ghostWidth;
            const real_t dx = params.dx;
            const real_t dy = params.dy;
            const real_t gamma0 = params.settings.gamma0;
            const real_t nx = params.nx;
            const real_t ny = params.ny;
            const real_t a = params.settings.radiativeConstant;

#ifdef USE_MPI
            const int i_mpi = params.myMpiPos[IX];
            const int j_mpi = params.myMpiPos[IY];
#else
            const int i_mpi = 0;
            const int j_mpi = 0;
#endif

            // To handle mpi dispatch
            real_t rho = ONE_F;
            for (int j_glob=1; j_glob<=ny*j_mpi; ++j_glob)
            {
              rho *= (TWO_F - dy) / (TWO_F + dy);
            }

            if(i >= params.imin && i <= params.imax)
            {
              for (int j=jmin+ghostWidth; j<jsize-ghostWidth; ++j)
              {
                const real_t z = (i+ghostWidth+nx*i_mpi)*dx;
                const real_t r = (j+ny*j_mpi)*dy; 
                const real_t zc = HALF_F;
                const real_t rc = ZERO_F;
                const real_t z0 = 0.1;
                const real_t r0 = 0.06;
                const real_t delta = 10.*((z-zc)*(z-zc)/z0/z0 + (r-rc)*(r-rc)/r0/r0 -ONE_F);
                const real_t rho0 = ONE_F;
                const real_t rho1 = 100.*rho0;
                const real_t rho = rho0 + (rho1-rho0)/(ONE_F+EXP(delta));
                /* real_t rho; */
                /* if(SQR((z-zc)/z0)+SQR((r-rc)/r0) <= ONE_F) */
                /*   rho = rho1; */
                /* else */
                /*   rho = rho0; */
                const real_t sigma0 = 0.1;

                Udata(i, j, ID) = rho;
                Udata(i, j, IP) = rho / (gamma0-ONE_F);
                Udata(i, j, IU) = ZERO_F;
                Udata(i, j, IV) = ZERO_F;

                Udata(i,j,ITRAD) = 290.;
                Udata(i,j,IO) = sigma0*POW(Udata(i,j, ITRAD)/290., -3.5)*SQR(rho/rho0);
                Udata(i,j,IERAD) = a*290.*290.*290.*290.;
                Udata(i,j,IFX) = ZERO_F;
                Udata(i,j,IFY) = ZERO_F;
              }
            }
          };
        const HydroParams params;
        DataArray Udata;
    }; // InitRT2DShadowFunctor2D


    class InitRT2DBeamFunctor2D : HydroBaseFunctor2D
    {
      public:
        InitRT2DBeamFunctor2D(HydroParams params_, DataArray Udata_) :
          HydroBaseFunctor2D(params_),
          params(params_), Udata(Udata_) {};

        static void apply(HydroParams params,
            DataArray Udata, int nbCells)
        {
          InitRT2DBeamFunctor2D functor(params, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int i) const
          {
            const int jsize = params.jsize;
            const int jmin  = params.jmin;
            const int ghostWidth = params.ghostWidth;
            const real_t dy = params.dy;
            const real_t gamma0 = params.settings.gamma0;
            const real_t ny = params.ny;
            const real_t a = params.settings.radiativeConstant;

#ifdef USE_MPI
            const int j_mpi = params.myMpiPos[IY];
#else
            const int j_mpi = 0;
#endif

            // To handle mpi dispatch
            real_t rho = ONE_F;
            for (int j_glob=1; j_glob<=ny*j_mpi; ++j_glob)
            {
              rho *= (TWO_F - dy) / (TWO_F + dy);
            }

            if(i >= params.imin && i <= params.imax)
            {
              for (int j=jmin+ghostWidth; j<jsize-ghostWidth; ++j)
              {
                rho *= (TWO_F - dy) / (TWO_F + dy);
                Udata(i, j, ID) = rho;
                Udata(i, j, IP) = rho / (gamma0-ONE_F);
                Udata(i, j, IU) = ZERO_F;
                Udata(i, j, IV) = ZERO_F;

                Udata(i, j, ITRAD) = 300.0;
                Udata(i, j, IERAD) = a*Udata(i, j, ITRAD)*Udata(i, j, ITRAD)*Udata(i, j, ITRAD)*Udata(i, j, ITRAD);
                Udata(i, j, IO) = ZERO_F;
                Udata(i, j, IFX) = ZERO_F;
                Udata(i, j, IFY) = ZERO_F;



              }
            }
          };
        const HydroParams params;
        DataArray Udata;
    }; // InitRT2DBeamFunctor2D

    class InitRadiativeShockFunctor2D : HydroBaseFunctor2D
    {
      public:
        InitRadiativeShockFunctor2D(HydroParams params_, RadiativeShockParams radShockParams_, DataArray Udata_) :
          HydroBaseFunctor2D(params_),
          params(params_), radShockParams(radShockParams_), Udata(Udata_) {};

        static void apply(HydroParams params,
            RadiativeShockParams radShockParams, 
            DataArray Udata, int nbCells)
        {
          InitRadiativeShockFunctor2D functor(params, radShockParams, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int i) const
          {
            const int jsize = params.jsize;
            const int jmin  = params.jmin;
            const int ghostWidth = params.ghostWidth;
            const int nx = params.nx;
#ifdef USE_MPI
            const int mx = params.mx;
            const int i_mpi = params.myMpiPos[IX];
#else
            const int mx = 1;
            const int i_mpi = 0;
#endif
            const real_t a = params.settings.radiativeConstant;
            const real_t cv = params.settings.cv;
            const real_t rho = radShockParams.rho;
            const real_t T = radShockParams.T;
            const real_t u0 = radShockParams.u0;
            const real_t sigma = radShockParams.sigma;

            if(i >= params.imin+ghostWidth   && i <= params.imax-ghostWidth)
            {
              for (int j=jmin+ghostWidth; j<jsize-ghostWidth; ++j)
              {
                const real_t Tloc = T+(mx*nx-(i_mpi)*nx-i)*0.25;
                Udata(i, j, IU) = rho*u0;
                Udata(i, j, IV) = ZERO_F;
                Udata(i, j, ID) = rho;
                Udata(i, j, IE) = rho*cv*Tloc + HALF_F*rho*u0*u0;

                Udata(i, j, ITRAD) = Tloc;
                Udata(i, j, IERAD) = a*Tloc*Tloc*Tloc*Tloc;
                Udata(i, j, IO) = sigma;
                Udata(i, j, IFX) = ZERO_F;
                Udata(i, j, IFY) = ZERO_F;
              }
            }
          };
        const HydroParams params;
        const RadiativeShockParams radShockParams;
        DataArray Udata;
    }; // InitRadiativeShockFunctor2D

    class InitHIIRegionFunctor2D : HydroBaseFunctor2D
    {
      public:
        InitHIIRegionFunctor2D(HydroParams params_, DataArray Udata_) :
          HydroBaseFunctor2D(params_),
          params(params_), Udata(Udata_) {};

        static void apply(HydroParams params,
            DataArray Udata, int nbCells)
        {
          InitHIIRegionFunctor2D functor(params, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int i) const
          {
            const int jsize = params.jsize;
            const int jmin  = params.jmin;
            const int ghostWidth = params.ghostWidth;
            const real_t cv = params.settings.cv;

            const real_t nH = 5e8*code_units::volume_u; // cm^-3
            const real_t rho = nH * code_units::constants::m_h; // g/cm^3
            const real_t T = 10.; // K
            const real_t sigma = 6e-22/(code_units::length_u*code_units::length_u); // cm^2

            if(i >= params.imin+ghostWidth   && i <= params.imax-ghostWidth)
            {
              for (int j=jmin+ghostWidth; j<jsize-ghostWidth; ++j)
              {
                Udata(i, j, IU) = ZERO_F;
                Udata(i, j, IV) = ZERO_F;
                Udata(i, j, ID) = rho;
                Udata(i, j, IE) = rho*cv*T;
                Udata(i, j, IS) = ZERO_F;

                Udata(i, j, ITRAD) = T;
                Udata(i, j, IERAD) = 1e-30;
                Udata(i, j, IO) = sigma*nH;
                Udata(i, j, IFX) = ZERO_F;
                Udata(i, j, IFY) = ZERO_F;
              }
            }
          };
        const HydroParams params;
        DataArray Udata;
    }; // InitHIIRegionFunctor2D



    class InitHIIConvFunctor2D : HydroBaseFunctor2D
    {
      public:
        InitHIIConvFunctor2D(HydroParams params_, 
            RadiativeConvectionParams rc_params_,
            DataArray Udata_) :
          HydroBaseFunctor2D(params_),
          params(params_), rc_params(rc_params_), Udata(Udata_) {};

        static void apply(HydroParams params,
            RadiativeConvectionParams rc_params,
            DataArray Udata, int nbCells)
        {
          InitHIIConvFunctor2D functor(params, rc_params, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int i) const
          {
            const int jsize = params.jsize;
            const int jmin  = params.jmin;
            const int ghostWidth = params.ghostWidth;
            const real_t dx = params.dx;
            const real_t dy = params.dy;
            const real_t xmax    = params.xmax;
            const real_t xmin    = params.xmin;
            const real_t ymax    = params.ymax;
            const real_t ymin    = params.ymin;
            const int nx = params.nx;
            const int ny = params.ny;

            const real_t mH = code_units::constants::m_h;
            const real_t g = params.settings.g_y;
            const real_t kb = code_units::constants::k_b;
            const real_t mmw = params.settings.mmw;

            const real_t amp    = rc_params.amp;
            const real_t mode   = rc_params.mode;
            const real_t Lx = xmax - xmin;
            const real_t Ly = ymax - ymin;
            const real_t x_mid = HALF_F * (xmax + xmin);
            const real_t y_mid = HALF_F * (ymax + ymin);
            const real_t T_ymin = rc_params.Trad_ymin;
            const real_t T_ymax = rc_params.Trad_ymax;
            const real_t sigma = 6e-22/(code_units::length_u*code_units::length_u); // cm^2

#ifdef USE_MPI
            const int i_mpi = params.myMpiPos[IX];
            const int j_mpi = params.myMpiPos[IY];
            const int my = params.my;
#else
            const int i_mpi = 0;
            const int j_mpi = 0;
            const int my = 1;
#endif

            real_t P = 1e-3;
            real_t T = T_ymin;
            HydroState q;
            // To handle mpi dispatch
            for (int j_glob=1; j_glob<=ny*j_mpi; ++j_glob)
            {
              const real_t csim = kb*T/(mmw*mH);
              T -= (T_ymin-T_ymax)/(ny*my);
              const real_t csi  = kb*T/(mmw*mH);
              P *= (ONE_F+HALF_F*g*dy/csim)/(ONE_F-HALF_F*g*dy/csi);
            }

            if(i >= params.imin && i <= params.imax)
            {
              const real_t x = xmin + (HALF_F+ i + nx*i_mpi - ghostWidth)*dx;
              for (int j=jmin+ghostWidth; j<jsize-ghostWidth; ++j)
              {

                const real_t csim = kb*T/(mmw*mH);
                T -= (T_ymin-T_ymax)/(ny*my);
                const real_t csi  = kb*T/(mmw*mH);
                P *= (ONE_F+HALF_F*g*dy/csim)/(ONE_F-HALF_F*g*dy/csi);
                q[ID] = P/csi;
                q[IP] = P;
                if(rc_params.perturb_velocity)
                {
                  const real_t y = ymin + (HALF_F + j +ny*j_mpi - ghostWidth)*dy;
                  q[IU] = amp * std::sin(mode*TWO_F*PI*(x-x_mid)/Lx) * std::sin(PI*(y-y_mid)/Ly);
                  q[IV] = amp * std::cos(mode*TWO_F*PI*(x-x_mid)/Lx) * std::cos(PI*(y-y_mid)/Ly);
                }
                else
                {
                  q[IU] = ZERO_F;
                  q[IV] = ZERO_F;
                }
                q[IS] = ZERO_F;
                q[IERAD] = 1e-30;
                q[ITRAD] = T;
                q[IO] = sigma*q[ID]/mH;
                q[IFX] = ZERO_F;
                q[IFY] = ZERO_F;
                setHydroState(Udata, computeConservatives(q), i, j);
              }
            }
          };
        const HydroParams params;
        const RadiativeConvectionParams rc_params;
        DataArray Udata;
    }; // InitHIIConvFunctor2D


  } // namespace all_regime_radiative_transfer

} // namespace ark_rt
