// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "HydroRunFunctorsRT2D.h"
#include "HydroRunFunctorsRT3D.h"
#include "shared/RadiativeRunFunctors.h"

namespace ark_rt { namespace all_regime_radiative_transfer
{

template<int dim>
struct RunFunctors
{
};

template<>
struct RunFunctors<2>
{
    using AcousticTimeStep    = ComputeAcousticDtFunctor2D;
    using TransportTimeStep   = ComputeTransportDtFunctor2D;
    using TimeStep            = ComputeDtFunctor2D;
    using AcousticStep        = ComputeAcousticStepFunctor2D;
    using TransportStep       = ComputeTransportStepFunctor2D;
    using ViscosityStep       = ComputeViscosityStepFunctor2D;
    using HeatDiffusionStep   = ComputeHeatDiffusionStepFunctor2D;
    using ConvertToPrimitives = ConvertToPrimitivesFunctor2D;
    using RadiativeTransfer   = radiative_transfer::ComputeRadiativeTransferStepFunctor2D;
    using Chemistry           = chemistry::ComputeReactionRateStepFunctor2D;
    using SourceTermsRT	      = SourceTermsRTStepFunctor2D;
    template <Direction dir>
    using ComputeFluxesRT     = ComputeFluxesRTStepFunctor2D<dir>;
    template <Direction dir>
    using UpdateCellsRT       = UpdateCellsRTStepFunctor2D<dir>;
    using ComputeTemperature  = ComputeTemperatureFunctor2D;
    using ComputeOpacity      = ComputeOpacityFunctor2D;
    using CouplingHydroRad    = CouplingHydroRadFunctor2D;
    using ComputeTotalEnergy  = ComputeTotalEnergyFunctor2D;
    template <Direction dir>
    using ComputeTotalMomentum= ComputeTotalMomentumFunctor2D<dir>;
    using Ionization 	      = ComputeIonizationFunctor2D;
    using RayTracing 	      = RayTracingFunctor2D;
    using ThermalDriving      = ThermalDrivingFunctor2D;
};

template<>
struct RunFunctors<3>
{
    using AcousticTimeStep    = ComputeAcousticDtFunctor3D;
    using TransportTimeStep   = ComputeTransportDtFunctor3D;
    using TimeStep            = ComputeDtFunctor3D;
    using AcousticStep        = ComputeAcousticStepFunctor3D;
    using TransportStep       = ComputeTransportStepFunctor3D;
    using ViscosityStep       = ComputeViscosityStepFunctor3D;
    using HeatDiffusionStep   = ComputeHeatDiffusionStepFunctor3D;
    using ConvertToPrimitives = ConvertToPrimitivesFunctor3D;
    using RadiativeTransfer   = radiative_transfer::ComputeRadiativeTransferStepFunctor3D;
    using Chemistry           = chemistry::ComputeReactionRateStepFunctor3D;
};

} // namespace all_regime_radiative_transfer

} // namespace ark_rt
