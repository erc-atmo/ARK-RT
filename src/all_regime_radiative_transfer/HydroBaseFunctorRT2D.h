// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "shared/kokkos_shared.h"
#include "shared/HydroParams.h"
#include "shared/HydroState.h"
#include "shared/units.h"

#include "eddingtonTensor.h"

#include "shared/RadiativeConvectionParams.h"

namespace ark_rt { namespace all_regime_radiative_transfer
  {

    /**
     * Base class to derive actual kokkos functor for hydro 2D.
     * params is passed by copy.
     */
    class HydroBaseFunctor2D
    {
      public:
        using HydroState     = HydroState2d;
        using DataArray      = DataArray2d;
        using DataArrayConst = DataArray2dConst;

        HydroBaseFunctor2D(HydroParams params_) :
          params(params_), nbvar(params_.nbvar) {};
        virtual ~HydroBaseFunctor2D() {};

        const HydroParams params;
        const int nbvar;

        /**
         * Compute gravitational potential at position (x, y)
         * @param[in]  x    x-coordinate
         * @param[in]  y    y-coordinate
         * @param[out] phi  gravitational potential
         */
        KOKKOS_INLINE_FUNCTION
          real_t phi(real_t x, real_t y) const
          {
            return - params.settings.g_x * x - params.settings.g_y * y;
          } // phi

        /**
         * Compute gravitational potential at the center
         * of the cell C(i, j)
         * @param[in]  i    logical x-coordinate of the cell C
         * @param[in]  j    logical y-coordinate of the cell C
         * @param[out] phi  gravitational potential
         */
        KOKKOS_INLINE_FUNCTION
          real_t phi(int i, int j) const
          {
#ifdef USE_MPI
            const int nx_mpi = params.nx * params.myMpiPos[IX];
            const int ny_mpi = params.ny * params.myMpiPos[IY];
            const real_t x = params.xmin + (HALF_F + i + nx_mpi - params.ghostWidth)*params.dx;
            const real_t y = params.ymin + (HALF_F + j + ny_mpi - params.ghostWidth)*params.dy;
#else
            const real_t x = params.xmin + (HALF_F + i - params.ghostWidth)*params.dx;
            const real_t y = params.ymin + (HALF_F + j - params.ghostWidth)*params.dy;
#endif

            return phi(x, y);
          } // phi

        /**
         * Compute \mathcal{M} = \rho * \Delta \phi between two cells
         * (to be renamed)
         */
        KOKKOS_INLINE_FUNCTION
          real_t computeM(const HydroState& qLoc, real_t phiLoc,
              const HydroState& qNei, real_t phiNei) const
          {
            return HALF_F * (qLoc[ID]+qNei[ID]) * (phiNei - phiLoc);
          } // computeM

        /**
         * Get HydroState from global array (either conservative or primitive)
         * at cell C(i, j) (global to local)
         * @param[in]  array global array
         * @param[in]  i     logical x-coordinate of the cell C
         * @param[in]  j     logical y-coordinate of the cell C
         * @param[out] state HydroState of cell C(i, j)
         */
        KOKKOS_INLINE_FUNCTION
          HydroState getHydroState(DataArrayConst array, int i, int j) const
          {
            HydroState state;
            state[ID] = array(i, j, ID);
            state[IP] = array(i, j, IP);
            state[IS] = array(i, j, IS);
            state[IU] = array(i, j, IU);
            state[IV] = array(i, j, IV);
            state[IERAD] = array(i, j, IERAD);
            state[ITRAD] = array(i, j, ITRAD);
            state[IO] = array(i, j, IO);
            state[IFX] = array(i, j, IFX);
            state[IFY] = array(i, j, IFY);
            return state;
          } // getHydroState

        /**
         * Set HydroState to global array (either conservative or primitive)
         * at cell C(i, j) (local to global)
         * @param[in, out]  array global array
         * @param[in]       state HydroState of cell C(i, j)
         * @param[in]       i     logical x-coordinate of the cell C
         * @param[in]       j     logical y-coordinate of the cell C
         */
        KOKKOS_INLINE_FUNCTION
          void setHydroState(DataArray array, const HydroState& state, int i, int j) const
          {
            array(i, j, ID) = state[ID];
            array(i, j, IP) = state[IP];
            array(i, j, IS) = state[IS];
            array(i, j, IU) = state[IU];
            array(i, j, IV) = state[IV];
            array(i, j, IERAD) = state[IERAD];
            array(i, j, ITRAD) = state[ITRAD];
            array(i, j, IO) = state[IO];
            array(i, j, IFX) = state[IFX];
            array(i, j, IFY) = state[IFY];
          } // setHydroState

        /**
         * Compute mean molecular weight from passive scalar
         * @param[in]  q   primitive variables array
         * @param[out] mmw mean molecular weight
         */
        KOKKOS_INLINE_FUNCTION
          real_t computeMmw(const HydroState& q) const
          {
            return params.settings.mmw1*q[IS] + params.settings.mmw2*(ONE_F-q[IS]);
          } // computeMmw

        /**
         * Compute temperature using ideal gas law
         * @param[in]  q  primitive variables array
         * @param[out] T  temperature
         */
        KOKKOS_INLINE_FUNCTION
          real_t computeTemperature(const HydroState& q) const
          {
            const real_t mmw = computeMmw(q);
            return mmw * code_units::constants::m_h * q[IP] / (q[ID] * code_units::constants::k_b);
          } // computeTemperature

        KOKKOS_INLINE_FUNCTION
          real_t computeOpacity(const HydroState& q) const
          {
            return params.settings.kap1*q[IS] + params.settings.kap2*(ONE_F-q[IS]);
          } // computeOpacity

        /**
         * Compute speed of sound using ideal gas law
         * @param[in]  q  primitive variables array
         * @param[out] c  speed of sound
         */
        KOKKOS_INLINE_FUNCTION
          real_t computeSpeedSound(const HydroState& q) const
          {
            return SQRT(params.settings.gamma0 * q[IP] / q[ID]);
          } // computeSpeedSound

        /**
         * Convert conservative variables (rho, rho*u, rho*v, rho*E) to
         * primitive variables (rho, u, v, p) using ideal gas law
         * @param[in]  u  conservative variables array
         * @param[out] q  primitive    variables array
         */
        KOKKOS_INLINE_FUNCTION
          HydroState computePrimitives(const HydroState& u) const
          {
            const real_t gamma0 = params.settings.gamma0;
            const real_t invD = ONE_F / u[ID];
            const real_t ekin = HALF_F*(u[IU]*u[IU]+u[IV]*u[IV])*invD;

            HydroState q;
            q[ID] = u[ID];
            q[IP] = (gamma0-ONE_F) * (u[IE] - ekin);
            q[IS] = u[IS] * invD;
            q[IU] = u[IU] * invD;
            q[IV] = u[IV] * invD;
            q[IERAD] = u[IERAD];
            q[ITRAD] = u[ITRAD];
            q[IO] = u[IO];
            q[IFX] = u[IFX];
            q[IFY] = u[IFY];

            return q;
          } // computePrimitives

        /**
         * Convert primitive variables (rho, p, u, v) to
         * conservative variables (rho, rho*E, rho*u, rho*v) using ideal gas law
         * @param[in]  q  primitive    variables array
         * @param[out] u  conservative variables array
         */
        KOKKOS_INLINE_FUNCTION
          HydroState computeConservatives(const HydroState& q) const
          {
            const real_t gamma0 = params.settings.gamma0;
            const real_t ekin = HALF_F*q[ID]*(q[IU]*q[IU]+q[IV]*q[IV]);

            HydroState u;
            u[ID] = q[ID];
            u[IE] = q[IP] / (gamma0-ONE_F) + ekin;
            u[IS] = q[ID] * q[IS];
            u[IU] = q[ID] * q[IU];
            u[IV] = q[ID] * q[IV];

            u[IO] = q[IO];
            u[IERAD] = q[IERAD];
            u[ITRAD] = q[ITRAD];
            u[IFX] = q[IFX];
            u[IFY] = q[IFY];

            return u;
          } // computeConservatives

        /**
         * Compute source terms for radiative flux
         * Source term remain centred at cells
         * @param[in]  without_source_terms     variables array computed from HLL flux, without source terms
         * @param[in]  dt			    time step
         * @param[out] u 			    variables array after splitting, ith source terms
         */
        KOKKOS_INLINE_FUNCTION
          HydroState add_source_terms(HydroState without_source_terms, real_t dt) const 
          {
            HydroState u;
            const real_t c = params.settings.speedOfLight;
            const real_t sigma = without_source_terms[IO];
            const real_t rho_cv = without_source_terms[ID]*params.settings.cv;

            const real_t a = params.settings.radiativeConstant;
            const int max_newton = params.settings.max_newton;
            const real_t tol_newton = params.settings.tol_newton;

            // Newton-Raphson method to compute radiative energy and gas temperature
            real_t x = without_source_terms[ITRAD];
            real_t x_prev = x + ONE_F;
            real_t f, fp;
            for(int k = 0; (k <= max_newton) && (tol_newton < FABS((x-x_prev)/x_prev)); k++)
            {
              x_prev = x;
              // d_t E = sigma c (a T^4 - E)
              // rho cv d_t T = - sigma c (a T^4 - E)
              f = (1.+c*sigma*dt)*rho_cv*x + (c*sigma*dt)*a*x*x*x*x - (1+c*sigma*dt)*rho_cv*without_source_terms[ITRAD] - (c*sigma*dt)*without_source_terms[IERAD];
              fp =(1.+c*sigma*dt)*rho_cv + (c*sigma*dt)*4.*a*x*x*x;
              x -= f/fp;
              if(k == max_newton)
                printf("explicit source term, max Newton reached, relative error = %f\n", FABS((x-x_prev)/x_prev));
            }

            u[ID] = without_source_terms[ID];
            u[IP] = without_source_terms[IP];
            u[IE] = without_source_terms[IE];
            u[IS] = without_source_terms[IS];
            u[IU] = without_source_terms[IU];
            u[IV] = without_source_terms[IV];
            // d_t E = sigma c (a T^4 - E)
            // rho cv d_t T = - sigma c (a T^4 - E)
            u[IERAD] = (without_source_terms[IERAD]+sigma*c*dt*a*x*x*x*x)/(1.+sigma*c*dt);
            u[ITRAD] = x;
            u[IO] = without_source_terms[IO];
            u[IFX] = without_source_terms[IFX]/(ONE_F+dt*c*sigma);
            u[IFY] = without_source_terms[IFY]/(ONE_F+dt*c*sigma);

            return u;
          } // add_source_terms


        KOKKOS_INLINE_FUNCTION
          HydroState add_source_terms_wellBalanced(HydroState u_hll, HydroState u_ij, HydroState u_imj, HydroState u_ipj, HydroState u_ijm, HydroState u_ijp, real_t dt) const 
          {
            HydroState u;
            const real_t c = params.settings.speedOfLight;
            const real_t sigma = u_ij[IO];
            const real_t rho_cv = u_ij[ID]*params.settings.cv;

            const real_t a = params.settings.radiativeConstant;
            const int max_newton = params.settings.max_newton;
            const real_t tol_newton = params.settings.tol_newton;

            // Newton-Raphson method to compute radiative energy and gas temperature
            real_t x = u_hll[ITRAD];
            real_t x_prev = x + ONE_F;
            real_t f, fp;
            for(int k = 0; (k <= max_newton) && (tol_newton < FABS((x-x_prev)/x_prev)); k++)
            {
              x_prev = x;
              // d_t E = sigma c (a T^4 - E)
              // rho cv d_t T = - sigma c (a T^4 - E)
              f = (1.+c*sigma*dt)*rho_cv*x + (c*sigma*dt)*a*x*x*x*x - (1+c*sigma*dt)*rho_cv*u_ij[ITRAD] - (c*sigma*dt)*u_ij[IERAD];
              fp =(1.+c*sigma*dt)*rho_cv + (c*sigma*dt)*4.*a*x*x*x;
              x -= f/fp;
              if(k == max_newton)
                printf("explicit source term, max Newton reached, relative error = %f\n", FABS((x-x_prev)/x_prev));
            }

            u[ID] = u_hll[ID];
            u[IP] = u_hll[IP];
            u[IE] = u_hll[IE];
            u[IS] = u_hll[IS];
            u[IU] = u_hll[IU];
            u[IV] = u_hll[IV];
            // d_t E = sigma c (a T^4 - E)
            // rho cv d_t T = - sigma c (a T^4 - E)
            u[IERAD] = (u_hll[IERAD]+sigma*c*dt*a*x*x*x*x)/(1.+sigma*c*dt);
            u[ITRAD] = x;
            u[IO] = u_hll[IO];
            u[IFX] = u_hll[IFX] -c*dt/8.*((u_ij[IO]+u_ipj[IO])*(u_ij[IFX]+u_ipj[IFX]) + (u_ij[IO]+u_imj[IO])*(u_ij[IFX]+u_imj[IFX]));
            u[IFY] = u_hll[IFY] -c*dt/8.*((u_ij[IO]+u_ijp[IO])*(u_ij[IFY]+u_ijp[IFY]) + (u_ij[IO]+u_ijm[IO])*(u_ij[IFY]+u_ijm[IFY]));

            return u;
          } // add_source_terms_wellBalanced

        KOKKOS_INLINE_FUNCTION
          HydroState fluxFunction(HydroState q) const
          {
            const real_t c = params.settings.speedOfLight;
            HydroState fluxes;
            fluxes[IERAD] = q[IFX];
            if(!params.settings.useP1) // M1 model
            {
              real_t fnorm2 = q[IFX]*q[IFX]+q[IFY]*q[IFY];
              real_t anisotropy2;
              if(params.settings.replaceEddingtonFactor)
                anisotropy2 = FMIN(ONE_F, fnorm2/(c*c* q[IERAD] * q[IERAD]));
              else
                anisotropy2 = fnorm2/(c*c* q[IERAD] * q[IERAD]);
              real_t eddingtonFactor = (3.0 + 4*anisotropy2) / (5.0 + TWO_F*SQRT(4-3*anisotropy2));
              if(anisotropy2 > ONE_F)
              {
                printf("compute Eddington tensor: f > 1: f = %f, e = %e, fx = %e, fy = %e, c = %e\n", SQRT(anisotropy2), q[IERAD], q[IFX], q[IFY], c);
              }
              if (fnorm2 == 0){
                fluxes[IFX] = (ONE_F-eddingtonFactor);
                fluxes[IFY] = ZERO_F;
              }
              else
              {
                fluxes[IFX] = (ONE_F-eddingtonFactor) + q[IFX]*q[IFX]*(3.0*eddingtonFactor - ONE_F) / fnorm2;
                fluxes[IFY] = q[IFX]*q[IFY]*(3*eddingtonFactor - ONE_F) / fnorm2;
              }
              fluxes[IFX] = HALF_F*fluxes[IFX]*q[IERAD];
              fluxes[IFY] = HALF_F*fluxes[IFY]*q[IERAD];
            }
            else // P1 model
            {
              fluxes[IFX] = q[IERAD]/3.;
              fluxes[IFY] = ZERO_F;
            }
            return fluxes;
          } // fluxFunction

        KOKKOS_INLINE_FUNCTION
          HydroState riemann_radiativeTransfer(const HydroState qleft, const HydroState qright, const real_t dx) const
          {
            const real_t c = params.settings.speedOfLight;

            real_t sL, sR;
            if(!params.settings.useP1) // M1 model
              eigenvaluesRT(sL, sR, c, qleft[IERAD], qleft[IFX], qleft[IFY], qright[IERAD], qright[IFX], qright[IFY], params.settings.useExactEigenvalues, params.settings.replaceEddingtonFactor, params.settings.valpmin);
            else // P1 model
            {
              sL = -c/SQRT(3.);
              sR =  c/SQRT(3.);
            }

            const real_t sigma = HALF_F * (qleft[IO]+qright[IO]);
            real_t alpha;
            if(params.settings.useAsymptoticCorrection)
            {
              real_t e = qleft[IERAD];
              real_t fx = qleft[IFX];
              real_t fy = qleft[IFY];
              const real_t f_left = SQRT(computeReducedFluxSquare(e, fx, fy, c));
              e = qright[IERAD];
              fx = qright[IFX];
              fy = qright[IFY];
              const real_t f_right = SQRT(computeReducedFluxSquare(e, fx, fy, c));
              const real_t f2 = HALF_F*(f_left+f_right)*HALF_F*(f_left+f_right);
              alpha = ONE_F / ( ONE_F -(ONE_F-f2)* 3. * sigma * dx *sR*sL / (c *(sR-sL) ) );
            }
            else
              alpha = ONE_F;

            HydroState fleft = fluxFunction(qleft);
            HydroState fright = fluxFunction(qright);

            HydroState flux;
            flux[IERAD] = alpha * (sR * fleft[IERAD] -
                sL * fright[IERAD] + 
                sR * sL * (qright[IERAD] - qleft[IERAD]) ) / (sR-sL);
            flux[IFX] = ( sR *c*c * fleft[IFX]
                - sL *c*c * fright[IFX] 
                + sR * sL * (qright[IFX] - qleft[IFX]) ) / (sR-sL);
            flux[IFY] = ( sR *c*c * fleft[IFY] 
                - sL *c*c * fright[IFY] 
                + sR * sL * (qright[IFY] - qleft[IFY]) ) / (sR-sL);

            return flux;
          } // riemann_radiativeTransfer

        KOKKOS_INLINE_FUNCTION
          void swapValues(real_t *a, real_t *b) const
          {

            real_t tmp = *a;

            *a = *b;
            *b = tmp;

          } // swapValues

        KOKKOS_INLINE_FUNCTION
          real_t computeGasTemperature(HydroState u) const
          {
            HydroState q = computePrimitives(u);
            return computeTemperature(q);
          } // computeGasTemperature

        KOKKOS_INLINE_FUNCTION
          real_t computeExtinctionCoeff(HydroState u) const
          {
            return computeOpacity(computePrimitives(u)) * u[ID];
          } // computeExtinctionCoeff

        KOKKOS_INLINE_FUNCTION
          HydroState couplingHydroRT(const RadiativeConvectionParams& rc_params, const int j, const HydroState& u_hydro, const HydroState& uij, const HydroState& uipj, const HydroState& uimj, const HydroState& uijp, const HydroState& uijm, const real_t& dt, const bool& useImplicitSolver) const
          {
            const real_t c = params.settings.speedOfLight;
            const real_t cv = params.settings.cv;
            const real_t a = params.settings.radiativeConstant;
            const real_t sigma = u_hydro[IO];
            const real_t k_b = code_units::constants::k_b;

            HydroState uCoupled;
            uCoupled[ID] = u_hydro[ID];
            uCoupled[IS] = u_hydro[IS];
            if(params.settings.wellBalancedScheme)
            {
              uCoupled[IU] = u_hydro[IU] + dt/c*HALF_F*HALF_F*((u_hydro[IO]+uipj[IO])*(u_hydro[IFX]+uipj[IFX]) + (u_hydro[IO]+uimj[IO])*(u_hydro[IFX]+uimj[IFX]));
              uCoupled[IV] = u_hydro[IV] + dt/c*HALF_F*HALF_F*((u_hydro[IO]+uijp[IO])*(u_hydro[IFY]+uijp[IFY]) + (u_hydro[IO]+uijm[IO])*(u_hydro[IFY]+uijm[IFY]));
            }
            else
            {
              uCoupled[IU] = u_hydro[IU] + dt/c*u_hydro[IO]*u_hydro[IFX];
              uCoupled[IV] = u_hydro[IV] + dt/c*u_hydro[IO]*u_hydro[IFY];
            }
            uCoupled[IERAD] = u_hydro[IERAD];
            uCoupled[IO] = u_hydro[IO];
            uCoupled[IFX] = u_hydro[IFX];
            uCoupled[IFY] = u_hydro[IFY];
            uCoupled[ITRAD] = u_hydro[ITRAD];

            if(params.settings.allTermsCoupling)
            {
              const real_t u2 = (uij[IU]*uij[IU]+uij[IV]*uij[IV])/(uij[ID]*uij[ID]);
              real_t eddTensor[4];
              if(!params.settings.useP1) // M1 model
              {
                all_regime_radiative_transfer::calc_eddTensor_rad(uij[IERAD], uij[IFX], uij[IFY], eddTensor, c, params.settings.replaceEddingtonFactor);
              }
              else // P1 model
              {
                eddTensor[0] = c*c/3;
                eddTensor[1] = ZERO_F;
                eddTensor[2] = ZERO_F;
                eddTensor[3] = c*c/3;
              }
              eddTensor[0] *= uij[IERAD];
              eddTensor[1] *= uij[IERAD];
              eddTensor[2] *= uij[IERAD];
              eddTensor[3] *= uij[IERAD];
              const real_t uPx = (eddTensor[0]*uij[IU]+eddTensor[1]*uij[IV])/uij[ID];
              const real_t uPy = (eddTensor[2]*uij[IU]+eddTensor[3]*uij[IV])/uij[ID];
              const real_t uF = (uij[IU]*uij[IFX]+uij[IV]*uij[IFY])/uij[ID];

              uCoupled[IU] += (-sigma*dt/c*uPx - dt*sigma/c*a*uij[IU]*uij[ITRAD]*uij[ITRAD]*uij[ITRAD]*uij[ITRAD] - dt*sigma/(c*c*c)*uij[IU]*(uij[IU]*uij[IFX] + uij[IV]*uij[IFY]));
              uCoupled[IV] += (-sigma*dt/c*uPy - dt*sigma/c*a*uij[IV]*uij[ITRAD]*uij[ITRAD]*uij[ITRAD]*uij[ITRAD] - dt*sigma/(c*c*c)*uij[IV]*(uij[IU]*uij[IFX] + uij[IV]*uij[IFY]));
              uCoupled[IERAD] += (dt*sigma/c*uF + dt*sigma/c*uij[IERAD]*u2 + sigma*dt/c*(uij[IU]*uPx+uij[IV]*uPy)/uij[ID]);
              uCoupled[IFX] += dt*c*sigma*uPx + dt*sigma*a*uij[IU]*uij[ITRAD]*uij[ITRAD]*uij[ITRAD]*uij[ITRAD] + dt*sigma/(c)*uij[IU]*(uij[IU]*uij[IFX] + uij[IV]*uij[IFY]);
              uCoupled[IFY] += dt*c*sigma*uPy + dt*sigma*a*uij[IV]*uij[ITRAD]*uij[ITRAD]*uij[ITRAD]*uij[ITRAD] + dt*sigma/(c)*uij[IV]*(uij[IU]*uij[IFX] + uij[IV]*uij[IFY]);
              uCoupled[ITRAD] += (- dt*sigma/c*uF - dt*sigma/c*uij[IERAD]*u2 - sigma*dt/c*(uij[IU]*uPx+uij[IV]*uPy)/uij[ID])/(cv*uij[ID]);
            }

            if(rc_params.forceTemperatureIonization)
            {
              const real_t gamma = params.settings.gamma0;
              const real_t e_gamma = 1.60218e-19/code_units::energy_u; // 1eV
              const real_t Teq_ionization = e_gamma*(gamma-ONE_F)/k_b;
              const real_t T_ymin = rc_params.Trad_ymin;
              const real_t T_ymax = rc_params.Trad_ymax;
              const int ny = params.ny;
              const int my = params.my;
              const int ghostWidth = params.ghostWidth;
              const int jmin = params.jmin;
#ifdef USE_MPI
              const int j_mpi = params.myMpiPos[IY];
#else
              const int j_mpi = 0;
#endif

              real_t Teq_forcing = T_ymin;
              // To handle mpi dispatch
              for (int j_glob=1; j_glob<=ny*j_mpi; ++j_glob)
              {
                Teq_forcing -= (T_ymin-T_ymax)/(ny*my);
              }
              for (int j_glob=jmin+ghostWidth; j_glob<=j; ++j_glob)
              {
                Teq_forcing -= (T_ymin-T_ymax)/(ny*my);
              }
              if(uCoupled[IS]/uCoupled[ID] > HALF_F)
                uCoupled[ITRAD] = Teq_ionization;
              else
                uCoupled[ITRAD] = Teq_forcing;
            }

            HydroState q = computePrimitives(u_hydro);
            const real_t mmw = computeMmw(q);
            const real_t P = uCoupled[ITRAD] * uCoupled[ID]*code_units::constants::k_b / (mmw * code_units::constants::m_h);
            const real_t ekin = HALF_F*(uCoupled[IU]*uCoupled[IU] + uCoupled[IV]*uCoupled[IV])/uCoupled[ID];
            uCoupled[IE] = P/(params.settings.gamma0-ONE_F)+ekin;
            return uCoupled;
          } // couplingHydroRT

        KOKKOS_INLINE_FUNCTION
          HydroState ionizationSourceTerms(HydroState u, const real_t dt, const RadiativeConvectionParams& rc_params, const int j) const
          {
            const real_t T_ymin = rc_params.Trad_ymin;
            const real_t T_ymax = rc_params.Trad_ymax;
            const real_t k_b = code_units::constants::k_b;
            const real_t m_h = code_units::constants::m_h;
            const real_t tau = rc_params.tau;
            const real_t ny = params.ny;
            const int jmin = params.jmin;
            const int ghostWidth = params.ghostWidth;
#ifdef USE_MPI
            const int j_mpi = params.myMpiPos[IY];
            const int my = params.my;
#else
            const int j_mpi = 0;
            const int my = 1;
#endif

            real_t Teq_forcing = T_ymin;
            // To handle mpi dispatch
            for (int j_glob=1; j_glob<=ny*j_mpi; ++j_glob)
            {
              Teq_forcing -= (T_ymin-T_ymax)/(ny*my);
            }
            for (int j_glob=jmin+ghostWidth; j_glob<=j; ++j_glob)
            {
              Teq_forcing -= (T_ymin-T_ymax)/(ny*my);
            }

            const real_t rho_cv = params.settings.cv * u[ID];
            const real_t c = params.settings.speedOfLight;
            const real_t gamma = params.settings.gamma0;
            const real_t e_gamma = 1.60218e-19/code_units::energy_u; // 1eV
            const real_t rho = u[ID];
            const real_t E = u[IERAD];
            const real_t Teq_ionization = e_gamma*(gamma-ONE_F)/k_b;
            const real_t beta = 2e-16*POW(Teq_ionization, -0.75) / (code_units::volume_u/code_units::time_u);
            const real_t betaEnergy = rc_params.recombinationEnergy ? beta : ZERO_F;
            const real_t nH = u[ID]/m_h;
            const real_t sigma0 = nH*6e-22/(code_units::length_u*code_units::length_u);
            const real_t Tn = computeTemperature(computePrimitives(u));
            /* const real_t Tn = u[ITRAD]; */

            HydroState uIonized;
            uIonized[ID] = u[ID];
            uIonized[IU] = u[IU];
            uIonized[IV] = u[IV];
            uIonized[IE] = u[IE];
            uIonized[IERAD] = u[IERAD];
            uIonized[IFX] = u[IFX];
            uIonized[IFY] = u[IFY];
            const real_t a_poly = beta*dt/m_h;
            const real_t b_poly = ONE_F + dt*m_h*sigma0*c*E/(e_gamma*rho);
            const real_t c_poly = -u[IS] - dt*m_h*sigma0*c*E/e_gamma;
            const real_t delta_poly = b_poly*b_poly - 4.*a_poly*c_poly;
            uIonized[IS] = rc_params.recombinationIonizationFraction ? (-b_poly + SQRT(delta_poly))/(TWO_F*a_poly) : -c_poly/b_poly;
            if(uIonized[IS] < 1e-100)
              uIonized[ITRAD] = u[ITRAD];
            else
            {
              if(params.settings.thermal_driving_enabled)
                uIonized[ITRAD] = (Tn + dt/tau*Teq_forcing + dt*c*u[IO]*u[IERAD]/rho_cv) / (ONE_F + dt*betaEnergy*uIonized[IS]*uIonized[IS]*k_b/(rho_cv*m_h*m_h*(gamma-ONE_F)) + dt/tau);
              else
                uIonized[ITRAD] = (Tn + dt*c*u[IO]*u[IERAD]/rho_cv) / (ONE_F + dt*betaEnergy*uIonized[IS]*uIonized[IS]*k_b/(rho_cv*m_h*m_h*(gamma-ONE_F)));
            }

            if(uIonized[IS] < ZERO_F)
            {
              printf("X < 0: %e\n", uIonized[IS]/uIonized[ID]);
              uIonized[IS] = ZERO_F;
            }
            if(uIonized[IS]/uIonized[ID] > ONE_F)
            {
              printf("X > 1: %e\n", uIonized[IS]/uIonized[ID]);
              uIonized[IS] = uIonized[ID];
            }
            if(rc_params.forceTemperatureIonization)
            {
              if(uIonized[IS]/uIonized[ID] > HALF_F)
                uIonized[ITRAD] = Teq_ionization;
              else
                uIonized[ITRAD] = Teq_forcing;
            }
            if(uIonized[IERAD] < 1e-100)
            {
              uIonized[IERAD] = 1e-100;
            }

            uIonized[IO] = sigma0*(ONE_F-uIonized[IS]/uIonized[ID]);

            return uIonized;
          } // ionizationSourceTerms

        KOKKOS_INLINE_FUNCTION
          HydroState rayTracing(HydroState uLoc, HydroState uNei, real_t dt) const
          {
            HydroState u;
            const real_t dy = params.dy;
            const real_t rho_cv = params.settings.cv * uLoc[ID];
            const real_t c = params.settings.speedOfLight;

            u[ID] = uLoc[ID];
            u[IU] = uLoc[IU];
            u[IV] = uLoc[IV];
            u[IE] = uLoc[IE];
            u[IO] = uLoc[IO];

            u[IFX]    = uLoc[IFX];
            u[IFY]    = uNei[IFY]  / (ONE_F + uLoc[IO]*dy);
            u[IERAD] = u[IFY]/c;
            u[IS] = uLoc[IS];
            u[ITRAD] = uLoc[ITRAD] + dt*uLoc[IO]*c*u[IERAD]/rho_cv;


            if(u[IS] < ZERO_F)
            {
              printf("X < 0: %e\n", u[IS]/u[ID]);
              u[IS] = ZERO_F;
            }
            if(u[IS]/u[ID] > ONE_F)
            {
              printf("X > 1: %e\n", u[IS]/u[ID]);
              u[IS] = u[ID];
            }
            return u;
          } // rayTracing



        KOKKOS_INLINE_FUNCTION
          HydroState thermal_driving(HydroState u, const RadiativeConvectionParams& rc_params, const real_t dt, const int j) const
          {
            const real_t T_ymin = rc_params.Trad_ymin;
            const real_t T_ymax = rc_params.Trad_ymax;
            const real_t k_b = code_units::constants::k_b;
            const real_t m_h = code_units::constants::m_h;
            const real_t tau = rc_params.tau;
            const real_t ny = params.ny;
            const int jmin = params.jmin;
            const int ghostWidth = params.ghostWidth;
#ifdef USE_MPI
            const int j_mpi = params.myMpiPos[IY];
            const int my = params.my;
#else
            const int j_mpi = 0;
            const int my = 1;
#endif

            real_t Teq = T_ymin;
            // To handle mpi dispatch
            for (int j_glob=1; j_glob<=ny*j_mpi; ++j_glob)
            {
              Teq -= (T_ymin-T_ymax)/(ny*my);
            }
            for (int j_glob=jmin+ghostWidth; j_glob<=j; ++j_glob)
            {
              Teq -= (T_ymin-T_ymax)/(ny*my);
            }
            HydroState q = computePrimitives(u);
            // Obtain current temperature.
            const real_t Tn = computeTemperature(q);
            const real_t mmw = computeMmw(q);
            const real_t T = (tau*Tn+dt*Teq)/(tau+dt);
            q[IP] = T*q[ID]*k_b/(mmw*m_h);
            return computeConservatives(q);
          } // thermal_driving

    }; // class HydroBaseFunctor2D

  } // namespace all_regime_radiative_transfer

} // namespace ark_rt
