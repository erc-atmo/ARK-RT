// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include <string>
#include <cstdio>
#include <cstdbool>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <type_traits>
#include <iostream>
#include <iomanip>
#include <limits>

// shared
#include "shared/SolverBase.h"
#include "shared/HydroParams.h"
#include "shared/kokkos_shared.h"

#include "all_regime_radiative_transfer/HydroRunFunctorsRT.h"
#include "all_regime_radiative_transfer/HydroInitFunctorsRT.h"

// for IO
#include <utils/io/IO_ReadWrite.h>

// for init condition
#include "shared/DamBreakParams.h"
#include "shared/GreshoParams.h"
#include "shared/IsentropicVortexParams.h"
#include "shared/PoiseuilleParams.h"
#include "shared/RayleighBenardParams.h"
#include "shared/initRiemannConfig2d.h"
#include "shared/BlastParams.h"
#include "shared/RadiativeShockParams.h"

// linear algebra
#include "linearAlgebra/LinearSolver_decl.h"


namespace ark_rt { namespace all_regime_radiative_transfer
  {

    /**
     * Main hydrodynamics data structure for 2D/3D All-Regime scheme.
     */
    template<int dim>
      class SolverHydroAllRegime : public ark_rt::SolverBase
    {
      public:
        //! Decide at compile-time which data array to use for 2d or 3d
        using DataArray = typename DataArrays<dim>::DataArray;

        //! Data array typedef for host memory space
        using DataArrayHost = typename DataArrays<dim>::DataArrayHost;

        //! Static creation method called by the solver factory.
        static SolverBase* create(HydroParams& params, ConfigMap& configMap);

        SolverHydroAllRegime(HydroParams& params, ConfigMap& configMap);
        virtual ~SolverHydroAllRegime();

        void getDeviceData(DataArray& Udata) override;

        //! init wrapper
        void init(DataArray Udata);

        //! compute time step inside an MPI process, at shared memory level.
        real_t compute_dt_local() override;

        //! perform 1 time step (time integration).
        void next_iteration_impl() override;

        //! numerical scheme
        void all_regime_scheme();

        // output
        void save_solution_impl() override;

        // read restart file specified in parameter file
        void read_restart_file() override;

        // check the eigenvalues for radiative transfer
        void checkValP() const;

        // Public Members
        DataArray     U;     /*!< hydrodynamics conservative variables arrays */
        DataArrayHost Uhost; /*!< U mirror on host memory space */
        DataArray     U2;    /*!< hydrodynamics conservative variables arrays */
        DataArray     Q;     /*!< hydrodynamics primitive    variables array  */
        DataArray     Un;     /*!< hydrodynamics conservative variables arrays at previous time */
        DataArray 	fluxData_x; /*!< numerical fluxes for radiative transfer in the X-direction */
        DataArray 	fluxData_y; /*!< numerical fluxes for radiative transfer in the Y-direction */
        linearAlgebra::LinearSolver<dim> solver; /*!< linear solver */
        int isize, jsize, ksize;
        int nbCells;
    }; // class SolverHydroAllRegime

    // =======================================================
    // ==== CLASS SolverHydroAllRegime IMPL ==================
    // =======================================================

    // =======================================================
    // =======================================================
    /**
     * Static creation method called by the solver factory.
     */
    template<int dim>
      SolverBase* SolverHydroAllRegime<dim>::create(HydroParams& params, ConfigMap& configMap)
      {
        SolverHydroAllRegime<dim>* solver = new SolverHydroAllRegime<dim>(params, configMap);

        return solver;
      }

    // =======================================================
    // =======================================================
    /**
     *
     */
    template<int dim>
      SolverHydroAllRegime<dim>::SolverHydroAllRegime(HydroParams& params_, ConfigMap& configMap_) :
        SolverBase(params_, configMap_),
        U(), U2(), Q(), fluxData_x(), fluxData_y(), solver(),
        isize(params_.isize), jsize(params_.jsize), ksize(params_.ksize),
        nbCells(params_.isize*params_.jsize)
    {
      solver_type = SOLVER_ALL_REGIME;

      if (dim==3)
      {
        nbCells = params.isize*params.jsize*params.ksize;
      }

      m_nCells = nbCells;
      m_nDofsPerCell = 1;

      int nbvar = params.nbvar;

      /*
       * memory allocation (use sizes with ghosts included).
       *
       * Note that Uhost is not just a view to U, Uhost will be used
       * to save data from multiple other device array.
       * That's why we didn't use create_mirror_view to initialize Uhost.
       */
      if (dim==2)
      {
        U     = DataArray("U", isize, jsize, nbvar);
        Uhost = Kokkos::create_mirror(U);
        U2    = DataArray("U2",isize, jsize, nbvar);
        Q     = DataArray("Q", isize, jsize, nbvar);
        Un    = DataArray("U", isize, jsize, nbvar);
        fluxData_x = DataArray("fluxData_x", isize, jsize, nbvar);
        fluxData_y = DataArray("fluxData_y", isize, jsize, nbvar);
      }
      else
      {
        U     = DataArray("U", isize, jsize, ksize, nbvar);
        Uhost = Kokkos::create_mirror(U);
        U2    = DataArray("U2",isize, jsize, ksize, nbvar);
        Q     = DataArray("Q", isize, jsize, ksize, nbvar);
      } // dim == 2 / 3

      if (m_restart_run_enabled)
      {
        read_restart_file();
      }
      else
      {
        // perform init condition
        init(U);
      }

      // initialize boundaries
      make_boundaries(U);

      // compute initialize time step
      compute_dt();

      if(params.settings.implicit_enabled == IMPLICIT)
      {
        // initialize linear solver
        timers[TIMER_LINEAR_ALGEBRA]->start();
        solver.create(U, params, nbCells, m_problem_name, m_dt, this);
        timers[TIMER_LINEAR_ALGEBRA]->stop();
      }

      // copy U into U2
      Kokkos::deep_copy(U2,U);

      RunFunctors<dim>::ConvertToPrimitives::apply(params, U, Q, nbCells);

      int myRank=0;
#ifdef USE_MPI
      myRank = params.myRank;
#endif // USE_MPI

      if (myRank==0)
      {
        std::cout << "##########################" << std::endl;
        std::cout << "Solver is " << m_solver_name << std::endl;
        std::cout << "Problem (init condition) is " << m_problem_name << std::endl;
        std::cout << "##########################" << std::endl;

        // print parameters on screen
        params.print();

        if (m_restart_run_enabled)
        {
          std::ostringstream oss;
          oss << std::scientific;
          oss << std::setprecision(std::numeric_limits<real_t>::max_digits10);

          oss << "Restarting computation at t=" << m_t << "\n";
          oss << "(No pvd nor xmf file will be generated !)" << std::endl;
          std::cout << oss.str();
        }

        /* checkValP(); */
      }

    } // SolverHydroAllRegime::SolverHydroAllRegime

    // =======================================================
    // =======================================================
    /**
     *
     */
    template<int dim>
      SolverHydroAllRegime<dim>::~SolverHydroAllRegime()
      {
      } // SolverHydroAllRegime::~SolverHydroAllRegime

    template<int dim>
      void SolverHydroAllRegime<dim>::getDeviceData(DataArray& Udata)
      {
        Udata = U;
      };

    // =======================================================
    // =======================================================
    /**
     * Compute time step satisfying CFL condition.
     *
     * \return dt time step
     */
    template<int dim>
      real_t SolverHydroAllRegime<dim>::compute_dt_local()
      {
        real_t invDt = ZERO_F;

        if (params.useAllRegimeTimeSteps)
        {
          // call device functor
          real_t invDtAcoustic = ZERO_F;
          RunFunctors<dim>::AcousticTimeStep::apply(params, Q, invDtAcoustic, nbCells);

          // call device functor
          real_t invDtTransport = ZERO_F;
          RunFunctors<dim>::TransportTimeStep::apply(params, Q,invDtTransport, nbCells);

          invDt = FMAX(invDtTransport, invDtAcoustic);
        }
        else
        {
          // call device functor
          RunFunctors<dim>::TimeStep::apply(params, U, invDt, nbCells);
        }

        return params.settings.cfl_hydro/invDt;
      } // SolverHydroAllRegime::compute_dt_local

    // =======================================================
    // =======================================================
    template<int dim>
      void SolverHydroAllRegime<dim>::next_iteration_impl()
      {
        // compute new dt
        timers[TIMER_DT]->start();
        compute_dt();
        if(params.settings.rtM1_enabled)
          m_dt = solver.compute_dt_rad(params, U, Un, nbCells, m_dt);
        timers[TIMER_DT]->stop();

        // perform one step integration
        all_regime_scheme();

      } // SolverHydroAllRegime::next_iteration_impl

    template<int dim>
      void SolverHydroAllRegime<dim>::all_regime_scheme()
      {
        // copy U into Un
        Kokkos::deep_copy(Un,U);
        // copy U into U2
        Kokkos::deep_copy(U2,U); // with the explicit solver, we need U2 at time n

        // Time state :
        // U  [inner region] : n
        // U  [ghost region] : n
        // Q  [whole region] : n
        // U2                : n

        timers[TIMER_NUM_SCHEME]->start();

        if(params.settings.hydro_enabled)
        {
          RunFunctors<dim>::AcousticStep::apply(params, U, U2, Q, m_dt, nbCells);
          // Time state :
          // U  [inner region] : n
          // U  [ghost region] : n
          // Q  [whole region] : n
          // U2                : n+1-

          RunFunctors<dim>::TransportStep::apply(params, U, U2, Q, m_dt, nbCells);
          // Time state :
          // U  [inner region] : n+1
          // U  [ghost region] : n
          // Q  [whole region] : n
          // U2                : n+1-

          if (params.settings.kappa > ZERO_F)
          {
            RunFunctors<dim>::HeatDiffusionStep::apply(params, U, Q, m_dt, nbCells);
          }

          if (params.settings.mu > ZERO_F)
          {
            RunFunctors<dim>::ViscosityStep::apply(params, U, Q, m_dt, nbCells);
          }

          if (params.settings.rt_enabled)
          {
            RadiativeConvectionParams rc_params(configMap);
            RunFunctors<dim>::RadiativeTransfer::apply(params, rc_params, U, Q, m_dt, params.isize);
          }

          if (params.settings.chem_enabled)
          {
            RadiativeConvectionParams rc_params(configMap);
            RunFunctors<dim>::Chemistry::apply(params, rc_params, U, Q, m_dt, nbCells);
          }
        }

        if(params.settings.thermal_driving_enabled && !params.settings.ionization_enabled)
        {
          // temperature forcing de/dt = - (T-T_forcing)/tau
          // if the ionization functor called, this is already done 
          RadiativeConvectionParams rc_params(configMap);
          RunFunctors<dim>::ThermalDriving::apply(params, rc_params, U, m_dt, nbCells);
        }

        if(params.settings.rtM1_enabled)
        {
          // Time state :
          // U  [inner region] : n+1 hydro
          // U  [ghost region] : n
          // Q  [whole region] : n
          // U2                : n+1-
          
          if(params.settings.hydro_enabled)
          {
            RunFunctors<dim>::ComputeTemperature::apply(params, U, nbCells);
          }

          if(params.settings.implicit_enabled == RAY_TRACING)
          {
            RunFunctors<2>::RayTracing::apply(params, Un, U, m_dt, nbCells);
          }
          else if(params.settings.implicit_enabled == EXPLICIT || (params.settings.restrictTimeStepRT && !solver.useImplicitSolver()))
          {
            /* std::cout << U2(2,2, IERAD) << std::endl; */
            // compute fluxes along X axis
            RunFunctors<2>::ComputeFluxesRT<XDIR>::apply(params, U, fluxData_x, m_dt, nbCells);
            // compute fluxes along Y axis
            RunFunctors<2>::ComputeFluxesRT<YDIR>::apply(params, U, fluxData_y, m_dt, nbCells);
            // update cells along X axis
            RunFunctors<2>::UpdateCellsRT<XDIR>::apply(params, U2, fluxData_x, nbCells);
            // update cells along Y axis
            RunFunctors<2>::UpdateCellsRT<YDIR>::apply(params, U2, fluxData_y, nbCells);
            timers[TIMER_NUM_SCHEME]->stop();
            timers[TIMER_BOUNDARIES]->start();
            make_boundaries(U2);
            timers[TIMER_BOUNDARIES]->stop();
            timers[TIMER_NUM_SCHEME]->start();
            // Time state :
            // U  [inner region] : n+1 hydro
            // U  [ghost region] : n
            // Q  [whole region] : n
            // U2                : n+1- hydro and U HLL for radiative transfer
            // source terms
            RunFunctors<dim>::SourceTermsRT::apply(params, U2, U, Un, m_dt, nbCells);
            timers[TIMER_NUM_SCHEME]->stop();
            timers[TIMER_BOUNDARIES]->start();
            make_boundaries(U);
            timers[TIMER_BOUNDARIES]->stop();
            timers[TIMER_NUM_SCHEME]->start();
            // Time state :
            // U  [inner region] : n+1 hydro + RT
            // U  [ghost region] : n+1 hydro + RT
            // Q  [whole region] : n
            // U2                : n+1- hydro and U HLL for radiative transfer
          }
          else
          {
            timers[TIMER_LINEAR_ALGEBRA]->start();
            solver.solve(U, params, nbCells, m_dt);
            timers[TIMER_LINEAR_ALGEBRA]->stop();
          }
          // Time state :
          // U  [inner region] : n+1 hydro + RT
          // U  [ghost region] : n+1 hydro + RT
          // Q  [whole region] : n
          // U2                : n+1- hydro and U HLL for explicit radiative transfer

          if(params.settings.ionization_enabled)
          {
            RadiativeConvectionParams rc_params(configMap);
            RunFunctors<dim>::Ionization::apply(params, rc_params, U, m_dt, nbCells);
          }
          // Time state :
          // U  [inner region] : n+1 ionization
          // U  [ghost region] : n+1 hydro + RT
          // Q  [whole region] : n
          // U2                : n+1- hydro and U HLL for explicit radiative transfer
           
          if(params.settings.hydro_enabled)
          {
            timers[TIMER_NUM_SCHEME]->stop();
            timers[TIMER_BOUNDARIES]->start();
            make_boundaries(U);
            timers[TIMER_BOUNDARIES]->stop();
            timers[TIMER_NUM_SCHEME]->start();
            // Time state :
            // U  [inner region] : n+1 ionization
            // U  [ghost region] : n+1 ionization
            // Q  [whole region] : n
            // U2                : n+1- hydro and U HLL for explicit radiative transfer
            // copy U into U2 
            Kokkos::deep_copy(U2,U);
            // Time state :
            // U  [inner region] : n+1 ionization
            // U  [ghost region] : n+1 ionization
            // Q  [whole region] : n
            // U2                : n+1 ionization
            RadiativeConvectionParams rc_params(configMap);
            RunFunctors<dim>::CouplingHydroRad::apply(params, rc_params, U2, Un, U, m_dt, nbCells, solver.useImplicitSolver());
          }
        }
        // Time state :
        // U  [inner region] : n+1
        // U  [ghost region] : n
        // Q  [whole region] : n
        // U2                : n+1 ionization

        timers[TIMER_NUM_SCHEME]->stop();

        timers[TIMER_BOUNDARIES]->start();
        make_boundaries(U);
        timers[TIMER_BOUNDARIES]->stop();
        // Time state :
        // U  [inner region] : n+1
        // U  [ghost region] : n+1
        // Q  [whole region] : n
        // U2                : n+1-

        timers[TIMER_NUM_SCHEME]->start();
        RunFunctors<dim>::ConvertToPrimitives::apply(params, U, Q, nbCells);
        timers[TIMER_NUM_SCHEME]->stop();
        // Time state :
        // U  [inner region] : n+1
        // U  [ghost region] : n+1
        // Q  [whole region] : n+1
        // U2                : n+1-
      }

    template<>
      void SolverHydroAllRegime<2>::init(DataArray Udata)
      {
        /*
         * initialize hydro array at t=0
         */
        if (m_problem_name == "implode")
        {
          InitFunctors<2>::Implode::apply(params, Udata, nbCells);
        }
        else if (m_problem_name == "dam_break")
        {
          DamBreakParams dbParams(configMap);
          InitFunctors<2>::DamBreak::apply(params, dbParams, Udata, nbCells);
        }
        else if (m_problem_name == "poiseuille")
        {
          PoiseuilleParams poiseuilleParams(configMap);
          InitFunctors<2>::Poiseuille::apply(params, poiseuilleParams, Udata, nbCells);
        }
        else if (m_problem_name == "blast")
        {
          BlastParams blastParams = BlastParams(configMap);
          InitFunctors<2>::Blast::apply(params, blastParams, Udata, nbCells);
        }
        else if (m_problem_name == "rayleigh_benard")
        {
          RayleighBenardParams rayleighBenardParams(configMap);
          InitFunctors<2>::RayleighBenard::apply(params, rayleighBenardParams, Udata, nbCells);
        }
        else if (m_problem_name == "rayleigh_taylor")
        {
          InitFunctors<2>::RayleighTaylor::apply(params, Udata, nbCells);
        }
        else if (m_problem_name == "atmosphere_at_rest")
        {
          InitFunctors<2>::AtmosphereAtRest::apply(params, Udata, nbCells);
        }
        else if (m_problem_name == "gresho")
        {
          GreshoParams greshoParams(configMap);
          InitFunctors<2>::Gresho::apply(params, greshoParams, Udata, nbCells);
        }
        else if (m_problem_name == "radiative_convection")
        {
          RadiativeConvectionParams rc_params(configMap);
          InitFunctors<2>::RadiativeConvection::apply(params, rc_params, Udata, params.isize);
        }
        else if (m_problem_name == "four_quadrant")
        {
          int configNumber = configMap.getInteger("riemann2d","config_number",0);
          real_t xt = configMap.getFloat("riemann2d","x",0.8);
          real_t yt = configMap.getFloat("riemann2d","y",0.8);

          HydroState2d U0, U1, U2, U3;
          getRiemannConfig2d(configNumber, U0, U1, U2, U3);

          primToCons_2D(U0, params.settings.gamma0);
          primToCons_2D(U1, params.settings.gamma0);
          primToCons_2D(U2, params.settings.gamma0);
          primToCons_2D(U3, params.settings.gamma0);

          InitFunctors<2>::FourQuadrant::apply(params, Udata, configNumber,
              U0, U1, U2, U3,
              xt, yt, nbCells);
        }
        else if (m_problem_name == "isentropic_vortex")
        {
          IsentropicVortexParams iparams(configMap);
          InitFunctors<2>::IsentropicVortex::apply(params, iparams, Udata, nbCells);
        }
        else if (m_problem_name == "riemann_problem")
        {
          RiemannProblemParams rp_params(configMap);
          InitFunctors<2>::RiemannProblem::apply(params, rp_params, Udata, nbCells);
        }
        else if (m_problem_name == "marshak_wave_1d")
        {
          RadiativeTransferParams rtParams(configMap);
          InitFunctors<2>::RadiativeTransferM1::apply(params, rtParams, Udata, nbCells);
        }
        else if (m_problem_name == "radiative_transfer_well_balanced")
        {
          RadiativeTransferParams rtParams(configMap);
          InitFunctors<2>::RadiativeTransferM1::apply(params, rtParams, Udata, nbCells);
        }
        else if (m_problem_name == "marshak_wave_2d")
        {
          RadiativeTransferParams rtParams(configMap);
          InitFunctors<2>::RadiativeTransferM1::apply(params, rtParams, Udata, nbCells);
        }
        else if (m_problem_name == "radiative_transfer_perf_2d")
        {
          RadiativeTransferParams rtParams(configMap);
          InitFunctors<2>::RTPerf2D::apply(params, rtParams, Udata, nbCells);
        }
        else if (m_problem_name == "shadow")
        {
          InitFunctors<2>::RT2DShadow::apply(params, Udata, nbCells);
        }
        else if (m_problem_name == "radiative_shock")
        {
          RadiativeShockParams radShockParams(configMap);
          InitFunctors<2>::RadiativeShock::apply(params, radShockParams, Udata, nbCells);
        }
        else if (m_problem_name == "beam")
        {
          InitFunctors<2>::RT2DBeam::apply(params, Udata, nbCells);
        }
        else if (m_problem_name == "HII_region")
        {
          InitFunctors<2>::HIIRegion::apply(params, Udata, nbCells);
        }
        else if (m_problem_name == "HII_conv")
        {
          RadiativeConvectionParams rc_params(configMap);
          InitFunctors<2>::HIIConv::apply(params, rc_params, Udata, nbCells);
        }
        else
        {
          std::cout << "Problem : " << m_problem_name
            << " is not recognized / implemented."
            << std::endl;
          std::cout << "Exiting..." << std::endl;
          std::exit(EXIT_FAILURE);
        }
      } // SolverHydroMuscinit / 2d

    template<>
      void SolverHydroAllRegime<3>::init(DataArray Udata)
      {
        /*
         * initialize hydro array at t=0
         */
        if (m_problem_name == "implode")
        {
          InitFunctors<3>::Implode::apply(params, Udata, nbCells);
        }
        else if (m_problem_name == "poiseuille")
        {
          PoiseuilleParams poiseuilleParams(configMap);
          InitFunctors<3>::Poiseuille::apply(params, poiseuilleParams, Udata, nbCells);
        }
        else if (m_problem_name == "blast")
        {
          BlastParams blastParams = BlastParams(configMap);
          InitFunctors<3>::Blast::apply(params, blastParams, Udata, nbCells);
        }
        else if (m_problem_name == "rayleigh_benard")
        {
          RayleighBenardParams rayleighBenardParams(configMap);
          InitFunctors<3>::RayleighBenard::apply(params, rayleighBenardParams, Udata, nbCells);
        }
        else if (m_problem_name == "rayleigh_taylor")
        {
          InitFunctors<3>::RayleighTaylor::apply(params, Udata, nbCells);
        }
        else
        {
          std::cout << "Problem : " << m_problem_name
            << " is not recognized / implemented."
            << std::endl;
          std::cout << "Exiting..." << std::endl;
          std::exit(EXIT_FAILURE);
        }
      } // SolverHydroMuscinit / 2d

    // =======================================================
    // =======================================================
    template<int dim>
      void SolverHydroAllRegime<dim>::save_solution_impl()
      {
        timers[TIMER_IO]->start();
        save_data(U,  Uhost, m_iOutput_global, m_iteration, m_t);
        timers[TIMER_IO]->stop();
      } // SolverHydroAllRegime::save_solution_impl()

    template<int dim>
      void SolverHydroAllRegime<dim>::read_restart_file()
      {
        // Need to construct a new IO_ReadWrite because the one contained
        // in the class is not yet initialized;
        io::IO_ReadWrite io_reader_writer(params, configMap, m_variables_names);
        io_reader_writer.load_data(U, Uhost, m_iOutput_global, m_iteration, m_t);
        m_tStart_local = m_t;
        if (m_restart_run_upscale)
        {
          m_iOutput_global   = 0;
          m_iteration = 0;
          m_t         = ZERO_F;
        }
        m_restart_iteration = m_iteration;
      } // SolverHydroAllRegime::read_restart_file()

    template<int dim>
      void SolverHydroAllRegime<dim>::checkValP() const
      {
        std::cout << "check eigenvalues\n";
        // valide the function eigenvaluesRT
        // reproduce fig. 2.1 of González 2006
        const int n = 500;
        const real_t c = ONE_F;//params.settings.speedOfLight;
        std::ofstream file;
        file.open("eigenvalues.txt");
        assert(file);
        std::stringstream ss;

        for(int i = 0; i < n+1; i++)
        {
          // theta = 0 and theta = pi/2
          real_t f = -1. + TWO_F*(real_t)i/n;
          real_t e = ONE_F;
          real_t Fx = f*c*e;
          real_t Fy = ZERO_F;

          real_t sr_0, sl_0;
          compute_analytical_eigenvalues(sr_0, sl_0, e, Fx, Fy, c, false); // theta = 0
          real_t sr_pi2, sl_pi2;
          compute_analytical_eigenvalues(sl_pi2, sr_pi2, e, Fy, Fx, c, false); // theta = pi/2

          // write file
          ss << i << '\t' << sl_0 << '\t' << sr_0 << '\t' << sl_pi2 << '\t' << sr_pi2 << '\t';

          // ||f|| = 1
          real_t theta = PI*(real_t)i/n;
          f = ONE_F;
          Fx = f*c*e*std::cos(theta);
          Fy = f*c*e*std::sin(theta);

          real_t sl_theta, sr_theta;
          compute_analytical_eigenvalues(sl_theta, sr_theta, e, Fx, Fy, c, false);

          // write file
          ss << '\t' << sl_theta << '\t' << sr_theta << '\n';
        }
        ss << std::flush;
        file << ss.str();
        file.close();


      }

  } // namespace all_regime_radiative_transfer

} // namespace ark_rt
