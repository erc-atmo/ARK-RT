// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include <limits> // for std::numeric_limits
#ifdef __CUDA_ARCH__
#include <math_constants.h> // for cuda math constants, e.g. CUDART_INF
#endif // __CUDA_ARCH__

#include "shared/kokkos_shared.h"
#include "HydroBaseFunctorRT2D.h"
#include "shared/RiemannSolvers.h"

namespace ark_rt { namespace all_regime_radiative_transfer
  {

    class ComputeAcousticStepFunctor2D : public HydroBaseFunctor2D
    {
      public:
        ComputeAcousticStepFunctor2D(HydroParams params_,
            DataArrayConst Udata_, DataArray U2data_, DataArrayConst Qdata_,
            real_t dt_) :
          HydroBaseFunctor2D(params_), Udata(Udata_), U2data(U2data_), Qdata(Qdata_), m_K(params.settings.K),
          dtdx(dt_/params.dx), dtdy(dt_/params.dy), half_dtdx(HALF_F * dtdx), half_dtdy(HALF_F * dtdy),
          conservative(params.settings.conservative) {};

        static void apply(HydroParams params,
            DataArrayConst Udata, DataArray U2data, DataArrayConst Qdata,
            real_t dt, int nbCells)
        {
          ComputeAcousticStepFunctor2D functor(params, Udata, U2data, Qdata, dt);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void computeAcousticRelaxation(const HydroState& qLoc, real_t cLoc,
              const HydroState& qNei, real_t cNei,
              real_t M, int IX, int dir,
              real_t& uStar, real_t& piStar) const
          {
            const real_t a = m_K * FMAX(qNei[ID] * cNei, qLoc[ID] * cLoc);
            uStar = dir * HALF_F * (qNei[IX] + qLoc[IX]) - HALF_F * (qNei[IP] - qLoc[IP] + M) / a;

            const real_t theta = params.settings.low_mach_correction ? FMIN(FABS(uStar) / FMAX(cNei, cLoc), ONE_F) : ONE_F;
            piStar = + HALF_F * (qNei[IP] + qLoc[IP]) - dir * HALF_F * theta * a * (qNei[IX] - qLoc[IX]);
          }

        KOKKOS_INLINE_FUNCTION
          void operator()(int index) const
          {
            int i, j;
            index2coord(index, i, j, params.isize, params.jsize);

            const int ghostWidth = params.ghostWidth;

            if (j>=ghostWidth-1 && j<=params.jmax-ghostWidth+1 &&
                i>=ghostWidth-1 && i<=params.imax-ghostWidth+1)
            {
              const HydroState qLoc = getHydroState(Qdata, i, j);
              const real_t cLoc = computeSpeedSound(qLoc);
              const real_t phiLoc = phi(i, j);

              real_t uStarMinusX, piStarMinusX, Mmx;
              {
                const HydroState qMx = getHydroState(Qdata, i-1, j);
                const real_t cMinusX = computeSpeedSound(qMx);
                const real_t phiMx = phi(i-1, j);
                Mmx = computeM(qLoc, phiLoc, qMx, phiMx);
                computeAcousticRelaxation(qLoc, cLoc, qMx, cMinusX, Mmx, IU, -1,
                    uStarMinusX, piStarMinusX);
              }

              real_t uStarPlusX, piStarPlusX, Mpx;
              {
                const HydroState qPx = getHydroState(Qdata, i+1, j);
                const real_t cPlusX = computeSpeedSound(qPx);
                const real_t phiPx = phi(i+1, j);
                Mpx = computeM(qLoc, phiLoc, qPx, phiPx);
                computeAcousticRelaxation(qLoc, cLoc, qPx, cPlusX, Mpx, IU, +1,
                    uStarPlusX, piStarPlusX);
              }

              real_t uStarMinusY, piStarMinusY, Mmy;
              {
                const HydroState qMy = getHydroState(Qdata, i, j-1);
                const real_t cMinusY = computeSpeedSound(qMy);
                const real_t phiMy = phi(i, j-1);
                Mmy = computeM(qLoc, phiLoc, qMy, phiMy);
                computeAcousticRelaxation(qLoc, cLoc, qMy, cMinusY, Mmy, IV, -1,
                    uStarMinusY, piStarMinusY);
              }

              real_t uStarPlusY, piStarPlusY, Mpy;
              {
                const HydroState qPy = getHydroState(Qdata, i, j+1);
                const real_t cPlusY = computeSpeedSound(qPy);
                const real_t phiPy = phi(i, j+1);
                Mpy = computeM(qLoc, phiLoc, qPy, phiPy);
                computeAcousticRelaxation(qLoc, cLoc, qPy, cPlusY, Mpy, IV, +1,
                    uStarPlusY, piStarPlusY);
              }

              HydroState uLoc = getHydroState(Udata, i, j);

              if (conservative)
              {
                uLoc[IE] += qLoc[ID] * phiLoc;
              }

              // Acoustic update
              uLoc[IU] -= dtdx * (piStarPlusX - piStarMinusX); ////
              uLoc[IV] -= dtdy * (piStarPlusY - piStarMinusY);
              uLoc[IP] -= dtdx * (piStarMinusX * uStarMinusX + piStarPlusX * uStarPlusX); ////
              uLoc[IP] -= dtdy * (piStarMinusY * uStarMinusY + piStarPlusY * uStarPlusY);

              uLoc[IU] -= half_dtdx * (Mpx - Mmx); ////
              uLoc[IV] -= half_dtdy * (Mpy - Mmy);
              if (!conservative)
              {
                uLoc[IP] -= half_dtdx * (Mpx * uStarPlusX + Mmx * uStarMinusX); ////
                uLoc[IP] -= half_dtdy * (Mpy * uStarPlusY + Mmy * uStarMinusY);
              }

              // Compute L factor
              const real_t L = (ONE_F+
                  dtdx * (uStarMinusX + uStarPlusX)+ ////
                  dtdy * (uStarMinusY + uStarPlusY));
              const real_t invL = ONE_F / L;

              uLoc[ID] *= invL;
              uLoc[IU] *= invL;
              uLoc[IV] *= invL;
              uLoc[IP] *= invL;
              uLoc[IS] *= invL;

              // Real update
              setHydroState(U2data, uLoc, i, j);
            }
          }

        const DataArrayConst Udata;
        const DataArray U2data;
        const DataArrayConst Qdata;
        const real_t m_K;
        const real_t dtdx;
        const real_t dtdy;
        const real_t half_dtdx;
        const real_t half_dtdy;
        const bool conservative;
    }; // ComputeAcousticStepFunctor2D


    class ComputeTransportStepFunctor2D : HydroBaseFunctor2D
    {
      public:
        ComputeTransportStepFunctor2D(HydroParams params_,
            DataArray Udata_, DataArrayConst U2data_, DataArrayConst Qdata_,
            real_t dt_) :
          HydroBaseFunctor2D(params_),
          Udata(Udata_), U2data(U2data_), Qdata(Qdata_),
          dtdx(dt_/params.dx), dtdy(dt_/params.dy),
          conservative(params.settings.conservative) {};

        static void apply(HydroParams params,
            DataArray Udata, DataArrayConst U2data, DataArrayConst Qdata,
            real_t dt, int nbCells)
        {
          ComputeTransportStepFunctor2D functor(params, Udata, U2data, Qdata, dt);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void computeAcousticRelaxation(const HydroState & qLoc, real_t cLoc,
              const HydroState & qNei, real_t cNei,
              real_t M, int IX, int dir, real_t& uStar) const
          {
            const real_t a = params.settings.K * FMAX(qNei[ID] * cNei, qLoc[ID] * cLoc);
            uStar = dir * HALF_F * (qNei[IX] + qLoc[IX]) - HALF_F * (qNei[IP] - qLoc[IP] + M) / a;
          }

        KOKKOS_INLINE_FUNCTION
          void operator()(int index) const
          {
            int i,j;
            index2coord(index, i, j, params.isize, params.jsize);

            const int ghostWidth = params.ghostWidth;

            if (j>=ghostWidth && j<=params.jmax-ghostWidth &&
                i>=ghostWidth && i<=params.imax-ghostWidth)
            {
              const HydroState qLoc = getHydroState(Qdata, i, j);
              const real_t cLoc = computeSpeedSound(qLoc);
              const real_t phiLoc = phi(i, j);

              real_t uStarMinusX;
              {
                const HydroState qMx = getHydroState(Qdata, i-1, j);
                const real_t cMinusX = computeSpeedSound(qMx);
                const real_t phiMx = phi(i-1, j);
                const real_t Mmx = computeM(qLoc, phiLoc, qMx, phiMx);
                computeAcousticRelaxation(qLoc, cLoc, qMx, cMinusX, Mmx, IU, -1,
                    uStarMinusX);
              }

              real_t uStarPlusX;
              {
                const HydroState qPx = getHydroState(Qdata, i+1, j);
                const real_t cPlusX = computeSpeedSound(qPx);
                const real_t phiPx = phi(i+1, j);
                const real_t Mpx = computeM(qLoc, phiLoc, qPx, phiPx);
                computeAcousticRelaxation(qLoc, cLoc, qPx, cPlusX, Mpx, IU, +1,
                    uStarPlusX);
              }

              real_t uStarMinusY;
              {
                const HydroState qMy = getHydroState(Qdata, i, j-1);
                const real_t cMinusY = computeSpeedSound(qMy);
                const real_t phiMy = phi(i, j-1);
                const real_t Mmy = computeM(qLoc, phiLoc, qMy, phiMy);
                computeAcousticRelaxation(qLoc, cLoc, qMy, cMinusY, Mmy, IV, -1,
                    uStarMinusY);
              }

              real_t uStarPlusY;
              {
                const HydroState qPy = getHydroState(Qdata, i, j+1);
                const real_t cPlusY = computeSpeedSound(qPy);
                const real_t phiPy = phi(i, j+1);
                const real_t Mpy = computeM(qLoc, phiLoc, qPy, phiPy);
                computeAcousticRelaxation(qLoc, cLoc, qPy, cPlusY, Mpy, IV, +1,
                    uStarPlusY);
              }

              const HydroState uLoc = getHydroState(U2data, i, j);
              HydroState u2Loc = uLoc;

              ////////
              u2Loc[ID] += dtdx * uLoc[ID] * (uStarMinusX + uStarPlusX);
              u2Loc[IU] += dtdx * uLoc[IU] * (uStarMinusX + uStarPlusX);
              u2Loc[IV] += dtdx * uLoc[IV] * (uStarMinusX + uStarPlusX);
              u2Loc[IP] += dtdx * uLoc[IP] * (uStarMinusX + uStarPlusX);
              u2Loc[IS] += dtdx * uLoc[IS] * (uStarMinusX + uStarPlusX);
              ////////

              u2Loc[ID] += dtdy * uLoc[ID] * (uStarMinusY + uStarPlusY);
              u2Loc[IU] += dtdy * uLoc[IU] * (uStarMinusY + uStarPlusY);
              u2Loc[IV] += dtdy * uLoc[IV] * (uStarMinusY + uStarPlusY);
              u2Loc[IP] += dtdy * uLoc[IP] * (uStarMinusY + uStarPlusY);
              u2Loc[IS] += dtdy * uLoc[IS] * (uStarMinusY + uStarPlusY);

              {
                ////////
                const int i0 = (uStarMinusX > ZERO_F) ? i : i - 1;
                const HydroState u0 = getHydroState(U2data, i0, j);
                u2Loc[ID] -= dtdx * u0[ID] * uStarMinusX;
                u2Loc[IU] -= dtdx * u0[IU] * uStarMinusX;
                u2Loc[IV] -= dtdx * u0[IV] * uStarMinusX;
                u2Loc[IP] -= dtdx * u0[IP] * uStarMinusX;
                u2Loc[IS] -= dtdx * u0[IS] * uStarMinusX;
                ////////
              }

              {
                ////////
                const int i0= (uStarPlusX > ZERO_F) ? i : i + 1;
                const HydroState u0 = getHydroState(U2data, i0, j);
                u2Loc[ID] -= dtdx * u0[ID] * uStarPlusX;
                u2Loc[IU] -= dtdx * u0[IU] * uStarPlusX;
                u2Loc[IV] -= dtdx * u0[IV] * uStarPlusX;
                u2Loc[IP] -= dtdx * u0[IP] * uStarPlusX;
                u2Loc[IS] -= dtdx * u0[IS] * uStarPlusX;
                ////////
              }

              {
                const int j0 = (uStarMinusY > ZERO_F) ? j : j - 1;
                const HydroState u0 = getHydroState(U2data, i, j0);
                u2Loc[ID] -= dtdy * u0[ID] * uStarMinusY;
                u2Loc[IU] -= dtdy * u0[IU] * uStarMinusY;
                u2Loc[IV] -= dtdy * u0[IV] * uStarMinusY;
                u2Loc[IP] -= dtdy * u0[IP] * uStarMinusY;
                u2Loc[IS] -= dtdy * u0[IS] * uStarMinusY;
              }

              {
                const int j0 = (uStarPlusY > ZERO_F) ? j : j + 1;
                const HydroState u0 = getHydroState(U2data, i, j0);
                u2Loc[ID] -= dtdy * u0[ID] * uStarPlusY;
                u2Loc[IU] -= dtdy * u0[IU] * uStarPlusY;
                u2Loc[IV] -= dtdy * u0[IV] * uStarPlusY;
                u2Loc[IP] -= dtdy * u0[IP] * uStarPlusY;
                u2Loc[IS] -= dtdy * u0[IS] * uStarPlusY;
              }

              if (conservative)
              {
                u2Loc[IE] -= u2Loc[ID] * phiLoc;
              }

              setHydroState(Udata, u2Loc, i, j);
            }
          }

        const DataArray Udata;
        const DataArrayConst U2data;
        const DataArrayConst Qdata;
        const real_t dtdx;
        const real_t dtdy;
        const bool conservative;
    }; // ComputeTransportStepFunctor2D


    class ComputeViscosityStepFunctor2D : HydroBaseFunctor2D
    {
      public:
        ComputeViscosityStepFunctor2D(HydroParams params_, DataArray Udata_, DataArrayConst Qdata_, real_t dt_):
          HydroBaseFunctor2D(params_), Udata(Udata_), Qdata(Qdata_), dt(dt_) {};

        static void apply(HydroParams params,
            DataArray Udata, DataArrayConst Qdata,
            real_t dt, int nbCells)
        {
          ComputeViscosityStepFunctor2D functor(params, Udata, Qdata, dt);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(int index) const
          {

            const int ghostWidth = params.ghostWidth;
            int i, j;
            index2coord(index, i, j, params.isize, params.jsize);

            if (j>=ghostWidth && j<=params.jmax-ghostWidth &&
                i>=ghostWidth && i<=params.imax-ghostWidth)
            {

              const real_t mu = params.settings.mu;
              const real_t lambda = ZERO_F;
              const real_t eta = lambda - TWO_F/(ONE_F+TWO_F) * mu;
              const real_t FOUR_F = TWO_F * TWO_F;

              const real_t dx = params.dx;
              const real_t dy = params.dy;
              const real_t dtdx = dt / dx;
              const real_t dtdy = dt / dy;

              {
                const HydroState qMx   = getHydroState(Qdata, i-1, j  );
                const HydroState qPx   = getHydroState(Qdata, i  , j  );
                const HydroState qMxMy = getHydroState(Qdata, i-1, j-1);
                const HydroState qMxPy = getHydroState(Qdata, i-1, j+1);
                const HydroState qPxPy = getHydroState(Qdata, i  , j+1);
                const HydroState qPxMy = getHydroState(Qdata, i  , j-1);
                const real_t uInterface = (qMx[IU] + qPx[IU]) / TWO_F;
                const real_t vInterface = (qMx[IV] + qPx[IV]) / TWO_F;
                const real_t uCornerUp   = (qMxPy[IU] + qPxPy[IU] + qMx[IU] + qPx[IU]) / FOUR_F;
                const real_t uCornerDown = (qMx[IU] + qPx[IU] + qMxMy[IU] + qPxMy[IU]) / FOUR_F;
                const real_t vCornerUp   = (qMxPy[IV] + qPxPy[IV] + qMx[IV] + qPx[IV]) / FOUR_F;
                const real_t vCornerDown = (qMx[IV] + qPx[IV] + qMxMy[IV] + qPxMy[IV]) / FOUR_F;

                const real_t du_dx = (qPx[IU] - qMx[IU]) / dx;
                const real_t dv_dx = (qPx[IV] - qMx[IV]) / dx;
                const real_t du_dy = (uCornerUp - uCornerDown) / dy;
                const real_t dv_dy = (vCornerUp - vCornerDown) / dy;

                // Compute fluxes
                const real_t tau_xx = TWO_F * mu * du_dx + eta * (du_dx + dv_dy);
                const real_t tau_xy = mu * (du_dy + dv_dx);

                // Update the right cell of the interface
                Udata(i, j, IU) += - dtdx * tau_xx;
                Udata(i, j, IV) += - dtdx * tau_xy;
                Udata(i, j, IP) += - dtdx * (uInterface * tau_xx + vInterface * tau_xy);
              }

              {
                const HydroState qMx   = getHydroState(Qdata, i  , j  );
                const HydroState qPx   = getHydroState(Qdata, i+1, j  );
                const HydroState qMxMy = getHydroState(Qdata, i  , j-1);
                const HydroState qMxPy = getHydroState(Qdata, i  , j+1);
                const HydroState qPxPy = getHydroState(Qdata, i+1, j+1);
                const HydroState qPxMy = getHydroState(Qdata, i+1, j-1);
                const real_t uInterface = (qMx[IU] + qPx[IU]) / TWO_F;
                const real_t vInterface = (qMx[IV] + qPx[IV]) / TWO_F;
                const real_t uCornerUp   = (qMxPy[IU] + qPxPy[IU] + qMx[IU] + qPx[IU]) / FOUR_F;
                const real_t uCornerDown = (qMx[IU] + qPx[IU] + qMxMy[IU] + qPxMy[IU]) / FOUR_F;
                const real_t vCornerUp   = (qMxPy[IV] + qPxPy[IV] + qMx[IV] + qPx[IV]) / FOUR_F;
                const real_t vCornerDown = (qMx[IV] + qPx[IV] + qMxMy[IV] + qPxMy[IV]) / FOUR_F;

                const real_t du_dx = (qPx[IU] - qMx[IU]) / dx;
                const real_t dv_dx = (qPx[IV] - qMx[IV]) / dx;
                const real_t du_dy = (uCornerUp - uCornerDown) / dy;
                const real_t dv_dy = (vCornerUp - vCornerDown) / dy;

                // Compute fluxes
                const real_t tau_xx = TWO_F * mu * du_dx + eta * (du_dx + dv_dy);
                const real_t tau_xy = mu * (du_dy + dv_dx);

                // Update the right cell of the interface
                Udata(i, j, IU) +=   dtdx * tau_xx;
                Udata(i, j, IV) +=   dtdx * tau_xy;
                Udata(i, j, IP) +=   dtdx * (uInterface * tau_xx + vInterface * tau_xy);
              }

              {
                const HydroState qMy   = getHydroState(Qdata, i  , j-1);
                const HydroState qPy   = getHydroState(Qdata, i  , j  );
                const HydroState qMxMy = getHydroState(Qdata, i-1, j-1);
                const HydroState qMxPy = getHydroState(Qdata, i-1, j  );
                const HydroState qPxPy = getHydroState(Qdata, i+1, j  );
                const HydroState qPxMy = getHydroState(Qdata, i+1, j-1);
                const real_t uInterface = (qMy[IU] + qPy[IU]) / TWO_F;
                const real_t vInterface = (qMy[IV] + qPy[IV]) / TWO_F;
                const real_t uCornerLeft  = (qMxPy[IU] + qPy[IU] + qMxMy[IU] + qMy[IU]) / FOUR_F;
                const real_t uCornerRight = (qPy[IU] + qPxPy[IU] + qMy[IU] + qPxMy[IU]) / FOUR_F;
                const real_t vCornerLeft  = (qMxPy[IV] + qPy[IV] + qMxMy[IV] + qMy[IV]) / FOUR_F;
                const real_t vCornerRight = (qPy[IV] + qPxPy[IV] + qMy[IV] + qPxMy[IV]) / FOUR_F;

                const real_t du_dx = (uCornerRight - uCornerLeft) / dx;
                const real_t dv_dx = (vCornerRight - vCornerLeft) / dx;
                const real_t du_dy = (qPy[IU] - qMy[IU]) / dy;
                const real_t dv_dy = (qPy[IV] - qMy[IV]) / dy;

                const real_t tau_yy = TWO_F * mu * dv_dy + eta * (du_dx + dv_dy);
                const real_t tau_xy = mu * (du_dy + dv_dx);

                // Update the up cell of the interface
                Udata(i, j, IU) += - dtdy * tau_xy;
                Udata(i, j, IV) += - dtdy * tau_yy;
                Udata(i, j, IP) += - dtdy * (uInterface * tau_xy + vInterface * tau_yy);
              }

              {
                const HydroState qMy   = getHydroState(Qdata, i  , j  );
                const HydroState qPy   = getHydroState(Qdata, i  , j+1);
                const HydroState qMxMy = getHydroState(Qdata, i-1, j  );
                const HydroState qMxPy = getHydroState(Qdata, i-1, j+1);
                const HydroState qPxPy = getHydroState(Qdata, i+1, j+1);
                const HydroState qPxMy = getHydroState(Qdata, i+1, j  );
                const real_t uInterface = (qMy[IU] + qPy[IU]) / TWO_F;
                const real_t vInterface = (qMy[IV] + qPy[IV]) / TWO_F;
                const real_t uCornerLeft  = (qMxPy[IU] + qPy[IU] + qMxMy[IU] + qMy[IU]) / FOUR_F;
                const real_t uCornerRight = (qPy[IU] + qPxPy[IU] + qMy[IU] + qPxMy[IU]) / FOUR_F;
                const real_t vCornerLeft  = (qMxPy[IV] + qPy[IV] + qMxMy[IV] + qMy[IV]) / FOUR_F;
                const real_t vCornerRight = (qPy[IV] + qPxPy[IV] + qMy[IV] + qPxMy[IV]) / FOUR_F;

                const real_t du_dx = (uCornerRight - uCornerLeft) / dx;
                const real_t dv_dx = (vCornerRight - vCornerLeft) / dx;
                const real_t du_dy = (qPy[IU] - qMy[IU]) / dy;
                const real_t dv_dy = (qPy[IV] - qMy[IV]) / dy;

                const real_t tau_yy = TWO_F * mu * dv_dy + eta * (du_dx + dv_dy);
                const real_t tau_xy = mu * (du_dy + dv_dx);

                // Update the bottom cell of the interface
                Udata(i, j, IU) +=   dtdy * tau_xy;
                Udata(i, j, IV) +=   dtdy * tau_yy;
                Udata(i, j, IP) +=   dtdy * (uInterface * tau_xy + vInterface * tau_yy);
              }

            }

          }

        const DataArray Udata;
        const DataArrayConst Qdata;
        const real_t dt;
    }; // ComputeViscosityStepFunctor2D


    class ComputeHeatDiffusionStepFunctor2D : HydroBaseFunctor2D
    {
      public:
        ComputeHeatDiffusionStepFunctor2D(HydroParams params_, DataArray Udata_, DataArrayConst Qdata_, real_t dt_):
          HydroBaseFunctor2D(params_), Udata(Udata_), Qdata(Qdata_), dt(dt_) {};

        static void apply(HydroParams params,
            DataArray Udata, DataArrayConst Qdata,
            real_t dt, int nbCells)
        {
          ComputeHeatDiffusionStepFunctor2D functor(params, Udata, Qdata, dt);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(int index) const
          {
            const int ghostWidth = params.ghostWidth;
            const real_t kappa = params.settings.kappa;
            const real_t dx = params.dx;
            const real_t dy = params.dy;
            const real_t dtdx = dt / dx;
            const real_t dtdy = dt / dy;

            int i, j;
            index2coord(index, i, j, params.isize, params.jsize);

            if (j>=ghostWidth && j<=params.jmax-ghostWidth &&
                i>=ghostWidth && i<=params.imax-ghostWidth)
            {
              const HydroState qLoc = getHydroState(Qdata, i, j);
              const real_t TLoc = computeTemperature(qLoc);
              real_t energy_fluxes = ZERO_F;

              {
                const HydroState qNei = getHydroState(Qdata, i-1, j);
                const real_t TNei = computeTemperature(qNei);
                energy_fluxes += dtdx * kappa * (TNei - TLoc) / dx;
              }

              {
                const HydroState qNei = getHydroState(Qdata, i+1, j);
                const real_t TNei = computeTemperature(qNei);
                energy_fluxes += dtdx * kappa * (TNei - TLoc) / dx;
              }

              {
                const HydroState qNei = getHydroState(Qdata, i, j-1);
                const real_t TNei = computeTemperature(qNei);
                energy_fluxes += dtdy * kappa * (TNei - TLoc) / dy;
              }

              {
                const HydroState qNei = getHydroState(Qdata, i, j+1);
                const real_t TNei = computeTemperature(qNei);
                energy_fluxes += dtdy * kappa * (TNei - TLoc) / dy;
              }

              Udata(i, j, IE) += energy_fluxes;
            }
          }

        const DataArray Udata;
        const DataArrayConst Qdata;
        const real_t dt;
    }; // ComputeHeatDiffusionStepFunctor2D

    template <Direction dir>
      class UpdateCellsRTStepFunctor2D : HydroBaseFunctor2D
    {
      public:
        UpdateCellsRTStepFunctor2D(HydroParams params_, DataArray Udata_, DataArray fluxData_):
          HydroBaseFunctor2D(params_), Udata(Udata_), fluxData(fluxData_) {};

        static void apply(HydroParams params,
            DataArray Udata, DataArray fluxData,
            int nbCells)
        {
          UpdateCellsRTStepFunctor2D functor(params, Udata, fluxData);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(int index) const
          {
            const int ghostWidth = params.ghostWidth;

            int i, j;
            index2coord(index, i, j, params.isize, params.jsize);

            if (j>=ghostWidth && j<=params.jmax-ghostWidth &&
                i>=ghostWidth && i<=params.imax-ghostWidth)
            {

              if(dir == XDIR)
              {
                Udata(i, j, IERAD) += fluxData(i, j, IERAD);
                Udata(i, j, IFX) += fluxData(i, j, IFX); 
                Udata(i, j, IFY) += fluxData(i, j, IFY);

                Udata(i, j, IERAD) -= fluxData(i+1, j, IERAD);
                Udata(i, j, IFX) -= fluxData(i+1, j, IFX); 
                Udata(i, j, IFY) -= fluxData(i+1, j, IFY);
              }

              else if(dir == YDIR)
              {
                Udata(i, j, IERAD) += fluxData(i, j, IERAD);
                Udata(i, j, IFX) += fluxData(i, j, IFX);
                Udata(i, j, IFY) += fluxData(i, j, IFY);

                Udata(i, j, IERAD) -= fluxData(i, j+1, IERAD);
                Udata(i, j, IFX) -= fluxData(i, j+1, IFX);
                Udata(i, j, IFY) -= fluxData(i, j+1, IFY);
              }
            }
          }

        DataArray Udata;
        const DataArray fluxData;
    }; // UpdateCellsRTStepFunctor2D

    template <Direction dir>
      class ComputeFluxesRTStepFunctor2D : HydroBaseFunctor2D
    {
      public:
        ComputeFluxesRTStepFunctor2D(HydroParams params_, DataArray Qdata_, DataArray fluxData_, real_t dt_):
          HydroBaseFunctor2D(params_), Qdata(Qdata_), fluxData(fluxData_), dt(dt_) {};

        static void apply(HydroParams params,
            DataArray Qdata, DataArray fluxData,
            real_t dt, int nbCells)
        {
          ComputeFluxesRTStepFunctor2D functor(params, Qdata, fluxData, dt);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(int index) const
          {
            const int ghostWidth = params.ghostWidth;
            const real_t dx = params.dx;
            const real_t dy = params.dy;
            const real_t dtdx = dt / dx;
            const real_t dtdy = dt / dy;

            int i, j;
            index2coord(index, i, j, params.isize, params.jsize);

            if (j>=ghostWidth && j<=params.jmax-ghostWidth+1 &&
                i>=ghostWidth && i<=params.imax-ghostWidth+1) 
            {
              if(dir == XDIR)
              {
                // left interface : right state
                HydroState qright = getHydroState(Qdata, i, j);

                // left interface : left state
                HydroState qleft = getHydroState(Qdata, i-1, j);

                // solve Riemann problem at X-interfaces and compute X-fluxes
                HydroState flux = riemann_radiativeTransfer(qleft, qright, dx);

                // store fluxes
                fluxData(i, j, IERAD) = flux[IERAD]*dtdx;
                fluxData(i, j, IFX) = flux[IFX]*dtdx;
                fluxData(i, j, IFY) = flux[IFY]*dtdx;

              }
              else if (dir == YDIR)
              {
                // left interface : right state
                HydroState qright = getHydroState(Qdata, i, j);

                // left interface : left state
                HydroState qleft = getHydroState(Qdata, i, j-1);

                // solve Riemann problem at Y-interfaces and compute Y-fluxes
                swapValues(&(qleft[IFX]) ,&(qleft[IFY]) );
                swapValues(&(qright[IFX]) ,&(qright[IFY]) );
                HydroState flux = riemann_radiativeTransfer(qleft, qright, dy);

                // update array
                fluxData(i, j, IERAD) = flux[IERAD]*dtdy;
                fluxData(i, j, IFX) = flux[IFY]*dtdy; // IFX/IFY swapped
                fluxData(i, j, IFY) = flux[IFX]*dtdy; // IFX/IFY swapped
              }
            }
          }

        const DataArray Qdata;
        DataArray fluxData;
        const real_t dt;
    }; // ComputeFluxesRTStepFunctor2D

    class SourceTermsRTStepFunctor2D : HydroBaseFunctor2D
    {
      public:
        SourceTermsRTStepFunctor2D(HydroParams params_, DataArray U_hll_, DataArray U_np1_, DataArray U_n_, real_t dt_):
          HydroBaseFunctor2D(params_), U_hll(U_hll_), U_np1(U_np1_), U_n(U_n_), dt(dt_) {};

        static void apply(HydroParams params,
            DataArray U_hll, DataArray U_np1, DataArray U_n, real_t dt,
            int nbCells)
        {
          SourceTermsRTStepFunctor2D functor(params, U_hll, U_np1, U_n, dt);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(int index) const
          {
            const int ghostWidth = params.ghostWidth;

            int i, j;
            index2coord(index, i, j, params.isize, params.jsize);

            if (j>=ghostWidth && j<=params.jmax-ghostWidth &&
                i>=ghostWidth && i<=params.imax-ghostWidth)
            {
              HydroState uLoc = getHydroState(U_n, i, j);
              HydroState uHLL = getHydroState(U_hll, i, j);
              HydroState uSourceterms;

              if(params.settings.wellBalancedScheme)
              {
                HydroState uLoc_imj = getHydroState(U_n, i-1, j);
                HydroState uLoc_ipj = getHydroState(U_n, i+1, j);
                HydroState uLoc_ijm = getHydroState(U_n, i, j-1);
                HydroState uLoc_ijp = getHydroState(U_n, i, j+1);
                uSourceterms = add_source_terms_wellBalanced(uHLL, uLoc, uLoc_imj, uLoc_ipj, uLoc_ijm, uLoc_ijp, dt);
              }
              else
                uSourceterms = add_source_terms(uHLL, dt);

              setHydroState(U_np1, uSourceterms, i, j);
            }
          }

        DataArray U_hll;
        DataArray U_np1;
        DataArray U_n;
        const real_t dt;
    }; // SourceTermsRTStepFunctor2D

    class ComputeDtFunctor2D : public HydroBaseFunctor2D
    {
      public:
        ComputeDtFunctor2D(HydroParams params_, DataArrayConst Udata_) :
          HydroBaseFunctor2D(params_), Udata(Udata_)  {};

        static void apply(HydroParams params, DataArrayConst Udata, real_t& invDt, int nbCells)
        {
          ComputeDtFunctor2D functor(params, Udata);
          Kokkos::parallel_reduce(nbCells, functor, invDt);
        }

        // Tell each thread how to initialize its reduction result.
        KOKKOS_INLINE_FUNCTION
          void init (real_t& dst) const
          {
            // The identity under max is -Inf.
            // Kokkos does not come with a portable way to access
            // floating-point Inf and NaN.
#ifdef __CUDA_ARCH__
            dst = -CUDART_INF;
#else
            dst = std::numeric_limits<real_t>::min();
#endif // __CUDA_ARCH__
          } // init

        /* this is a reduce (max) functor */
        KOKKOS_INLINE_FUNCTION
          void operator()(const int index, real_t& invDt) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;
            const int ghostWidth = params.ghostWidth;

            const real_t dx = params.dx;
            const real_t dy = params.dy;

            int i,j;
            index2coord(index,i,j,isize,jsize);

            if(j >= ghostWidth && j < jsize - ghostWidth &&
                i >= ghostWidth && i < isize - ghostWidth)
            {
              // get local conservative variable
              const HydroState uLoc = getHydroState(Udata, i, j);
              // get primitive variables in current cell
              const HydroState qLoc = computePrimitives(uLoc);
              const real_t c = computeSpeedSound(qLoc);
              const real_t vx = c+FABS(qLoc[IU]);
              const real_t vy = c+FABS(qLoc[IV]);

              // Hyperbolic part
              invDt = FMAX(invDt, vx/dx + vy/dy);
              // Viscous flux
              // (1.0*mu+abs(-2.0/3.0*mu)=8.0/3.0*mu
              //  formula still needs some justification)
              invDt = FMAX(invDt, 8.0 / 3.0 * params.settings.mu / uLoc[ID] * (ONE_F/(dx*dx) + ONE_F/(dy*dy)));
              // Heat flux
              invDt = FMAX(invDt, params.settings.kappa / (uLoc[ID] * params.settings.cp) * (ONE_F/(dx*dx) + ONE_F/(dy*dy)));
            }
          } // operator ()

        // "Join" intermediate results from different threads.
        // This should normally implement the same reduction
        // operation as operator() above. Note that both input
        // arguments MUST be declared volatile.
        KOKKOS_INLINE_FUNCTION
          void join (volatile real_t& dst,
              const volatile real_t& src) const
          {
            // max reduce
            if (dst < src)
              dst = src;
          } // join

        const DataArrayConst Udata;
    }; // ComputeDtFunctor2D


    class ComputeAcousticDtFunctor2D : public HydroBaseFunctor2D
    {
      public:
        ComputeAcousticDtFunctor2D(HydroParams params_, DataArrayConst Qdata_) :
          HydroBaseFunctor2D(params_), Qdata(Qdata_)  {};

        static void apply(HydroParams params, DataArrayConst Qdata, real_t& invDt, int nbCells)
        {
          ComputeAcousticDtFunctor2D functor(params, Qdata);
          Kokkos::parallel_reduce(nbCells, functor, invDt);
        }

        // Tell each thread how to initialize its reduction result.
        KOKKOS_INLINE_FUNCTION
          void init (real_t& dst) const
          {
            dst = ZERO_F;
          } // init

        /* this is a reduce (max) functor */
        KOKKOS_INLINE_FUNCTION
          void operator()(const int index, real_t& invDt) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;
            const int ghostWidth = params.ghostWidth;

            const real_t dx = params.dx;
            const real_t dy = params.dy;

            int i, j;
            index2coord(index, i, j, isize, jsize);

            if(j >= ghostWidth && j <= jsize - ghostWidth &&
                i >= ghostWidth && i <= isize - ghostWidth)
            {
              const HydroState qLoc = getHydroState(Qdata, i, j);
              const real_t cLoc = computeSpeedSound(qLoc);
              const real_t K    = params.settings.K;

              if (j != jsize-ghostWidth)
              {
                const HydroState qNei = getHydroState(Qdata, i-1, j);
                const real_t cNei = computeSpeedSound(qNei);

                const real_t aNei = K * FMAX(qNei[ID] * cNei, qLoc[ID] * cLoc);
                const real_t invDtNei = aNei / FMIN(dx * qNei[ID], dx * qLoc[ID]);
                invDt = FMAX(invDt, invDtNei);
              }

              if (i != isize-ghostWidth)
              {
                const HydroState qNei = getHydroState(Qdata, i, j-1);
                const real_t cNei = computeSpeedSound(qNei);

                const real_t aNei = K * FMAX(qNei[ID] * cNei, qLoc[ID] * cLoc);
                const real_t invDtNei = aNei / FMIN(dy * qNei[ID], dy * qLoc[ID]);
                invDt = FMAX(invDt, invDtNei);
              }
            }
          } // operator ()

        // "Join" intermediate results from different threads.
        // This should normally implement the same reduction
        // operation as operator() above. Note that both input
        // arguments MUST be declared volatile.
        KOKKOS_INLINE_FUNCTION
          void join (volatile real_t& dst,
              const volatile real_t& src) const
          {
            // max reduce
            if (dst < src)
              dst = src;
          } // join

        const DataArrayConst Qdata;
    }; // ComputeAcousticDtFunctor2D


    class ComputeTransportDtFunctor2D : public HydroBaseFunctor2D
    {
      public:
        ComputeTransportDtFunctor2D(HydroParams params_, DataArrayConst Qdata_) :
          HydroBaseFunctor2D(params_), Qdata(Qdata_)  {};

        static void apply(HydroParams params, DataArrayConst Qdata, real_t& invDt, int nbCells)
        {
          ComputeTransportDtFunctor2D functor(params, Qdata);
          Kokkos::parallel_reduce(nbCells, functor, invDt);
        }

        KOKKOS_INLINE_FUNCTION
          void computeAcousticRelaxation(const HydroState& qLoc, real_t cLoc,
              const HydroState& qNei, real_t cNei,
              real_t M, int IX, int dir, real_t& uStar) const
          {
            const real_t a = params.settings.K * FMAX(qNei[ID] * cNei, qLoc[ID] * cLoc);
            uStar = dir * HALF_F * (qNei[IX] + qLoc[IX]) - HALF_F * (qNei[IP] - qLoc[IP] + M) / a;
          }

        // Tell each thread how to initialize its reduction result.
        KOKKOS_INLINE_FUNCTION
          void init (real_t& dst) const
          {
            dst = ZERO_F;
          } // init

        /* this is a reduce (max) functor */
        KOKKOS_INLINE_FUNCTION
          void operator()(const int index, real_t& invDt) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;
            const int ghostWidth = params.ghostWidth;

            const real_t dx = params.dx;
            const real_t dy = params.dy;

            int i,j;
            index2coord(index,i,j,isize,jsize);

            if(j >= ghostWidth && j <= jsize - ghostWidth &&
                i >= ghostWidth && i <= isize - ghostWidth)
            {
              const HydroState qLoc = getHydroState(Qdata, i, j);
              const real_t cLoc = computeSpeedSound(qLoc);
              const real_t phiLoc = phi(i, j);

              real_t invDtLoc = ZERO_F;

              {
                const HydroState qMx = getHydroState(Qdata, i-1, j);
                const real_t cMinusX = computeSpeedSound(qMx);
                const real_t phiMx = phi(i-1, j);
                const real_t Mmx = computeM(qLoc, phiLoc, qMx, phiMx);
                real_t uStar;
                computeAcousticRelaxation(qLoc, cLoc, qMx, cMinusX, Mmx, IU, -1,
                    uStar);
                invDtLoc += FABS(uStar) / dx;
              }

              {
                const HydroState qPx = getHydroState(Qdata, i+1, j);
                const real_t cPlusX = computeSpeedSound(qPx);
                const real_t phiPx = phi(i+1, j);
                const real_t Mpx = computeM(qLoc, phiLoc, qPx, phiPx);
                real_t uStar;
                computeAcousticRelaxation(qLoc, cLoc, qPx, cPlusX, Mpx, IU, +1,
                    uStar);
                invDtLoc += FABS(uStar) / dx;
              }

              {
                const HydroState qMy = getHydroState(Qdata, i, j-1);
                const real_t cMinusY = computeSpeedSound(qMy);
                const real_t phiMy = phi(i, j-1);
                const real_t Mmy = computeM(qLoc, phiLoc, qMy, phiMy);
                real_t uStar;
                computeAcousticRelaxation(qLoc, cLoc, qMy, cMinusY, Mmy, IV, -1,
                    uStar);
                invDtLoc += FABS(uStar) / dy;
              }

              {
                const HydroState qPy = getHydroState(Qdata, i, j+1);
                const real_t cPlusY = computeSpeedSound(qPy);
                const real_t phiPy = phi(i, j+1);
                const real_t Mpy = computeM(qLoc, phiLoc, qPy, phiPy);
                real_t uStar;
                computeAcousticRelaxation(qLoc, cLoc, qPy, cPlusY, Mpy, IV, +1,
                    uStar);
                invDtLoc += FABS(uStar) / dy;
              }

              invDt = FMAX(invDt, invDtLoc);
            }
          } // operator ()

        // "Join" intermediate results from different threads.
        // This should normally implement the same reduction
        // operation as operator() above. Note that both input
        // arguments MUST be declared volatile.
        KOKKOS_INLINE_FUNCTION
          void join (volatile real_t& dst,
              const volatile real_t& src) const
          {
            // max reduce
            if (dst < src)
              dst = src;
          } // join

        const DataArrayConst Qdata;
    }; // ComputeTransportDtFunctor2D


    class ConvertToPrimitivesFunctor2D : public HydroBaseFunctor2D
    {
      public:
        ConvertToPrimitivesFunctor2D(HydroParams params_, DataArrayConst Udata_, DataArray Qdata_) :
          HydroBaseFunctor2D(params_), Udata(Udata_), Qdata(Qdata_)  {};

        static void apply(HydroParams params,
            DataArrayConst Udata, DataArray Qdata,
            int nbCells)
        {
          ConvertToPrimitivesFunctor2D functor(params, Udata, Qdata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;

            int i,j;
            index2coord(index,i,j,isize,jsize);

            if(j >= 0 && j < jsize  && i >= 0 && i < isize)
            {
              // get local conservative variable
              const HydroState uLoc = getHydroState(Udata, i, j);
              // get primitive variables in current cell
              const HydroState qLoc = computePrimitives(uLoc);
              // copy q state in q global
              setHydroState(Qdata, qLoc, i, j);
            }
          }

        const DataArrayConst Udata;
        const DataArray Qdata;
    }; // ConvertToPrimitivesFunctor2D

    class ComputeTemperatureFunctor2D : public HydroBaseFunctor2D
    {
      public:
        ComputeTemperatureFunctor2D(HydroParams params_, DataArray Udata_) :
          HydroBaseFunctor2D(params_), Udata(Udata_)  {};

        static void apply(HydroParams params,
            DataArray Udata,
            int nbCells)
        {
          ComputeTemperatureFunctor2D functor(params, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;

            int i,j;
            index2coord(index,i,j,isize,jsize);

            if(j >= 0 && j < jsize  && i >= 0 && i < isize)
            {
              HydroState uLoc = getHydroState(Udata, i, j);
              Udata(i, j, ITRAD) = computeGasTemperature(uLoc);
            }
          }

        DataArray Udata;
    }; // ComputeTemperatureFunctor2D

    class ComputeOpacityFunctor2D : public HydroBaseFunctor2D
    {
      public:
        ComputeOpacityFunctor2D(HydroParams params_, DataArray Udata_) :
          HydroBaseFunctor2D(params_), Udata(Udata_)  {};

        static void apply(HydroParams params,
            DataArray Udata,
            int nbCells)
        {
          ComputeOpacityFunctor2D functor(params, Udata);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;

            int i,j;
            index2coord(index,i,j,isize,jsize);

            if(j >= 0 && j < jsize  && i >= 0 && i < isize)
            {
              HydroState uLoc = getHydroState(Udata, i, j);
              Udata(i, j, IO) = computeExtinctionCoeff(uLoc);
            }
          }

        DataArray Udata;
    }; // ComputeOpacityFunctor2D

    class CouplingHydroRadFunctor2D : public HydroBaseFunctor2D
    {
      public:
        CouplingHydroRadFunctor2D(HydroParams params_, RadiativeConvectionParams rc_params_, DataArray U_hydro_, DataArray U_n_, DataArray U_np1_, real_t dt_, bool useImplicitSolver_) :
          HydroBaseFunctor2D(params_), rc_params(rc_params_), U_hydro(U_hydro_), U_n(U_n_), U_np1(U_np1_), dt(dt_), useImplicitSolver(useImplicitSolver_) {};

        static void apply(HydroParams params,
            RadiativeConvectionParams rc_params,
            DataArray U_hydro,
            DataArray U_n,
            DataArray U_np1,
            real_t dt,
            int nbCells,
            bool useImplicitSolver)
        {
          CouplingHydroRadFunctor2D functor(params, rc_params, U_hydro, U_n, U_np1, dt, useImplicitSolver);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;
            const int ghostWidth = params.ghostWidth;

            int i,j;
            index2coord(index,i,j,isize,jsize);

            if(j >= ghostWidth && j < jsize-ghostWidth  && i >= ghostWidth && i < isize-ghostWidth)
            {
              HydroState uLoc_hydro = getHydroState(U_hydro, i, j);
              HydroState uLoc_ij = getHydroState(U_n, i, j);
              HydroState uLoc_imj = getHydroState(U_hydro, i-1, j);
              HydroState uLoc_ipj = getHydroState(U_hydro, i+1, j);
              HydroState uLoc_ijm = getHydroState(U_hydro, i, j-1);
              HydroState uLoc_ijp = getHydroState(U_hydro, i, j+1);
              HydroState uCoupled = couplingHydroRT(rc_params, j, uLoc_hydro, uLoc_ij, uLoc_imj, uLoc_ipj, uLoc_ijm, uLoc_ijp, dt, useImplicitSolver);
              setHydroState(U_np1, uCoupled, i, j);
            }
          }

        RadiativeConvectionParams rc_params;
        DataArray U_hydro;
        DataArray U_n;
        DataArray U_np1;
        real_t dt;
        bool useImplicitSolver;
    }; // CouplingHydroRadFunctor2D

    class ComputeTotalEnergyFunctor2D : public HydroBaseFunctor2D
    {
      public:
        ComputeTotalEnergyFunctor2D(HydroParams params_, DataArray Udata_) :
          HydroBaseFunctor2D(params_), Udata(Udata_) {};

        static void apply(HydroParams params,
            DataArray Udata,
            real_t& total_energy,
            int nbCells)
        {
          ComputeTotalEnergyFunctor2D functor(params, Udata);
          Kokkos::parallel_reduce(nbCells, functor, total_energy);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index, real_t& total_energy) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;
            const int ghostWidth = params.ghostWidth;
            const real_t gamma0 = params.settings.gamma0;

            int i,j;
            index2coord(index,i,j,isize,jsize);

            if(j >= ghostWidth && j < jsize-ghostWidth  && i >= ghostWidth && i < isize-ghostWidth)
            {
              total_energy += Udata(i, j, IP)/(gamma0-ONE_F) + HALF_F*Udata(i, j, ID)*(Udata(i, j, IU)*Udata(i, j, IU)+Udata(i, j, IV)*Udata(i, j, IV));
            }
          }

        DataArray Udata;
    }; // ComputeTotalEnergyFunctor2D

    template <Direction dir>
      class ComputeTotalMomentumFunctor2D : public HydroBaseFunctor2D
    {
      public:
        ComputeTotalMomentumFunctor2D(HydroParams params_, DataArray Udata_) :
          HydroBaseFunctor2D(params_), Udata(Udata_) {};

        static void apply(HydroParams params,
            DataArray Udata,
            real_t& total_momentum,
            int nbCells)
        {
          ComputeTotalMomentumFunctor2D functor(params, Udata);
          Kokkos::parallel_reduce(nbCells, functor, total_momentum);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index, real_t& total_momentum) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;
            const int ghostWidth = params.ghostWidth;
            const real_t c2 = params.settings.speedOfLight*params.settings.speedOfLight;

            int i,j;
            index2coord(index,i,j,isize,jsize);

            if(j >= ghostWidth && j < jsize-ghostWidth  && i >= ghostWidth && i < isize-ghostWidth)
            {
              if(dir == XDIR)
                total_momentum += Udata(i, j, ID)*Udata(i, j, IU) + Udata(i, j, IFX)/c2;
              else
                total_momentum += Udata(i, j, ID)*Udata(i, j, IV) + Udata(i, j, IFY)/c2;
            }
          }

        DataArray Udata;
    }; // ComputeTotalMomentumFunctor2D

    class ComputeIonizationFunctor2D : public HydroBaseFunctor2D
    {
      public:
        ComputeIonizationFunctor2D(HydroParams params_, RadiativeConvectionParams rc_params_, DataArray Udata_, real_t dt_) :
          HydroBaseFunctor2D(params_), rc_params(rc_params_), Udata(Udata_), dt(dt_)  {};

        static void apply(HydroParams params,
            RadiativeConvectionParams rc_params, 
            DataArray Udata,
            real_t dt,
            int nbCells)
        {
          ComputeIonizationFunctor2D functor(params, rc_params, Udata, dt);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;
            const int ghostWidth = params.ghostWidth;

            int i,j;
            index2coord(index,i,j,isize,jsize);

            if(j >= ghostWidth && j < jsize-ghostWidth  && i >= ghostWidth && i < isize-ghostWidth)
            {
              HydroState uLoc = getHydroState(Udata, i, j);
              setHydroState(Udata, ionizationSourceTerms(uLoc, dt, rc_params, j), i, j);
            }
          }

        RadiativeConvectionParams rc_params;
        DataArray Udata;
        real_t dt;
    }; // ComputeIonizationFunctor2D


    class RayTracingFunctor2D : public HydroBaseFunctor2D
    {
      public:
        RayTracingFunctor2D(HydroParams params_, DataArray Uin_, DataArray Uout_, real_t dt_) :
          HydroBaseFunctor2D(params_), Uin(Uin_), Uout(Uout_), dt(dt_) {};

        static void apply(HydroParams params,
            DataArray Uin,
            DataArray Uout,
            real_t dt,
            int nbCells)
        {
          RayTracingFunctor2D functor(params, Uin, Uout, dt);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;
            const int ghostWidth = params.ghostWidth;

            int i,j;
            index2coord(index,i,j,isize,jsize);

            if(j >= ghostWidth && j < jsize-ghostWidth  && i >= ghostWidth && i < isize-ghostWidth)
            {
              // because of parallelism,
              // we have to read the radiative variables in one array
              // and to write them in another array
              // after the hydro step, U contains the updated conservatives hydro variables
              // for the coupling with the hydrodynamics,
              // U must contain the updated radiative variables
              HydroState uLoc = getHydroState(Uin, i, j);
              // in uNei, we are only interested in the radiative variables, 
              // so we can use hydro variables that are not updated
              HydroState uNei = getHydroState(Uin, i, j-1);
              HydroState u = rayTracing(uLoc, uNei, dt);
              setHydroState(Uout, u, i, j);
            }
          }

        DataArray Uin;
        DataArray Uout;
        real_t dt;
    }; // RayTracingFunctor2D



    class ThermalDrivingFunctor2D : public HydroBaseFunctor2D
    {
      public:
        ThermalDrivingFunctor2D(HydroParams params_, RadiativeConvectionParams rc_params_, DataArray Udata_, real_t dt_) :
          HydroBaseFunctor2D(params_), rc_params(rc_params_), Udata(Udata_), dt(dt_)  {};

        static void apply(HydroParams params,
            RadiativeConvectionParams rc_params,
            DataArray Udata,
            real_t dt,
            int nbCells)
        {
          ThermalDrivingFunctor2D functor(params, rc_params, Udata, dt);
          Kokkos::parallel_for(nbCells, functor);
        }

        KOKKOS_INLINE_FUNCTION
          void operator()(const int index) const
          {
            const int isize = params.isize;
            const int jsize = params.jsize;
            const int ghostWidth = params.ghostWidth;

            int i,j;
            index2coord(index,i,j,isize,jsize);

            if(j >= ghostWidth && j < jsize-ghostWidth  && i >= ghostWidth && i < isize-ghostWidth)
            {
              HydroState u = getHydroState(Udata, i, j);
              setHydroState(Udata, thermal_driving(u, rc_params, dt, j), i, j);
            }

          }

        const RadiativeConvectionParams rc_params;
        DataArray Udata;
        real_t dt;
    }; // ThermalDrivingFunctor2D


  } // namespace all_regime_radiative_transfer

} // namespace ark_rt
