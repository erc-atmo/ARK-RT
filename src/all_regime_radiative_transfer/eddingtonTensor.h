// Copyright CEA Saclay - Maison de la Simulation, (September 2020)
// contributors : Pierre Kestener, Thomas Padioleau, Pascal Tremblin, Hélène Bloch

// helene.bloch@cea.fr

// This software is a computer program whose purpose is to implement
// radiation hydrodynamics with an asymptotic perserving and well-balanced scheme and Trilinos.

// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".

// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.

// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.

// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

#pragma once

#include "shared/kokkos_shared.h"
#include <Kokkos_DualView.hpp>

#include <fstream>
#include <stdlib.h>

extern "C" void  dgeev_(char *jobvl, char *jobvr, int *n, double *a, int *lda, double *wr, double *wi, double *vl, int *ldvl, double *vr, int *ldvr, double *work, int *lwork, int *info);

namespace ark_rt { namespace all_regime_radiative_transfer
  {
    using DataArray = typename DataArrays<2>::DataArray;
    using DataArrayHost = typename DataArrays<2>::DataArrayHost;

    KOKKOS_INLINE_FUNCTION
      void calc_eddTensor_rad(real_t e, real_t fx, real_t fy, real_t *eddTensor, real_t LIGHTSP , bool replaceEdingtonFactor)
      {
        /**
         * Compute Eddington tensor and its derivatives
         * @param[in]  e 		radiative energy
         * @param[in]  fx 		radiative flux, x coordinate
         * @param[in]  fy 		radiative flux, y coordinate
         * @param[out] eddTensor 	Eddington tensor, eddTensor[0] = Dxx, eddTensor[1] = Dxy, eddTensor[2] = Dyx, eddTensor[3] = Dyy 
         * @param[in]  LIGHTSP 	speed of light
         */

        real_t	fnorm2		= (fx*fx+fy*fy);
        real_t	fnorm		= SQRT(fnorm2);
        real_t	anisotropy2;

        if(e < 1e-100)
        {
          anisotropy2 = ZERO_F;
          fnorm = ZERO_F;
          fnorm2 = ZERO_F;
        }

        if(replaceEdingtonFactor)
          anisotropy2 = FMIN(ONE_F, fnorm2/(LIGHTSP * LIGHTSP * e * e));
        else
          anisotropy2	= fnorm2/(LIGHTSP * LIGHTSP * e * e);
        real_t	eddingtonFactor;
        real_t	nx = ZERO_F,ny = ZERO_F, g, h;

        eddingtonFactor = (3.0 + 4.0*anisotropy2) / (5.0 + TWO_F*SQRT(4.0-3.0*anisotropy2));
        if(isnan(eddingtonFactor))
          printf("NaN on eddington factor calc_eddTensor_rad, f = %f, E_rad = %e, Fx_rad = %e, Fy_rad = %e, c = %e\n", SQRT(anisotropy2), e, fx, fy, LIGHTSP);
        g		  = (ONE_F-eddingtonFactor) * HALF_F;
        h		  = (3.*eddingtonFactor-ONE_F) * HALF_F;
        if (fnorm > 1e-5){
          nx		  = fx / fnorm;
          ny		  = fy / fnorm;
        }

        eddTensor[0] = g + h*nx*nx ;
        eddTensor[1] =     h*nx*ny ;
        eddTensor[2] =     h*ny*nx ; // Corresponding to value 2,1
        eddTensor[3] = g + h*ny*ny ;
      } // calc_eddTensor_rad


    KOKKOS_INLINE_FUNCTION
      void compute_analytical_eigenvalues(real_t& sl, real_t& sr, real_t e, real_t Fx, real_t Fy, real_t c, bool replaceEddingtonFactor)
      {
        real_t f2 = (Fx*Fx+Fy*Fy)/(c*c*e*e);

        if(e < 1e-100)
          f2 = ZERO_F;

        real_t xi = SQRT(4.-3.*f2);
        real_t fx = Fx/(c*e);
        real_t fy = Fy/(c*e);

        if(f2 > ONE_F)
        {
          if(replaceEddingtonFactor)
          {
            fx = ONE_F/SQRT(TWO_F);
            fy = ONE_F/SQRT(TWO_F);
            f2 = ONE_F;
            xi = SQRT(4.-3.*f2);
            const real_t lambda_p = c*(fx/xi + SQRT(TWO_F)*SQRT((xi-ONE_F)*(xi+TWO_F)*(TWO_F*((xi-ONE_F)*(xi+TWO_F)+3.*fy*fy)))/(SQRT(3.)*xi*(xi+TWO_F)));
            const real_t lambda_m = c*(fx/xi - SQRT(TWO_F)*SQRT((xi-ONE_F)*(xi+TWO_F)*(TWO_F*((xi-ONE_F)*(xi+TWO_F)+3.*fy*fy)))/(SQRT(3.)*xi*(xi+TWO_F)));
            sr = lambda_p;
            sl = lambda_m;
          }
          else
          {
            printf("compute eigenvalues: f > 1: f = %f, e = %e, Fx = %e, Fy = %e, fx = %e, fy = %e, c = %e\n", SQRT(f2), e, Fx, Fy, fx, fy, c);
            /* sr = ZERO_F; */
            /* sl = ZERO_F; */
          }
        }
        /* else */
        /* { */
        const real_t lambda_p = c*(fx/xi + SQRT(TWO_F)*SQRT((xi-ONE_F)*(xi+TWO_F)*(TWO_F*((xi-ONE_F)*(xi+TWO_F)+3.*fy*fy)))/(SQRT(3.)*xi*(xi+TWO_F)));
        const real_t lambda_m = c*(fx/xi - SQRT(TWO_F)*SQRT((xi-ONE_F)*(xi+TWO_F)*(TWO_F*((xi-ONE_F)*(xi+TWO_F)+3.*fy*fy)))/(SQRT(3.)*xi*(xi+TWO_F)));
        sr = FMAX(lambda_p, lambda_m);
        sl = FMIN(lambda_p, lambda_m);
        /* } */

      } // compute_analytical_eigenvalues

      KOKKOS_INLINE_FUNCTION
      void eigenvaluesRT(real_t& sL, real_t& sR, const real_t c, const real_t el, const real_t fxl, const real_t fyl, const real_t er, const real_t fxr, const real_t fyr, const bool useExactEigenvalues, const bool replaceEdingtonFactor, const real_t valpmin)
      {
        /**
         * Interpol the eigenvalues 
         * @param[out]  sL 	left eigenvalue
         * @param[out]  sR 	right eigenvalue
         * @param[in]   c	speed of light
         * @param[in]   el	radiative energy in the left cell
         * @param[in]   fxl	x coordinate of radiative flux in the left cell
         * @param[in]   fyl	y coordinate of radiative flux in the left cell
         * @param[in]   er	radiative energy in the right cell
         * @param[in]   fxr	x coordinate of radiative flux in the right cell
         * @param[in]   fyr	y coordinate of radiative flux in the right cell
         */

        if(!useExactEigenvalues)
        {
          sL = -c;
          sR =  c;
        }

        else
        {
          real_t SLL, SLR, SRL, SRR;
          compute_analytical_eigenvalues(SLL, SRL, el, fxl, fyl, c, replaceEdingtonFactor); // left cell
          compute_analytical_eigenvalues(SLR, SRR, er, fxr, fyr, c, replaceEdingtonFactor); // right cell

          sL = FMIN(-valpmin*c, FMIN(SLL, SLR));
          sR = FMAX( valpmin*c, FMAX(SRL, SRR));
        }


      } // eigenvaluesRT

    KOKKOS_INLINE_FUNCTION
      void eddingtonTensor_derivatives(const real_t e, const real_t fx, const real_t fy, real_t& dDxxdE, real_t& dDxxdFx, real_t& dDxxdFy, real_t& dDxydE, real_t& dDxydFx, real_t& dDxydFy, real_t& dDyxdE, real_t& dDyxdFx, real_t& dDyxdFy, real_t& dDyydE, real_t& dDyydFx, real_t& dDyydFy, const real_t c, const bool replaceEddingtonFactor)
      {
        const real_t c2 = c*c;
        real_t	fnorm2		= (fx*fx+fy*fy);
        real_t	fnorm		= SQRT(fnorm2);
        real_t	anisotropy2 = fnorm2/(c * c * e * e);

        if(e < 1e-100)
        {
          anisotropy2 = ZERO_F;
          fnorm = ZERO_F;
          fnorm2 = ZERO_F;
        }

        const bool replaceEddingtonDerivatives = replaceEddingtonFactor && (anisotropy2 > ONE_F);
        if(replaceEddingtonDerivatives)
          anisotropy2 = ONE_F;

        real_t        anisotropy       = SQRT(anisotropy2);
        real_t	eddingtonFactor;
        real_t	eddingtonFPrime, nx = ZERO_F,ny = ZERO_F, g, h, gPrime, hPrime;
        eddingtonFactor = (3.0 + 4.0*anisotropy2) / (5.0 + TWO_F*SQRT(4.0-3.0*anisotropy2));
        if(isnan(eddingtonFactor))
        {
          printf("nan, Eddington tensor: f = %f, E_rad = %e, Fx_rad = %e, Fy_rad = %e, c = %e\n", anisotropy, e, fx, fy, c);
          /* throw std::runtime_error("NaN on eddington factor!"); */
        }
        g		  = (ONE_F-eddingtonFactor) * HALF_F;
        h		  = (3.*eddingtonFactor-ONE_F) * HALF_F;
        eddingtonFPrime = TWO_F*anisotropy/SQRT(4.-3.*anisotropy2);
        if(replaceEddingtonDerivatives)
        {
          gPrime = ZERO_F;
          hPrime = ZERO_F;
        }
        else
        {
          gPrime	  = -eddingtonFPrime * HALF_F;
          hPrime	  = 3.*eddingtonFPrime * HALF_F;
        }
        if (fnorm > 1e-5){
          if(fx > 1e-6)
            nx		  = fx / fnorm;
          if(fy > 1e-6)
            ny		  = fy / fnorm;
        }

        dDxxdE = c2*(g+h*nx*nx-anisotropy*gPrime-anisotropy*hPrime*nx*nx);
        dDxydE = c2*(h*nx*ny-anisotropy*hPrime*nx*ny);
        dDyxdE = c2*(h*nx*ny-anisotropy*hPrime*nx*ny);
        dDyydE = c2*(g+h*ny*ny-anisotropy*gPrime-anisotropy*hPrime*ny*ny);

        if (fnorm > 1e-5){
          dDxxdFx = c*(gPrime*nx+hPrime*nx*nx*nx+h*(-TWO_F*nx*nx*nx+TWO_F*nx)/anisotropy);
          dDxydFx = c*(hPrime*nx*nx*ny+h*(-TWO_F*ny*nx*nx+ny)/anisotropy);
          dDyxdFx = c*(hPrime*nx*nx*ny+h*(-TWO_F*ny*nx*nx+ny)/anisotropy);
          dDyydFx = c*(gPrime*nx+hPrime*nx*ny*ny+h*(-TWO_F*nx*ny*ny)/anisotropy);

          dDxxdFy = c*(gPrime*ny+hPrime*ny*nx*nx+h*(-TWO_F*ny*nx*nx)/anisotropy);
          dDxydFy = c*(hPrime*ny*ny*nx+h*(-TWO_F*nx*ny*ny+nx)/anisotropy);
          dDyxdFy = c*(hPrime*ny*ny*nx+h*(-TWO_F*nx*ny*ny+nx)/anisotropy);
          dDyydFy = c*(gPrime*ny+hPrime*ny*ny*ny+h*(-TWO_F*ny*ny*ny+TWO_F*ny)/anisotropy);
        }
        else
        {
          dDxxdFx = ZERO_F;
          dDxydFx = ZERO_F;
          dDyxdFx = ZERO_F;
          dDyydFx = ZERO_F;

          dDxxdFy = ZERO_F;
          dDxydFy = ZERO_F;
          dDyxdFy = ZERO_F;
          dDyydFy = ZERO_F;
        }
      }; // eddingtonTensor_derivatives

    KOKKOS_INLINE_FUNCTION
      void eddingtonTensor(const real_t e, const real_t fx, const real_t fy, real_t& Dxx, real_t& Dxy, real_t& Dyx, real_t& Dyy, const real_t c, const bool replaceEdingtonFactor)
      {
        const real_t c2 = c*c;
        real_t eddTensor[4]={ZERO_F};
        all_regime_radiative_transfer::calc_eddTensor_rad(e,fx,fy,eddTensor, c, replaceEdingtonFactor);
        Dxx = c2*e*eddTensor[0];
        Dxy = c2*e*eddTensor[1];
        Dyx = c2*e*eddTensor[2];
        Dyy = c2*e*eddTensor[3];
      }; // eddingtonTensor

    KOKKOS_INLINE_FUNCTION
      real_t computeReducedFluxSquare(const real_t e, const real_t Fx, const real_t Fy, const real_t c)
      {
        return FMIN(ONE_F, (Fx*Fx+Fy*Fy)/(c*c*e*e));
      } // computeReducedFluxSquare

  } // namespace all_regime_radiative_transfer

} // namespace ark_rt
