/**
 * Hydro solver (Muscl-Hancock).
 *
 * \date April, 16 2016
 * \author P. Kestener
 */

#include <cstdlib>
#include <memory>

#include "ark_rt.h"
#include "shared/GreshoParams.h"
#include "shared/HydroParams.h"  // read parameter file
#include "shared/kokkos_shared.h"
#include "shared/real_type.h"    // choose between single and double precision
#include "shared/SolverFactory.h" // solver
#include "utils/io/IO_HDF5.h"
#include "utils.hpp"

void configure(ConfigMap& configMap)
{
    // run
    configMap.setString("run", "solver_name", "Hydro_All_Regime_2D");
    configMap.setFloat("run", "tend", 0.01);
    configMap.setInteger("run", "nstepmax", 100000);
    configMap.setInteger("run", "nOutput", 0);
    configMap.setInteger("run", "nlog", 10);
    // mesh
    configMap.setInteger("mesh", "nx", 128);
    configMap.setInteger("mesh", "ny", 128);
    configMap.setInteger("mesh","boundary_type_xmin", BC_PERIODIC);
    configMap.setInteger("mesh","boundary_type_xmax", BC_PERIODIC);
    configMap.setInteger("mesh","boundary_type_ymin", BC_PERIODIC);
    configMap.setInteger("mesh","boundary_type_ymax", BC_PERIODIC);
    // hydro
    configMap.setString("hydro", "problem", "gresho");
    // gresho
    configMap.setFloat("gresho", "mach", 0.01);
}

int main(int argc, char* argv[])
{
    namespace ek = ark_rt;

    ek::initialize(argc, argv);

    std::atexit(ek::finalize);

    {
        // read parameter file and initialize parameter
        // parse parameters from input file
        ConfigMap configMap("");
        configure(configMap);

        ek::SolverFactory& solver_factory = ek::SolverFactory::Instance();

        HydroParams params;
        params.setup(configMap);

        // retrieve solver name from settings
        const std::string solver_name = configMap.getString("run", "solver_name", "Unknown");

        // initialize workspace memory (U, U2, ...)
        ek::SolverBase* solver = solver_factory.create(solver_name, params, configMap);

        // start computation
        std::cout << "Starting computation...\n";

        // Hydrodynamics solver loop
        while (!solver->finished())
        {
            solver->next_iteration();
        } // end solver loop

        // Post-processing

        // Loads reference solution
        DataArray2d Uref("Uref", params.isize, params.jsize, params.nbvar);
        {
            DataArray2dHost Uhost = Kokkos::create_mirror_view(Uref);
            ek::io::Load_HDF5<TWO_D> reader(Uref, Uhost, params, configMap, HYDRO_2D_NBVAR, solver->m_variables_names);
            reader.load("reference_gresho.h5");
            Kokkos::deep_copy(Uref, Uhost);
        }

        DataArray2d Uerr("Uerr", params.isize, params.jsize, params.nbvar);
        {
            DataArray2d Udata;
            solver->getDeviceData(Udata); // shallow copy
            Kokkos::deep_copy(Uerr, Udata);

            utils::DifferenceFunctor2D::apply(params, Uerr, Uref, params.isize*params.jsize);
        }

        const HydroState2d errL1 = utils::NormL1Functor2D::apply(params, Uerr, params.isize*params.jsize);
        for (int ivar=0; ivar<params.nbvar; ++ivar)
        {
            std::cout << "Absolute L1 error on " << solver->m_variables_names[ivar] << " : " << errL1[ivar] << std::endl;
        }

        const HydroState2d normL1 = utils::NormL1Functor2D::apply(params, Uref, params.isize*params.jsize);
        HydroState2d errL1_rel = errL1;
        for (int ivar=0; ivar<params.nbvar; ++ivar)
        {
            if (normL1[ivar] != ZERO_F)
            {
                errL1_rel[ivar] /= normL1[ivar];
            }
            std::cout << "Relative L1 error on " << solver->m_variables_names[ivar] << " : " << errL1_rel[ivar] << std::endl;
        }

        const HydroState2d errLinf = utils::NormLinfFunctor2D::apply(params, Uerr, params.isize*params.jsize);
        for (int ivar=0; ivar<params.nbvar; ++ivar)
        {
            std::cout << "Absolute Linf error on " << solver->m_variables_names[ivar] << " : " << errLinf[ivar] << std::endl;
        }

        const HydroState2d normLinf = utils::NormLinfFunctor2D::apply(params, Uref, params.isize*params.jsize);
        HydroState2d errLinf_rel = errLinf;
        for (int ivar=0; ivar<params.nbvar; ++ivar)
        {
            if (normLinf[ivar] != ZERO_F)
            {
                errLinf_rel[ivar] /= normLinf[ivar];
            }
            std::cout << "Relative Linf error on " << solver->m_variables_names[ivar] << " : " << errLinf_rel[ivar] << std::endl;
        }

        if (errLinf_rel[ID] >= 1E-15 ||
            errLinf_rel[IE] >= 1E-15 ||
            errLinf_rel[IU] >= 1E-15 ||
            errLinf_rel[IV] >= 1E-15)
        {
            return EXIT_FAILURE;
        }

        delete solver;
    }

    return EXIT_SUCCESS;
} // end main
