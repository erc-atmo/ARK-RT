#pragma once

#include "shared/GreshoParams.h"
#include "shared/HydroParams.h"
#include "shared/kokkos_shared.h"
#include "shared/real_type.h"    // choose between single and double precision
#include "src/all_regime/HydroBaseFunctor2D.h"

namespace ek = ark_rt;

namespace utils
{

class DifferenceFunctor2D : ek::all_regime::HydroBaseFunctor2D
{
public:
    DifferenceFunctor2D(HydroParams params, DataArray U1, DataArrayConst U2)
        : HydroBaseFunctor2D(params)
        , m_U1(U1)
        , m_U2(U2)
    {
    }

    static void apply(HydroParams params, DataArray U1, DataArrayConst U2, int nbCells)
    {
        DifferenceFunctor2D functor(params, U1, U2);
        Kokkos::parallel_for(nbCells, functor);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int index) const
    {
        int i,j;
        index2coord(index, i, j, params.isize, params.jsize);

        for (int ivar=0; ivar<params.nbvar; ++ivar)
        {
            m_U1(i, j, ivar) -= m_U2(i, j, ivar);
        }
    }

private:
    DataArray m_U1;
    DataArrayConst m_U2;
};

class NormL1Functor2D : ek::all_regime::HydroBaseFunctor2D
{
public:
    using value_type = real_t[];
    using size_type = DataArray::size_type;

    NormL1Functor2D(HydroParams params, DataArray Udata)
        : HydroBaseFunctor2D(params)
        , value_count(params.nbvar)
        , m_Udata(Udata)
        , m_Surface(ZERO_F)
        , m_ElementarySurface(ZERO_F)
    {
        m_Surface = (params.xmax - params.xmin)*(params.ymax - params.ymin);
        m_ElementarySurface = params.dx*params.dy;
    }

    static HydroState apply(HydroParams params, DataArray Udata, int nbCells)
    {
        HydroState err{};
        NormL1Functor2D functor(params, Udata);
        Kokkos::parallel_reduce(nbCells, functor, err.data());
        return err;
    }

    KOKKOS_INLINE_FUNCTION
    void init(value_type dst) const
    {
        for (size_type ivar=0; ivar<value_count; ++ivar)
        {
            dst[ivar] = ZERO_F;
        }
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int index, value_type err) const
    {
        int i,j;
        index2coord(index, i, j, params.isize, params.jsize);

        if (i>=params.imin+params.ghostWidth && i<=params.imax-params.ghostWidth &&
            j>=params.jmin+params.ghostWidth && j<=params.jmax-params.ghostWidth)
        {
            for (size_type ivar=0; ivar<value_count; ++ivar)
            {
                err[ivar] += std::abs(m_Udata(i, j, ivar)) * m_ElementarySurface / m_Surface;
            }
        }
    }

    KOKKOS_INLINE_FUNCTION
    void join(volatile value_type dst, const volatile value_type src) const
    {
        for (size_type ivar=0; ivar<value_count; ++ivar)
        {
            dst[ivar] += src[ivar];
        }
    }

    size_type value_count;
private:
    DataArray m_Udata;
    real_t m_Surface;
    real_t m_ElementarySurface;
}; // NormFunctor2D

class NormLinfFunctor2D : ek::all_regime::HydroBaseFunctor2D
{
public:
    using value_type = real_t[];
    using size_type = DataArray::size_type;

    NormLinfFunctor2D(HydroParams params, DataArray Udata)
        : HydroBaseFunctor2D(params)
        , value_count(params.nbvar)
        , m_Udata(Udata)
    {
    }

    static HydroState apply(HydroParams params, DataArray Udata, int nbCells)
    {
        HydroState err{};
        NormLinfFunctor2D functor(params, Udata);
        Kokkos::parallel_reduce(nbCells, functor, err.data());
        return err;
    }

    KOKKOS_INLINE_FUNCTION
    void init(value_type dst) const
    {
        for (size_type ivar=0; ivar<value_count; ++ivar)
        {
            dst[ivar] = ZERO_F;
        }
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int index, value_type err) const
    {
        int i,j;
        index2coord(index, i, j, params.isize, params.jsize);

        if (i>=params.imin+params.ghostWidth && i<=params.imax-params.ghostWidth &&
            j>=params.jmin+params.ghostWidth && j<=params.jmax-params.ghostWidth)
        {
            for (size_type ivar=0; ivar<value_count; ++ivar)
            {
                err[ivar] = std::fmax(err[ivar], std::abs(m_Udata(i, j, ivar)));
            }
        }
    }

    KOKKOS_INLINE_FUNCTION
    void join(volatile value_type dst, const volatile value_type src) const
    {
        for (size_type ivar=0; ivar<value_count; ++ivar)
        {
            dst[ivar] = std::fmax(dst[ivar], src[ivar]);
        }
    }

    size_type value_count;
private:
    DataArray m_Udata;
}; // NormFunctor2D

}
