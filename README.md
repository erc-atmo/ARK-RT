# ARK-RT

## What is it ?

Provide performance portable Kokkos implementation for radiation
hydrodynamics.

## Dependencies

* [Trilinos](https://github.com/trilinos/Trilinos) with version >= 12.18.1
* [CMake](https://cmake.org) with version >= 3.3

Optional
* [Cuda](https://developer.nvidia.com/cuda-downloads) with version >= 8.0
* MPI, for example [OpenMPI](https://www.open-mpi.org)
* [HDF5](https://portal.hdfgroup.org)
* [PnetCDF](https://github.com/Parallel-NetCDF/PnetCDF)

### Dependencies on Poincare (cluster at Maison de la Simulation)

* Add a .modulerc file in your home containing:
```shell
#%Module1.0
# ~/.modulerc
module use /gpfshome/mds/staff/hbloch/modulefiles
```

* module load trilinos/12.18.1 cmake/3.14.1-gnu54 gnu-env/7.3.0 openmpi/2.1.3_gnu73

### Dependencies on K80 cluster at Maison de la Simulation

* Add a .modulerc file in your home containing:
```shell
#%Module1.0
# ~/.modulerc
module use /home/hbloch/modulefiles
```
* module load cmake/3.13.4 cuda/8.0 gnu/5.4.0 openmpi/3.1.2_cuda8.0 trilinos/12.18.1

## How to build

```shell
git clone https://gitlab.erc-atmo.eu/erc-atmo/ark-rt.git
cd ark-rt
mkdir build ; cd build
cmake ..
cmake --build . -- -j 4
```

## How to run

If you add kokkos options (see [Table 5.1](https://github.com/kokkos/kokkos/wiki/Initialization#table-51-command-line-options-for-kokkosinitialize-) for a list of options) from the command line, use the prefixed version (--kokkos-xxx)

### Run MPI-OpenMP

``` shell
mpirun -np 2 --map-by socket --bind-to socket ./src/ark-rt path/to/configuration_file.ini --kokkos-threads=8
```

### Run MPI-Cuda

To run on 4 GPUs, 1 GPU per MPI task
```shell
mpirun -np 4 ./src/erk-rt path/to/configuration_file.ini --kokkos-ndevices=4
```

## Developping with vim and youcomplete plugin

Assuming you are using vim (or neovim) text editor and have installed
the youcomplete plugin, you can have semantic autocompletion in a C++
project.

Make sure to have CMake variable CMAKE_EXPORT_COMPILE_COMMANDS set to
ON, and symlink the generated file to the top level source directory.

## Coding rules

For cmake follow [Modern
CMake](https://cliutils.gitlab.io/modern-cmake/) which also cites
other sources like coding rules from [Effective Modern
CMake](https://gist.github.com/mbinna/c61dbb39bca0e4fb7d1f73b0d66a4fd1).

